"use strict";const{Cu}=require("chrome");const EventEmitter=require("devtools/shared/event-emitter");const{isNodeValid}=require("./utils/markup");const{getAdjustedQuads}=require("devtools/shared/layout/utils");
const BOX_MODEL_REGIONS=["margin","border","padding","content"];function AutoRefreshHighlighter(highlighterEnv){EventEmitter.decorate(this);this.highlighterEnv=highlighterEnv;this.currentNode=null;this.currentQuads={};this.update=this.update.bind(this);}
AutoRefreshHighlighter.prototype={get win(){if(!this.highlighterEnv){return null;}
return this.highlighterEnv.window;},show:function(node,options={}){let isSameNode=node===this.currentNode;let isSameOptions=this._isSameOptions(options);if(!isNodeValid(node)||(isSameNode&&isSameOptions)){return false;}
this.options=options;this._stopRefreshLoop();this.currentNode=node;this._updateAdjustedQuads();this._startRefreshLoop();let shown=this._show();if(shown){this.emit("shown");}
return shown;},hide:function(){if(!isNodeValid(this.currentNode)){return;}
this._hide();this._stopRefreshLoop();this.currentNode=null;this.currentQuads={};this.options=null;this.emit("hidden");},_isSameOptions:function(options){if(!this.options){return false;}
let keys=Object.keys(options);if(keys.length!==Object.keys(this.options).length){return false;}
for(let key of keys){if(this.options[key]!==options[key]){return false;}}
return true;},_updateAdjustedQuads:function(){for(let region of BOX_MODEL_REGIONS){this.currentQuads[region]=getAdjustedQuads(this.win,this.currentNode,region);}},_hasMoved:function(){let oldQuads=JSON.stringify(this.currentQuads);this._updateAdjustedQuads();let newQuads=JSON.stringify(this.currentQuads);return oldQuads!==newQuads;},update:function(){if(!isNodeValid(this.currentNode)||!this._hasMoved()){return;}
this._update();this.emit("updated");},_show:function(){

 throw new Error("Custom highlighter class had to implement _show method");},_update:function(){


 throw new Error("Custom highlighter class had to implement _update method");},_hide:function(){
 throw new Error("Custom highlighter class had to implement _hide method");},_startRefreshLoop:function(){let win=this.currentNode.ownerDocument.defaultView;this.rafID=win.requestAnimationFrame(this._startRefreshLoop.bind(this));this.rafWin=win;this.update();},_stopRefreshLoop:function(){if(this.rafID&&!Cu.isDeadWrapper(this.rafWin)){this.rafWin.cancelAnimationFrame(this.rafID);}
this.rafID=this.rafWin=null;},destroy:function(){this.hide();this.highlighterEnv=null;this.currentNode=null;}};exports.AutoRefreshHighlighter=AutoRefreshHighlighter;