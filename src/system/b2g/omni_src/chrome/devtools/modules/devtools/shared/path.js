"use strict";const URL=require("URL");exports.joinURI=(initialPath,...paths)=>{let url;try{url=new URL(initialPath);}
catch(e){return;}
for(let path of paths){if(path){url=new URL(path,url);}}
return url.href;}