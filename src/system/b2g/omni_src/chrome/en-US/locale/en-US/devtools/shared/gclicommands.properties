

helpDesc=Get help on the available commands

helpAvailable=Available Commands

notAvailableInE10S=The command ‘%1$S’ is not available in multiprocess mode (E10S)

consoleDesc=Commands to control the console

consoleManual=Filter, clear and close the web console

consoleclearDesc=Clear the console

screenshotDesc=Save an image of the page

screenshotManual=Save a PNG image of the entire visible window (optionally after a delay)

screenshotFilenameDesc=Destination filename

screenshotFilenameManual=The name of the file (should have a ‘.png’ extension) to which we write the screenshot.

screenshotClipboardDesc=Copy screenshot to clipboard? (true/false)

screenshotClipboardManual=True if you want to copy the screenshot instead of saving it to a file.

screenshotChromeDesc2=Capture %1$S chrome window? (true/false)

screenshotChromeManual2=True if you want to take the screenshot of the %1$S window rather than the web page’s content window.

screenshotGroupOptions=Options

screenshotAdvancedOptions=Advanced Options

screenshotDelayDesc=Delay (seconds)

screenshotDelayManual=The time to wait (in seconds) before the screenshot is taken

screenshotDPRDesc=Device pixel ratio

screenshotDPRManual=The device pixel ratio to use when taking the screenshot

screenshotFullPageDesc=Entire webpage? (true/false)

screenshotFullPageManual=True if the screenshot should also include parts of the webpage which are outside the current scrolled bounds.

screenshotSelectorChromeConflict=selector option is not supported when chrome option is true

screenshotGeneratedFilename=Screen Shot %1$S at %2$S

screenshotErrorSavingToFile=Error saving to

screenshotSavedToFile=Saved to

screenshotErrorCopying=Error occurred while copying to clipboard.

screenshotCopied=Copied to clipboard.

screenshotTooltip=Take a fullpage screenshot

screenshotImgurDesc=Upload to imgur.com

screenshotImgurManual=Use if you want to upload to imgur.com instead of saving to disk

screenshotImgurError=Could not reach imgur API

screenshotImgurUploaded=Uploaded to %1$S

highlightDesc=Highlight nodes

highlightManual=Highlight nodes that match a selector on the page

highlightSelectorDesc=CSS selector

highlightSelectorManual=The CSS selector used to match nodes in the page

highlightOptionsDesc=Options

highlightHideGuidesDesc=Hide guides

highlightHideGuidesManual=Hide the guides around the highlighted node

highlightShowInfoBarDesc=Show the node infobar

highlightShowInfoBarManual=Show the infobar above the highlighted node (the infobar displays the tagname, attributes and dimension)

highlightShowAllDesc=Show all matches

highlightShowAllManual=If too many nodes match the selector, only the first 100 will be shown to avoid slowing down the page too much. Use this option to show all matches instead

highlightRegionDesc=Box model region

highlightRegionManual=Which box model region should be highlighted: ‘content’, ‘padding’, ‘border’ or ‘margin’

highlightFillDesc=Fill style

highlightFillManual=Override the default region fill style with a custom color

highlightKeepDesc=Keep existing highlighters

highlightKeepManual=By default, existing highlighters are hidden when running the command, unless this option is set

highlightOutputConfirm2=%1$S node highlighted;%1$S nodes highlighted

highlightOutputMaxReached=%1$S nodes matched, but only %2$S nodes highlighted. Use ‘--showall’ to show all

unhighlightDesc=Unhighlight all nodes

unhighlightManual=Unhighlight all nodes previously highlighted with the ‘highlight’ command

restartBrowserDesc=Restart %1$S

restartBrowserNocacheDesc=Disables loading content from cache upon restart

restartBrowserRequestCancelled=Restart request cancelled by user.

restartBrowserRestarting=Restarting %1$S…

restartBrowserGroupOptions=Options

restartBrowserSafemodeDesc=Enables Safe Mode upon restart

inspectDesc=Inspect a node

inspectManual=Investigate the dimensions and properties of an element using a CSS selector to open the DOM highlighter

inspectNodeDesc=CSS selector

inspectNodeManual=A CSS selector for use with document.querySelector which identifies a single element

eyedropperDesc=Grab a color from the page

eyedropperManual=Open a panel that magnifies an area of page to inspect pixels and copy color values

eyedropperTooltip=Grab a color from the page

debuggerClosed=The debugger must be opened before using this command

debuggerStopped=The debugger must be opened before setting breakpoints

breakDesc=Manage breakpoints

breakManual=Commands to list, add and remove breakpoints

breaklistDesc=Display known breakpoints

breaklistNone=No breakpoints set

breaklistOutRemove=Remove

breakaddAdded=Added breakpoint

breakaddFailed=Could not set breakpoint: %S

breakaddDesc=Add a breakpoint

breakaddManual=Breakpoint types supported: line

breakaddlineDesc=Add a line breakpoint

breakaddlineFileDesc=JS file URI

breakaddlineLineDesc=Line number

breakdelDesc=Remove a breakpoint

breakdelBreakidDesc=Index of breakpoint

breakdelRemoved=Breakpoint removed

dbgDesc=Manage debugger

dbgManual=Commands to interrupt or resume the main thread, step in, out and over lines of code

dbgOpen=Open the debugger

dbgClose=Close the debugger

dbgInterrupt=Pauses the main thread

dbgContinue=Resumes the main thread, and continues execution following a breakpoint, until the next breakpoint or the termination of the script.

dbgStepDesc=Manage stepping

dbgStepManual=Commands to step in, out and over lines of code

dbgStepOverDesc=Executes the current statement and then stops at the next statement. If the current statement is a function call then the debugger executes the whole function, and it stops at the next statement after the function call

dbgStepInDesc=Executes the current statement and then stops at the next statement. If the current statement is a function call, then the debugger steps into that function, otherwise it stops at the next statement

dbgStepOutDesc=Steps out of the current function and up one level if the function is nested. If in the main body, the script is executed to the end, or to the next breakpoint. The skipped statements are executed, but not stepped through

dbgListSourcesDesc=List the source URLs loaded in the debugger

dbgBlackBoxDesc=Black box sources in the debugger

dbgBlackBoxSourceDesc=A specific source to black box

dbgBlackBoxGlobDesc=Black box all sources that match this glob (for example: “*.min.js”)

dbgBlackBoxInvertDesc=Invert matching, so that we black box every source that is not the source provided or does not match the provided glob pattern.

dbgBlackBoxEmptyDesc=(No sources black boxed)

dbgBlackBoxNonEmptyDesc=The following sources were black boxed:

dbgBlackBoxErrorDesc=Error black boxing:

dbgUnBlackBoxDesc=Stop black boxing sources in the debugger

dbgUnBlackBoxSourceDesc=A specific source to stop black boxing

dbgUnBlackBoxGlobDesc=Stop black boxing all sources that match this glob (for example: “*.min.js”)

dbgUnBlackBoxEmptyDesc=(Did not stop black boxing any sources)

dbgUnBlackBoxNonEmptyDesc=Stopped black boxing the following sources:

dbgUnBlackBoxErrorDesc=Error stopping black boxing:

dbgUnBlackBoxInvertDesc=Invert matching, so that we stop black boxing every source that is not the source provided or does not match the provided glob pattern.

consolecloseDesc=Close the console

consoleopenDesc=Open the console

editDesc=Tweak a page resource

editManual2=Edit one of the resources that is part of this page

editResourceDesc=URL to edit

editLineToJumpToDesc=Line to jump to

resizePageDesc=Resize the page

resizePageArgWidthDesc=Width in pixels

resizePageArgHeightDesc=Height in pixels

resizeModeOnDesc=Enter Responsive Design Mode

resizeModeOffDesc=Exit Responsive Design Mode

resizeModeToggleDesc=Toggle Responsive Design Mode

resizeModeToggleTooltip=Responsive Design Mode

resizeModeToDesc=Alter page size

resizeModeDesc=Control Responsive Design Mode

resizeModeManual2=Responsive websites respond to their environment, so they look good on a mobile display, a cinema display and everything in-between. Responsive Design Mode allows you to easily test a variety of page sizes in %1$S without needing to resize your whole browser.

cmdDesc=Manipulate the commands

cmdRefreshDesc=Re-read mozcmd directory

cmdStatus3=Loaded commands from ‘%1$S’

cmdSetdirDesc=Setup a mozcmd directory

cmdSetdirManual3=A ‘mozcmd’ directory is an easy way to create new custom commands. For more information see https://developer.mozilla.org/docs/Tools/GCLI/Customization

cmdSetdirDirectoryDesc=Directory containing .mozcmd files

addonDesc=Manipulate add-ons

addonListDesc=List installed add-ons

addonListTypeDesc=Select an add-on type

addonListDictionaryHeading=The following dictionaries are currently installed:
addonListExtensionHeading=The following extensions are currently installed:
addonListLocaleHeading=The following locales are currently installed:
addonListPluginHeading=The following plugins are currently installed:
addonListThemeHeading=The following themes are currently installed:
addonListAllHeading=The following add-ons are currently installed:
addonListUnknownHeading=The following add-ons of the selected type are currently installed:

addonListOutEnable=Enable
addonListOutDisable=Disable

addonPending=pending
addonPendingEnable=enable
addonPendingDisable=disable
addonPendingUninstall=uninstall
addonPendingInstall=install
addonPendingUpgrade=upgrade

addonNameDesc=The name of the add-on

addonNoneOfType=There are no add-ons of that type installed.

addonEnableDesc=Enable the specified add-on

addonAlreadyEnabled=%S is already enabled.

addonEnabled=%S enabled.

addonDisableDesc=Disable the specified add-on

addonAlreadyDisabled=%S is already disabled.

addonDisabled=%S disabled.

addonCtpDesc=Set the specified plugin to click-to-play.

addonCtp=%S set to click-to-play.

addonAlreadyCtp=%S is already set to click-to-play.

addonCantCtp=%S cannot be set to click-to-play because it is not a plugin.

addonNoCtp=%S cannot be set to click-to-play.

exportDesc=Export resources

exportHtmlDesc=Export HTML from page

pagemodDesc=Make page changes

pagemodReplaceDesc=Search and replace in page elements

pagemodReplaceSearchDesc=What to search for

pagemodReplaceReplaceDesc=Replacement string

pagemodReplaceIgnoreCaseDesc=Perform case-insensitive search

pagemodReplaceRootDesc=CSS selector to root of search

pagemodReplaceSelectorDesc=CSS selector to match in search

pagemodReplaceAttributesDesc=Attribute match regexp

pagemodReplaceAttrOnlyDesc=Restrict search to attributes

pagemodReplaceContentOnlyDesc=Restrict search to text nodes

pagemodReplaceResult=Elements matched by selector: %1$S. Replaces in text nodes: %2$S. Replaces in attributes: %3$S.

pagemodRemoveDesc=Remove elements and attributes from page

pagemodRemoveElementDesc=Remove elements from page

pagemodRemoveElementSearchDesc=CSS selector specifying elements to remove

pagemodRemoveElementRootDesc=CSS selector specifying root of search

pagemodRemoveElementStripOnlyDesc=Remove element, but leave content

pagemodRemoveElementIfEmptyOnlyDesc=Remove only empty elements

pagemodRemoveElementResultMatchedAndRemovedElements=Elements matched by selector: %1$S. Elements removed: %2$S.

pagemodRemoveAttributeDesc=Remove matching attributes

pagemodRemoveAttributeSearchAttributesDesc=Regexp specifying attributes to remove

pagemodRemoveAttributeSearchElementsDesc=CSS selector of elements to include

pagemodRemoveAttributeRootDesc=CSS selector of root of search

pagemodRemoveAttributeIgnoreCaseDesc=Perform case-insensitive search

pagemodRemoveAttributeResult=Elements matched by selector: %1$S. Attributes removed: %2$S.

toolsDesc2=Hack the %1$S Developer Tools

toolsManual2=Various commands related to hacking directly on the %1$S Developer Tools.

toolsSrcdirDesc=Load tools from a mozilla-central checkout

toolsSrcdirNotFound2=%1$S does not exist or is not a mozilla-central checkout.

toolsSrcdirReloaded2=Tools loaded from %1$S.

toolsSrcdirManual2=Load the %1$S Developer Tools from a complete mozilla-central checkout.

toolsSrcdirDir=A mozilla-central checkout

toolsBuiltinDesc=Use the builtin tools

toolsBuiltinManual=Use the builtin tools, overriding any previous srcdir command.

toolsBuiltinReloaded=Builtin tools loaded.

toolsReloadDesc=Reload the developer tools

toolsReloaded2=Tools reloaded.

cookieDesc=Display and alter cookies

cookieManual=Commands to list, create, delete and alter cookies for the current domain.

cookieListDesc=Display cookies

cookieListManual=Display a list of the cookies relevant to the current page.

cookieListOutHost=Host:
cookieListOutPath=Path:
cookieListOutExpires=Expires:
cookieListOutAttributes=Attributes:

cookieListOutNone=None

cookieListOutSession=At browser exit (session)

cookieListOutNonePage=No cookies found for this page

cookieListOutNoneHost=No cookies found for host %1$S

cookieListOutEdit=Edit

cookieListOutRemove=Remove

cookieRemoveDesc=Remove a cookie

cookieRemoveManual=Remove a cookie, given its key

cookieRemoveKeyDesc=The key of the cookie to remove

cookieSetDesc=Set a cookie

cookieSetManual=Set a cookie by specifying a key name, its value and optionally one or more of the following attributes: expires (max-age in seconds or the expires date in GMTString format), path, domain, secure

cookieSetKeyDesc=The key of the cookie to set

cookieSetValueDesc=The value of the cookie to set

cookieSetOptionsDesc=Options

cookieSetPathDesc=The path of the cookie to set

cookieSetDomainDesc=The domain of the cookie to set

cookieSetSecureDesc=Only transmitted over https

cookieSetHttpOnlyDesc=Not accessible from client side script

cookieSetSessionDesc=Only valid for the lifetime of the browser session

cookieSetExpiresDesc=The expiry date of the cookie (quoted RFC2822 or ISO 8601 date)

jsbDesc=JavaScript beautifier

jsbUrlDesc=The URL of the JS file to beautify

jsbIndentSizeDesc=Indentation size in chars

jsbIndentSizeManual=The number of chars with which to indent each line

jsbIndentCharDesc=The chars used to indent each line

jsbIndentCharManual=The chars used to indent each line. The possible choices are space or tab.

jsbDoNotPreserveNewlinesDesc=Do not preserve line breaks

jsbPreserveNewlinesManual=Should existing line breaks be preserved

jsbPreserveMaxNewlinesDesc=Max consecutive line breaks

jsbPreserveMaxNewlinesManual=The maximum number of consecutive line breaks to preserve

jsbJslintHappyDesc=Enforce jslint-stricter mode?

jsbJslintHappyManual=When set to true, jslint-stricter mode is enforced

jsbBraceStyleDesc2=Select the coding style of braces

jsbBraceStyleManual2=Select the coding style of braces: collapse - put braces on the same line as control statements; expand - put braces on own line (Allman / ANSI style); end-expand - put end braces on own line; expand-strict - put braces on own line even if it will break your code.

jsbNoSpaceBeforeConditionalDesc=No space before conditional statements

jsbUnescapeStringsDesc=Unescape \\xNN characters?

jsbUnescapeStringsManual=Should printable characters in strings encoded in \\xNN notation be unescaped?

jsbInvalidURL=Please enter a valid URL

jsbOptionsDesc=Options

calllogDesc=Commands to manipulate function call logging

calllogStartDesc=Start logging function calls to the console

calllogStartReply=Call logging started.

calllogStopDesc=Stop function call logging

calllogStopNoLogging=No call logging is currently active

calllogStopReply=Stopped call logging. Active contexts: %1$S.

calllogChromeStartDesc=Start logging function calls for chrome code to the console

calllogChromeSourceTypeDesc=Global object, JSM URI, or JS to get a global object from

calllogChromeSourceTypeManual=The global object, URI of a JSM, or JS to execute in the chrome window from which to obtain a global object

calllogChromeStartReply=Call logging started.

calllogChromeStopDesc=Stop function call logging

calllogChromeStopNoLogging=No call logging for chrome code is currently active

calllogChromeStopReply=Stopped call logging. Active contexts: %1$S.

callLogChromeAnonFunction=<anonymous>

callLogChromeMethodCall=Method call

callLogChromeInvalidJSM=Invalid JSM!

callLogChromeVarNotFoundContent=Variable not found in content window.

callLogChromeVarNotFoundChrome=Variable not found in chrome window.

callLogChromeEvalException=Evaluated JavaScript threw the following exception

callLogChromeEvalNeedsObject=The JavaScript source must evaluate to an object whose method calls are to be logged e.g. “({a1: function() {this.a2()},a2: function() {}});”

scratchpadOpenTooltip=Scratchpad

paintflashingDesc=Highlight painted area

paintflashingOnDesc=Turn on paint flashing

paintflashingOffDesc=Turn off paint flashing

paintflashingChromeDesc=chrome frames

paintflashingManual=Draw repainted areas in different colors

paintflashingTooltip=Highlight painted area

paintflashingToggleDesc=Toggle paint flashing

splitconsoleTooltip=Toggle split console

appCacheDesc=Application cache utilities

appCacheValidateDesc=Validate cache manifest

appCacheValidateManual=Find issues relating to a cache manifest and the files that it references

appCacheValidateUriDesc=URI to check

appCacheValidatedSuccessfully=Appcache validated successfully.

appCacheClearDesc=Clear entries from the application cache

appCacheClearManual=Clear one or more entries from the application cache

appCacheClearCleared=Entries cleared successfully.

appCacheListDesc=Display a list of application cache entries.

appCacheListManual=Display a list of all application cache entries. If the search parameter is used then the table displays the entries containing the search term.

appCacheListSearchDesc=Filter results using a search term.

appCacheListKey=Key:
appCacheListDataSize=Data size:
appCacheListDeviceID=Device ID:
appCacheListExpirationTime=Expires:
appCacheListFetchCount=Fetch count:
appCacheListLastFetched=Last fetched:
appCacheListLastModified=Last modified:

appCacheListViewEntry=View Entry

appCacheViewEntryDesc=Open a new tab containing the specified cache entry information.

appCacheViewEntryManual=Open a new tab containing the specified cache entry information.

appCacheViewEntryKey=The key for the entry to display.

profilerDesc=Manage profiler

profilerManual=Commands to start or stop a JavaScript profiler

profilerOpenDesc=Open the profiler

profilerCloseDesc=Close the profiler

profilerStartDesc=Start profiling

profilerStartManual=Name of a profile you wish to start.

profilerStopDesc=Stop profiling

profilerStopManual=Name of a profile you wish to stop.

profilerListDesc=List all profiles

profilerShowDesc=Show individual profile

profilerShowManual=Name of a profile.

profilerAlreadyStarted2=Profile has already been started

profilerNotFound=Profile not found

profilerNotStarted3=Profiler has not been started yet. Use ‘profile start’ to start profiling

profilerStarted2=Recording…

profilerStopped=Stopped…

profilerNotReady=For this command to work you need to open the profiler first

listenDesc=Open a remote debug port

listenManual2=%1$S can allow remote debugging over a TCP/IP connection. For security reasons this is turned off by default, but can be enabled using this command.

listenPortDesc=The TCP port to listen on

listenDisabledOutput=Listen is disabled by the devtools.debugger.remote-enabled preference

listenInitOutput=Listening on port %1$S

listenNoInitOutput=DebuggerServer not initialized

unlistenDesc=Close all remote debug ports

unlistenManual=Closes all the open ports for remote debugging.

unlistenOutput=All TCP ports closed

mediaDesc=CSS media type emulation
mediaEmulateDesc=Emulate a specified CSS media type
mediaEmulateManual=View the document as if rendered on a device supporting the given media type, with the relevant CSS rules applied.
mediaEmulateType=The media type to emulate
mediaResetDesc=Stop emulating a CSS media type

qsaDesc=Perform querySelectorAll on the current document and return number of matches
qsaQueryDesc=CSS selectors separated by comma

injectDesc=Inject common libraries into the page
injectManual2=Inject common libraries into the content of the page which can also be accessed from the console.
injectLibraryDesc=Select the library to inject or enter a valid script URI to inject
injectLoaded=%1$S loaded
injectFailed=Failed to load %1$S - Invalid URI

folderDesc=Open folders
folderOpenDesc=Open folder path
folderOpenDir=Directory Path
folderOpenProfileDesc=Open profile directory

folderInvalidPath=Please enter a valid path

folderOpenDirResult=Opened %1$S

mdnDesc=Retrieve documentation from MDN
mdnCssDesc=Retrieve documentation about a given CSS property name from MDN
mdnCssProp=Property name
mdnCssPropertyNotFound=MDN documentation for the CSS property ‘%1$S’ was not found.
mdnCssVisitPage=Visit MDN page

securityPrivacyDesc=Display supported security and privacy features
securityManual=Commands to list and get suggestions about security features for the current domain.
securityListDesc=Display security features
securityListManual=Display a list of all relevant security features of the current page.
securityCSPDesc=Display CSP specific security features
securityCSPManual=Display feedback about the CSP applied to the current page.
securityCSPRemWildCard=Can you remove the wildcard(*)?
securityCSPPotentialXSS=Potential XSS vulnerability!
securityCSPNoCSPOnPage=Could not find Content-Security-Policy for
securityCSPHeaderOnPage=Content-Security-Policy for
securityCSPROHeaderOnPage=Content-Security-Policy-Report-Only for
securityReferrerPolicyDesc=Display the current Referrer Policy
securityReferrerPolicyManual=Display the Referrer Policy for the current page with example referrers for different URIs.
securityReferrerNextURI=When Visiting
securityReferrerCalculatedReferrer=Referrer Will Be
securityReferrerPolicyReportHeader=Referrer Policy for %1$S
securityReferrerPolicyOtherDomain=Other Origin
securityReferrerPolicyOtherDomainDowngrade=Other Origin HTTP
securityReferrerPolicySameDomain=Same Origin
securityReferrerPolicySameDomainDowngrade=Same Host HTTP

rulersDesc=Toggle rulers for the page

rulersManual=Toggle the horizontal and vertical rulers for the current page

rulersTooltip=Toggle rulers for the page

measureDesc=Measure a portion of the page

measureManual=Activate the measuring tool to measure an arbitrary area of the page

measureTooltip=Measure a portion of the page
