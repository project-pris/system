
"use strict";this.EXPORTED_SYMBOLS=["AddonWatcher"];const{classes:Cc,interfaces:Ci,utils:Cu}=Components;Cu.import("resource://gre/modules/XPCOMUtils.jsm");XPCOMUtils.defineLazyModuleGetter(this,"Preferences","resource://gre/modules/Preferences.jsm");XPCOMUtils.defineLazyModuleGetter(this,"Task","resource://gre/modules/Task.jsm");XPCOMUtils.defineLazyModuleGetter(this,"console","resource://gre/modules/Console.jsm");XPCOMUtils.defineLazyModuleGetter(this,"PerformanceWatcher","resource://gre/modules/PerformanceWatcher.jsm");XPCOMUtils.defineLazyServiceGetter(this,"Telemetry","@mozilla.org/base/telemetry;1",Ci.nsITelemetry);XPCOMUtils.defineLazyModuleGetter(this,"Services","resource://gre/modules/Services.jsm");XPCOMUtils.defineLazyServiceGetter(this,"IdleService","@mozilla.org/widget/idleservice;1",Ci.nsIIdleService);let SUSPICIOUSLY_MANY_ADDONS=5;this.AddonWatcher={TOPIC_SLOW_ADDON_DETECTED:"addon-watcher-detected-slow-addon",init:function(){this._initializedTimeStamp=Cu.now();try{this._ignoreList=new Set(JSON.parse(Preferences.get("browser.addon-watch.ignore",null)));}catch(ex){ this._ignoreList=new Set();}
this._warmupPeriod=Preferences.get("browser.addon-watch.warmup-ms",60*1000 );this._idleThreshold=Preferences.get("browser.addon-watch.deactivate-after-idle-ms",3000);this.paused=false;},uninit:function(){this.paused=true;},_initializedTimeStamp:0,set paused(paused){if(paused){if(this._listener){PerformanceWatcher.removePerformanceListener({addonId:"*"},this._listener);}
this._listener=null;}else{this._listener=this._onSlowAddons.bind(this);PerformanceWatcher.addPerformanceListener({addonId:"*"},this._listener);}},get paused(){return!this._listener;},_listener:null,_getAlerts:function(addonId){let alerts=this._alerts.get(addonId);if(!alerts){alerts={occurrences:0,occurrencesSinceLastNotification:0,latestNotificationTimeStamp:0,};this._alerts.set(addonId,alerts);}
return alerts;},_alerts:new Map(),_onSlowAddons:function(addons){try{if(IdleService.idleTime>=this._idleThreshold){

return;}
if(addons.length>SUSPICIOUSLY_MANY_ADDONS){


return;}
let now=Cu.now();if(now-this._initializedTimeStamp<this._warmupPeriod){return;}

for(let{source:{addonId},details}of addons){Telemetry.getKeyedHistogramById("PERF_MONITORING_SLOW_ADDON_JANK_US").add(addonId,details.highestJank);Telemetry.getKeyedHistogramById("PERF_MONITORING_SLOW_ADDON_CPOW_US").add(addonId,details.highestCPOW);}






let freezeThreshold=Preferences.get("browser.addon-watch.freeze-threshold-micros",5000000);let jankThreshold=Preferences.get("browser.addon-watch.jank-threshold-micros",256000);let occurrencesBetweenAlerts=Preferences.get("browser.addon-watch.occurrences-between-alerts",3);let delayBetweenAlerts=Preferences.get("browser.addon-watch.delay-between-alerts-ms",6*3600*1000 );let delayBetweenFreezeAlerts=Preferences.get("browser.addon-watch.delay-between-freeze-alerts-ms",2*60*1000 );let prescriptionDelay=Preferences.get("browser.addon-watch.prescription-delay",5*60*1000 );let highestNumberOfAddonsToReport=Preferences.get("browser.addon-watch.max-simultaneous-reports",1);addons=addons.filter(x=>x.details.highestJank>=jankThreshold).sort((a,b)=>a.details.highestJank-b.details.highestJank);for(let{source:{addonId},details}of addons){if(highestNumberOfAddonsToReport<=0){return;}
if(this._ignoreList.has(addonId)){continue;}
let alerts=this._getAlerts(addonId);if(now-alerts.latestOccurrence>=prescriptionDelay){
alerts.occurrencesSinceLastNotification=0;}
alerts.occurrencesSinceLastNotification++;alerts.occurrences++;if(details.highestJank<freezeThreshold){if(alerts.occurrencesSinceLastNotification<=occurrencesBetweenAlerts){
continue;}
if(now-alerts.latestNotificationTimeStamp<=delayBetweenAlerts){continue;}}else{if(now-alerts.latestNotificationTimeStamp<=delayBetweenFreezeAlerts){continue;}}
alerts.latestNotificationTimeStamp=now;alerts.occurrencesSinceLastNotification=0;Services.obs.notifyObservers(null,this.TOPIC_SLOW_ADDON_DETECTED,addonId);highestNumberOfAddonsToReport--;}}catch(ex){Cu.reportError("Error in AddonWatcher._onSlowAddons "+ex);Cu.reportError(Task.Debugging.generateReadableStack(ex.stack));}},ignoreAddonForSession:function(addonid){this._ignoreList.add(addonid);},ignoreAddonPermanently:function(addonid){this._ignoreList.add(addonid);try{let ignoreList=JSON.parse(Preferences.get("browser.addon-watch.ignore","[]"))
if(!ignoreList.includes(addonid)){ignoreList.push(addonid);Preferences.set("browser.addon-watch.ignore",JSON.stringify(ignoreList));}}catch(ex){Preferences.set("browser.addon-watch.ignore",JSON.stringify([addonid]));}},get alerts(){let result=new Map();for(let[k,v]of this._alerts){result.set(k,Cu.cloneInto(v,this));}
return result;},};