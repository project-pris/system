"use strict";const Cc=Components.classes;const Ci=Components.interfaces;const Cr=Components.results;const Cu=Components.utils;


if("@mozilla.org/xre/app-info;1"in Cc){let runtime=Cc["@mozilla.org/xre/app-info;1"].getService(Ci.nsIXULRuntime);if(runtime.processType!=Ci.nsIXULRuntime.PROCESS_TYPE_DEFAULT){throw new Error("You cannot use the AddonManager in child processes!");}}
Cu.import("resource://gre/modules/AppConstants.jsm");const MOZ_COMPATIBILITY_NIGHTLY=!['aurora','beta','release','esr'].includes(AppConstants.MOZ_UPDATE_CHANNEL);const PREF_BLOCKLIST_PINGCOUNTVERSION="extensions.blocklist.pingCountVersion";const PREF_DEFAULT_PROVIDERS_ENABLED="extensions.defaultProviders.enabled";const PREF_EM_UPDATE_ENABLED="extensions.update.enabled";const PREF_EM_LAST_APP_VERSION="extensions.lastAppVersion";const PREF_EM_LAST_PLATFORM_VERSION="extensions.lastPlatformVersion";const PREF_EM_AUTOUPDATE_DEFAULT="extensions.update.autoUpdateDefault";const PREF_EM_STRICT_COMPATIBILITY="extensions.strictCompatibility";const PREF_EM_CHECK_UPDATE_SECURITY="extensions.checkUpdateSecurity";const PREF_EM_UPDATE_BACKGROUND_URL="extensions.update.background.url";const PREF_APP_UPDATE_ENABLED="app.update.enabled";const PREF_APP_UPDATE_AUTO="app.update.auto";const PREF_EM_HOTFIX_ID="extensions.hotfix.id";const PREF_EM_HOTFIX_LASTVERSION="extensions.hotfix.lastVersion";const PREF_EM_HOTFIX_URL="extensions.hotfix.url";const PREF_EM_CERT_CHECKATTRIBUTES="extensions.hotfix.cert.checkAttributes";const PREF_EM_HOTFIX_CERTS="extensions.hotfix.certs.";const PREF_MATCH_OS_LOCALE="intl.locale.matchOS";const PREF_SELECTED_LOCALE="general.useragent.locale";const UNKNOWN_XPCOM_ABI="unknownABI";const PREF_MIN_WEBEXT_PLATFORM_VERSION="extensions.webExtensionsMinPlatformVersion";const UPDATE_REQUEST_VERSION=2;const CATEGORY_UPDATE_PARAMS="extension-update-params";const XMLURI_BLOCKLIST="http://www.mozilla.org/2006/addons-blocklist";const KEY_PROFILEDIR="ProfD";const KEY_APPDIR="XCurProcD";const FILE_BLOCKLIST="blocklist.xml";const BRANCH_REGEXP=/^([^\.]+\.[0-9]+[a-z]*).*/gi;const PREF_EM_CHECK_COMPATIBILITY_BASE="extensions.checkCompatibility";var PREF_EM_CHECK_COMPATIBILITY=MOZ_COMPATIBILITY_NIGHTLY?PREF_EM_CHECK_COMPATIBILITY_BASE+".nightly":undefined;const TOOLKIT_ID="toolkit@mozilla.org";const VALID_TYPES_REGEXP=/^[\w\-]+$/;Cu.import("resource://gre/modules/Services.jsm");Cu.import("resource://gre/modules/XPCOMUtils.jsm");Cu.import("resource://gre/modules/AsyncShutdown.jsm");XPCOMUtils.defineLazyModuleGetter(this,"Task","resource://gre/modules/Task.jsm");XPCOMUtils.defineLazyModuleGetter(this,"Promise","resource://gre/modules/Promise.jsm");XPCOMUtils.defineLazyModuleGetter(this,"AddonRepository","resource://gre/modules/addons/AddonRepository.jsm");XPCOMUtils.defineLazyModuleGetter(this,"FileUtils","resource://gre/modules/FileUtils.jsm");XPCOMUtils.defineLazyGetter(this,"CertUtils",function(){let certUtils={};Components.utils.import("resource://gre/modules/CertUtils.jsm",certUtils);return certUtils;});const INTEGER=/^[1-9]\d*$/;this.EXPORTED_SYMBOLS=["AddonManager","AddonManagerPrivate"];const CATEGORY_PROVIDER_MODULE="addon-provider-module";const DEFAULT_PROVIDERS=["resource://gre/modules/addons/XPIProvider.jsm","resource://gre/modules/LightweightThemeManager.jsm"];Cu.import("resource://gre/modules/Log.jsm");
const PARENT_LOGGER_ID="addons";var parentLogger=Log.repository.getLogger(PARENT_LOGGER_ID);parentLogger.level=Log.Level.Warn;var formatter=new Log.BasicFormatter();
parentLogger.addAppender(new Log.ConsoleAppender(formatter));
parentLogger.addAppender(new Log.DumpAppender(formatter));
const LOGGER_ID="addons.manager";var logger=Log.repository.getLogger(LOGGER_ID);



const PREF_LOGGING_ENABLED="extensions.logging.enabled";const NS_PREFBRANCH_PREFCHANGE_TOPIC_ID="nsPref:changed";const UNNAMED_PROVIDER="<unnamed-provider>";function providerName(aProvider){return aProvider.name||UNNAMED_PROVIDER;}
var PrefObserver={init:function(){Services.prefs.addObserver(PREF_LOGGING_ENABLED,this,false);Services.obs.addObserver(this,"xpcom-shutdown",false);this.observe(null,NS_PREFBRANCH_PREFCHANGE_TOPIC_ID,PREF_LOGGING_ENABLED);},observe:function(aSubject,aTopic,aData){if(aTopic=="xpcom-shutdown"){Services.prefs.removeObserver(PREF_LOGGING_ENABLED,this);Services.obs.removeObserver(this,"xpcom-shutdown");}
else if(aTopic==NS_PREFBRANCH_PREFCHANGE_TOPIC_ID){let debugLogEnabled=false;try{debugLogEnabled=Services.prefs.getBoolPref(PREF_LOGGING_ENABLED);}
catch(e){}
if(debugLogEnabled){parentLogger.level=Log.Level.Debug;}
else{parentLogger.level=Log.Level.Warn;}}}};PrefObserver.init();function safeCall(aCallback,...aArgs){try{aCallback.apply(null,aArgs);}
catch(e){logger.warn("Exception calling callback",e);}}
function makeSafe(aCallback){return function(...aArgs){safeCall(aCallback,...aArgs);}}
function reportProviderError(aProvider,aMethod,aError){let method=`provider ${providerName(aProvider)}.${aMethod}`;AddonManagerPrivate.recordException("AMI",method,aError);logger.error("Exception calling "+method,aError);}
function callProvider(aProvider,aMethod,aDefault,...aArgs){if(!(aMethod in aProvider))
return aDefault;try{return aProvider[aMethod].apply(aProvider,aArgs);}
catch(e){reportProviderError(aProvider,aMethod,e);return aDefault;}}
function callProviderAsync(aProvider,aMethod,...aArgs){let callback=aArgs[aArgs.length-1];if(!(aMethod in aProvider)){callback(undefined);return undefined;}
try{return aProvider[aMethod].apply(aProvider,aArgs);}
catch(e){reportProviderError(aProvider,aMethod,e);callback(undefined);return undefined;}}
function promiseCallProvider(aProvider,aMethod,...aArgs){return new Promise(resolve=>{callProviderAsync(aProvider,aMethod,...aArgs,resolve);});}
function getLocale(){try{if(Services.prefs.getBoolPref(PREF_MATCH_OS_LOCALE))
return Services.locale.getLocaleComponentForUserAgent();}
catch(e){}
try{let locale=Services.prefs.getComplexValue(PREF_SELECTED_LOCALE,Ci.nsIPrefLocalizedString);if(locale)
return locale;}
catch(e){}
try{return Services.prefs.getCharPref(PREF_SELECTED_LOCALE);}
catch(e){}
return"en-US";}
function webAPIForAddon(addon){if(!addon){return null;}
let result={};


for(let prop in addon){if(prop[0]!="_"&&typeof(addon[prop])!="function"){result[prop]=addon[prop];}} 
result.isEnabled=!addon.userDisabled;return result;}
function AsyncObjectCaller(aObjects,aMethod,aListener){this.objects=[...aObjects];this.method=aMethod;this.listener=aListener;this.callNext();}
AsyncObjectCaller.prototype={objects:null,method:null,listener:null,callNext:function(){if(this.objects.length==0){this.listener.noMoreObjects(this);return;}
let object=this.objects.shift();if(!this.method||this.method in object)
this.listener.nextObject(this,object);else
this.callNext();}};function BrowserListener(aBrowser,aInstallingPrincipal,aInstalls){this.browser=aBrowser;this.principal=aInstallingPrincipal;this.installs=aInstalls;this.installCount=aInstalls.length;aBrowser.addProgressListener(this,Ci.nsIWebProgress.NOTIFY_LOCATION);Services.obs.addObserver(this,"message-manager-close",true);for(let install of this.installs)
install.addListener(this);this.registered=true;}
BrowserListener.prototype={browser:null,installs:null,installCount:null,registered:false,unregister:function(){if(!this.registered)
return;this.registered=false;Services.obs.removeObserver(this,"message-manager-close"); if(this.browser.removeProgressListener)
this.browser.removeProgressListener(this);for(let install of this.installs)
install.removeListener(this);this.installs=null;},cancelInstalls:function(){for(let install of this.installs){try{install.cancel();}
catch(e){}}},observe:function(subject,topic,data){if(subject!=this.browser.messageManager)
return;
 this.cancelInstalls();},onLocationChange:function(webProgress,request,location){if(this.browser.contentPrincipal&&this.principal.subsumes(this.browser.contentPrincipal))
return; this.cancelInstalls();},onDownloadCancelled:function(install){ install.removeListener(this); if(--this.installCount==0)
this.unregister();},onDownloadFailed:function(install){this.onDownloadCancelled(install);},onInstallFailed:function(install){this.onDownloadCancelled(install);},onInstallEnded:function(install){this.onDownloadCancelled(install);},QueryInterface:XPCOMUtils.generateQI([Ci.nsISupportsWeakReference,Ci.nsIWebProgressListener,Ci.nsIObserver])};function AddonAuthor(aName,aURL){this.name=aName;this.url=aURL;}
AddonAuthor.prototype={name:null,url:null, toString:function(){return this.name||"";}}
function AddonScreenshot(aURL,aWidth,aHeight,aThumbnailURL,aThumbnailWidth,aThumbnailHeight,aCaption){this.url=aURL;if(aWidth)this.width=aWidth;if(aHeight)this.height=aHeight;if(aThumbnailURL)this.thumbnailURL=aThumbnailURL;if(aThumbnailWidth)this.thumbnailWidth=aThumbnailWidth;if(aThumbnailHeight)this.thumbnailHeight=aThumbnailHeight;if(aCaption)this.caption=aCaption;}
AddonScreenshot.prototype={url:null,width:null,height:null,thumbnailURL:null,thumbnailWidth:null,thumbnailHeight:null,caption:null, toString:function(){return this.url||"";}}
function AddonCompatibilityOverride(aType,aMinVersion,aMaxVersion,aAppID,aAppMinVersion,aAppMaxVersion){this.type=aType;this.minVersion=aMinVersion;this.maxVersion=aMaxVersion;this.appID=aAppID;this.appMinVersion=aAppMinVersion;this.appMaxVersion=aAppMaxVersion;}
AddonCompatibilityOverride.prototype={type:null,minVersion:null,maxVersion:null,appID:null,appMinVersion:null,appMaxVersion:null};function AddonType(aID,aLocaleURI,aLocaleKey,aViewType,aUIPriority,aFlags){if(!aID)
throw Components.Exception("An AddonType must have an ID",Cr.NS_ERROR_INVALID_ARG);if(aViewType&&aUIPriority===undefined)
throw Components.Exception("An AddonType with a defined view must have a set UI priority",Cr.NS_ERROR_INVALID_ARG);if(!aLocaleKey)
throw Components.Exception("An AddonType must have a displayable name",Cr.NS_ERROR_INVALID_ARG);this.id=aID;this.uiPriority=aUIPriority;this.viewType=aViewType;this.flags=aFlags;if(aLocaleURI){XPCOMUtils.defineLazyGetter(this,"name",()=>{let bundle=Services.strings.createBundle(aLocaleURI);return bundle.GetStringFromName(aLocaleKey.replace("%ID%",aID));});}
else{this.name=aLocaleKey;}}
var gStarted=false;var gStartupComplete=false;var gCheckCompatibility=true;var gStrictCompatibility=true;var gCheckUpdateSecurityDefault=true;var gCheckUpdateSecurity=gCheckUpdateSecurityDefault;var gUpdateEnabled=true;var gAutoUpdateDefault=true;var gHotfixID=null;var gWebExtensionsMinPlatformVersion=null;var gShutdownBarrier=null;var gRepoShutdownState="";var gShutdownInProgress=false;var gPluginPageListener=null;var AddonManagerInternal={managerListeners:[],installListeners:[],addonListeners:[],typeListeners:[],pendingProviders:new Set(),providers:new Set(),providerShutdowns:new Map(),types:{},startupChanges:{}, telemetryDetails:{},recordTimestamp:function(name,value){this.TelemetryTimestamps.add(name,value);},validateBlocklist:function(){let appBlocklist=FileUtils.getFile(KEY_APPDIR,[FILE_BLOCKLIST]); if(!appBlocklist.exists())
return;let profileBlocklist=FileUtils.getFile(KEY_PROFILEDIR,[FILE_BLOCKLIST]);
 if(!profileBlocklist.exists()){try{appBlocklist.copyTo(profileBlocklist.parent,FILE_BLOCKLIST);}
catch(e){logger.warn("Failed to copy the application shipped blocklist to the profile",e);}
return;}
let fileStream=Cc["@mozilla.org/network/file-input-stream;1"].createInstance(Ci.nsIFileInputStream);try{let cstream=Cc["@mozilla.org/intl/converter-input-stream;1"].createInstance(Ci.nsIConverterInputStream);fileStream.init(appBlocklist,FileUtils.MODE_RDONLY,FileUtils.PERMS_FILE,0);cstream.init(fileStream,"UTF-8",0,0);let data="";let str={};let read=0;do{read=cstream.readString(0xffffffff,str);data+=str.value;}while(read!=0);let parser=Cc["@mozilla.org/xmlextras/domparser;1"].createInstance(Ci.nsIDOMParser);var doc=parser.parseFromString(data,"text/xml");}
catch(e){logger.warn("Application shipped blocklist could not be loaded",e);return;}
finally{try{fileStream.close();}
catch(e){logger.warn("Unable to close blocklist file stream",e);}}
 
if(doc.documentElement.namespaceURI!=XMLURI_BLOCKLIST){logger.warn("Application shipped blocklist has an unexpected namespace ("+
doc.documentElement.namespaceURI+")");return;}
 
if(!doc.documentElement.hasAttribute("lastupdate"))
return;
 if(doc.documentElement.getAttribute("lastupdate")<=profileBlocklist.lastModifiedTime)
return; try{appBlocklist.copyTo(profileBlocklist.parent,FILE_BLOCKLIST);}
catch(e){logger.warn("Failed to copy the application shipped blocklist to the profile",e);}},_startProvider(aProvider,aAppChanged,aOldAppVersion,aOldPlatformVersion){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);logger.debug(`Starting provider: ${providerName(aProvider)}`);callProvider(aProvider,"startup",null,aAppChanged,aOldAppVersion,aOldPlatformVersion);if('shutdown'in aProvider){let name=providerName(aProvider);let AMProviderShutdown=()=>{


if(this.providers.has(aProvider)){this.providers.delete(aProvider);this.pendingProviders.add(aProvider);}
return new Promise((resolve,reject)=>{logger.debug("Calling shutdown blocker for "+name);resolve(aProvider.shutdown());}).catch(err=>{logger.warn("Failure during shutdown of "+name,err);AddonManagerPrivate.recordException("AMI","Async shutdown of "+name,err);});};logger.debug("Registering shutdown blocker for "+name);this.providerShutdowns.set(aProvider,AMProviderShutdown);AddonManager.shutdown.addBlocker(name,AMProviderShutdown);}
this.pendingProviders.delete(aProvider);this.providers.add(aProvider);logger.debug(`Provider finished startup: ${providerName(aProvider)}`);},_getProviderByName(aName){for(let provider of this.providers){if(providerName(provider)==aName)
return provider;}
return undefined;},startup:function(){try{if(gStarted)
return;this.recordTimestamp("AMI_startup_begin"); for(let provider in this.telemetryDetails)
delete this.telemetryDetails[provider];let appChanged=undefined;let oldAppVersion=null;try{oldAppVersion=Services.prefs.getCharPref(PREF_EM_LAST_APP_VERSION);appChanged=Services.appinfo.version!=oldAppVersion;}
catch(e){}
let oldPlatformVersion=null;try{oldPlatformVersion=Services.prefs.getCharPref(PREF_EM_LAST_PLATFORM_VERSION);}
catch(e){}
if(appChanged!==false){logger.debug("Application has been upgraded");Services.prefs.setCharPref(PREF_EM_LAST_APP_VERSION,Services.appinfo.version);Services.prefs.setCharPref(PREF_EM_LAST_PLATFORM_VERSION,Services.appinfo.platformVersion);Services.prefs.setIntPref(PREF_BLOCKLIST_PINGCOUNTVERSION,(appChanged===undefined?0:-1));this.validateBlocklist();}
if(!MOZ_COMPATIBILITY_NIGHTLY){PREF_EM_CHECK_COMPATIBILITY=PREF_EM_CHECK_COMPATIBILITY_BASE+"."+
Services.appinfo.version.replace(BRANCH_REGEXP,"$1");}
try{gCheckCompatibility=Services.prefs.getBoolPref(PREF_EM_CHECK_COMPATIBILITY);}catch(e){}
Services.prefs.addObserver(PREF_EM_CHECK_COMPATIBILITY,this,false);try{gStrictCompatibility=Services.prefs.getBoolPref(PREF_EM_STRICT_COMPATIBILITY);}catch(e){}
Services.prefs.addObserver(PREF_EM_STRICT_COMPATIBILITY,this,false);try{let defaultBranch=Services.prefs.getDefaultBranch("");gCheckUpdateSecurityDefault=defaultBranch.getBoolPref(PREF_EM_CHECK_UPDATE_SECURITY);}catch(e){}
try{gCheckUpdateSecurity=Services.prefs.getBoolPref(PREF_EM_CHECK_UPDATE_SECURITY);}catch(e){}
Services.prefs.addObserver(PREF_EM_CHECK_UPDATE_SECURITY,this,false);try{gUpdateEnabled=Services.prefs.getBoolPref(PREF_EM_UPDATE_ENABLED);}catch(e){}
Services.prefs.addObserver(PREF_EM_UPDATE_ENABLED,this,false);try{gAutoUpdateDefault=Services.prefs.getBoolPref(PREF_EM_AUTOUPDATE_DEFAULT);}catch(e){}
Services.prefs.addObserver(PREF_EM_AUTOUPDATE_DEFAULT,this,false);try{gHotfixID=Services.prefs.getCharPref(PREF_EM_HOTFIX_ID);}catch(e){}
Services.prefs.addObserver(PREF_EM_HOTFIX_ID,this,false);try{gWebExtensionsMinPlatformVersion=Services.prefs.getCharPref(PREF_MIN_WEBEXT_PLATFORM_VERSION);}catch(e){}
Services.prefs.addObserver(PREF_MIN_WEBEXT_PLATFORM_VERSION,this,false);let defaultProvidersEnabled=true;try{defaultProvidersEnabled=Services.prefs.getBoolPref(PREF_DEFAULT_PROVIDERS_ENABLED);}catch(e){}
AddonManagerPrivate.recordSimpleMeasure("default_providers",defaultProvidersEnabled); if(defaultProvidersEnabled){for(let url of DEFAULT_PROVIDERS){try{let scope={};Components.utils.import(url,scope);
 let syms=Object.keys(scope);if((syms.length<1)||(typeof scope[syms[0]].startup!="function")){logger.warn("Provider "+url+" has no startup()");AddonManagerPrivate.recordException("AMI","provider "+url,"no startup()");}
logger.debug("Loaded provider scope for "+url+": "+Object.keys(scope).toSource());}
catch(e){AddonManagerPrivate.recordException("AMI","provider "+url+" load failed",e);logger.error("Exception loading default provider \""+url+"\"",e);}}} 
let catman=Cc["@mozilla.org/categorymanager;1"].getService(Ci.nsICategoryManager);let entries=catman.enumerateCategory(CATEGORY_PROVIDER_MODULE);while(entries.hasMoreElements()){let entry=entries.getNext().QueryInterface(Ci.nsISupportsCString).data;let url=catman.getCategoryEntry(CATEGORY_PROVIDER_MODULE,entry);try{Components.utils.import(url,{});logger.debug(`Loaded provider scope for ${url}`);}
catch(e){AddonManagerPrivate.recordException("AMI","provider "+url+" load failed",e);logger.error("Exception loading provider "+entry+" from category \""+
url+"\"",e);}} 
gShutdownBarrier=new AsyncShutdown.Barrier("AddonManager: Waiting for providers to shut down.");AsyncShutdown.profileBeforeChange.addBlocker("AddonManager: shutting down.",this.shutdownManager.bind(this),{fetchState:this.shutdownState.bind(this)});gStarted=true;for(let provider of this.pendingProviders){this._startProvider(provider,appChanged,oldAppVersion,oldPlatformVersion);} 
if(appChanged===undefined){for(let type in this.startupChanges)
delete this.startupChanges[type];}

let{RemotePages}=Cu.import("resource://gre/modules/RemotePageManager.jsm",{});gPluginPageListener=new RemotePages("about:plugins");gPluginPageListener.addMessageListener("RequestPlugins",this.requestPlugins);gStartupComplete=true;this.recordTimestamp("AMI_startup_end");}
catch(e){logger.error("startup failed",e);AddonManagerPrivate.recordException("AMI","startup failed",e);}
logger.debug("Completed startup sequence");this.callManagerListeners("onStartup");},registerProvider:function(aProvider,aTypes){if(!aProvider||typeof aProvider!="object")
throw Components.Exception("aProvider must be specified",Cr.NS_ERROR_INVALID_ARG);if(aTypes&&!Array.isArray(aTypes))
throw Components.Exception("aTypes must be an array or null",Cr.NS_ERROR_INVALID_ARG);this.pendingProviders.add(aProvider);if(aTypes){for(let type of aTypes){if(!(type.id in this.types)){if(!VALID_TYPES_REGEXP.test(type.id)){logger.warn("Ignoring invalid type "+type.id);return;}
this.types[type.id]={type:type,providers:[aProvider]};let typeListeners=this.typeListeners.slice(0);for(let listener of typeListeners)
safeCall(()=>listener.onTypeAdded(type));}
else{this.types[type.id].providers.push(aProvider);}}}
if(gStarted){this._startProvider(aProvider);}},unregisterProvider:function(aProvider){if(!aProvider||typeof aProvider!="object")
throw Components.Exception("aProvider must be specified",Cr.NS_ERROR_INVALID_ARG);this.providers.delete(aProvider);

this.pendingProviders.delete(aProvider);for(let type in this.types){this.types[type].providers=this.types[type].providers.filter(p=>p!=aProvider);if(this.types[type].providers.length==0){let oldType=this.types[type].type;delete this.types[type];let typeListeners=this.typeListeners.slice(0);for(let listener of typeListeners)
safeCall(()=>listener.onTypeRemoved(oldType));}}
if(gStarted&&!gShutdownInProgress){logger.debug("Unregistering shutdown blocker for "+providerName(aProvider));let shutter=this.providerShutdowns.get(aProvider);if(shutter){this.providerShutdowns.delete(aProvider);gShutdownBarrier.client.removeBlocker(shutter);return shutter();}}
return undefined;},markProviderSafe:function(aProvider){if(!gStarted){throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);}
if(!aProvider||typeof aProvider!="object"){throw Components.Exception("aProvider must be specified",Cr.NS_ERROR_INVALID_ARG);}
if(!this.pendingProviders.has(aProvider)){return;}
this.pendingProviders.delete(aProvider);this.providers.add(aProvider);},callProviders:function(aMethod,...aArgs){if(!aMethod||typeof aMethod!="string")
throw Components.Exception("aMethod must be a non-empty string",Cr.NS_ERROR_INVALID_ARG);let providers=[...this.providers];for(let provider of providers){try{if(aMethod in provider)
provider[aMethod].apply(provider,aArgs);}
catch(e){reportProviderError(provider,aMethod,e);}}},shutdownState(){let state=[];if(gShutdownBarrier){state.push({name:gShutdownBarrier.client.name,state:gShutdownBarrier.state});}
state.push({name:"AddonRepository: async shutdown",state:gRepoShutdownState});return state;},shutdownManager:Task.async(function*(){logger.debug("shutdown");this.callManagerListeners("onShutdown");gRepoShutdownState="pending";gShutdownInProgress=true; Services.prefs.removeObserver(PREF_EM_CHECK_COMPATIBILITY,this);Services.prefs.removeObserver(PREF_EM_STRICT_COMPATIBILITY,this);Services.prefs.removeObserver(PREF_EM_CHECK_UPDATE_SECURITY,this);Services.prefs.removeObserver(PREF_EM_UPDATE_ENABLED,this);Services.prefs.removeObserver(PREF_EM_AUTOUPDATE_DEFAULT,this);Services.prefs.removeObserver(PREF_EM_HOTFIX_ID,this);gPluginPageListener.destroy();gPluginPageListener=null;let savedError=null;if(gStarted){try{yield gShutdownBarrier.wait();}
catch(err){savedError=err;logger.error("Failure during wait for shutdown barrier",err);AddonManagerPrivate.recordException("AMI","Async shutdown of AddonManager providers",err);}}
try{gRepoShutdownState="in progress";yield AddonRepository.shutdown();gRepoShutdownState="done";}
catch(err){savedError=err;logger.error("Failure during AddonRepository shutdown",err);AddonManagerPrivate.recordException("AMI","Async shutdown of AddonRepository",err);}
logger.debug("Async provider shutdown done");this.managerListeners.splice(0,this.managerListeners.length);this.installListeners.splice(0,this.installListeners.length);this.addonListeners.splice(0,this.addonListeners.length);this.typeListeners.splice(0,this.typeListeners.length);this.providerShutdowns.clear();for(let type in this.startupChanges)
delete this.startupChanges[type];gStarted=false;gStartupComplete=false;gShutdownBarrier=null;gShutdownInProgress=false;if(savedError){throw savedError;}}),requestPlugins:function({target:port}){ const NEEDED_PROPS=["name","pluginLibraries","pluginFullpath","version","isActive","blocklistState","description","pluginMimeTypes"];function filterProperties(plugin){let filtered={};for(let prop of NEEDED_PROPS){filtered[prop]=plugin[prop];}
return filtered;}
AddonManager.getAddonsByTypes(["plugin"],function(aPlugins){port.sendAsyncMessage("PluginList",aPlugins.map(filterProperties));});},observe:function(aSubject,aTopic,aData){switch(aData){case PREF_EM_CHECK_COMPATIBILITY:{let oldValue=gCheckCompatibility;try{gCheckCompatibility=Services.prefs.getBoolPref(PREF_EM_CHECK_COMPATIBILITY);}catch(e){gCheckCompatibility=true;}
this.callManagerListeners("onCompatibilityModeChanged");if(gCheckCompatibility!=oldValue)
this.updateAddonAppDisabledStates();break;}
case PREF_EM_STRICT_COMPATIBILITY:{let oldValue=gStrictCompatibility;try{gStrictCompatibility=Services.prefs.getBoolPref(PREF_EM_STRICT_COMPATIBILITY);}catch(e){gStrictCompatibility=true;}
this.callManagerListeners("onCompatibilityModeChanged");if(gStrictCompatibility!=oldValue)
this.updateAddonAppDisabledStates();break;}
case PREF_EM_CHECK_UPDATE_SECURITY:{let oldValue=gCheckUpdateSecurity;try{gCheckUpdateSecurity=Services.prefs.getBoolPref(PREF_EM_CHECK_UPDATE_SECURITY);}catch(e){gCheckUpdateSecurity=true;}
this.callManagerListeners("onCheckUpdateSecurityChanged");if(gCheckUpdateSecurity!=oldValue)
this.updateAddonAppDisabledStates();break;}
case PREF_EM_UPDATE_ENABLED:{let oldValue=gUpdateEnabled;try{gUpdateEnabled=Services.prefs.getBoolPref(PREF_EM_UPDATE_ENABLED);}catch(e){gUpdateEnabled=true;}
this.callManagerListeners("onUpdateModeChanged");break;}
case PREF_EM_AUTOUPDATE_DEFAULT:{let oldValue=gAutoUpdateDefault;try{gAutoUpdateDefault=Services.prefs.getBoolPref(PREF_EM_AUTOUPDATE_DEFAULT);}catch(e){gAutoUpdateDefault=true;}
this.callManagerListeners("onUpdateModeChanged");break;}
case PREF_EM_HOTFIX_ID:{try{gHotfixID=Services.prefs.getCharPref(PREF_EM_HOTFIX_ID);}catch(e){gHotfixID=null;}
break;}
case PREF_MIN_WEBEXT_PLATFORM_VERSION:{gWebExtensionsMinPlatformVersion=Services.prefs.getCharPref(PREF_MIN_WEBEXT_PLATFORM_VERSION);break;}}},escapeAddonURI:function(aAddon,aUri,aAppVersion)
{if(!aAddon||typeof aAddon!="object")
throw Components.Exception("aAddon must be an Addon object",Cr.NS_ERROR_INVALID_ARG);if(!aUri||typeof aUri!="string")
throw Components.Exception("aUri must be a non-empty string",Cr.NS_ERROR_INVALID_ARG);if(aAppVersion&&typeof aAppVersion!="string")
throw Components.Exception("aAppVersion must be a string or null",Cr.NS_ERROR_INVALID_ARG);var addonStatus=aAddon.userDisabled||aAddon.softDisabled?"userDisabled":"userEnabled";if(!aAddon.isCompatible)
addonStatus+=",incompatible";if(aAddon.blocklistState==Ci.nsIBlocklistService.STATE_BLOCKED)
addonStatus+=",blocklisted";if(aAddon.blocklistState==Ci.nsIBlocklistService.STATE_SOFTBLOCKED)
addonStatus+=",softblocked";try{var xpcomABI=Services.appinfo.XPCOMABI;}catch(ex){xpcomABI=UNKNOWN_XPCOM_ABI;}
let uri=aUri.replace(/%ITEM_ID%/g,aAddon.id);uri=uri.replace(/%ITEM_VERSION%/g,aAddon.version);uri=uri.replace(/%ITEM_STATUS%/g,addonStatus);uri=uri.replace(/%APP_ID%/g,Services.appinfo.ID);uri=uri.replace(/%APP_VERSION%/g,aAppVersion?aAppVersion:Services.appinfo.version);uri=uri.replace(/%REQ_VERSION%/g,UPDATE_REQUEST_VERSION);uri=uri.replace(/%APP_OS%/g,Services.appinfo.OS);uri=uri.replace(/%APP_ABI%/g,xpcomABI);uri=uri.replace(/%APP_LOCALE%/g,getLocale());uri=uri.replace(/%CURRENT_APP_VERSION%/g,Services.appinfo.version);
var catMan=null;uri=uri.replace(/%(\w{3,})%/g,function(aMatch,aParam){if(!catMan){catMan=Cc["@mozilla.org/categorymanager;1"].getService(Ci.nsICategoryManager);}
try{var contractID=catMan.getCategoryEntry(CATEGORY_UPDATE_PARAMS,aParam);var paramHandler=Cc[contractID].getService(Ci.nsIPropertyBag2);return paramHandler.getPropertyAsAString(aParam);}
catch(e){return aMatch;}});return uri.replace(/\+/g,"%2B");},backgroundUpdateCheck:function(){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);let buPromise=Task.spawn(function*(){let hotfixID=this.hotfixID;let appUpdateEnabled=Services.prefs.getBoolPref(PREF_APP_UPDATE_ENABLED)&&Services.prefs.getBoolPref(PREF_APP_UPDATE_AUTO);let checkHotfix=hotfixID&&appUpdateEnabled;logger.debug("Background update check beginning");Services.obs.notifyObservers(null,"addons-background-update-start",null);if(this.updateEnabled){let scope={};Components.utils.import("resource://gre/modules/LightweightThemeManager.jsm",scope);scope.LightweightThemeManager.updateCurrentTheme();let allAddons=yield new Promise((resolve,reject)=>this.getAllAddons(resolve));
yield AddonRepository.backgroundUpdateCheck(); let updates=[];for(let addon of allAddons){if(addon.id==hotfixID){continue;}
 
updates.push(new Promise((resolve,reject)=>{addon.findUpdates({onUpdateAvailable:function(aAddon,aInstall){
logger.debug("Found update for add-on ${id}",aAddon);if(aAddon.permissions&AddonManager.PERM_CAN_UPGRADE&&AddonManager.shouldAutoUpdate(aAddon)){logger.debug("Starting install of ${id}",aAddon);aInstall.install();}},onUpdateFinished:aAddon=>{logger.debug("onUpdateFinished for ${id}",aAddon);resolve();}},AddonManager.UPDATE_WHEN_PERIODIC_UPDATE);}));}
yield Promise.all(updates);}
if(checkHotfix){var hotfixVersion="";try{hotfixVersion=Services.prefs.getCharPref(PREF_EM_HOTFIX_LASTVERSION);}
catch(e){}
let url=null;if(Services.prefs.getPrefType(PREF_EM_HOTFIX_URL)==Ci.nsIPrefBranch.PREF_STRING)
url=Services.prefs.getCharPref(PREF_EM_HOTFIX_URL);else
url=Services.prefs.getCharPref(PREF_EM_UPDATE_BACKGROUND_URL);url=AddonManager.escapeAddonURI({id:hotfixID,version:hotfixVersion,userDisabled:false,appDisabled:false},url);Components.utils.import("resource://gre/modules/addons/AddonUpdateChecker.jsm");let update=null;try{let foundUpdates=yield new Promise((resolve,reject)=>{AddonUpdateChecker.checkForUpdates(hotfixID,null,url,{onUpdateCheckComplete:resolve,onUpdateCheckError:reject});});update=AddonUpdateChecker.getNewestCompatibleUpdate(foundUpdates);}catch(e){}

if(update){if(Services.vc.compare(hotfixVersion,update.version)<0){logger.debug("Downloading hotfix version "+update.version);let aInstall=yield new Promise((resolve,reject)=>AddonManager.getInstallForURL(update.updateURL,resolve,"application/x-xpinstall",update.updateHash,null,null,update.version));aInstall.addListener({onDownloadEnded:function(aInstall){if(aInstall.addon.id!=hotfixID){logger.warn("The downloaded hotfix add-on did not have the "+"expected ID and so will not be installed.");aInstall.cancel();return;}
 
if(aInstall.addon.signedState==AddonManager.SIGNEDSTATE_SIGNED)
return;try{if(!Services.prefs.getBoolPref(PREF_EM_CERT_CHECKATTRIBUTES))
return;}
catch(e){return;}
try{CertUtils.validateCert(aInstall.certificate,CertUtils.readCertPrefs(PREF_EM_HOTFIX_CERTS));}
catch(e){logger.warn("The hotfix add-on was not signed by the expected "+"certificate and so will not be installed.",e);aInstall.cancel();}},onInstallEnded:function(aInstall){Services.prefs.setCharPref(PREF_EM_HOTFIX_LASTVERSION,aInstall.version);},onInstallCancelled:function(aInstall){
Services.prefs.setCharPref(PREF_EM_HOTFIX_LASTVERSION,hotfixVersion);}});aInstall.install();}}}
if(appUpdateEnabled){try{yield AddonManagerInternal._getProviderByName("XPIProvider").updateSystemAddons();}
catch(e){logger.warn("Failed to update system addons",e);}}
logger.debug("Background update check complete");Services.obs.notifyObservers(null,"addons-background-update-complete",null);}.bind(this));buPromise.then(null,e=>logger.warn("Error in background update",e));return buPromise;},addStartupChange:function(aType,aID){if(!aType||typeof aType!="string")
throw Components.Exception("aType must be a non-empty string",Cr.NS_ERROR_INVALID_ARG);if(!aID||typeof aID!="string")
throw Components.Exception("aID must be a non-empty string",Cr.NS_ERROR_INVALID_ARG);if(gStartupComplete)
return;logger.debug("Registering startup change '"+aType+"' for "+aID); for(let type in this.startupChanges)
this.removeStartupChange(type,aID);if(!(aType in this.startupChanges))
this.startupChanges[aType]=[];this.startupChanges[aType].push(aID);},removeStartupChange:function(aType,aID){if(!aType||typeof aType!="string")
throw Components.Exception("aType must be a non-empty string",Cr.NS_ERROR_INVALID_ARG);if(!aID||typeof aID!="string")
throw Components.Exception("aID must be a non-empty string",Cr.NS_ERROR_INVALID_ARG);if(gStartupComplete)
return;if(!(aType in this.startupChanges))
return;this.startupChanges[aType]=this.startupChanges[aType].filter(aItem=>aItem!=aID);},callManagerListeners:function(aMethod,...aArgs){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);if(!aMethod||typeof aMethod!="string")
throw Components.Exception("aMethod must be a non-empty string",Cr.NS_ERROR_INVALID_ARG);let managerListeners=this.managerListeners.slice(0);for(let listener of managerListeners){try{if(aMethod in listener)
listener[aMethod].apply(listener,aArgs);}
catch(e){logger.warn("AddonManagerListener threw exception when calling "+aMethod,e);}}},callInstallListeners:function(aMethod,aExtraListeners,...aArgs){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);if(!aMethod||typeof aMethod!="string")
throw Components.Exception("aMethod must be a non-empty string",Cr.NS_ERROR_INVALID_ARG);if(aExtraListeners&&!Array.isArray(aExtraListeners))
throw Components.Exception("aExtraListeners must be an array or null",Cr.NS_ERROR_INVALID_ARG);let result=true;let listeners;if(aExtraListeners)
listeners=aExtraListeners.concat(this.installListeners);else
listeners=this.installListeners.slice(0);for(let listener of listeners){try{if(aMethod in listener){if(listener[aMethod].apply(listener,aArgs)===false)
result=false;}}
catch(e){logger.warn("InstallListener threw exception when calling "+aMethod,e);}}
return result;},callAddonListeners:function(aMethod,...aArgs){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);if(!aMethod||typeof aMethod!="string")
throw Components.Exception("aMethod must be a non-empty string",Cr.NS_ERROR_INVALID_ARG);let addonListeners=this.addonListeners.slice(0);for(let listener of addonListeners){try{if(aMethod in listener)
listener[aMethod].apply(listener,aArgs);}
catch(e){logger.warn("AddonListener threw exception when calling "+aMethod,e);}}},notifyAddonChanged:function(aID,aType,aPendingRestart){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);if(aID&&typeof aID!="string")
throw Components.Exception("aID must be a string or null",Cr.NS_ERROR_INVALID_ARG);if(!aType||typeof aType!="string")
throw Components.Exception("aType must be a non-empty string",Cr.NS_ERROR_INVALID_ARG);









let providers=[...this.providers,...this.pendingProviders];for(let provider of providers){callProvider(provider,"addonChanged",null,aID,aType,aPendingRestart);}},updateAddonAppDisabledStates:function(){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);this.callProviders("updateAddonAppDisabledStates");},updateAddonRepositoryData:function(aCallback){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);if(typeof aCallback!="function")
throw Components.Exception("aCallback must be a function",Cr.NS_ERROR_INVALID_ARG);new AsyncObjectCaller(this.providers,"updateAddonRepositoryData",{nextObject:function(aCaller,aProvider){callProviderAsync(aProvider,"updateAddonRepositoryData",aCaller.callNext.bind(aCaller));},noMoreObjects:function(aCaller){safeCall(aCallback); Services.obs.notifyObservers(null,"TEST:addon-repository-data-updated",null);}});},getInstallForURL:function(aUrl,aCallback,aMimetype,aHash,aName,aIcons,aVersion,aBrowser){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);if(!aUrl||typeof aUrl!="string")
throw Components.Exception("aURL must be a non-empty string",Cr.NS_ERROR_INVALID_ARG);if(typeof aCallback!="function")
throw Components.Exception("aCallback must be a function",Cr.NS_ERROR_INVALID_ARG);if(!aMimetype||typeof aMimetype!="string")
throw Components.Exception("aMimetype must be a non-empty string",Cr.NS_ERROR_INVALID_ARG);if(aHash&&typeof aHash!="string")
throw Components.Exception("aHash must be a string or null",Cr.NS_ERROR_INVALID_ARG);if(aName&&typeof aName!="string")
throw Components.Exception("aName must be a string or null",Cr.NS_ERROR_INVALID_ARG);if(aIcons){if(typeof aIcons=="string")
aIcons={"32":aIcons};else if(typeof aIcons!="object")
throw Components.Exception("aIcons must be a string, an object or null",Cr.NS_ERROR_INVALID_ARG);}else{aIcons={};}
if(aVersion&&typeof aVersion!="string")
throw Components.Exception("aVersion must be a string or null",Cr.NS_ERROR_INVALID_ARG);if(aBrowser&&(!(aBrowser instanceof Ci.nsIDOMElement)))
throw Components.Exception("aBrowser must be a nsIDOMElement or null",Cr.NS_ERROR_INVALID_ARG);let providers=[...this.providers];for(let provider of providers){if(callProvider(provider,"supportsMimetype",false,aMimetype)){callProviderAsync(provider,"getInstallForURL",aUrl,aHash,aName,aIcons,aVersion,aBrowser,function getInstallForURL_safeCall(aInstall){safeCall(aCallback,aInstall);});return;}}
safeCall(aCallback,null);},getInstallForFile:function(aFile,aCallback,aMimetype){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);if(!(aFile instanceof Ci.nsIFile))
throw Components.Exception("aFile must be a nsIFile",Cr.NS_ERROR_INVALID_ARG);if(typeof aCallback!="function")
throw Components.Exception("aCallback must be a function",Cr.NS_ERROR_INVALID_ARG);if(aMimetype&&typeof aMimetype!="string")
throw Components.Exception("aMimetype must be a string or null",Cr.NS_ERROR_INVALID_ARG);new AsyncObjectCaller(this.providers,"getInstallForFile",{nextObject:function(aCaller,aProvider){callProviderAsync(aProvider,"getInstallForFile",aFile,function(aInstall){if(aInstall)
safeCall(aCallback,aInstall);else
aCaller.callNext();});},noMoreObjects:function(aCaller){safeCall(aCallback,null);}});},getInstallsByTypes:function(aTypes,aCallback){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);if(aTypes&&!Array.isArray(aTypes))
throw Components.Exception("aTypes must be an array or null",Cr.NS_ERROR_INVALID_ARG);if(typeof aCallback!="function")
throw Components.Exception("aCallback must be a function",Cr.NS_ERROR_INVALID_ARG);let installs=[];new AsyncObjectCaller(this.providers,"getInstallsByTypes",{nextObject:function(aCaller,aProvider){callProviderAsync(aProvider,"getInstallsByTypes",aTypes,function(aProviderInstalls){if(aProviderInstalls){installs=installs.concat(aProviderInstalls);}
aCaller.callNext();});},noMoreObjects:function(aCaller){safeCall(aCallback,installs);}});},getAllInstalls:function(aCallback){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);this.getInstallsByTypes(null,aCallback);},mapURIToAddonID:function(aURI){if(!(aURI instanceof Ci.nsIURI)){throw Components.Exception("aURI is not a nsIURI",Cr.NS_ERROR_INVALID_ARG);} 
let providers=[...this.providers];for(let provider of providers){var id=callProvider(provider,"mapURIToAddonID",null,aURI);if(id!==null){return id;}}
return null;},isInstallEnabled:function(aMimetype){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);if(!aMimetype||typeof aMimetype!="string")
throw Components.Exception("aMimetype must be a non-empty string",Cr.NS_ERROR_INVALID_ARG);let providers=[...this.providers];for(let provider of providers){if(callProvider(provider,"supportsMimetype",false,aMimetype)&&callProvider(provider,"isInstallEnabled"))
return true;}
return false;},isInstallAllowed:function(aMimetype,aInstallingPrincipal){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);if(!aMimetype||typeof aMimetype!="string")
throw Components.Exception("aMimetype must be a non-empty string",Cr.NS_ERROR_INVALID_ARG);if(!aInstallingPrincipal||!(aInstallingPrincipal instanceof Ci.nsIPrincipal))
throw Components.Exception("aInstallingPrincipal must be a nsIPrincipal",Cr.NS_ERROR_INVALID_ARG);let providers=[...this.providers];for(let provider of providers){if(callProvider(provider,"supportsMimetype",false,aMimetype)&&callProvider(provider,"isInstallAllowed",null,aInstallingPrincipal))
return true;}
return false;},installAddonsFromWebpage:function(aMimetype,aBrowser,aInstallingPrincipal,aInstalls){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);if(!aMimetype||typeof aMimetype!="string")
throw Components.Exception("aMimetype must be a non-empty string",Cr.NS_ERROR_INVALID_ARG);if(aBrowser&&!(aBrowser instanceof Ci.nsIDOMElement))
throw Components.Exception("aSource must be a nsIDOMElement, or null",Cr.NS_ERROR_INVALID_ARG);if(!aInstallingPrincipal||!(aInstallingPrincipal instanceof Ci.nsIPrincipal))
throw Components.Exception("aInstallingPrincipal must be a nsIPrincipal",Cr.NS_ERROR_INVALID_ARG);if(!Array.isArray(aInstalls))
throw Components.Exception("aInstalls must be an array",Cr.NS_ERROR_INVALID_ARG);if(!("@mozilla.org/addons/web-install-listener;1"in Cc)){logger.warn("No web installer available, cancelling all installs");for(let install of aInstalls)
install.cancel();return;}




let topBrowser=aBrowser;let docShell=aBrowser.ownerDocument.defaultView.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDocShell).QueryInterface(Ci.nsIDocShellTreeItem);if(docShell.itemType==Ci.nsIDocShellTreeItem.typeContent)
topBrowser=docShell.chromeEventHandler;try{let weblistener=Cc["@mozilla.org/addons/web-install-listener;1"].getService(Ci.amIWebInstallListener);if(!this.isInstallEnabled(aMimetype)){for(let install of aInstalls)
install.cancel();weblistener.onWebInstallDisabled(topBrowser,aInstallingPrincipal.URI,aInstalls,aInstalls.length);return;}
else if(!aBrowser.contentPrincipal||!aInstallingPrincipal.subsumes(aBrowser.contentPrincipal)){for(let install of aInstalls)
install.cancel();if(weblistener instanceof Ci.amIWebInstallListener2){weblistener.onWebInstallOriginBlocked(topBrowser,aInstallingPrincipal.URI,aInstalls,aInstalls.length);}
return;}

new BrowserListener(aBrowser,aInstallingPrincipal,aInstalls);if(!this.isInstallAllowed(aMimetype,aInstallingPrincipal)){if(weblistener.onWebInstallBlocked(topBrowser,aInstallingPrincipal.URI,aInstalls,aInstalls.length)){for(let install of aInstalls)
install.install();}}
else if(weblistener.onWebInstallRequested(topBrowser,aInstallingPrincipal.URI,aInstalls,aInstalls.length)){for(let install of aInstalls)
install.install();}}
catch(e){

logger.warn("Failure calling web installer",e);for(let install of aInstalls)
install.cancel();}},addInstallListener:function(aListener){if(!aListener||typeof aListener!="object")
throw Components.Exception("aListener must be a InstallListener object",Cr.NS_ERROR_INVALID_ARG);if(!this.installListeners.some(function(i){return i==aListener;}))
this.installListeners.push(aListener);},removeInstallListener:function(aListener){if(!aListener||typeof aListener!="object")
throw Components.Exception("aListener must be a InstallListener object",Cr.NS_ERROR_INVALID_ARG);let pos=0;while(pos<this.installListeners.length){if(this.installListeners[pos]==aListener)
this.installListeners.splice(pos,1);else
pos++;}},installTemporaryAddon:function(aFile){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);if(!(aFile instanceof Ci.nsIFile))
throw Components.Exception("aFile must be a nsIFile",Cr.NS_ERROR_INVALID_ARG);return AddonManagerInternal._getProviderByName("XPIProvider").installTemporaryAddon(aFile);},getAddonByInstanceID:function(aInstanceID){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);if(!aInstanceID||typeof aInstanceID!="symbol")
throw Components.Exception("aInstanceID must be a Symbol()",Cr.NS_ERROR_INVALID_ARG);return AddonManagerInternal._getProviderByName("XPIProvider").getAddonByInstanceID(aInstanceID);},getPreferredIconURL:function(aAddon,aSize,aWindow=undefined){if(aWindow&&aWindow.devicePixelRatio){aSize*=aWindow.devicePixelRatio;}
let icons=aAddon.icons; if(!icons){icons={};if(aAddon.iconURL){icons[32]=aAddon.iconURL;icons[48]=aAddon.iconURL;}
if(aAddon.icon64URL){icons[64]=aAddon.icon64URL;}} 
if(icons[aSize]){return icons[aSize];}
let bestSize=null;for(let size of Object.keys(icons)){if(!INTEGER.test(size)){throw Components.Exception("Invalid icon size, must be an integer",Cr.NS_ERROR_ILLEGAL_VALUE);}
size=parseInt(size,10);if(!bestSize){bestSize=size;continue;}
if(size>aSize&&bestSize>aSize){
 bestSize=Math.min(bestSize,size);}
else{
 bestSize=Math.max(bestSize,size);}}
return icons[bestSize]||null;},getAddonByID:function(aID){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);if(!aID||typeof aID!="string")
throw Components.Exception("aID must be a non-empty string",Cr.NS_ERROR_INVALID_ARG);let promises=Array.from(this.providers,p=>promiseCallProvider(p,"getAddonByID",aID));return Promise.all(promises).then(aAddons=>{return aAddons.find(a=>!!a)||null;});},getAddonBySyncGUID:function(aGUID,aCallback){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);if(!aGUID||typeof aGUID!="string")
throw Components.Exception("aGUID must be a non-empty string",Cr.NS_ERROR_INVALID_ARG);if(typeof aCallback!="function")
throw Components.Exception("aCallback must be a function",Cr.NS_ERROR_INVALID_ARG);new AsyncObjectCaller(this.providers,"getAddonBySyncGUID",{nextObject:function(aCaller,aProvider){callProviderAsync(aProvider,"getAddonBySyncGUID",aGUID,function(aAddon){if(aAddon){safeCall(aCallback,aAddon);}else{aCaller.callNext();}});},noMoreObjects:function(aCaller){safeCall(aCallback,null);}});},getAddonsByIDs:function(aIDs){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);if(!Array.isArray(aIDs))
throw Components.Exception("aIDs must be an array",Cr.NS_ERROR_INVALID_ARG);let promises=aIDs.map(a=>AddonManagerInternal.getAddonByID(a));return Promise.all(promises);},getAddonsByTypes:function(aTypes,aCallback){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);if(aTypes&&!Array.isArray(aTypes))
throw Components.Exception("aTypes must be an array or null",Cr.NS_ERROR_INVALID_ARG);if(typeof aCallback!="function")
throw Components.Exception("aCallback must be a function",Cr.NS_ERROR_INVALID_ARG);let addons=[];new AsyncObjectCaller(this.providers,"getAddonsByTypes",{nextObject:function(aCaller,aProvider){callProviderAsync(aProvider,"getAddonsByTypes",aTypes,function(aProviderAddons){if(aProviderAddons){addons=addons.concat(aProviderAddons);}
aCaller.callNext();});},noMoreObjects:function(aCaller){safeCall(aCallback,addons);}});},getAllAddons:function(aCallback){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);if(typeof aCallback!="function")
throw Components.Exception("aCallback must be a function",Cr.NS_ERROR_INVALID_ARG);this.getAddonsByTypes(null,aCallback);},getAddonsWithOperationsByTypes:function(aTypes,aCallback){if(!gStarted)
throw Components.Exception("AddonManager is not initialized",Cr.NS_ERROR_NOT_INITIALIZED);if(aTypes&&!Array.isArray(aTypes))
throw Components.Exception("aTypes must be an array or null",Cr.NS_ERROR_INVALID_ARG);if(typeof aCallback!="function")
throw Components.Exception("aCallback must be a function",Cr.NS_ERROR_INVALID_ARG);let addons=[];new AsyncObjectCaller(this.providers,"getAddonsWithOperationsByTypes",{nextObject:function getAddonsWithOperationsByTypes_nextObject
(aCaller,aProvider){callProviderAsync(aProvider,"getAddonsWithOperationsByTypes",aTypes,function getAddonsWithOperationsByTypes_concatAddons
(aProviderAddons){if(aProviderAddons){addons=addons.concat(aProviderAddons);}
aCaller.callNext();});},noMoreObjects:function(caller){safeCall(aCallback,addons);}});},addManagerListener:function(aListener){if(!aListener||typeof aListener!="object")
throw Components.Exception("aListener must be an AddonManagerListener object",Cr.NS_ERROR_INVALID_ARG);if(!this.managerListeners.some(i=>i==aListener))
this.managerListeners.push(aListener);},removeManagerListener:function(aListener){if(!aListener||typeof aListener!="object")
throw Components.Exception("aListener must be an AddonManagerListener object",Cr.NS_ERROR_INVALID_ARG);let pos=0;while(pos<this.managerListeners.length){if(this.managerListeners[pos]==aListener)
this.managerListeners.splice(pos,1);else
pos++;}},addAddonListener:function(aListener){if(!aListener||typeof aListener!="object")
throw Components.Exception("aListener must be an AddonListener object",Cr.NS_ERROR_INVALID_ARG);if(!this.addonListeners.some(i=>i==aListener))
this.addonListeners.push(aListener);},removeAddonListener:function(aListener){if(!aListener||typeof aListener!="object")
throw Components.Exception("aListener must be an AddonListener object",Cr.NS_ERROR_INVALID_ARG);let pos=0;while(pos<this.addonListeners.length){if(this.addonListeners[pos]==aListener)
this.addonListeners.splice(pos,1);else
pos++;}},addTypeListener:function(aListener){if(!aListener||typeof aListener!="object")
throw Components.Exception("aListener must be a TypeListener object",Cr.NS_ERROR_INVALID_ARG);if(!this.typeListeners.some(i=>i==aListener))
this.typeListeners.push(aListener);},removeTypeListener:function(aListener){if(!aListener||typeof aListener!="object")
throw Components.Exception("aListener must be a TypeListener object",Cr.NS_ERROR_INVALID_ARG);let pos=0;while(pos<this.typeListeners.length){if(this.typeListeners[pos]==aListener)
this.typeListeners.splice(pos,1);else
pos++;}},get addonTypes(){ return new Proxy(this.types,{defineProperty(target,property,descriptor){ return false;},deleteProperty(target,property){ return false;},get(target,property,receiver){if(!target.hasOwnProperty(property))
return undefined;return target[property].type;},getOwnPropertyDescriptor(target,property){if(!target.hasOwnProperty(property))
return undefined;return{value:target[property].type,writable:false,configurable:true,enumerable:true}},preventExtensions(target){ return false;},set(target,property,value,receiver){ return false;},setPrototypeOf(target,prototype){ return false;}});},get autoUpdateDefault(){return gAutoUpdateDefault;},set autoUpdateDefault(aValue){aValue=!!aValue;if(aValue!=gAutoUpdateDefault)
Services.prefs.setBoolPref(PREF_EM_AUTOUPDATE_DEFAULT,aValue);return aValue;},get checkCompatibility(){return gCheckCompatibility;},set checkCompatibility(aValue){aValue=!!aValue;if(aValue!=gCheckCompatibility){if(!aValue)
Services.prefs.setBoolPref(PREF_EM_CHECK_COMPATIBILITY,false);else
Services.prefs.clearUserPref(PREF_EM_CHECK_COMPATIBILITY);}
return aValue;},get strictCompatibility(){return gStrictCompatibility;},set strictCompatibility(aValue){aValue=!!aValue;if(aValue!=gStrictCompatibility)
Services.prefs.setBoolPref(PREF_EM_STRICT_COMPATIBILITY,aValue);return aValue;},get checkUpdateSecurityDefault(){return gCheckUpdateSecurityDefault;},get checkUpdateSecurity(){return gCheckUpdateSecurity;},set checkUpdateSecurity(aValue){aValue=!!aValue;if(aValue!=gCheckUpdateSecurity){if(aValue!=gCheckUpdateSecurityDefault)
Services.prefs.setBoolPref(PREF_EM_CHECK_UPDATE_SECURITY,aValue);else
Services.prefs.clearUserPref(PREF_EM_CHECK_UPDATE_SECURITY);}
return aValue;},get updateEnabled(){return gUpdateEnabled;},set updateEnabled(aValue){aValue=!!aValue;if(aValue!=gUpdateEnabled)
Services.prefs.setBoolPref(PREF_EM_UPDATE_ENABLED,aValue);return aValue;},get hotfixID(){return gHotfixID;},webAPI:{installs:new Map(),nextInstall:0,sendEvent:null,setEventHandler(fn){this.sendEvent=fn;},getAddonByID(target,id){return new Promise(resolve=>{AddonManager.getAddonByID(id,(addon)=>{resolve(webAPIForAddon(addon));});});}, copyProps(install,obj){obj.state=AddonManager.stateToString(install.state);obj.error=AddonManager.errorToString(install.error);obj.progress=install.progress;obj.maxProgress=install.maxProgress;},makeListener(id,target){const events=["onDownloadStarted","onDownloadProgress","onDownloadEnded","onDownloadCancelled","onDownloadFailed","onInstallStarted","onInstallEnded","onInstallCancelled","onInstallFailed",];let listener={};events.forEach(event=>{listener[event]=(install)=>{let data={event,id};AddonManager.webAPI.copyProps(install,data);this.sendEvent(target,data);}});return listener;},forgetInstall(id){let info=this.installs.get(id);if(!info){throw new Error(`forgetInstall cannot find ${id}`);}
info.install.removeListener(info.listener);this.installs.delete(id);},createInstall(target,options){return new Promise((resolve)=>{let newInstall=install=>{let id=this.nextInstall++;let listener=this.makeListener(id,target);install.addListener(listener);this.installs.set(id,{install,target,listener});let result={id};this.copyProps(install,result);resolve(result);};AddonManager.getInstallForURL(options.url,newInstall,"application/x-xpinstall");});},addonUninstall(target,id){return new Promise(resolve=>{AddonManager.getAddonByID(id,addon=>{if(!addon){resolve(false);}
try{addon.uninstall();resolve(true);}catch(err){Cu.reportError(err);resolve(false);}});});},addonInstallDoInstall(target,id){let state=this.installs.get(id);if(!state){return Promise.reject(`invalid id ${id}`);}
return Promise.resolve(state.install.install());},addonInstallCancel(target,id){let state=this.installs.get(id);if(!state){return Promise.reject(`invalid id ${id}`);}
return Promise.resolve(state.install.cancel());},clearInstalls(ids){for(let id of ids){this.forgetInstall(id);}},clearInstallsFrom(mm){for(let[id,info]of this.installs){if(info.target==mm){this.forgetInstall(id);}}},},};this.AddonManagerPrivate={startup:function(){AddonManagerInternal.startup();},registerProvider:function(aProvider,aTypes){AddonManagerInternal.registerProvider(aProvider,aTypes);},unregisterProvider:function(aProvider){AddonManagerInternal.unregisterProvider(aProvider);},markProviderSafe:function(aProvider){AddonManagerInternal.markProviderSafe(aProvider);},backgroundUpdateCheck:function(){return AddonManagerInternal.backgroundUpdateCheck();},backgroundUpdateTimerHandler(){let checkHotfix=AddonManagerInternal.hotfixID&&Services.prefs.getBoolPref(PREF_APP_UPDATE_ENABLED)&&Services.prefs.getBoolPref(PREF_APP_UPDATE_AUTO);if(!AddonManagerInternal.updateEnabled&&!checkHotfix){logger.info("Skipping background update check");return;}
AddonManagerInternal.backgroundUpdateCheck();},addStartupChange:function(aType,aID){AddonManagerInternal.addStartupChange(aType,aID);},removeStartupChange:function(aType,aID){AddonManagerInternal.removeStartupChange(aType,aID);},notifyAddonChanged:function(aID,aType,aPendingRestart){AddonManagerInternal.notifyAddonChanged(aID,aType,aPendingRestart);},updateAddonAppDisabledStates:function(){AddonManagerInternal.updateAddonAppDisabledStates();},updateAddonRepositoryData:function(aCallback){AddonManagerInternal.updateAddonRepositoryData(aCallback);},callInstallListeners:function(...aArgs){return AddonManagerInternal.callInstallListeners.apply(AddonManagerInternal,aArgs);},callAddonListeners:function(...aArgs){AddonManagerInternal.callAddonListeners.apply(AddonManagerInternal,aArgs);},AddonAuthor:AddonAuthor,AddonScreenshot:AddonScreenshot,AddonCompatibilityOverride:AddonCompatibilityOverride,AddonType:AddonType,recordTimestamp:function(name,value){AddonManagerInternal.recordTimestamp(name,value);},_simpleMeasures:{},recordSimpleMeasure:function(name,value){this._simpleMeasures[name]=value;},recordException:function(aModule,aContext,aException){let report={module:aModule,context:aContext};if(typeof aException=="number"){report.message=Components.Exception("",aException).name;}
else{report.message=aException.toString();if(aException.fileName){report.file=aException.fileName;report.line=aException.lineNumber;}}
this._simpleMeasures.exception=report;},getSimpleMeasures:function(){return this._simpleMeasures;},getTelemetryDetails:function(){return AddonManagerInternal.telemetryDetails;},setTelemetryDetails:function(aProvider,aDetails){AddonManagerInternal.telemetryDetails[aProvider]=aDetails;},
 simpleTimer:function(aName){let startTime=Cu.now();return{done:()=>this.recordSimpleMeasure(aName,Math.round(Cu.now()-startTime))};},callNoUpdateListeners:function(addon,listener,reason,appVersion,platformVersion){if("onNoCompatibilityUpdateAvailable"in listener){safeCall(listener.onNoCompatibilityUpdateAvailable.bind(listener),addon);}
if("onNoUpdateAvailable"in listener){safeCall(listener.onNoUpdateAvailable.bind(listener),addon);}
if("onUpdateFinished"in listener){safeCall(listener.onUpdateFinished.bind(listener),addon);}},get webExtensionsMinPlatformVersion(){return gWebExtensionsMinPlatformVersion;},};this.AddonManager={
_states:new Map([["STATE_AVAILABLE",0],["STATE_DOWNLOADING",1],["STATE_CHECKING",2],["STATE_DOWNLOADED",3],["STATE_DOWNLOAD_FAILED",4],["STATE_INSTALLING",5],["STATE_INSTALLED",6],["STATE_INSTALL_FAILED",7],["STATE_CANCELLED",8],]),
_errors:new Map([["ERROR_NETWORK_FAILURE",-1],["ERROR_INCORRECT_HASH",-2],["ERROR_CORRUPT_FILE",-3],["ERROR_FILE_ACCESS",-4],["ERROR_SIGNEDSTATE_REQUIRED",-5],["ERROR_UNEXPECTED_ADDON_TYPE",-6],]),UPDATE_STATUS_NO_ERROR:0, UPDATE_STATUS_TIMEOUT:-1,UPDATE_STATUS_DOWNLOAD_ERROR:-2,UPDATE_STATUS_PARSE_ERROR:-3,UPDATE_STATUS_UNKNOWN_FORMAT:-4,UPDATE_STATUS_SECURITY_ERROR:-5,UPDATE_STATUS_CANCELLED:-6,
UPDATE_WHEN_USER_REQUESTED:1,
UPDATE_WHEN_NEW_APP_DETECTED:2,UPDATE_WHEN_NEW_APP_INSTALLED:3,UPDATE_WHEN_PERIODIC_UPDATE:16,UPDATE_WHEN_ADDON_INSTALLED:17,
PENDING_NONE:0,PENDING_ENABLE:1,PENDING_DISABLE:2,PENDING_UNINSTALL:4,PENDING_INSTALL:8,PENDING_UPGRADE:16,
OP_NEEDS_RESTART_NONE:0,OP_NEEDS_RESTART_ENABLE:1,OP_NEEDS_RESTART_DISABLE:2,OP_NEEDS_RESTART_UNINSTALL:4,OP_NEEDS_RESTART_INSTALL:8,PERM_CAN_UNINSTALL:1,PERM_CAN_ENABLE:2,PERM_CAN_DISABLE:4,PERM_CAN_UPGRADE:8,
PERM_CAN_ASK_TO_ACTIVATE:16,SCOPE_PROFILE:1,SCOPE_USER:2,SCOPE_APPLICATION:4,SCOPE_SYSTEM:8, SCOPE_TEMPORARY:16,SCOPE_ALL:31,VIEW_TYPE_LIST:"list",
TYPE_UI_HIDE_EMPTY:16,
TYPE_SUPPORTS_ASK_TO_ACTIVATE:32,
TYPE_SUPPORTS_UNDO_RESTARTLESS_UNINSTALL:64,AUTOUPDATE_DISABLE:0,
AUTOUPDATE_DEFAULT:1,AUTOUPDATE_ENABLE:2, OPTIONS_TYPE_DIALOG:1, OPTIONS_TYPE_INLINE:2, OPTIONS_TYPE_TAB:3,OPTIONS_TYPE_INLINE_INFO:4,

OPTIONS_TYPE_INLINE_BROWSER:5,
 OPTIONS_NOTIFICATION_DISPLAYED:"addon-options-displayed", OPTIONS_NOTIFICATION_HIDDEN:"addon-options-hidden",

STARTUP_CHANGE_INSTALLED:"installed",

STARTUP_CHANGE_CHANGED:"changed",
STARTUP_CHANGE_UNINSTALLED:"uninstalled",

STARTUP_CHANGE_DISABLED:"disabled",

STARTUP_CHANGE_ENABLED:"enabled",
SIGNEDSTATE_NOT_REQUIRED:undefined,SIGNEDSTATE_BROKEN:-2,
SIGNEDSTATE_UNKNOWN:-1,SIGNEDSTATE_MISSING:0,SIGNEDSTATE_PRELIMINARY:1,SIGNEDSTATE_SIGNED:2,SIGNEDSTATE_SYSTEM:3,


STATE_ASK_TO_ACTIVATE:"askToActivate",get __AddonManagerInternal__(){return AppConstants.DEBUG?AddonManagerInternal:undefined;},get isReady(){return gStartupComplete&&!gShutdownInProgress;},init(){this._stateToString=new Map();for(let[name,value]of this._states){this[name]=value;this._stateToString.set(value,name);}
this._errorToString=new Map();for(let[name,value]of this._errors){this[name]=value;this._errorToString.set(value,name);}},stateToString(state){return this._stateToString.get(state);},errorToString(err){return err?this._errorToString.get(err):null;},getInstallForURL:function(aUrl,aCallback,aMimetype,aHash,aName,aIcons,aVersion,aBrowser){AddonManagerInternal.getInstallForURL(aUrl,aCallback,aMimetype,aHash,aName,aIcons,aVersion,aBrowser);},getInstallForFile:function(aFile,aCallback,aMimetype){AddonManagerInternal.getInstallForFile(aFile,aCallback,aMimetype);},getStartupChanges:function(aType){if(!(aType in AddonManagerInternal.startupChanges))
return[];return AddonManagerInternal.startupChanges[aType].slice(0);},getAddonByID:function(aID,aCallback){if(typeof aCallback!="function")
throw Components.Exception("aCallback must be a function",Cr.NS_ERROR_INVALID_ARG);AddonManagerInternal.getAddonByID(aID).then(makeSafe(aCallback)).catch(logger.error);},getAddonBySyncGUID:function(aGUID,aCallback){AddonManagerInternal.getAddonBySyncGUID(aGUID,aCallback);},getAddonsByIDs:function(aIDs,aCallback){if(typeof aCallback!="function")
throw Components.Exception("aCallback must be a function",Cr.NS_ERROR_INVALID_ARG);AddonManagerInternal.getAddonsByIDs(aIDs).then(makeSafe(aCallback)).catch(logger.error);},getAddonsWithOperationsByTypes:function(aTypes,aCallback){AddonManagerInternal.getAddonsWithOperationsByTypes(aTypes,aCallback);},getAddonsByTypes:function(aTypes,aCallback){AddonManagerInternal.getAddonsByTypes(aTypes,aCallback);},getAllAddons:function(aCallback){AddonManagerInternal.getAllAddons(aCallback);},getInstallsByTypes:function(aTypes,aCallback){AddonManagerInternal.getInstallsByTypes(aTypes,aCallback);},getAllInstalls:function(aCallback){AddonManagerInternal.getAllInstalls(aCallback);},mapURIToAddonID:function(aURI){return AddonManagerInternal.mapURIToAddonID(aURI);},isInstallEnabled:function(aType){return AddonManagerInternal.isInstallEnabled(aType);},isInstallAllowed:function(aType,aInstallingPrincipal){return AddonManagerInternal.isInstallAllowed(aType,aInstallingPrincipal);},installAddonsFromWebpage:function(aType,aBrowser,aInstallingPrincipal,aInstalls){AddonManagerInternal.installAddonsFromWebpage(aType,aBrowser,aInstallingPrincipal,aInstalls);},installTemporaryAddon:function(aDirectory){return AddonManagerInternal.installTemporaryAddon(aDirectory);},getAddonByInstanceID:function(aInstanceID){return AddonManagerInternal.getAddonByInstanceID(aInstanceID);},addManagerListener:function(aListener){AddonManagerInternal.addManagerListener(aListener);},removeManagerListener:function(aListener){AddonManagerInternal.removeManagerListener(aListener);},addInstallListener:function(aListener){AddonManagerInternal.addInstallListener(aListener);},removeInstallListener:function(aListener){AddonManagerInternal.removeInstallListener(aListener);},addAddonListener:function(aListener){AddonManagerInternal.addAddonListener(aListener);},removeAddonListener:function(aListener){AddonManagerInternal.removeAddonListener(aListener);},addTypeListener:function(aListener){AddonManagerInternal.addTypeListener(aListener);},removeTypeListener:function(aListener){AddonManagerInternal.removeTypeListener(aListener);},get addonTypes(){return AddonManagerInternal.addonTypes;},shouldAutoUpdate:function(aAddon){if(!aAddon||typeof aAddon!="object")
throw Components.Exception("aAddon must be specified",Cr.NS_ERROR_INVALID_ARG);if(!("applyBackgroundUpdates"in aAddon))
return false;if(aAddon.applyBackgroundUpdates==AddonManager.AUTOUPDATE_ENABLE)
return true;if(aAddon.applyBackgroundUpdates==AddonManager.AUTOUPDATE_DISABLE)
return false;return this.autoUpdateDefault;},get checkCompatibility(){return AddonManagerInternal.checkCompatibility;},set checkCompatibility(aValue){AddonManagerInternal.checkCompatibility=aValue;},get strictCompatibility(){return AddonManagerInternal.strictCompatibility;},set strictCompatibility(aValue){AddonManagerInternal.strictCompatibility=aValue;},get checkUpdateSecurityDefault(){return AddonManagerInternal.checkUpdateSecurityDefault;},get checkUpdateSecurity(){return AddonManagerInternal.checkUpdateSecurity;},set checkUpdateSecurity(aValue){AddonManagerInternal.checkUpdateSecurity=aValue;},get updateEnabled(){return AddonManagerInternal.updateEnabled;},set updateEnabled(aValue){AddonManagerInternal.updateEnabled=aValue;},get autoUpdateDefault(){return AddonManagerInternal.autoUpdateDefault;},set autoUpdateDefault(aValue){AddonManagerInternal.autoUpdateDefault=aValue;},get hotfixID(){return AddonManagerInternal.hotfixID;},escapeAddonURI:function(aAddon,aUri,aAppVersion){return AddonManagerInternal.escapeAddonURI(aAddon,aUri,aAppVersion);},getPreferredIconURL:function(aAddon,aSize,aWindow=undefined){return AddonManagerInternal.getPreferredIconURL(aAddon,aSize,aWindow);},get webAPI(){return AddonManagerInternal.webAPI;},get shutdown(){return gShutdownBarrier.client;},};this.AddonManager.init();Cu.import("resource://gre/modules/TelemetryTimestamps.jsm",AddonManagerInternal);Object.freeze(AddonManagerInternal);Object.freeze(AddonManagerPrivate);Object.freeze(AddonManager);