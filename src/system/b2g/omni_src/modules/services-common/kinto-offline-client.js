this.EXPORTED_SYMBOLS=["loadKinto"];(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.loadKinto=f()}})(function(){var define,module,exports;return(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){"use strict";Object.defineProperty(exports,"__esModule",{value:true});var _base=require("../src/adapters/base");var _base2=_interopRequireDefault(_base);var _utils=require("../src/utils");function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}
Components.utils.import("resource://gre/modules/Sqlite.jsm");Components.utils.import("resource://gre/modules/Task.jsm");const statements={"createCollectionData":`
    CREATE TABLE collection_data (
      collection_name TEXT,
      record_id TEXT,
      record TEXT
    );`,"createCollectionMetadata":`
    CREATE TABLE collection_metadata (
      collection_name TEXT PRIMARY KEY,
      last_modified INTEGER
    ) WITHOUT ROWID;`,"createCollectionDataRecordIdIndex":`
    CREATE UNIQUE INDEX unique_collection_record
      ON collection_data(collection_name, record_id);`,"clearData":`
    DELETE FROM collection_data
      WHERE collection_name = :collection_name;`,"createData":`
    INSERT INTO collection_data (collection_name, record_id, record)
      VALUES (:collection_name, :record_id, :record);`,"updateData":`
    UPDATE collection_data
      SET record = :record
        WHERE collection_name = :collection_name
        AND record_id = :record_id;`,"deleteData":`
    DELETE FROM collection_data
      WHERE collection_name = :collection_name
      AND record_id = :record_id;`,"saveLastModified":`
    REPLACE INTO collection_metadata (collection_name, last_modified)
      VALUES (:collection_name, :last_modified);`,"getLastModified":`
    SELECT last_modified
      FROM collection_metadata
        WHERE collection_name = :collection_name;`,"getRecord":`
    SELECT record
      FROM collection_data
        WHERE collection_name = :collection_name
        AND record_id = :record_id;`,"listRecords":`
    SELECT record
      FROM collection_data
        WHERE collection_name = :collection_name;`,"importData":`
    REPLACE INTO collection_data (collection_name, record_id, record)
      VALUES (:collection_name, :record_id, :record);`};const createStatements=["createCollectionData","createCollectionMetadata","createCollectionDataRecordIdIndex"];const currentSchemaVersion=1;class FirefoxAdapter extends _base2.default{constructor(collection){super();this.collection=collection;}
_init(connection){return Task.spawn(function*(){yield connection.executeTransaction(function*doSetup(){const schema=yield connection.getSchemaVersion();if(schema==0){for(let statementName of createStatements){yield connection.execute(statements[statementName]);}
yield connection.setSchemaVersion(currentSchemaVersion);}else if(schema!=1){throw new Error("Unknown database schema: "+schema);}});return connection;});}
_executeStatement(statement,params){if(!this._connection){throw new Error("The storage adapter is not open");}
return this._connection.executeCached(statement,params);}
open(){const self=this;return Task.spawn(function*(){const opts={path:"kinto.sqlite",sharedMemoryCache:false};if(!self._connection){self._connection=yield Sqlite.openConnection(opts).then(self._init);}});}
close(){if(this._connection){const promise=this._connection.close();this._connection=null;return promise;}
return Promise.resolve();}
clear(){const params={collection_name:this.collection};return this._executeStatement(statements.clearData,params);}
execute(callback,options={preload:[]}){if(!this._connection){throw new Error("The storage adapter is not open");}
const preloaded=options.preload.reduce((acc,record)=>{acc[record.id]=record;return acc;},{});const proxy=transactionProxy(this.collection,preloaded);let result;try{result=callback(proxy);}catch(e){return Promise.reject(e);}
const conn=this._connection;return conn.executeTransaction(function*doExecuteTransaction(){for(let{statement,params}of proxy.operations){yield conn.executeCached(statement,params);}}).then(_=>result);}
get(id){const params={collection_name:this.collection,record_id:id};return this._executeStatement(statements.getRecord,params).then(result=>{if(result.length==0){return;}
return JSON.parse(result[0].getResultByName("record"));});}
list(params={filters:{},order:""}){const parameters={collection_name:this.collection};return this._executeStatement(statements.listRecords,parameters).then(result=>{const records=[];for(let k=0;k<result.length;k++){const row=result[k];records.push(JSON.parse(row.getResultByName("record")));}
return records;}).then(results=>{return(0,_utils.reduceRecords)(params.filters,params.order,results);});}
loadDump(records){const connection=this._connection;const collection_name=this.collection;return Task.spawn(function*(){yield connection.executeTransaction(function*doImport(){for(let record of records){const params={collection_name:collection_name,record_id:record.id,record:JSON.stringify(record)};yield connection.execute(statements.importData,params);}
const lastModified=Math.max(...records.map(record=>record.last_modified));const params={collection_name:collection_name};const previousLastModified=yield connection.execute(statements.getLastModified,params).then(result=>{return result.length>0?result[0].getResultByName('last_modified'):-1;});if(lastModified>previousLastModified){const params={collection_name:collection_name,last_modified:lastModified};yield connection.execute(statements.saveLastModified,params);}});return records;});}
saveLastModified(lastModified){const parsedLastModified=parseInt(lastModified,10)||null;const params={collection_name:this.collection,last_modified:parsedLastModified};return this._executeStatement(statements.saveLastModified,params).then(()=>parsedLastModified);}
getLastModified(){const params={collection_name:this.collection};return this._executeStatement(statements.getLastModified,params).then(result=>{if(result.length==0){return 0;}
return result[0].getResultByName("last_modified");});}}
exports.default=FirefoxAdapter;function transactionProxy(collection,preloaded){const _operations=[];return{get operations(){return _operations;},create(record){_operations.push({statement:statements.createData,params:{collection_name:collection,record_id:record.id,record:JSON.stringify(record)}});},update(record){_operations.push({statement:statements.updateData,params:{collection_name:collection,record_id:record.id,record:JSON.stringify(record)}});},delete(id){_operations.push({statement:statements.deleteData,params:{collection_name:collection,record_id:id}});},get(id){return id in preloaded?preloaded[id]:undefined;}};}},{"../src/adapters/base":6,"../src/utils":8}],2:[function(require,module,exports){"use strict";Object.defineProperty(exports,"__esModule",{value:true});exports.default=loadKinto;var _base=require("../src/adapters/base");var _base2=_interopRequireDefault(_base);var _KintoBase=require("../src/KintoBase");var _KintoBase2=_interopRequireDefault(_KintoBase);var _FirefoxStorage=require("./FirefoxStorage");var _FirefoxStorage2=_interopRequireDefault(_FirefoxStorage);var _utils=require("../src/utils");function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}
const{classes:Cc,interfaces:Ci,utils:Cu}=Components;function loadKinto(){const{EventEmitter}=Cu.import("resource://devtools/shared/event-emitter.js",{});const{generateUUID}=Cc["@mozilla.org/uuid-generator;1"].getService(Ci.nsIUUIDGenerator);const{KintoHttpClient}=Cu.import("resource://services-common/kinto-http-client.js");Cu.import("resource://gre/modules/Timer.jsm");Cu.importGlobalProperties(['fetch']);function makeIDSchema(){return{validate:_utils.RE_UUID.test.bind(_utils.RE_UUID),generate:function(){return generateUUID().toString().replace(/[{}]/g,"");}};}
class KintoFX extends _KintoBase2.default{static get adapters(){return{BaseAdapter:_base2.default,FirefoxAdapter:_FirefoxStorage2.default};}
constructor(options={}){const emitter={};EventEmitter.decorate(emitter);const defaults={events:emitter,ApiClass:KintoHttpClient};const expandedOptions=Object.assign(defaults,options);super(expandedOptions);}
collection(collName,options={}){const idSchema=makeIDSchema();const expandedOptions=Object.assign({idSchema},options);return super.collection(collName,expandedOptions);}}
return KintoFX;}
if(typeof module==="object"){module.exports=loadKinto;}},{"../src/KintoBase":5,"../src/adapters/base":6,"../src/utils":8,"./FirefoxStorage":1}],3:[function(require,module,exports){},{}],4:[function(require,module,exports){'use strict'
function isArguments(object){return Object.prototype.toString.call(object)==='[object Arguments]'}
function deeper(a,b){return deeper_(a,b,[],[])}
module.exports=deeper
try{deeper.fastEqual=require('buffertools').equals}catch(e){}
function deeper_(a,b,ca,cb){if(a===b){return true}else if(typeof a!=='object'||typeof b!=='object'){return false}else if(a===null||b===null){return false}else if(Buffer.isBuffer(a)&&Buffer.isBuffer(b)){if(a.equals){return a.equals(b)}else if(deeper.fastEqual){return deeper.fastEqual.call(a,b)}else{if(a.length!==b.length)return false
for(var i=0;i<a.length;i++)if(a[i]!==b[i])return false
return true}}else if(a instanceof Date&&b instanceof Date){return a.getTime()===b.getTime()}else if(a instanceof RegExp&&b instanceof RegExp){return a.source===b.source&&a.global===b.global&&a.multiline===b.multiline&&a.lastIndex===b.lastIndex&&a.ignoreCase===b.ignoreCase}else if(isArguments(a)||isArguments(b)){if(!(isArguments(a)&&isArguments(b)))return false
var slice=Array.prototype.slice
return deeper_(slice.call(a),slice.call(b),ca,cb)}else{if(a.constructor!==b.constructor)return false
var ka=Object.keys(a)
var kb=Object.keys(b) 
if(ka.length===0&&kb.length===0)return true
if(ka.length!==kb.length)return false
var cal=ca.length
while(cal--)if(ca[cal]===a)return cb[cal]===b
ca.push(a);cb.push(b)
ka.sort();kb.sort()
for(var j=ka.length-1;j>=0;j--)if(ka[j]!==kb[j])return false
var key
for(var k=ka.length-1;k>=0;k--){key=ka[k]
if(!deeper_(a[key],b[key],ca,cb))return false}
ca.pop();cb.pop()
return true}}},{"buffertools":3}],5:[function(require,module,exports){"use strict";Object.defineProperty(exports,"__esModule",{value:true});var _collection=require("./collection");var _collection2=_interopRequireDefault(_collection);var _base=require("./adapters/base");var _base2=_interopRequireDefault(_base);function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}
const DEFAULT_BUCKET_NAME="default";const DEFAULT_REMOTE="http://localhost:8888/v1";class KintoBase{static get adapters(){return{BaseAdapter:_base2.default};}
static get syncStrategy(){return _collection2.default.strategy;}
constructor(options={}){const defaults={bucket:DEFAULT_BUCKET_NAME,remote:DEFAULT_REMOTE};this._options=Object.assign(defaults,options);if(!this._options.adapter){throw new Error("No adapter provided");}
const{remote,events,headers,requestMode,ApiClass}=this._options;this._api=new ApiClass(remote,{events,headers,requestMode}); this.events=this._options.events;}
collection(collName,options={}){if(!collName){throw new Error("missing collection name");}
const bucket=this._options.bucket;return new _collection2.default(bucket,collName,this._api,{events:this._options.events,adapter:this._options.adapter,dbPrefix:this._options.dbPrefix,idSchema:options.idSchema,remoteTransformers:options.remoteTransformers,hooks:options.hooks});}}
exports.default=KintoBase;},{"./adapters/base":6,"./collection":7}],6:[function(require,module,exports){"use strict";Object.defineProperty(exports,"__esModule",{value:true});class BaseAdapter{open(){return Promise.resolve();}
close(){return Promise.resolve();}
clear(){throw new Error("Not Implemented.");}
execute(callback,options={preload:[]}){throw new Error("Not Implemented.");}
get(id){throw new Error("Not Implemented.");}
list(params={filters:{},order:""}){throw new Error("Not Implemented.");}
saveLastModified(lastModified){throw new Error("Not Implemented.");}
getLastModified(){throw new Error("Not Implemented.");}
loadDump(records){throw new Error("Not Implemented.");}}
exports.default=BaseAdapter;},{}],7:[function(require,module,exports){"use strict";Object.defineProperty(exports,"__esModule",{value:true});exports.SyncResultObject=undefined;exports.cleanRecord=cleanRecord;var _base=require("./adapters/base");var _base2=_interopRequireDefault(_base);var _utils=require("./utils");var _uuid=require("uuid");var _deeper=require("deeper");var _deeper2=_interopRequireDefault(_deeper);function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}
const RECORD_FIELDS_TO_CLEAN=["_status","last_modified"];const AVAILABLE_HOOKS=["incoming-changes"];function cleanRecord(record,excludeFields=RECORD_FIELDS_TO_CLEAN){return Object.keys(record).reduce((acc,key)=>{if(excludeFields.indexOf(key)===-1){acc[key]=record[key];}
return acc;},{});}
class SyncResultObject{static get defaults(){return{ok:true,lastModified:null,errors:[],created:[],updated:[],deleted:[],published:[],conflicts:[],skipped:[],resolved:[]};}
constructor(){this.ok=true;Object.assign(this,SyncResultObject.defaults);}
add(type,entries){if(!Array.isArray(this[type])){return;}
this[type]=this[type].concat(entries);this.ok=this.errors.length+this.conflicts.length===0;return this;}
reset(type){this[type]=SyncResultObject.defaults[type];this.ok=this.errors.length+this.conflicts.length===0;return this;}}
exports.SyncResultObject=SyncResultObject;function createUUIDSchema(){return{generate(){return(0,_uuid.v4)();},validate(id){return(0,_utils.isUUID)(id);}};}
function markStatus(record,status){return Object.assign({},record,{_status:status});}
function markDeleted(record){return markStatus(record,"deleted");}
function markSynced(record){return markStatus(record,"synced");}
function importChange(transaction,remote){const local=transaction.get(remote.id);if(!local){
if(remote.deleted){return{type:"skipped",data:remote};}
const synced=markSynced(remote);transaction.create(synced);return{type:"created",data:synced};}
const identical=(0,_deeper2.default)(cleanRecord(local),cleanRecord(remote));if(local._status!=="synced"){if(local._status==="deleted"){return{type:"skipped",data:local};}
if(identical){

const synced=markSynced(remote);transaction.update(synced);return{type:"updated",data:synced,previous:local};}
return{type:"conflicts",data:{type:"incoming",local:local,remote:remote}};}
if(remote.deleted){transaction.delete(remote.id);return{type:"deleted",data:{id:local.id}};}
const synced=markSynced(remote);transaction.update(synced); const type=identical?"void":"updated";return{type,data:synced};}
class Collection{constructor(bucket,name,api,options={}){this._bucket=bucket;this._name=name;this._lastModified=null;const DBAdapter=options.adapter;if(!DBAdapter){throw new Error("No adapter provided");}
const dbPrefix=options.dbPrefix||"";const db=new DBAdapter(`${ dbPrefix }${ bucket }/${ name }`);if(!(db instanceof _base2.default)){throw new Error("Unsupported adapter.");} 
this.db=db;this.api=api;this._apiCollection=this.api.bucket(this.bucket).collection(this.name);this.events=options.events;this.idSchema=this._validateIdSchema(options.idSchema);this.remoteTransformers=this._validateRemoteTransformers(options.remoteTransformers);this.hooks=this._validateHooks(options.hooks);}
get name(){return this._name;}
get bucket(){return this._bucket;}
get lastModified(){return this._lastModified;}
static get strategy(){return{CLIENT_WINS:"client_wins",SERVER_WINS:"server_wins",MANUAL:"manual"};}
_validateIdSchema(idSchema){if(typeof idSchema==="undefined"){return createUUIDSchema();}
if(typeof idSchema!=="object"){throw new Error("idSchema must be an object.");}else if(typeof idSchema.generate!=="function"){throw new Error("idSchema must provide a generate function.");}else if(typeof idSchema.validate!=="function"){throw new Error("idSchema must provide a validate function.");}
return idSchema;}
_validateRemoteTransformers(remoteTransformers){if(typeof remoteTransformers==="undefined"){return[];}
if(!Array.isArray(remoteTransformers)){throw new Error("remoteTransformers should be an array.");}
return remoteTransformers.map(transformer=>{if(typeof transformer!=="object"){throw new Error("A transformer must be an object.");}else if(typeof transformer.encode!=="function"){throw new Error("A transformer must provide an encode function.");}else if(typeof transformer.decode!=="function"){throw new Error("A transformer must provide a decode function.");}
return transformer;});}
_validateHook(hook){if(!Array.isArray(hook)){throw new Error("A hook definition should be an array of functions.");}
return hook.map(fn=>{if(typeof fn!=="function"){throw new Error("A hook definition should be an array of functions.");}
return fn;});}
_validateHooks(hooks){if(typeof hooks==="undefined"){return{};}
if(Array.isArray(hooks)){throw new Error("hooks should be an object, not an array.");}
if(typeof hooks!=="object"){throw new Error("hooks should be an object.");}
const validatedHooks={};for(let hook in hooks){if(AVAILABLE_HOOKS.indexOf(hook)===-1){throw new Error("The hook should be one of "+AVAILABLE_HOOKS.join(", "));}
validatedHooks[hook]=this._validateHook(hooks[hook]);}
return validatedHooks;}
clear(){return this.db.clear().then(_=>this.db.saveLastModified(null)).then(_=>({data:[],permissions:{}}));}
_encodeRecord(type,record){if(!this[`${ type }Transformers`].length){return Promise.resolve(record);}
return(0,_utils.waterfall)(this[`${ type }Transformers`].map(transformer=>{return record=>transformer.encode(record);}),record);}
_decodeRecord(type,record){if(!this[`${ type }Transformers`].length){return Promise.resolve(record);}
return(0,_utils.waterfall)(this[`${ type }Transformers`].reverse().map(transformer=>{return record=>transformer.decode(record);}),record);}
create(record,options={useRecordId:false,synced:false}){const reject=msg=>Promise.reject(new Error(msg));if(typeof record!=="object"){return reject("Record is not an object.");}
if((options.synced||options.useRecordId)&&!record.id){return reject("Missing required Id; synced and useRecordId options require one");}
if(!options.synced&&!options.useRecordId&&record.id){return reject("Extraneous Id; can't create a record having one set.");}
const newRecord=Object.assign({},record,{id:options.synced||options.useRecordId?record.id:this.idSchema.generate(),_status:options.synced?"synced":"created"});if(!this.idSchema.validate(newRecord.id)){return reject(`Invalid Id: ${ newRecord.id }`);}
return this.db.execute(transaction=>{transaction.create(newRecord);return{data:newRecord,permissions:{}};}).catch(err=>{if(options.useRecordId){throw new Error("Couldn't create record. It may have been virtually deleted.");}
throw err;});}
update(record,options={synced:false,patch:false}){if(typeof record!=="object"){return Promise.reject(new Error("Record is not an object."));}
if(!record.id){return Promise.reject(new Error("Cannot update a record missing id."));}
if(!this.idSchema.validate(record.id)){return Promise.reject(new Error(`Invalid Id: ${ record.id }`));}
return this.get(record.id).then(res=>{const existing=res.data;const newStatus=options.synced?"synced":"updated";return this.db.execute(transaction=>{const source=options.patch?Object.assign({},existing,record):record;const updated=markStatus(source,newStatus);if(existing.last_modified&&!updated.last_modified){updated.last_modified=existing.last_modified;}
transaction.update(updated);return{data:updated,permissions:{}};});});}
get(id,options={includeDeleted:false}){if(!this.idSchema.validate(id)){return Promise.reject(Error(`Invalid Id: ${ id }`));}
return this.db.get(id).then(record=>{if(!record||!options.includeDeleted&&record._status==="deleted"){throw new Error(`Record with id=${ id } not found.`);}else{return{data:record,permissions:{}};}});}
delete(id,options={virtual:true}){if(!this.idSchema.validate(id)){return Promise.reject(new Error(`Invalid Id: ${ id }`));}
return this.get(id,{includeDeleted:true}).then(res=>{const existing=res.data;return this.db.execute(transaction=>{if(options.virtual){transaction.update(markDeleted(existing));}else{transaction.delete(id);}
return{data:{id:id},permissions:{}};});});}
list(params={},options={includeDeleted:false}){params=Object.assign({order:"-last_modified",filters:{}},params);return this.db.list(params).then(results=>{let data=results;if(!options.includeDeleted){data=results.filter(record=>record._status!=="deleted");}
return{data,permissions:{}};});}
importChanges(syncResultObject,changeObject){return Promise.all(changeObject.changes.map(change=>{if(change.deleted){return Promise.resolve(change);}
return this._decodeRecord("remote",change);})).then(decodedChanges=>{if(decodedChanges.length===0){return Promise.resolve(syncResultObject);}
const remoteIds=decodedChanges.map(change=>change.id);return this.list({filters:{id:remoteIds},order:""},{includeDeleted:true}).then(res=>({decodedChanges,existingRecords:res.data})).then(({decodedChanges,existingRecords})=>{return this.db.execute(transaction=>{return decodedChanges.map(remote=>{return importChange(transaction,remote);});},{preload:existingRecords});}).catch(err=>{ err.type="incoming"; return[{type:"errors",data:err}];}).then(imports=>{for(let imported of imports){if(imported.type!=="void"){syncResultObject.add(imported.type,imported.data);}}
return syncResultObject;});}).then(syncResultObject=>{syncResultObject.lastModified=changeObject.lastModified; if(!syncResultObject.ok){return syncResultObject;} 
return this.db.saveLastModified(syncResultObject.lastModified).then(lastModified=>{this._lastModified=lastModified;return syncResultObject;});});}
resetSyncStatus(){let _count;return this.list({filters:{_status:["deleted","synced"]},order:""},{includeDeleted:true}).then(unsynced=>{return this.db.execute(transaction=>{_count=unsynced.data.length;unsynced.data.forEach(record=>{if(record._status==="deleted"){transaction.delete(record.id);}else{transaction.update(Object.assign({},record,{last_modified:undefined,_status:"created"}));}});});}).then(()=>this.db.saveLastModified(null)).then(()=>_count);}
gatherLocalChanges(){let _toDelete;return Promise.all([this.list({filters:{_status:["created","updated"]},order:""}),this.list({filters:{_status:"deleted"},order:""},{includeDeleted:true})]).then(([unsynced,deleted])=>{_toDelete=deleted.data;return Promise.all(unsynced.data.map(this._encodeRecord.bind(this,"remote")));}).then(toSync=>({toDelete:_toDelete,toSync}));}
pullChanges(syncResultObject,options={}){if(!syncResultObject.ok){return Promise.resolve(syncResultObject);}
options=Object.assign({strategy:Collection.strategy.MANUAL,lastModified:this.lastModified,headers:{}},options); return this._apiCollection.listRecords({since:options.lastModified||undefined,headers:options.headers}).then(({data,last_modified})=>{
const unquoted=last_modified?parseInt(last_modified.replace(/"/g,""),10):undefined;
const localSynced=options.lastModified;const serverChanged=unquoted>options.lastModified;const emptyCollection=data.length===0;if(localSynced&&serverChanged&&emptyCollection){throw Error("Server has been flushed.");}
const payload={lastModified:unquoted,changes:data};return this.applyHook("incoming-changes",payload);})
.then(changes=>this.importChanges(syncResultObject,changes))
.then(result=>this._handleConflicts(result,options.strategy));}
applyHook(hookName,payload){if(typeof this.hooks[hookName]=="undefined"){return Promise.resolve(payload);}
return(0,_utils.waterfall)(this.hooks[hookName].map(hook=>{return record=>hook(payload,this);}),payload);}
pushChanges(syncResultObject,options={}){if(!syncResultObject.ok){return Promise.resolve(syncResultObject);}
const safe=options.strategy===Collection.SERVER_WINS;options=Object.assign({safe},options); return this.gatherLocalChanges().then(({toDelete,toSync})=>{ return this._apiCollection.batch(batch=>{toDelete.forEach(r=>{ if(r.last_modified){batch.deleteRecord(r);}});toSync.forEach(r=>{const isCreated=r._status==="created";delete r._status;if(isCreated){batch.createRecord(r);}else{batch.updateRecord(r);}});},{headers:options.headers,safe:true,aggregate:true});})
.then(synced=>{ syncResultObject.add("errors",synced.errors.map(error=>{error.type="outgoing";return error;}));const conflicts=synced.conflicts.map(c=>{return{type:c.type,local:c.local.data,remote:c.remote};});const published=synced.published.map(c=>c.data);const skipped=synced.skipped.map(c=>c.data); syncResultObject.add("conflicts",conflicts); const missingRemotely=skipped.map(r=>Object.assign({},r,{deleted:true}));const toApplyLocally=published.concat(missingRemotely);

 const toDeleteLocally=toApplyLocally.filter(r=>r.deleted);const toUpdateLocally=toApplyLocally.filter(r=>!r.deleted); return Promise.all(toUpdateLocally.map(record=>{return this._decodeRecord("remote",record);}))
.then(results=>{return this.db.execute(transaction=>{const updated=results.map(record=>{const synced=markSynced(record);transaction.update(synced);return{data:synced};});const deleted=toDeleteLocally.map(record=>{transaction.delete(record.id); return{data:{id:record.id,deleted:true}};});return updated.concat(deleted);});}).then(published=>{syncResultObject.add("published",published.map(res=>res.data));return syncResultObject;});})
.then(result=>this._handleConflicts(result,options.strategy)).then(result=>{const resolvedUnsynced=result.resolved.filter(record=>record._status!=="synced"); if(resolvedUnsynced.length===0||options.resolved){return result;}else if(options.strategy===Collection.strategy.CLIENT_WINS&&!options.resolved){ return this.pushChanges(result,Object.assign({},options,{resolved:true}));}else if(options.strategy===Collection.strategy.SERVER_WINS){
return this.db.execute(transaction=>{resolvedUnsynced.forEach(record=>{transaction.update(markSynced(record));});return result;});}});}
resolve(conflict,resolution){return this.update(Object.assign({},resolution,{ last_modified:conflict.remote.last_modified}));}
_handleConflicts(result,strategy=Collection.strategy.MANUAL){if(strategy===Collection.strategy.MANUAL||result.conflicts.length===0){return Promise.resolve(result);}
return Promise.all(result.conflicts.map(conflict=>{const resolution=strategy===Collection.strategy.CLIENT_WINS?conflict.local:conflict.remote;return this.resolve(conflict,resolution);})).then(imports=>{return result.reset("conflicts").add("resolved",imports.map(res=>res.data));});}
sync(options={strategy:Collection.strategy.MANUAL,headers:{},ignoreBackoff:false,remote:null}){const previousRemote=this.api.remote;if(options.remote){this.api.remote=options.remote;}
if(!options.ignoreBackoff&&this.api.backoff>0){const seconds=Math.ceil(this.api.backoff/1000);return Promise.reject(new Error(`Server is asking clients to back off; retry in ${ seconds }s or use the ignoreBackoff option.`));}
const result=new SyncResultObject();const syncPromise=this.db.getLastModified().then(lastModified=>this._lastModified=lastModified).then(_=>this.pullChanges(result,options)).then(result=>this.pushChanges(result,options)).then(result=>{if(result.published.length===0){return result;}
return this.pullChanges(result,options);}); return(0,_utils.pFinally)(syncPromise,()=>this.api.remote=previousRemote);}
loadDump(records){const reject=msg=>Promise.reject(new Error(msg));if(!Array.isArray(records)){return reject("Records is not an array.");}
for(let record of records){if(!record.id||!this.idSchema.validate(record.id)){return reject("Record has invalid ID: "+JSON.stringify(record));}
if(!record.last_modified){return reject("Record has no last_modified value: "+JSON.stringify(record));}} 
return this.list({},{includeDeleted:true}).then(res=>{return res.data.reduce((acc,record)=>{acc[record.id]=record;return acc;},{});}).then(existingById=>{return records.filter(record=>{const localRecord=existingById[record.id];const shouldKeep=localRecord===undefined|| localRecord._status==="synced"&& localRecord.last_modified!==undefined&&record.last_modified>localRecord.last_modified;return shouldKeep;});}).then(newRecords=>newRecords.map(markSynced)).then(newRecords=>this.db.loadDump(newRecords));}}
exports.default=Collection;},{"./adapters/base":6,"./utils":8,"deeper":4,"uuid":3}],8:[function(require,module,exports){"use strict";Object.defineProperty(exports,"__esModule",{value:true});exports.sortObjects=sortObjects;exports.filterObjects=filterObjects;exports.reduceRecords=reduceRecords;exports.isUUID=isUUID;exports.waterfall=waterfall;exports.pFinally=pFinally;const RE_UUID=exports.RE_UUID=/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i;function _isUndefined(value){return typeof value==="undefined";}
function sortObjects(order,list){const hasDash=order[0]==="-";const field=hasDash?order.slice(1):order;const direction=hasDash?-1:1;return list.slice().sort((a,b)=>{if(a[field]&&_isUndefined(b[field])){return direction;}
if(b[field]&&_isUndefined(a[field])){return-direction;}
if(_isUndefined(a[field])&&_isUndefined(b[field])){return 0;}
return a[field]>b[field]?direction:-direction;});}
function filterObjects(filters,list){return list.filter(entry=>{return Object.keys(filters).every(filter=>{const value=filters[filter];if(Array.isArray(value)){return value.some(candidate=>candidate===entry[filter]);}
return entry[filter]===value;});});}
function reduceRecords(filters,order,list){const filtered=filters?filterObjects(filters,list):list;return order?sortObjects(order,filtered):filtered;}
function isUUID(uuid){return RE_UUID.test(uuid);}
function waterfall(fns,init){if(!fns.length){return Promise.resolve(init);}
return fns.reduce((promise,nextFn)=>{return promise.then(nextFn);},Promise.resolve(init));}
function pFinally(promise,fn){return promise.then(value=>Promise.resolve(fn()).then(()=>value),reason=>Promise.resolve(fn()).then(()=>{throw reason;}));}},{}]},{},[2])(2)});