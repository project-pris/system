"use strict";const DEBUG=false;function debug(s){if(DEBUG)dump(`-*- CustomizationConfigManager:  ${s}\n`);}
this.EXPORTED_SYMBOLS=["CustomizationConfigManager"];const{classes:Cc,interfaces:Ci,utils:Cu}=Components;Cu.import("resource://gre/modules/XPCOMUtils.jsm");Cu.import("resource://gre/modules/Services.jsm");Cu.import("resource://gre/modules/FileUtils.jsm");Cu.import("resource://gre/modules/AppsUtils.jsm");Cu.import("resource://gre/modules/CustomizationTable.jsm");XPCOMUtils.defineLazyServiceGetter(this,"gIccService","@mozilla.org/icc/iccservice;1","nsIIccService");const PREF_VARIANT_DIR="dom.customization.variant_dir";const PREF_VARIANT_VER="dom.customization.variant_ver";const MVNO_FILE_NAME="mvno.json";

const INDEX_FILE_NAME="fileIndex.json";const BUILT_IN_DIR="DefRt";const DEST_DIR_PARENT="customizationPDir";const DEST_DIR_NAME="config";

const CUSTOMIZATION_DIR_NAME="customization";this.CustomizationConfigManager={shouldReboot:false,cuzVer:null,cuzDir:null,cuzInfoMap:new Map(),cached:false,blacklist:null,init:function(){debug(`init`);CustomizationTable.getTable();},isImsiMatches:function(imsi,mvnoData){if(!imsi||!mvnoData||mvnoData.length>imsi.length){return false;}
for(let i=0;i<mvnoData.length;i++){let c=mvnoData[i];if((c!=="x")&&(c!=="X")&&(c!==imsi[i])){return false;}}
return true;},isGidMatches:function(aGid,mvnoData){if(!aGid||!mvnoData){return false;}
let gid=aGid.toLowerCase();let data=mvnoData.toLowerCase();return(gid.indexOf(data)===0);},isSpnMatches:function(aSpn,mvnoData){if(!aSpn||!mvnoData){return false;}
let spn=aSpn.toLowerCase();let data=mvnoData.toLowerCase();return(spn===data);},getVariant:function(baseDir,CustomDir,icc){if(!icc.iccInfo||!icc.iccInfo.mcc||!icc.iccInfo.mnc){return Promise.resolve(null);}
let variantDir=icc.iccInfo.mcc+icc.iccInfo.mnc;let mvnoFile=this.getFile(baseDir,[CustomDir,variantDir,MVNO_FILE_NAME]);if(!mvnoFile.exists()){debug(`load ${mvnoFile.path} failed`);return Promise.resolve(variantDir);}
return AppsUtils.loadJSONAsync(mvnoFile.path).then((mvnoContents)=>{if(!mvnoContents){return Promise.resolve(variantDir);}
let foundLevel=0;let matchVariant;
for(let mvno in mvnoContents){let mvnoContent=mvnoContents[mvno];let mvno_type=mvnoContent["mvno_type"];let mvno_match_data=mvnoContent["mvno_match_data"];let uid=mvnoContent["uid"];let matchFlag=false;if(mvno_type&&mvno_match_data){switch(mvno_type){case"spn":if(this.isSpnMatches(icc.iccInfo.spn,mvno_match_data)){matchFlag=true;}
break;case"imsi":if(this.isImsiMatches(icc.imsi,mvno_match_data)){matchFlag=true;}
break;case"gid":if(this.isGidMatches(icc.gid,mvno_match_data)){matchFlag=true;}
break;default:debug(`Unknown MVNO type? The mvno.json file had wrong mvno_type = ${mvno_type}`);break;}
if(matchFlag&&foundLevel<2){if(icc.uid){if(uid&&uid===icc.uid){foundLevel=3;matchVariant=mvno;debug(`matchLevel 3: find ${matchVariant}`);break;}}else{foundLevel=2;matchVariant=mvno;debug(`matchLevel 2: find ${matchVariant}`);break;}}}else if(!foundLevel){foundLevel=1;matchVariant=mvno;debug(`matchLevel 1: find ${matchVariant}`);}}
return Promise.resolve(matchVariant);});},applyVariant:function(aMatchInfo,aBlacklist){let icc;if(aMatchInfo){icc={iccInfo:{}};icc.iccInfo.mcc=aMatchInfo.mcc;icc.iccInfo.mnc=aMatchInfo.mnc;if(aMatchInfo.imsi){icc.imsi=aMatchInfo.imsi;}
if(aMatchInfo.spn){icc.iccInfo.spn=aMatchInfo.spn;}
if(aMatchInfo.gid){icc.gid=aMatchInfo.gid;}
if(aMatchInfo.uid){icc.uid=aMatchInfo.uid;}}else{ let clientId=0;icc=gIccService.getIccByServiceId(clientId);}
if(aBlacklist&&Array.isArray(aBlacklist)){this.blacklist=aBlacklist.slice();debug(`applyVariant blacklist [ ${this.blacklist} ]`);}else{this.blacklist=null;debug(`applyVariant null blacklist`);}
return this.applyBuiltInMatchVariant(icc);},applyBuiltInMatchVariant:function(icc){if(!icc){debug(`No icc parameter.`);return Promise.reject("No icc parameter.");}
this.cuzInfoMap.clear();this.cached=false;this.shouldReboot=false;return this.getVariant(BUILT_IN_DIR,CUSTOMIZATION_DIR_NAME,icc).then((variantDir)=>{if(!variantDir){debug(`Not find the matching variant directory.`);return Promise.reject("Not find the matching variant directory.");}
let indexFile=this.getFile(BUILT_IN_DIR,[CUSTOMIZATION_DIR_NAME,variantDir,INDEX_FILE_NAME]);if(!indexFile.exists()){debug(`Not find the mandatory file ${indexFile.path}.`);return Promise.reject("Not find the mandatory file.");}
return this.copyVariantToDest(BUILT_IN_DIR,CUSTOMIZATION_DIR_NAME,variantDir).then((result)=>{if(!result){return Promise.reject("Copy variant fail.");}
debug(`Copy variant success.`);return this.parseFileInArray(DEST_DIR_PARENT,[DEST_DIR_NAME],[INDEX_FILE_NAME]).then(()=>{let details={shouldReboot:this.shouldReboot?true:false};try{Services.prefs.setCharPref(PREF_VARIANT_DIR,this.cuzDir);Services.prefs.setCharPref(PREF_VARIANT_VER,this.cuzVer);Services.prefs.savePrefFile(null);}catch(e){debug(`Set prefs cuzDir and cuzVer caused error ${e}`);}
this.cached=true;debug(`Everything is done.`);return Promise.resolve(details);});});});},getFile:function(baseDir,pathArray){var file=FileUtils.getDir(baseDir,pathArray.slice(0,-1),false);file.append(pathArray[pathArray.length-1]);return file;},parseFileInArray:function(baseDir,currentPathArray,fileArray,cachedOnly){let promises=[];for(let i=0;i<fileArray.length;i++){let filePath=fileArray[i];let pathArray=[];pathArray.push.apply(pathArray,currentPathArray);pathArray.push.apply(pathArray,filePath.split("/"));let file=this.getFile(baseDir,pathArray);if(!file.exists()){debug(`load ${file.path} failed`);continue;}
promises.push(AppsUtils.loadJSONAsync(file.path).then((fileContent)=>{if(!fileContent){return;}
for(let key in fileContent){if(key==="content"){let cuzTable=fileContent["content"];for(let item in cuzTable){this.cuzInfoMap.set(item,cuzTable[item]);}}else{this.cuzInfoMap.set(key,fileContent[key]);}}
let nextPathArray=pathArray.slice(0,pathArray.length-1)
if(fileContent["content"].length){return this.parseFileInArray(baseDir,nextPathArray,fileContent["content"],cachedOnly);}else if(!cachedOnly){return this.applyCustomizatons(fileContent);}}));}
return Promise.all(promises);},copyVariantToDest:function(baseDir,customDir,variantDir){debug(`copy variant directory ${variantDir}`);let lastMatchedDir;let lastVersion;try{lastMatchedDir=Services.prefs.getCharPref(PREF_VARIANT_DIR);lastVersion=Services.prefs.getCharPref(PREF_VARIANT_VER);}catch(e){debug(`Get last match directory and version error! ${e}`);}
let srcDir=FileUtils.getDir(baseDir,[customDir,variantDir],false,true);if(!srcDir.exists()){debug(`Source variant directory is not exists.`);return Promise.resolve(false);}
let srcPathArray=[customDir,variantDir,INDEX_FILE_NAME];return this.getVariantInfo(baseDir,srcPathArray).then((srcInfo)=>{if(!srcInfo){return Promise.resolve(false);}

this.cuzVer=srcInfo.version;this.cuzDir=variantDir;let destDir=FileUtils.getDir(DEST_DIR_PARENT,[DEST_DIR_NAME],false,true);if(destDir.exists()){if(lastMatchedDir!==variantDir||srcInfo.version>lastVersion){try{destDir.remove(true);}catch(e){debug(`Out-of-date variant directory remove caused error! ${e.name}`);return Promise.resolve(false);}}else{debug(`Using the variant in the dest Dir.`);CustomizationTable.cuzPath=destDir.path;return Promise.resolve(true);}}
let destDirParent=FileUtils.getDir(DEST_DIR_PARENT,[""],false,true);if(!destDirParent.exists()){debug(`Dest dir parent is not exists.`);return Promise.resolve(false);}
 
try{srcDir.copyTo(destDirParent,DEST_DIR_NAME);}catch(e){debug(`Dir copy caused error! ${e.name}`);return Promise.resolve(false);}
destDir=FileUtils.getDir(DEST_DIR_PARENT,[DEST_DIR_NAME],false,true);if(destDir.exists()){CustomizationTable.cuzPath=destDir.path;}
return Promise.resolve(true);});},getVariantInfo:function(baseDir,pathArray){return new Promise((resolve)=>{let file=this.getFile(baseDir,pathArray);if(!file.exists()){debug(`load ${file.path} failed`);resolve(null);return;}
AppsUtils.loadJSONAsync(file.path).then((aData)=>{let fileContent=aData;if(!fileContent){resolve(null);}
resolve({"version":fileContent["version"]});});});},getValue:function(aKey){return new Promise((resolve,reject)=>{if(!this.cached){this.cuzInfoMap.clear();this.parseFileInArray(DEST_DIR_PARENT,[DEST_DIR_NAME],[INDEX_FILE_NAME],true).then(()=>{this.cached=true;resolve(this.cuzInfoMap.get(aKey));},()=>{reject("Parse file error, can not get the value.");});}else{resolve(this.cuzInfoMap.get(aKey));}});},isItemInBlacklist:function(item){if(!this.blacklist){return false;}
var length=this.blacklist.length;for(var i=0;i<length;i++){if(this.blacklist[i]===item){debug(`isItemInBlacklist for ${item} in blacklist is true`);return true;}} 
debug(`isItemInBlacklist for ${item} in blacklist is false`);return false;},applyCustomizatons:function(fileContent){let promises=[];let cuzTable=fileContent["content"];for(let item in cuzTable){ if(this.isItemInBlacklist(item)){continue;}
let matchItem=CustomizationTable.getItem(item);let handler=matchItem.handler;this.shouldReboot=this.shouldReboot||(matchItem.item&&matchItem.item.shouldReboot);let key=(matchItem.item&&matchItem.item.key)?matchItem.item.key:item;promises.push(CustomizationTable[handler](key,cuzTable[item]).then((key)=>{debug(`Apply ${key} success.`);},(key)=>{debug(`Apply ${key} error.`);}));}
return Promise.all(promises);}};this.CustomizationConfigManager.init();