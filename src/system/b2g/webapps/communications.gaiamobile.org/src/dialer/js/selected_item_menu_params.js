/* globals CallLog, MozActivity, OptionHelper, CallInfo, LazyLoader,
           NavigationMap, CallLogApp */
'use strict';
(function(exports) {
  var _showCallInfo = function() {
    LazyLoader.load(['/dialer/js/call_info.js',
                     '/dialer/style/call_info.css'], function() {
      var option = OptionHelper.option;
      CallInfo.show(option.phoneNumber, option.date, option.type, option.status, option.id);
      OptionHelper.CallInfoVisible = true;
      setTimeout(function() {
        NavigationMap.reset('focus');
      }, 300);
    });
  };
  var _showEditMode = function() {
    CallLog.showEditMode();
  };
  var _sendSms = function() {
    console.info("_sendSms");
    var option = OptionHelper.option;
    try {
      new MozActivity({
        name: 'new',
        data: {
          type: 'websms/sms',
          number: option.phoneNumber
        }
      });
    } catch (e) {
      console.error('Error while creating activity: ' + e);
    }
  };
  var launchActivity = function(name, phoneNumber) {
    phoneNumber = phoneNumber || OptionHelper.option.phoneNumber;
    try {
      var activity = new MozActivity({
        name: name,
        data: {
          type: 'webcontacts/contact',
          params: {
            'tel': phoneNumber
          }
        }
      });
      activity.onsuccess = function() {
        CallLog.renderOptionMenu();
      };
    } catch (e) {
      console.error('Error while creating activity');
    }
  };
  var _createNewContact = function () {
    launchActivity('new');
  };
  var _addToExistingContact = function () {
    launchActivity('update');
  };

  let dialOption = {
    name: 'Call',
    l10nId: 'call',
    iconMixedText: true,
    icon: null,
    priority: 2,
    method: () => {
      navigator.hasFeature('device.capability.vilte').then((hasVT) => {
        CallMenu.dial(OptionHelper.defaultServiceId, null, !hasVT);
      });
    }
  };

  // determine sim chooser dial icon
  if (window.navigator.mozMobileConnections.length > 1) {
    SettingsListener.observe('ril.telephony.defaultServiceId', 0, (index) => {
      OptionHelper.defaultServiceId = index;
      if (index >= 0 ) {
        dialOption.icon = `sim-${index + 1}`;
      } else {
        dialOption.icon = null;
      }
    });
  }

  var selectedItemTemplate = {
    header: { l10nId:'tcl-options' },
    items: [{
      name: '',
      l10nId: '',
      priority: 1,
      method: function() {
      }
    }, dialOption,
    {
      l10nId: 'callInformation',
      name: 'Call Information',
      priority: 4,
      method: _showCallInfo
    }, {
      name: 'Send Message',
      l10nId: 'sendSms',
      priority: 5,
      method: _sendSms
    }]
  };

  let selectedItem = Object.assign({}, selectedItemTemplate);
  selectedItem.items = selectedItemTemplate.items.concat(
    {
      name: 'Call Log Edit',
      l10nId: 'tcl-option-menu-call-log-edit',
      priority: 6,
      method: _showEditMode
    }
  );

  let selectedItemForNew = Object.assign({}, selectedItemTemplate);
  selectedItemForNew.items = selectedItemTemplate.items.concat(
    {
      l10nId: 'createNewContact',
      name: 'Create new Contact',
      priority: 6,
      method: _createNewContact
    }, {
      l10nId: 'addToExistingContact',
      name: 'Add to existing Contact',
      priority: 7,
      method: _addToExistingContact
    }, {
      name: 'Call Log Edit',
      l10nId: 'tcl-option-menu-call-log-edit',
      priority: 8,
      method: _showEditMode
    });

  const chooserItem = {
    header: { l10nId:'tcl-options' },
    items: [{
      name: 'Select',
      l10nId: 'select',
      priority: 2
    }]
  };

  OptionHelper.optionParams["log-item"] = selectedItem;
  OptionHelper.optionParams["log-item-new"] = selectedItemForNew;
  OptionHelper.optionParams["missed-call"] = selectedItem;
  OptionHelper.optionParams["dialed-call"] = selectedItem;
  OptionHelper.optionParams["received-call"] = selectedItem;
  OptionHelper.optionParams["chooser"] = chooserItem;
})(window);
