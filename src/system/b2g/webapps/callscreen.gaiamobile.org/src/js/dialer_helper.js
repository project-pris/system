'use strict';var dialerHelper=(function(){var dailer=document.getElementById('dialer-helper');var input=document.getElementById('dialer-number-input');var inputContainer=document.getElementById('input-container');var centerButton;var phoneNumber='';var acceptKey={'1':'1','2':'2','3':'3','4':'4','5':'5','6':'6','7':'7','8':'8','9':'9','*':'*','0':'0','#':'#'};var SPECIAL_CHARS=['*','+',','];var lastKey=null;var lastInputTime=0;window.addEventListener('keydown',handleKeyDown);function showDialerHelper(){phoneNumber='';KeypadManager.disableKeyPad();dailer.classList.add('display');centerButton=document.getElementById('software-keys-center');updateView();}
function hideDialerHelper(){KeypadManager.enableKeyPad();centerButton.style.visibility='visible';dailer.classList.remove('display');CallScreenHelper.render();}
function isDialerHelperShown(){return dailer.classList.contains('display');}
function callNumber(){if(phoneNumber.length!==0){var serviceId=CallsHandler.serviceId;window.navigator.mozTelephony.dial(phoneNumber,serviceId);hideDialerHelper();}}
function handleKeyDown(event){if(!isDialerHelperShown()){return;}
var key=event.key;if(key==='Call'||key==='Shift'){callNumber();}
else if(key==='Backspace'&&CallScreen.isEndCallKeyExisted()){if(phoneNumber.length!==0){phoneNumber=phoneNumber.substring(0,phoneNumber.length-1);updateView();}
else{hideDialerHelper();}}
else{if('*'===key){var currentTime=new Date();if('*'===lastKey&&currentTime-lastInputTime<1000){phoneNumber=phoneNumber.substring(0,phoneNumber.length-1)+getSpecialChars();}else{phoneNumber+=!acceptKey[key]?'':acceptKey[key];}
lastInputTime=currentTime;}else{phoneNumber+=!acceptKey[key]?'':acceptKey[key];}
lastKey=key;updateView();}}
function updateView(){input.textContent=phoneNumber;FontSizeManager.adaptToSpace(FontSizeManager.SINGLE_CALL,input,false,'begin');centerButton.style.visibility=phoneNumber.length?'visible':'hidden';}
function getSpecialChars(){var lastCharIndex=SPECIAL_CHARS.indexOf(phoneNumber[phoneNumber.length-1]);var newChar=SPECIAL_CHARS[(lastCharIndex+1)%SPECIAL_CHARS.length];return newChar;}
function clearPhoneNumber(){if(phoneNumber.length!==0){phoneNumber=phoneNumber.substring(0,phoneNumber.length-1);updateView();}
else{hideDialerHelper();}}
return{showDialerHelper:showDialerHelper,isDialerHelperShown:isDialerHelperShown,hideDialerHelper:hideDialerHelper,callNumber:callNumber,clearPhoneNumber:clearPhoneNumber};})();