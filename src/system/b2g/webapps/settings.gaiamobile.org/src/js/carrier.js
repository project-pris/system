/* -*- Mode: js; js-indent-level: 2; indent-tabs-mode: nil -*- */
/* vim: set shiftwidth=2 tabstop=2 autoindent cindent expandtab: */

//
define(['require','shared/settings_listener','shared/toaster','shared/settings_helper','shared/template','shared/simslot_manager'],function(require) {
  

  var SettingsListener = require('shared/settings_listener');
  var Toaster = require('shared/toaster');
  var SettingsHelper = require('shared/settings_helper');
  var Template = require('shared/template');
  var SIMSlotManager = require('shared/simslot_manager');
  /**
   * Singleton object that handles some cell and data settings.
   */
  var CarrierSettings = function cs_carriersettings() {
    var DATA_KEY = 'ril.data.enabled';
    var DATA_ROAMING_KEY = 'ril.data.roaming_enabled';
    var NETWORK_TYPE_SETTING = 'operatorResources.data.icon';
    var networkTypeMapping = {};
    var dataConnectionInit = false;
    var dataRoamingOberverInit = false;
    var dataConnectionSelect = document.querySelector('select.dataConnection-select');
    var dataConnectionDescElement = document.querySelector('small#mobilenetworkanddata-desc');
    var dataRoamingDescElement = document.querySelector('small#data-roaming-desc');
    var roamingPreferenceItem = document.getElementById('operator-roaming-preference');
    var operatorHeader = document.querySelector('#carrier-operatorSettings gaia-header h1');

    var _networkTypeCategory = {
      'gprs': 'gsm',
      'edge': 'gsm',
      'umts': 'gsm',
      'hsdpa': 'gsm',
      'hsupa': 'gsm',
      'hspa': 'gsm',
      'hspa+': 'gsm',
      'lte': 'gsm',
      'gsm': 'gsm',
      'is95a': 'cdma',
      'is95b': 'cdma',
      '1xrtt': 'cdma',
      'evdo0': 'cdma',
      'evdoa': 'cdma',
      'evdob': 'cdma',
      'ehrpd': 'cdma'
    };

    var _;
    var _settings;
    var _mobileConnections;
    var _iccManager;
    var _voiceTypes;

    /** mozMobileConnection instance the panel settings rely on */
    var _mobileConnection = null;
    /** Flag */
    var _restartingDataConnection = false;

    var gOperatorNetworkList = null;
    var networkMode;
    var l10n = window.navigator.mozL10n;

    /**
     * Init function.
     */
    function cs_init() {
      _settings = window.navigator.mozSettings;
      _mobileConnections = window.navigator.mozMobileConnections;
      _iccManager = window.navigator.mozIccManager;
      if (!_settings || !_mobileConnections || !_iccManager) {
        return;
      }
      window.navigator.mozSettings.addObserver('ril.data.defaultServiceId',
        function (value) {
          for (let i = 0; i < _mobileConnections.length; i++) {
            getSupportedNetworkInfo(_mobileConnections[i], (result) => {
              cs_updateNetworkTypeSelector(result);
              cs_updateAutomaticOperatorSelection();
            });
          };
        }
      );

      // init observer for data connection
      SettingsListener.observe(DATA_KEY, false, function (value) {
        dataConnectionDescElement.setAttribute('data-l10n-id', value ? 'on' : 'off');
        if (dataConnectionInit) {
          var toast = {
            messageL10nId: 'changessaved',
            latency: 2000,
            useTransition: true
          };
          Toaster.showToast(toast);
        } else {
          dataConnectionInit = true;
          dataConnectionSelect.value = value ? true : false;
        }
      });

      _voiceTypes = Array.prototype.map.call(_mobileConnections,
        function () {
          return null;
        });

      // Get the mozMobileConnection instace for this ICC card.
      _mobileConnection = _mobileConnections[
        DsdsSettings.getIccCardIndexForCellAndDataSettings()];
      if (!_mobileConnection) {
        return;
      }

      cs_addVoiceTypeChangeListeners();
      cs_updateNetworkTypeLimitedItemsVisibility(_mobileConnection);

      // Show carrier name.
      cs_showCarrierName();
      cs_initIccsUI();

      // Init network type selector.
      cs_initNetworkTypeText(cs_initNetworkTypeSelector());
      cs_initOperatorSelector();
      cs_initRoamingPreferenceSelector();

      window.addEventListener('panelready', function (e) {
        var selector = document.getElementById('preferredNetworkType');
        var opAutoSelectOptions = document.getElementById('auto-select-options');
        selector.value = '';
        opAutoSelectOptions.value = '';
        // Get the mozMobileConnection instace for this ICC card.
        _mobileConnection = _mobileConnections[
          DsdsSettings.getIccCardIndexForCellAndDataSettings()];
        if (!_mobileConnection) {
          return;
        }

        var currentHash = e.detail.current;
        if (currentHash === '#carrier') {
          cs_updateNetworkTypeLimitedItemsVisibility(_mobileConnection);
          // Show carrier name.
          cs_showCarrierName();
          cs_updateRoamingPreferenceSelector();
          return;
        }

        if (currentHash === '#apn-settings') {
          if (DsdsSettings.getNumberOfIccSlots() > 1) {
            var apnHeader = document.querySelector('#apn-settings gaia-header h1');
            // Modified by yingsen.zhang@t2mbole.com for sync SIM card name 2018.01.02 begin
            updateSIMCardName(DsdsSettings.getIccCardIndexForCellAndDataSettings(),
              function(cardindex, cardName) {
                if (cardName && cardName.length !== 0) {
                  apnHeader.textContent = l10n.get('apnSettings') + ' - ' + cardName;
                }
            });
            // Modified by yingsen.zhang@t2mbole.com for sync SIM card name 2018.01.02 end
            return;
          } else {
            return;
          }
        } else if (!currentHash.startsWith('#carrier-')) {
          return;
        }

        if (currentHash === '#carrier-operatorSettings') {
          if (DsdsSettings.getNumberOfIccSlots() > 1) {
            // Modified by yingsen.zhang@t2mbole.com for sync SIM card name 2018.01.02 begin
            updateSIMCardName(DsdsSettings.getIccCardIndexForCellAndDataSettings(),
              function(cardindex, cardName) {
                if (cardName && cardName.length !== 0) {
                  operatorHeader.textContent = l10n.get('dataNetwork') + ' - ' + cardName;
                }
            });
            // Modified by yingsen.zhang@t2mbole.com for sync SIM card name 2018.01.02 end
          }

          cs_updateAutomaticOperatorSelection();
          getSupportedNetworkInfo(_mobileConnection, function (result) {
            cs_updateNetworkTypeSelector(result);
          });
          return;
        }
      });

      // Modified by yingsen.zhang@t2mbole.com 2018.01.02 begin
      // Modified for listen SIM card name changed event
      window.addEventListener('cardnamechanged', function(e) {
        var apnHeader = document.querySelector('#apn-settings gaia-header h1');
        updateSIMCardName(e.detail.cardIndex, function(cardIndex, cardName) {
          if (cardName && cardName.length !== 0) {
            apnHeader.textContent = l10n.get('apnSettings') + ' - ' + cardName;
            operatorHeader.textContent = l10n.get('dataNetwork') + ' - ' + cardName;
          }
        });
      });
      // Modified by yingsen.zhang@t2mbole.com 2018.01.02 end

      // Modified for task 5346471/5110457 by yingsen.zhang@t2mobile.com begin
      // Observe data roaming key according to current customization value
      navigator.customization.getValue('stz.roaming.domestic.enable').then((result) => {
        var customized = (result === 'undefined') ? false : result;
        console.log('[carrier] read data roaming customized value: ' + customized);
        navigator.mozSettings.createLock().set({'data.roaming.domestic.customized' : customized});

        document.getElementById('menuItem-dataRoaming').href = customized
                ? '#data-roaming-customization' : '#data-roaming';
        var data_roaming_key = customized
                ? 'data.roaming.domestic_international.enabled'
                : DATA_ROAMING_KEY;

        // init observer for data roaming
        SettingsListener.observe(data_roaming_key, 0, function(value) {
          console.log('[carrier] listen \'' + data_roaming_key + '\' value: ' + value);
          cs_resetDataRoamingConfiguration(value, customized);
          if (dataRoamingOberverInit) {
            var toast = {
              messageL10nId: 'changessaved',
              latency: 2000,
              useTransition: true
            };
            Toaster.showToast(toast);
          } else {
            dataRoamingOberverInit = true;
          }
       });
      });
      // Modified for task 5346471/5110457 by yingsen.zhang@t2mobile.com end
    }

    function isNeedToUpdateOperator(mcc,mnc){
      if (!mcc || !mnc) {
        return false;
      }
        console.log("curent mcc = "+mcc+" ; mnc ="+mnc);
      if (mcc == '260' && mnc == '06') {
        return true;
      } else if(mcc == '260'){
          return true;
      } else{
        return false;
      }
    }
    //add by zhaobing.tuo@tcl.com for task 5157262 end

  //task5574025 bin-shen@t2mobile.com begin
  function isNeedUse4G3G2GAutoDescription(mcc,mnc){
    if (!mcc || !mnc) {
      return false;
    }
      console.log("curent mcc = "+mcc+" ; mnc ="+mnc);
    if ((mcc == '222' && mnc == '10')
        || (mcc == '214' && mnc == '01')
        || (mcc == '234' && mnc == '15')
        || (mcc == '262' && mnc == '02')) {
      return true;
    } else{
      return false;
    }
  }

  function isNeedUse3G2GAutoDescription(mcc,mnc){
    if (!mcc || !mnc) {
      return false;
    }
      console.log("curent mcc = "+mcc+" ; mnc ="+mnc);
    if ((mcc == '222' && mnc == '10')
        || (mcc == '214' && mnc == '01')
        || (mcc == '234' && mnc == '15')
        || (mcc == '262' && mnc == '02')) {
      return true;
    } else{
      return false;
    }
  }

  function isNeedUse4G3G2GRecommendedDescription(mcc,mnc){
    if (!mcc || !mnc) {
      return false;
    }
      console.log("curent mcc = "+mcc+" ; mnc ="+mnc);
    if ((mcc == '204' && mnc == '04')
        || (mcc == '216' && mnc == '70')) {
      return true;
    } else{
      return false;
    }
  }

  function isNeedUse3G2GDescription(mcc,mnc){
    if (!mcc || !mnc) {
      return false;
    }
      console.log("curent mcc = "+mcc+" ; mnc ="+mnc);
    if ((mcc == '204' && mnc == '04')
        || (mcc == '216' && mnc == '70')) {
      return true;
    } else{
      return false;
    }
  }
  //task5574025 bin-shen@t2mobile.com end


  // Modified for task 5346471/5110457 by yingsen.zhang@t2mobile.com begin
  // Display current roaming type according to current customization value
  // reset some params related to data roaming after we change its status
  function cs_resetDataRoamingConfiguration(value, customized) {
    var enabled = customized ? value : ((value === 0) ? false : true);

    var iccId = DsdsSettings.getIccCardIndexForCellAndDataSettings();
    _mobileConnection = _mobileConnections[iccId];
    var voiceType = _mobileConnection.voice && _mobileConnection.voice.type;
    if ((_networkTypeCategory[voiceType] === 'cdma') && enabled) {
      roamingPreferenceItem.hidden = false;
    } else {
      roamingPreferenceItem.hidden = true;
    }

    if (customized) {
      if (value === 2) {
        dataRoamingDescElement.setAttribute('data-l10n-id', 'dataRoamingInternational');
      } else if (value === 1) {
        dataRoamingDescElement.setAttribute('data-l10n-id', 'dataRoamingDomestic');
      } else if (value === 0) {
        dataRoamingDescElement.setAttribute('data-l10n-id', 'dataRoamingNotAllowed');
      }
      navigator.mozSettings.createLock().set({'ril.data.roaming.customized_value' : value});
    } else {
      if (value) {
        dataRoamingDescElement.setAttribute('data-l10n-id', 'on');
      } else {
        dataRoamingDescElement.setAttribute('data-l10n-id', 'off');
      }
    }
  }
  // Modified for task 5346471/5110457 by yingsen.zhang@t2mobile.com end

    // reset some params related to data roaming after we change its status
   /* function cs_resetDataRoamingConfiguration(enabled) {
      var iccId = DsdsSettings.getIccCardIndexForCellAndDataSettings();
      _mobileConnection = _mobileConnections[iccId];
      var voiceType = _mobileConnection.voice && _mobileConnection.voice.type;
      if ((_networkTypeCategory[voiceType] === 'cdma') && enabled) {
        roamingPreferenceItem.hidden = false;
      } else {
        roamingPreferenceItem.hidden = true;
      }
      if (enabled) {
        dataRoamingDescElement.setAttribute('data-l10n-id', 'on');
      } else {
        dataRoamingDescElement.setAttribute('data-l10n-id', 'off');
      }
    }*/

    function cs_initIccsUI() {
      var isMultiSim = DsdsSettings.getNumberOfIccSlots() > 1;
      var carrierInfo = document.querySelector('#carrier .carrier-info');
      var advancedSettings =
        document.querySelector('#carrier .carrier-advancedSettings');
      var simSettings =
        document.querySelector('#carrier .carrier-settings');
      var apnSettings =
        document.querySelector('#carrier .apn-settings');
      var operatorSettingsHeader =
        document.querySelector('#carrier-operatorSettings gaia-header');

      if (isMultiSim) {
        LazyLoader.load([
          '/js/carrier_iccs.js'
        ], function () {
          IccHandlerForCarrierSettings.init();
        });
      }

      operatorSettingsHeader.dataset.href = '#carrier';
      carrierInfo.hidden = isMultiSim;
      advancedSettings.hidden = isMultiSim;
      simSettings.hidden = !isMultiSim;
      apnSettings.hidden = !isMultiSim;
    }

    function cs_initDataToggles() {
      var dataToggle = document.querySelector('#menuItem-enableDataCall input');
      var dataRoamingToggle =
        document.querySelector('#menuItem-enableDataRoaming input');

      function updateDataRoamingToggle(dataEnabled) {
        if (dataEnabled) {
          dataRoamingToggle.disabled = false;
        } else {
          dataRoamingToggle.disabled = true;
          dataRoamingToggle.checked = false;
          dataRoamingToggle.dispatchEvent(new Event('change'));
        }
      }

      function getDataEnabled() {
        return new Promise(function (resolve, reject) {
          var transaction = _settings.createLock();
          var req = transaction.get(DATA_KEY);
          req.onsuccess = function () {
            resolve(req.result[DATA_KEY]);
          };
          req.onerror = function () {
            resolve(false);
          };
        });
      }

      getDataEnabled().then(function (dataEnabled) {
        updateDataRoamingToggle(dataEnabled);
      });
      // We need to disable data roaming when data connection is disabled.
      _settings.addObserver(DATA_KEY, function observerCb(event) {
        dataToggle.checked = event.settingValue;
        if (_restartingDataConnection) {
          return;
        }
        updateDataRoamingToggle(event.settingValue);
      });

      // Init warnings the user sees before enabling data calls and roaming.
      // The function also registers handlers for the changes of the toggles.
      cs_initWarning(DATA_KEY,
        dataToggle,
        'dataConnection-warning-head',
        'dataConnection-warning-message',
        'dataConnection-expl');
      cs_initWarning(DATA_ROAMING_KEY,
        dataRoamingToggle,
        'dataRoaming-warning-head',
        'dataRoaming-warning-message',
        'dataRoaming-expl');
    }

    /**
     * Add listeners on 'voicechange' for show/hide network type limited items.
     */
    function cs_addVoiceTypeChangeListeners() {
      Array.prototype.forEach.call(_mobileConnections, function (conn, index) {
        _voiceTypes[index] = conn.voice.type;
        conn.addEventListener('voicechange', function () {
          var voiceType = conn.voice && conn.voice.type;
          var voiceTypeChange = voiceType !== _voiceTypes[index];

          _voiceTypes[index] = voiceType;
          if (index === DsdsSettings.getIccCardIndexForCellAndDataSettings() &&
            voiceTypeChange) {
            cs_updateNetworkTypeLimitedItemsVisibility(conn);
          }
        });
      });
    }

    /**
     * Update the network type limited items' visibility based on the voice type.
     */
    function cs_updateNetworkTypeLimitedItemsVisibility(conn) {
      // The following features are limited to GSM types.
      var autoSelectOperatorItem = document.getElementById('operator-autoSelect');
      var availableOperatorsHeader =
        document.getElementById('availableOperatorsHeader');
      var availableOperators = document.getElementById('availableOperators');

      var voiceType = conn.voice && conn.voice.type;

      function doUpdate(mode) {
        networkMode = mode;
        autoSelectOperatorItem.hidden = availableOperatorsHeader.hidden =
          availableOperators.hidden = (mode !== 'gsm');
      }
      if (!voiceType) {
        getSupportedNetworkInfo(conn, function (result) {
          if (result.gsm || result.wcdma || result.lte) {
            doUpdate('gsm');
          } else {
            doUpdate('cdma');
          }
        });
      } else {
        doUpdate(_networkTypeCategory[voiceType]);
      }
  }

    /**
     * Show the carrier name in the ICC card.
     */
    function cs_showCarrierName() {
      if (DsdsSettings.getNumberOfIccSlots() > 1) {
        return;
      }
      var desc = document.getElementById('dataNetwork-desc');
      var iccCard = _iccManager.getIccById(_mobileConnection.iccId);
      var network = _mobileConnection.voice.network;
      var iccInfo = iccCard.iccInfo;
      var carrier = network ? (network.shortName || network.longName) : null;

      if (carrier && iccInfo && iccInfo.isDisplaySpnRequired && iccInfo.spn) {
        if (iccInfo.isDisplayNetworkNameRequired && carrier !== iccInfo.spn) {
          carrier = carrier + ' ' + iccInfo.spn;
        } else {
          carrier = iccInfo.spn;
        }
      }

      if (!carrier) {
        desc.setAttribute('data-l10n-id', 'no-operator');
      } else {
        desc.textContent = carrier;
      }
    }

    /**
     * Helper function. Get the value for the ril.data.defaultServiceId setting
     * from the setting database.
     *
     * @param {Function} callback Callback function to be called once the work is
     *                            done.
     */
    function cs_getDefaultServiceIdForData(callback) {
      var request = _settings.createLock().get('ril.data.defaultServiceId');
      request.onsuccess = function onSuccessHandler() {
        var defaultServiceId =
          parseInt(request.result['ril.data.defaultServiceId'], 10);
        if (callback) {
          callback(defaultServiceId);
        }
      };
    }

    function cs_initNetworkTypeText(aNext) {
      var req;
      try {
        networkTypeMapping = {};
        req = _settings.createLock().get(NETWORK_TYPE_SETTING);
        req.onsuccess = function () {
          var networkTypeValues = req.result[NETWORK_TYPE_SETTING] || {};
          for (var key in networkTypeValues) {
            networkTypeMapping[key] = networkTypeValues[key];
          }
          aNext && aNext();
        };
        req.onerror = function () {
          console.error('Error loading ' + NETWORK_TYPE_SETTING + ' settings. ' +
          req.error && req.error.name);
          aNext && aNext();
        };
      } catch (e) {
        console.error('Error loading ' + NETWORK_TYPE_SETTING + ' settings. ' +
        e);
        aNext && aNext();
      }
    }

    /**
     * Init network type selector. Add the event listener that handles the changes
     * for the network type.
     */

    function cs_initNetworkTypeSelector() {
      if (!_mobileConnection.setPreferredNetworkType)
        return;

      var alertDialog = document.getElementById('preferredNetworkTypeAlert');
      var message = document.getElementById('preferredNetworkTypeAlertMessage');

      var preferredNetworkTypeHelper =
        SettingsHelper('ril.radio.preferredNetworkType');
      var selector = document.getElementById('preferredNetworkType');
      selector.addEventListener('blur', function evenHandler() {
        var targetIndex = DsdsSettings.getIccCardIndexForCellAndDataSettings();
        var type = selector.value;
        var request = _mobileConnection.setPreferredNetworkType(type);

        request.onsuccess = function onSuccessHandler() {
          preferredNetworkTypeHelper.get(function gotPNT(values) {
            values[targetIndex] = type;
            preferredNetworkTypeHelper.set(values);
           //task5157262-add by deming.wu-start
            navigator.customization.getValue('disable.data.2gonly').then((result) => {
            console.log("select netyworktype = " + type+" ; isO2UKOperator = "+JSON.stringify(result));
            if(type === 'gsm' && `${JSON.stringify(result)}` == 'true'){
                navigator.mozSettings.createLock().set({'ril.data.enabled' : false});
            }
          });
            //task5157262-add by deming.wu-end
            var toast = {
              messageL10nId: 'changessaved',
              useTransition: true
            };
            Toaster.showToast(toast);
          });
        };
        request.onerror = function onErrorHandler() {
          // XXX, we don't show error dialog now
        };
      });

      window.addEventListener('keydown', evt => {
        if (!alertDialog.hidden && evt.key === 'Enter') {
          alertDialog.hidden = true;
          getSupportedNetworkInfo(_mobileConnection, result => {
            cs_updateNetworkTypeSelector(result);
            cs_updateAutomaticOperatorSelection();
          });
        }

         if (evt.key === 'Backspace') {
           var mode = _mobileConnection.networkSelectionMode;
           if (mode === 'automatic') {
             gOperatorNetworkList.stop();
           }
         }
      });
    }

    /**
     * Update network type selector.
     */
    function cs_updateNetworkTypeSelector(supportedNetworkTypeResult) {
      if (!_mobileConnection.getPreferredNetworkType || !supportedNetworkTypeResult.networkTypes) {
        return;
      }

      var selector = document.getElementById('preferredNetworkType');
      // Clean up all option before updating again.
      while (selector.hasChildNodes()) {
        selector.removeChild(selector.lastChild);
      }

      var request = _mobileConnection.getPreferredNetworkType();
      request.onsuccess = function onSuccessHandler() {
        _addNetworkTypes(request.result);
      };
      request.onerror = function onErrorHandler() {
        console.warn('carrier: could not retrieve network type');
        // XXX, add 'lte' when we can't get preferred network types
        _addNetworkTypes('lte');
      };

      /*function _addNetworkTypes(networkType) {
        var supportedNetworkTypes = supportedNetworkTypeResult.networkTypes;
        if (networkType) {
          supportedNetworkTypes.forEach(function (type) {
            var option = document.createElement('option');
            option.value = type;
            option.selected = (networkType === type);
            // show user friendly network mode names
            if (type in networkTypeMapping) {
              option.text = networkTypeMapping[type];
            } else {
              var l10nId = supportedNetworkTypeResult.l10nIdForType(type);
              option.setAttribute('data-l10n-id', l10nId);
              // fallback to the network type
              if (!l10nId) {
                option.textContent = type;
              }
            }
            selector.appendChild(option);
          });
        } else {
          console.warn('carrier: could not retrieve network type');
        }
      }*/
    //bin.shen add for task5306222 begin
    function _addNetworkTypes(networkType) {
      var supportedNetworkTypes;
      //var request=navigator.mozSettings.createLock().get('preffered.network.type.list');
      try {
         navigator.customization.getValue('preffered.network.type.list').then((result) => {
         var prefferedNetworkTypeList = `${JSON.stringify(result)}`;

      //request.onsuccess=function onGetTypeSuccessHandler() {
      //  var prefferedNetworkTypeList = request.result['preffered.network.type.list'];
        console.log("carrier.js getPrefferedTypelist=" + prefferedNetworkTypeList
          + " networkType = " + networkType);

        switch (prefferedNetworkTypeList) {
          case '0':
             supportedNetworkTypes = supportedNetworkTypeResult.networkTypes_0;
             break;
          case '1':
            supportedNetworkTypes = supportedNetworkTypeResult.networkTypes_1;
            break;
          case '2':
            supportedNetworkTypes = supportedNetworkTypeResult.networkTypes_2;
            break;
          case '3':
            supportedNetworkTypes = supportedNetworkTypeResult.networkTypes_3;
            break;
            case '4':
                supportedNetworkTypes = supportedNetworkTypeResult.networkTypes_4;
                break;
          default:
            supportedNetworkTypes = supportedNetworkTypeResult.networkTypes_0;
            break;
        }
        console.log("carrier.js start _addNetworkTypes_me");
        //_addNetworkTypes_me(networkType,supportedNetworkTypes);
        //defect1455-bin-shen@t2mobile.com-begin
        require(['shared/simslot_manager'], function(simslotmanager) {
          SIMSlotManager = simslotmanager;
          _addNetworkTypes_me_ex(networkType, supportedNetworkTypes, prefferedNetworkTypeList);////Modified by lijuanli for task 5366486 end
         //}
        });
        //defect1455-bin-shen@t2mobile.com-end
         /*request.onerror = function onGetTypeErrorHandler() {
        supportedNetworkTypes = supportedNetworkTypeResult.networkTypes;
        _addNetworkTypes_me(networkType,supportedNetworkTypes);
         }*/
         });
      } catch (e) {
         console.log("carrier.js getValue('preffered.network.type.list') error");
      }
    }

    function _addNetworkTypes_me(networkType,supportedNetworkTypes) {
      console.log("carrier.js begin _addNetworkTypes_me networkType=" +networkType);
      if (networkType) {
        supportedNetworkTypes.forEach(function(type) {
          var option = document.createElement('option');
          option.value = type;
          option.selected = (networkType === type);
          console.log("carrier.js networktype=" + type);
          // show user friendly network mode names
          if (type in networkTypeMapping) {
            option.text = networkTypeMapping[type];
          } else {
            var l10nId = supportedNetworkTypeResult.l10nIdForType(type);
            option.setAttribute('data-l10n-id', l10nId);
            // fallback to the network type
            if (!l10nId) {
              option.textContent = type;
            }
          }
          selector.appendChild(option);
        });
      } else {
        console.warn('carrier: could not retrieve network type');
      }
    }
//Added by lijuanli for task 5366486 begin
      function _addNetworkTypes_me_ex(networkType,supportedNetworkTypes,prefferedNetworkTypeList) {
          console.log("carrier.js begin _addNetworkTypes_me networkType=" +networkType);
          if (networkType) {
              supportedNetworkTypes.forEach(function(type) {
                  var option = document.createElement('option');
                  option.value = type;
                  option.selected = (networkType === type);
                  console.log("carrier.js networktype=" + type);
                  // show user friendly network mode names
                  if (type in networkTypeMapping) {
                      option.text = networkTypeMapping[type];
                  } else {
                      var l10nId;
                      //task5400741-add by deming.wu-change 4g to lte-start
                      console.log("carrier.js prefferedNetworkTypeList=" +prefferedNetworkTypeList);
                      var simmcc, simmnc;
                      var connections = window.navigator.mozMobileConnections ||
                          [navigator.mozMobileConnection];
                      for (var i = 0; i < connections.length; ++i) {
                          var conn = connections[i];
                          if (conn && conn.voice && conn.voice.network && conn.voice.connected) {
                              // we have connection available, so we use it
                              simmcc = conn.voice.network.mcc;
                              simmnc = conn.voice.network.mnc;
                              break;
                          }
                      }
                      //defect1455-bin-shen@t2mobile.com-begin
                      if(!simmcc && !simmnc) {
                          var primarySimSlot = SIMSlotManager.getSlots()[0];
                          if (primarySimSlot && primarySimSlot.simCard && primarySimSlot.simCard.iccInfo) {
                              simmcc = primarySimSlot.simCard.iccInfo.mcc;
                              simmnc = primarySimSlot.simCard.iccInfo.mnc;
                          }
                      }
                      //defect1455-bin-shen@t2mobile.com-end
                      //defect2461-bin-shen@t2mobile.com-begin
                      if(!simmcc && !simmnc) {
                          var primarySimSlot = SIMSlotManager.getSlots()[1];
                          if (primarySimSlot && primarySimSlot.simCard && primarySimSlot.simCard.iccInfo) {
                              simmcc = primarySimSlot.simCard.iccInfo.mcc;
                              simmnc = primarySimSlot.simCard.iccInfo.mnc;
                          }
                      }
                      //defect2461-bin-shen@t2mobile.com-end
                      //task5400741-add by deming.wu-change 4g to lte-end
                      switch (prefferedNetworkTypeList) {
                          case '0':
                              console.log("carrier.js  CASE 0");
                              if (type === 'lte/wcdma/gsm') {
                                  if(isNeedToUpdateOperator(simmcc,simmnc)){
                                      //l10nId = 'operator-networkType-auto-LTE-WCDMA-GSM-RECOMMENDED_EURO';
                                      l10nId = 'operator-networkType-auto-LTE-3G-2G';
                                  }else if(isNeedUse4G3G2GRecommendedDescription(simmcc,simmnc)) {
                                      l10nId = 'operator-networkType-auto-LTE-WCDMA-GSM-RECOMMENDED';//task5574025 bin-shen@t2mobile.com
                                  } else {
                                      l10nId = 'operator-networkType-auto-4G-3G-2G';//defect1201 bin-shen@t2mobile.com
                                  }
                              } else if (type === 'wcdma/gsm-auto') {
                                  if(isNeedToUpdateOperator(simmcc,simmnc)){
                                      //l10nId = 'operator-networkType-auto-WCDMA-GSM-CUSTOM_EURO';
                                      l10nId = 'operator-networkType-auto-3G-2G';
                                  }else if(isNeedUse3G2GDescription(simmcc,simmnc)) {
                                      l10nId = 'operator-networkType-auto-WCDMA-GSM-CUSTOM';//task5574025 bin-shen@t2mobile.com
                                  } else {
                                      l10nId = 'operator-networkType-auto-3G-2G';//defect1201 bin-shen@t2mobile.com
                                  }
                              } else {
                                  l10nId = supportedNetworkTypeResult.l10nIdForType(type);
                              }
                              break;
                          case '1':
                              console.log("carrier.js  CASE 1");
                              if (type === 'lte/wcdma/gsm') {
                                if(isNeedToUpdateOperator(simmcc,simmnc)){
                                    //l10nId = 'operator-networkType-auto-LTE-WCDMA-GSM-RECOMMENDED_EURO';
                                    l10nId = 'operator-networkType-auto-LTE-3G-2G';
                                }else {
                                    l10nId = 'operator-networkType-auto-LTE-WCDMA-GSM-RECOMMENDED';
                                }
                              } else if (type === 'wcdma/gsm-auto') {
                                  if(isNeedToUpdateOperator(simmcc,simmnc)){
                                      //l10nId = 'operator-networkType-auto-WCDMA-GSM-CUSTOM_EURO';
                                      l10nId = 'operator-networkType-auto-3G-2G';
                                  }else {
                                      l10nId = 'operator-networkType-auto-WCDMA-GSM-CUSTOM';
                                  }
                              } else {
                                  l10nId = supportedNetworkTypeResult.l10nIdForType(type);
                              }
                              break;
                          case '2':
                              //task5574025 bin-shen@t2mobile.com begin
                              if(isNeedUse4G3G2GAutoDescription(simmcc,simmnc) && type === 'lte/wcdma/gsm') {
                                l10nId = 'operator-networkType-auto-4G-3G-2G';//defect1201 bin-shen@t2mobile.com
                                break;
                              } else if(isNeedUse3G2GAutoDescription(simmcc,simmnc) && type === 'wcdma/gsm-auto') {
                                l10nId = 'operator-networkType-auto-3G-2G';//defect1201 bin-shen@t2mobile.com
                                break;
                              }
                              //task5574025 bin-shen@t2mobile.com end
                          case '3':
                              if (type === 'lte/wcdma/gsm') {
                                  if(isNeedToUpdateOperator(simmcc,simmnc)){
                                      //l10nId = 'operator-networkType-auto-LTE-WCDMA-GSM-RECOMMENDED_EURO';
                                      l10nId = 'operator-networkType-auto-LTE-3G-2G';
                                  }else {
                                      l10nId = 'operator-networkType-auto-LTE-WCDMA-GSM-RECOMMENDED';
                                  }
                              } else if (type === 'wcdma/gsm-auto') {
                                  if(isNeedToUpdateOperator(simmcc,simmnc)){
                                      //l10nId = 'operator-networkType-auto-WCDMA-GSM-RECOMMENDED_EURO';
                                      l10nId = 'operator-networkType-auto-3G-2G';
                                  }else {
                                      l10nId = 'operator-networkType-auto-WCDMA-GSM-RECOMMENDED';
                                  }
                              } else {
                                  l10nId = supportedNetworkTypeResult.l10nIdForType(type);
                              }
                              break;
                          case '4':
                              if (type === 'lte/wcdma/gsm') { //4G
                                  if(isNeedToUpdateOperator(simmcc,simmnc)){
                                      //l10nId = 'operator-networkType-auto-LTE-WCDMA-GSM-RECOMMENDED_EURO';
                                      l10nId = 'operator-networkType-auto-LTE-WCDMA-GSM-RECOMMENDED_EURO';
                                  }else {
                                      l10nId = 'operator-networkType-prefer4G';
                                  }
                              } else if (type === 'wcdma/gsm-auto') { //3G
                                  if(isNeedToUpdateOperator(simmcc,simmnc)){
                                   
                                      l10nId = 'operator-networkType-auto';
                                  }else{
                                      l10nId = 'operator-networkType-3G';
                                  }
                              }else{
                                  l10nId = '2G-only option hidden';
                              }
                              break;
                          default:
                              l10nId = supportedNetworkTypeResult.l10nIdForType(type);
                              break;
                      }
                      if(l10nId=="2G-only option hidden"){
                          return false;
                      }else{
                          option.setAttribute('data-l10n-id', l10nId); //ֻ�������������� ��ʾtext
                      }
                      // fallback to the network type
                      if (!l10nId) {
                          option.textContent = type;
                      }
                  }
                  selector.appendChild(option);
              });
          } else {
              console.warn('carrier: could not retrieve network type');
          }
      }
    //Added by lijuanli for task 5366486 end
    }

    /**
     * Network operator selection: auto/manual.
     */
    function cs_initOperatorSelector() {
      var opAutoSelectOptions = document.getElementById('auto-select-options');

      _mobileConnection.addEventListener('voicechange', function (evt) {
        var state = evt.target.voice.state;
        cs_updateOperatorSelector(state);
      });

      _mobileConnection.addEventListener('datachange', function (evt) {
        var state = evt.target.data.state;
        cs_updateOperatorSelector(state);
      });

      /**
       * Toggle autoselection.
       */
      opAutoSelectOptions.onchange = function () {
        var enabled = (opAutoSelectOptions.value === 'true');
        var targetIndex = DsdsSettings.getIccCardIndexForCellAndDataSettings();
        gOperatorNetworkList.setAutomaticSelection(targetIndex,
            enabled);
      };

      /**
       * Create a network operator list item.
       */
      function newListItem(network, callback) {
        /**
         * A network list item has the following HTML structure:
         *   <li>
         *     <a>
         *       <span>Network Name</span>
         *       <small>Network State</small>
         *     </a>
         *   </li>
         */
     //task5317617-add by deming.wu-start
      var simmcc, simmnc;
      var connections = window.navigator.mozMobileConnections ||
            [navigator.mozMobileConnection];
      for (var i = 0; i < connections.length; ++i) {
            var conn = connections[i];
            if (conn && conn.voice && conn.voice.network && conn.voice.connected) {
                // we have connection available, so we use it
                simmcc = conn.voice.network.mcc;
                simmnc = conn.voice.network.mnc;
                break;
            }
        }
        //task5317617-add by deming.wu-end

        // network name
        // Modified by yingsen.zhang@t2mobile.com begin
        // We should check if network.longName more than 14, then put network.shortName
        // into listItem, otherwise put network.longName into listItem
        var name = document.createElement('span');
        name.textContent = (network.longName && network.longName.length <= 14)
            ? network.longName : network.shortName;
        console.log('[carrier] add new network: ' + name.textContent);
        // Modified by yingsen.zhang@t2mobile.com end

        // state
        var state = document.createElement('small');
        state.setAttribute('data-l10n-id',
            network.state ? ('state-' + network.state) : 'state-unknown');
        //task5317617-add by deming.wu-start
        if(isNeedToUpdateOperator(simmcc,simmnc)){
            console.log("need to updateoperator name 4G to LTE, curent mcc = "+simmcc+" ; mnc ="+simmnc);
            name.textContent = name.textContent.replace(/4G/, "LTE");
        }
        //task5317617-add by deming.wu-end
        var a = document.createElement('a');
        a.appendChild(name);
        a.appendChild(state);

        // create list item
        var li = document.createElement('li');
        li.appendChild(a);

        li.dataset.cachedState = network.state || 'unknown';
        li.classList.add('operatorItem');

        // bind connection callback
        li.onclick = function () {
          callback(network, true);
        };
        return li;
      }

      // operator network list
      gOperatorNetworkList = (function operatorNetworkList(list) {
        var networkType = document.getElementById('network-type');
        var networkTypeSelect = document.getElementById('preferredNetworkType');
        // get the "Searching..." and "Search Again" items, respectively
        var infoItem = document.getElementById('operators-info');
        var scanItem = list.querySelector('li[data-state="ready"]');

        var currentConnectedNetwork = null;
        var connecting = false;
        var operatorItemMap = {};

        var scanRequest = null;

        var opAutoSelectStates = Array.prototype.map.call(_mobileConnections,
          function () {
            return true;
          });

        networkType.addEventListener('keydown', (evt) => {
          if (evt.key === 'Enter' && networkTypeSelect.hasChildNodes())
          {
            networkTypeSelect.removeAttribute('disabled');
            networkTypeSelect && networkTypeSelect.focus();
          }
        });

        scanItem.addEventListener('keydown', rescanEventHandler);

        /**
         * "Search Again" button click event handler
         */
        function rescanEventHandler(evt) {
          if (scanItem.disabled) {
            return;
          }
          if (evt.key === 'Enter') {
            scan();
          }
        }

        /**
         * Clear the list.
         */
        function clear() {
          operatorItemMap = {};
          var operatorItems = list.querySelectorAll('li:not([data-state])');
          var len = operatorItems.length;
          for (var i = len - 1; i >= 0; i--) {
            list.removeChild(operatorItems[i]);
          }

          networkTypeSelect.setAttribute('disabled', true);
          scanItem.classList.add('non-focus');
          NavigationMap.reset();
        }

        /**
         * Reset operator item state.
         */
        function resetOperatorItemState() {
          var operatorItems =
            Array.prototype.slice.call(list.querySelectorAll('.operatorItem'));
          operatorItems.forEach(function (operatorItem) {
            var state = operatorItem.dataset.cachedState;
            var messageElement = operatorItem.querySelector('small');

            if (!state) {
              state = 'unknown';
            } else if (state === 'current') {
              state = 'available';
            }

            messageElement.setAttribute('data-l10n-id', 'state-' + state);
          });
        }

        /**
         * Select operator.
         */
        function selectOperator(network, manuallySelect) {
          if (connecting) {
            return;
          }

          var listItem = operatorItemMap[network.mcc + '.' + network.mnc];
          if (!listItem) {
            return;
          }

          var messageElement = listItem.querySelector('small');

          connecting = true;
          // update current network state as 'available' (the string display
          // on the network to connect)
          if (manuallySelect) {
            resetOperatorItemState();
          }

          var req = _mobileConnection.selectNetwork(network);
          messageElement.setAttribute('data-l10n-id',
            'operator-status-connecting');
          req.onsuccess = function onsuccess() {
            currentConnectedNetwork = network;
            messageElement.setAttribute('data-l10n-id',
              'operator-status-connected');
            connecting = false;
            checkAutomaticSelection();
          };
          req.onerror = function onerror() {
            connecting = false;
            checkAutomaticSelection();
            messageElement.setAttribute('data-l10n-id',
              'operator-status-connectingfailed');
            if (currentConnectedNetwork) {
              recoverAvailableOperator();
            } else {
              doEnableAutomaticSelection();
            }
          };
        }

        /**
         * Recover available operators.
         */
        function recoverAvailableOperator() {
          if (currentConnectedNetwork) {
            selectOperator(currentConnectedNetwork, false);
          }
        }

        /**
         * Scan available operators.
         */
        function scan() {
          clear();
          list.dataset.state = 'on'; // "Searching..."

          // If we want to show the infoItem content,
          // we should set 'data-state' same as list state.
          infoItem.setAttribute('data-state', 'on');
          infoItem.setAttribute('data-l10n-id', 'scanning');

          // invalidate the original request if it exists
          invalidateRequest(scanRequest);
          scanRequest = _mobileConnection.getNetworks();

          scanRequest.onsuccess = function onsuccess() {
            var networks = scanRequest.result;
            for (var i = 0; i < networks.length; i++) {
              var network = networks[i];
              var listItem = newListItem(network, selectOperator);
              list.insertBefore(listItem, scanItem);

              operatorItemMap[network.mcc + '.' + network.mnc] = listItem;
              if (network.state === 'current') {
                currentConnectedNetwork = network;
              }
            }
            scanItem.classList.remove('non-focus');
            list.dataset.state = 'ready'; // "Search Again" button

            scanRequest = null;
            NavigationMap.reset();
          };

          scanRequest.onerror = function onScanError(error) {
            console.warn('carrier: could not retrieve any network operator. ');
            scanItem.classList.remove('non-focus');
            list.dataset.state = 'ready'; // "Search Again" button

            scanRequest = null;
            NavigationMap.reset();
          };
        }

        function invalidateRequest(request) {
          if (request) {
            request.onsuccess = request.onerror = function () {
            };
          }
        }

        function stop() {
          list.dataset.state = 'off';
          infoItem.setAttribute('data-state', 'off');
          infoItem.setAttribute('data-l10n-id', 'operator-turnAutoSelectOff');
          clear();
          invalidateRequest(scanRequest);
          scanRequest = null;
        }

        var pendingAutomaticSelectionRequest = false;

        function checkAutomaticSelection() {
          if (pendingAutomaticSelectionRequest) {
            doEnableAutomaticSelection();
            pendingAutomaticSelectionRequest = false;
          }
        }

        function doEnableAutomaticSelection() {
          var req = _mobileConnection.selectNetworkAutomatically();
          req.onsuccess = function () {
            cs_updateOperatorSelector(
              _mobileConnection.voice.state || _mobileConnection.data.state);
            opAutoSelectOptions.value = 'true';
          };
          req.onerror = function () {
            cs_updateOperatorSelector(
              _mobileConnection.voice.state || _mobileConnection.data.state);
            opAutoSelectOptions.value = 'true';
          };
        }

        function setAutomaticSelection(index, enabled) {
          opAutoSelectStates[index] = enabled;
          if (enabled) {
            stop();
            // When RIL is actively connecting to an operator, we are not able
            // to set automatic selection. Instead we set a flag indicating that
            // there is a pending automatic selection request.
            if (connecting) {
              pendingAutomaticSelectionRequest = true;
            } else {
              doEnableAutomaticSelection();
            }
          } else {
            pendingAutomaticSelectionRequest = false;
            scan();
          }
        }

        function getAutomaticSelection(index) {
          return opAutoSelectStates[index];
        }

        // API
        return {
          stop: stop,
          scan: scan,
          setAutomaticSelection: setAutomaticSelection,
          getAutomaticSelection: getAutomaticSelection
        };
      })(document.getElementById('availableOperators'));
    }

    function cs_updateOperatorSelector(state) {
      var opAutoSelectOptions = document.getElementById('auto-select-options');
      var opAutoSelect = document.getElementById('operator-autoSelect');
      var availableOperatorsHeader =
        document.getElementById('availableOperatorsHeader');
      var availableOperators = document.getElementById('availableOperators');
      var scanItem = document.getElementById('search-again'); // "Search Again" button

      opAutoSelect.hidden = availableOperatorsHeader.hidden =
        availableOperators.hidden = (networkMode !== 'gsm');

      if (networkMode !== 'gsm') {
        return;
      }
      if (state === 'searching') {
        opAutoSelectOptions.disabled = true;
        opAutoSelect.setAttribute('aria-disabled', 'true');
        scanItem.disabled = true;
        scanItem.setAttribute('aria-disabled', 'true');
      } else {
        opAutoSelectOptions.disabled = false;
        opAutoSelect.removeAttribute('aria-disabled');
        scanItem.disabled = false;
        scanItem.removeAttribute('aria-disabled');
      }
      window.dispatchEvent(new CustomEvent('refresh'));
    }

    /**
     * Update the automatic operator selection.
     */
    function cs_updateAutomaticOperatorSelection() {
      var opAutoSelectOptions = document.getElementById('auto-select-options');
      var mode = _mobileConnection.networkSelectionMode;
      opAutoSelectOptions.value = (mode === 'automatic') ? 'true' : 'false';
      opAutoSelectOptions.dispatchEvent(new Event('change'));

      cs_updateOperatorSelector(
        _mobileConnection.voice.state || _mobileConnection.data.state);
    }

    /**
     * Update the roaming preference selector.
     */
    function cs_updateRoamingPreferenceSelector() {
      var menuItem = document.getElementById('operator-roaming-preference');
      if (!menuItem || menuItem.hidden) {
        return;
      }

      var selector =
        document.getElementById('operator-roaming-preference-selector');
      var req = _mobileConnection.getRoamingPreference();

      req.onsuccess = function () {
        selector.value = req.result;
      };

      req.onerror = function () {
        console.warn('get roaming preference : ' + req.error.name);
      };
    }

    /**
     * Init roaming preference selector.
     */
    function cs_initRoamingPreferenceSelector() {
      if (!_mobileConnection.getRoamingPreference) {
        document.getElementById('operator-roaming-preference').hidden = true;
        return;
      }

      var defaultRoamingPreferences =
        Array.prototype.map.call(_mobileConnections,
          function () {
            return ['any', 'any'];
          });
      var roamingPreferenceHelper =
        SettingsHelper('ril.roaming.preference', defaultRoamingPreferences);

      var selector =
        document.getElementById('operator-roaming-preference-selector');
      selector.addEventListener('change', (evt) => {
        roamingPreferenceHelper.get(function gotRP(values) {
          var targetIndex =
            DsdsSettings.getIccCardIndexForCellAndDataSettings();
          var setReq = _mobileConnection.setRoamingPreference(selector.value);

          setReq.onsuccess = function set_rp_success() {
            values[targetIndex] = selector.value;
            roamingPreferenceHelper.set(values);

            var toast = {
              messageL10nId: 'changessaved',
              latency: 2000,
              useTransition: true
            };
            Toaster.showToast(toast);
          };

          setReq.onerror = function set_rp_error() {
            selector.value = values[targetIndex];
          };
        });
      });
    }

    /**
     * Init a warning dialog.
     *
     * @param {String} settingKey The key of the setting.
     * @param {String} l10n id of the title.
     * @param {String} l10n id of the message.
     * @param {String} explanationItemId The id of the explanation item.
     */
    function cs_initWarning(settingKey,
                            input,
                            titleL10nId,
                            messageL10nId,
                            explanationItemId) {
      var warningDialogEnabledKey = settingKey + '.warningDialog.enabled';
      var explanationItem = document.getElementById(explanationItemId);

      /**
       * Figure out whether the warning is enabled or not.
       *
       * @param {Function} callback Callback function to be called once the
       *                            work is done.
       */
      function getWarningEnabled(callback) {
        var request = _settings.createLock().get(warningDialogEnabledKey);

        request.onsuccess = function onSuccessHandler() {
          var warningEnabled = request.result[warningDialogEnabledKey];
          if (warningEnabled === null) {
            warningEnabled = true;
          }
          if (callback) {
            callback(warningEnabled);
          }
        };
      }

      /**
       * Set the value of the setting into the settings database.
       *
       * @param {Boolean} state State to be stored.
       */
      function setState(state) {
        var cset = {};
        cset[settingKey] = !!state;
        _settings.createLock().set(cset);
      }

      function getState(callback) {
        var request = _settings.createLock().get(settingKey);
        request.onsuccess = function onSuccessHandler() {
          if (callback) {
            callback(request.result[settingKey]);
          }
        };
      }

      function setWarningDialogState(state) {
        var cset = {};
        cset[warningDialogEnabledKey] = !!state;
        _settings.createLock().set(cset);
      }

      /**
       * Helper function. Handler to be called once the user click on the
       * accept button form the warning dialog.
       */
      function onSubmit() {
        setWarningDialogState(false);
        setState(true);
        explanationItem.hidden = false;
        input.checked = true;
      }

      /**
       * Helper function. Handler to be called once the user click on the
       * cancel button form the warning dialog.
       */
      function onReset() {
        setWarningDialogState(true);
        setState(false);
        input.checked = false;
      }

      // Initialize the state of the input.
      getState(function (enabled) {
        input.checked = enabled;

        // Register an observer to monitor setting changes.
        input.addEventListener('change', function () {
          var enabled = this.checked;
          getWarningEnabled(function getWarningEnabledCb(warningEnabled) {
            if (warningEnabled) {
              if (enabled) {
                require(['modules/dialog_service'], function (DialogService) {
                  DialogService.confirm(messageL10nId, {
                    title: titleL10nId,
                    submitButton: 'turnOn',
                    cancelButton: 'notNow'
                  }).then(function (result) {
                    var type = result.type;
                    if (type === 'submit') {
                      onSubmit();
                    } else {
                      onReset();
                    }
                  });
                });
              }
            } else {
              setState(enabled);
              explanationItem.hidden = false;
            }
          });
        });
      });

      // Initialize the visibility of the warning message.
      getWarningEnabled(function getWarningEnabledCb(warningEnabled) {
        if (warningEnabled) {
          var request = _settings.createLock().get(settingKey);
          request.onsuccess = function onSuccessCb() {
            var enabled = false;
            if (request.result[settingKey] !== undefined) {
              enabled = request.result[settingKey];
            }
            if (enabled) {
              setWarningDialogState(false);
              explanationItem.hidden = false;
            }
          };
        } else {
          explanationItem.hidden = false;
        }
      });
    }

    return {
      init: function (panel) {
        cs_init();
      }
    };
  };

  return CarrierSettings;

});
