/* global SettingsSoftkey */
define(['require','modules/settings_panel','media_storage','shared/toaster'],function(require) {
  
  var SettingsPanel = require('modules/settings_panel');
  var MediaStorage = require('media_storage');
  var Toaster = require('shared/toaster');

  return function ctor_storage_panel() {
    var _selectLocationContainer,
      _selectLocationItem,
      _defaultLocationDesc;

    function _initSoftKey() {
      var softkeyParams = {
        menuClassName: 'menu-button',
        header: {
          l10nId: 'message'
        },
        items: [{
          name: 'Select',
          l10nId: 'select',
          priority: 2,
          method: function() {}
        }]
      };

      SettingsSoftkey.init(softkeyParams);
      SettingsSoftkey.show();
    }

    function _showDialog() {
      var dialogConfig = {
        title: {
          id: 'confirmation',
          args: {}
        },
        body: {
          id: 'change-default-media-location-confirmation',
          args: {}
        },
        cancel: {
          name: 'Cancel',
          l10nId: 'cancel',
          priority: 1,
          callback: function() {}
        },
        confirm: {
          name: 'Change',
          l10nId: 'change',
          priority: 3,
          callback: function() {
            _selectLocationItem.hidden = false;
            _selectLocationItem.focus();
            _selectLocationItem.hidden = true;
          }
        },
      };
      var dialog = new ConfirmDialogHelper(dialogConfig);
      dialog.show(document.getElementById('app-confirmation-dialog'));
    }

    function _updateDefaultState() {
      _selectLocationItem.hidden = true;
      var index = _selectLocationItem.selectedIndex;
      var l10nId =
        _selectLocationItem.options[index].getAttribute('data-l10n-id');
      _defaultLocationDesc.setAttribute('data-l10n-id', l10nId);
      var toast = {
        messageL10nId: 'media-storage-changed',
        latency: 3000,
        useTransition: true
      };
      Toaster.showToast(toast);
    }

    function _keyDownHandle(evt) {
      if (evt.key === 'Enter' && !_selectLocationItem.disabled) {
        _showDialog();
      }
    }

    return SettingsPanel({
      onInit: function(panel) {
        this.once = false;
        this.boundFocusHandle = function() {
          panel.querySelector('#volume-list').scrollTop = 0;
        }.bind(this);

        this.panelReady = () => {
          var el = panel.querySelector('#stacked-bar');
          el.onfocus = this.boundFocusHandle;
          _selectLocationContainer =
            panel.querySelector('.default-media-location');
          _selectLocationItem =
            document.getElementById('defaultMediaLocation');
          _defaultLocationDesc =
            document.getElementById('default-media-location-desc');

          _selectLocationContainer.addEventListener('keydown', _keyDownHandle);
          _selectLocationItem.addEventListener('change', _updateDefaultState);
        };
      },

      onBeforeShow: function() {
        if (!this.once) {
          navigator.mozL10n.once(MediaStorage.init.bind(MediaStorage));
          window.addEventListener('panelready', this.panelReady);
          this.once = true;
        }
      }
    });
  };
});
