/* set data roaming*/
define(['require','modules/settings_panel','modules/settings_service','shared/toaster'],function(require) {
  
  var SettingsPanel = require('modules/settings_panel');
  var SettingsService = require('modules/settings_service');
  var Toaster = require('shared/toaster');

  // Modified for task 5346471/5110457 by yingsen.zhang@t2mobile.com
  // Indicate current customization value
  var dataRoamingCustomized = false;

  function log(msg) {
    console.log('[data_roaming_panel] ' + msg);
  };

  return function data_roaming_panel() {
    return SettingsPanel({
      onInit: function (panel) {
        var onInitPoint = this;
        this.elements = {
          dataRoamingKey:'ril.data.roaming_enabled',
          panel: panel,
          params : {
            menuClassName: 'menu-button',
            header: { l10nId:'message' },
            items: [{
              name: 'Select',
              l10nId: 'select',
              priority: 2,
              method: function() {
                onInitPoint.onDataRoamingSet();
              }
            }]
          },
        };
        this.initUI();
      },

      //switch data roaming
      onDataRoamingSet: function(){
        if(!_self.elements.panel.querySelector('.focus input[name="data_roaming_switch"]')) {
          return;
        }
        var set = {};
        // Modified for task 5346471/5110457 by yingsen.zhang@t2mobile.com begin
        // Set current data roaming key according to current customization value
        set[_self.elements.dataRoamingKey] = dataRoamingCustomized
            ? parseInt(_self.elements.panel.querySelector('.focus input[name="data_roaming_switch"]').value)
            : Boolean(parseInt(_self.elements.panel.querySelector('.focus input[name="data_roaming_switch"]').value));
        // Modified for task 5346471/5110457 by yingsen.zhang@t2mobile.com end
        window.navigator.mozSettings.createLock().set(set);
        log('[onDataRoamingSet] set data roaming settings value: ' + JSON.stringify(set));
        var toast = {
          messageL10nId: 'changessaved',
          latency: 2000,
          useTransition: true
        };
        Toaster.showToast(toast);
        SettingsService.navigate("carrier");
      },

      onBeforeShow: function() {
        _self = this;
        SettingsSoftkey.init(_self.elements.params);
        SettingsSoftkey.show();
      },

      onBeforeHide: function() {
        SettingsSoftkey.hide();
      },

      initUI: function() {
        // Modified for task 5346471/5110457 by yingsen.zhang@t2mobile.com begin
        // Show current roaming type according to current customization value
        navigator.customization.getValue('stz.roaming.domestic.enable').then((result) => {
          dataRoamingCustomized = (result === 'undefined') ? false : result;
          log('[initialize] read data roaming customized value: ' + dataRoamingCustomized);

          var elements = this.elements;
          // dataRoamingCustomized = true; //bug2761-add by deming-wu
          elements.dataRoamingKey = dataRoamingCustomized
              ? 'data.roaming.domestic_international.enabled'
              : 'ril.data.roaming_enabled';

          var request = navigator.mozSettings.createLock().get(elements.dataRoamingKey);
          request.onsuccess = function() {
            var value = request.result[elements.dataRoamingKey];
            log('[initialize] read \'' + elements.dataRoamingKey + '\' value: ' + value);

            value = (value === 'undefined') ? 0 : (dataRoamingCustomized ? value : ((value ? 1 : 0)));
            var selector = 'li input[value=\'' + value + '\']';
            elements.panel.querySelector('li input[value=\'' + value + '\']').click();
          };
        });
        // Modified for task 5346471/5110457 by yingsen.zhang@t2mobile.com end
      }
    });
  };
});
