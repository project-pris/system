 /* global DsdsSettings */
define(['require'],function(require) {
  

  var telephony = navigator.mozTelephony;

  function VoLTEItem(element) {
    this._descItem = element;
    this._imsHandler = null;
    this._init();
  }

  VoLTEItem.prototype = {

    _init: function() {
      this._updateImsHandler();
      this.updateVolteDesc = this._updateVolteDesc.bind(this);
      this.initEventListener = this._initEventListener.bind(this);
    },

    _initEventListener: function() {
      if (telephony) {
        telephony.ontelephonycoveragelosing = (evt) => {
          this.updateVolteDesc('poorSignal');
        };
      }
    },

    _updateImsHandler: function() {
      var request =
        navigator.mozSettings.createLock().get('ril.data.defaultServiceId');
      request.onsuccess = function onSuccessHandler() {
        var _settings = navigator.mozSettings;
        var imsEnabled = null;
        var imsProfile = null;
        var serviceId = request.result['ril.data.defaultServiceId'];
        var imsHandler = navigator.mozMobileConnections[serviceId].imsHandler;

        if (imsHandler) {
          var req = _settings.createLock().get('ril.ims.enabled');
          req.onsuccess = function() {
            imsEnabled = req.result['ril.ims.enabled'];
            var profileReq = _settings.createLock().get(
              'ril.ims.preferredProfile'
            );
            profileReq.onsuccess = function () {
              imsProfile = profileReq.result['ril.ims.preferredProfile'];
              var status = imsEnabled ? 'on' : 'off';
              this.updateVolteDesc(status);

              // Do not add any desc when VoLTE is OFF
              if (imsEnabled) {
                if (imsProfile === 'cellular-preferred') {
                  this.updateVolteDesc('cellularData');
                }

                // If the VoLTE has error code,
                //   devices would not to listen on other status change.
                if (imsHandler.unregisteredReason) {
                  this.updateVolteDesc('errorCode', {
                    unregisteredReason: imsHandler.unregisteredReason
                  });
                }
              }
              this.initEventListener();
            };
          };
        }
      };
    },

    _updateVolteDesc: function root_updateVolteDesc(status, args) {
      if (status === '') {
        return;
      }

      var l10n = 'volte-status-' + status;
      navigator.mozL10n.setAttributes(this._descItem, l10n, args);
    }
  };

  return function ctor_volteItem(element) {
    return new VoLTEItem(element);
  };
});
