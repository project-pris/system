/**
 * Created by xa on 5/14/14.
 */
'use strict';

$('menuItem-curef').addEventListener('click', function()
{
	emcuref();
});
//var nvArray = new Array();
var nvValueget;
function emcuref(){

  function init(){
	  $('curef-read').addEventListener('click', function(){
		  getCURefStatus();
	  });
	  $('curef-write').addEventListener('click', function(){
		  setCURefStatus();
	  });
  }
	function getCURefStatus(){
		var engmodeEx = navigator.engmodeExtension;
		var cu_ref = '';
    var nvArray = new Array();
		if(engmodeEx){
      var r = engmodeEx.readNvitem(55);
      r.onsuccess = function(e) {
        debug('onsuccess: ' + r.result.join());
        nvArray = nvArray.concat(r.result);
        var tmp_result = cuhexArrayToAsciiStr(nvArray);
        cu_ref = tmp_result.toString();
        nvValueget = cu_ref;
        $('curef-vaule-input').value = cu_ref;
      }
    }
	}
  function cuhexArrayToAsciiStr(arr){
    debug('traceData.hexArrayToAsciiStr');
    //var arr = arr.slice(38, 58);
    var ascii = [];
    for (var i in arr) {
      //ASCII character scope
      if (arr[i] >= 32 && arr[i] <= 126)
      {
        var ss = String.fromCharCode(arr[i]);
        ascii.push(ss);
      }
    }
    var ascii_code = ascii.join('');
    return ascii_code;
  }
  function cuAsciiStrTohexArray(text){
    var nvArray = new Array();
    for(var i = 0; i < 20; i++){
      var tmpValue = text.charCodeAt(i);
      if(i < text.length)
        //nvArray[38 + i] = tmpValue;
        nvArray[i] = tmpValue;
      else
        //nvArray[38 + i] = 0;
        nvArray[i] = 0;
    }
    return nvArray;
  }
  function setCURefStatus(){
		var setVaule = $('curef-vaule-input').value;
    if(('' == setVaule) || (nvValueget === setVaule)){
      alert('The CU-Ref Value no Chaneged!');
      return;
    }
		var engmodeEx = navigator.engmodeExtension;
		if(engmodeEx){
      var value = cuAsciiStrTohexArray(setVaule);
			var initRequest = engmodeEx.writeNvitem(55, value.length, value);
		}
		initRequest.onsuccess = function(){
			$('curef-vaule-input').value = '';
      alert('The CU-Ref Set Seuccessed!');
		}
    initRequest.onerror = function(){
      getCURefStatus();
      alert('The CU-Ref Set Failed!');
    }
	}
	return init();
}
