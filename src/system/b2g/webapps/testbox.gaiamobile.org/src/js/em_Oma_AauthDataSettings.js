/**
 * Created by xa on 15-11-19.
 */
'use strict';
var inited = false;
var input_Aatuthdata_value = '';
$('menuItem-AauthData').addEventListener('click', function() {
  if(true != inited) {
    OmaAauthDataSettings.init();
  }
});
var OmaAauthDataSettings = {
  init: function init() {
    inited = true;

    $('oma-aatuthdata-write').addEventListener('click', function(e) {

      input_Aatuthdata_value = $('oma-aatuthdata-input').value.toLowerCase();
      if(0 < input_Aatuthdata_value.length) {
        var initRequest = navigator.engmodeExtension.setPropertyLE('oma_AauthData', input_Aatuthdata_value);
        initRequest.onsuccess = function() {
          alert('Success!\n\"' + input_Aatuthdata_value +'\"\nwas wrote into AauthData!');
          $('oma-aatuthdata-input').value = '';
        };
        initRequest.onerror = function() {
          alert('Failed!\n Write AauthData failed!');
        };
      }
    });
  }

}
