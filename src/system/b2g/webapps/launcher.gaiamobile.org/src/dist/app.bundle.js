webpackJsonp([1], {
    0: function(e, t, n) {
        "use strict";

        function i(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function a(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function o(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function r(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }

        function s() {
            var e = navigator.mozMobileConnections,
                t = Array.from(e).map(function(e) {
                    var t = navigator.mozIccManager.getIccById(e.iccId);
                    if (!t) return null;
                    var n = t.iccInfo;
                    return {
                        iccid: e.iccId,
                        mccmnc: n ? n.mcc + n.mnc : null
                    }
                });
            t && dump("launcher app newoperators = " + JSON.stringify(t));
            var n = JSON.parse(localStorage.getItem("operators"));
            n && dump("launcher app oldOperators = " + JSON.stringify(n));
            var i = function(e) {
                dump("apn_selection item = " + e), t && t[e] && "23415" === t[e].mccmnc && (n && (dump("apn_selection oldOperators != null..."), n[e] && dump("apn_selection oldOperators[item] = " + JSON.stringify(n[e]))), t[e] && dump("apn_selection operators[item]     = " + JSON.stringify(t[e])), n && JSON.stringify(n[e]) === JSON.stringify(t[e]) || (e > 0 && t[e - 1] && "23415" === t[e - 1].mccmnc && (!n || JSON.stringify(t[e - 1]) !== JSON.stringify(n[e - 1]) && JSON.stringify(t[e]) !== JSON.stringify(n[e])) ? (dump("apn_selection waiting for open the 2nd dialog"), U.default.on("user-has-selected", function() {
                    dump("launcher app recv user-has-selected message..."), t[e] && "23415" === t[e].mccmnc && setTimeout(function() {
                        b.default.request("APNSelection:setOperatorId", e), b.default.request("openSheet", "apnselection")
                    }, 500)
                })) : (b.default.request("APNSelection:setOperatorId", e), b.default.request("openSheet", "apnselection"))))
            };
            for (var a in t) i(a);
            localStorage.setItem("operators", JSON.stringify(t))
        }
        var l = Object.assign || function(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = arguments[t];
                    for (var i in n) Object.prototype.hasOwnProperty.call(n, i) && (e[i] = n[i])
                }
                return e
            },
            u = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            c = n(3),
            f = i(c),
            d = n(12),
            p = i(d),
            h = n(6),
            m = i(h),
            v = n(73),
            g = i(v),
            y = n(5),
            b = i(y),
            w = n(72),
            _ = i(w),
            k = n(209),
            S = i(k),
            O = n(200),
            E = i(O),
            C = n(214),
            I = i(C),
            P = n(207),
            M = i(P),
            T = n(203),
            j = i(T),
            A = n(212),
            L = i(A),
            D = n(199),
            N = i(D),
            x = n(106),
            z = i(x),
            R = n(107),
            q = i(R),
            F = n(206),
            K = i(F),
            B = n(108),
            U = i(B);
        n(42), n(113), n(112), window.performance.mark("navigationLoaded"), window.addEventListener("load", function() {
            window.performance.mark("fullyLoaded"), document.body.classList.add("loaded"), setTimeout(function() {
                document.body.classList.remove("loaded")
            }, 3e3), s()
        });
        var V = function(e) {
            function t(e) {
                a(this, t);
                var n = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                return n.name = "App", n.panels = {}, n.state = {
                    grid: K.default.grid
                }, window.performance.mark("navigationInteractive"), n
            }
            return r(t, e), u(t, [{
                key: "componentWillMount",
                value: function() {
                    window.performance.mark("contentInteractive")
                }
            }, {
                key: "componentDidMount",
                value: function() {
                    this.element = p.default.findDOMNode(this), window.performance.mark("visuallyLoaded"), this.focusWhenReady(), this._handle_largetextenabledchanged(), window.addEventListener("largetextenabledchanged", this), b.default.register("openSheet", this), b.default.register("closeSheet", this), b.default.registerState("lastSheet", this), b.default.registerState("panelAnimationRunning", this), this.element.style.setProperty("--grid-row", this.state.grid.row), this.element.style.setProperty("--grid-col", this.state.grid.col)
                }
            }, {
                key: "_handle_largetextenabledchanged",
                value: function() {
                    document.body.classList.toggle("large-text", navigator.largeTextEnabled)
                }
            }, {
                key: "focusWhenReady",
                value: function() {
                    var e = this;
                    if (!this.focusMainView()) {
                        var t = function t() {
                            e.focusMainView(), document.removeEventListener("visibilitychange", t)
                        };
                        document.addEventListener("visibilitychange", t)
                    }
                }
            }, {
                key: "focusMainView",
                value: function() {
                    return this.panels.mainView.focus(), !document.hidden
                }
            }, {
                key: "openSheet",
                value: function(e) {
                    this.lastSheet = e, this.panels[e].open(), "dialer" !== e && "qrface" !== e && this.element.classList.add("panel-" + e + "--opened")
                }
            }, {
                key: "closeSheet",
                value: function(e) {
                    this.panels[e].isClosed() || (this.panels[e].close(), this.element.classList.remove("panel-" + e + "--opened"), "dialer" !== e && "qrface" !== e || this.panels.appList.isClosed() ? (this.panels.mainView.focus(), this.lastSheet = "MainView") : (this.panels.appList.focus(), this.lastSheet = "AppList"))
                }
            }, {
                key: "render",
                value: function() {
                    var e = this;
                    return f.default.createElement("div", {
                        className: "app-workspace"
                    }, f.default.createElement("div", {
                        className: "app-content"
                    }, f.default.createElement(S.default, {
                        ref: function(t) {
                            e.panels.mainView = t
                        }
                    }), f.default.createElement(E.default, l({
                        ref: function(t) {
                            e.panels.appList = t
                        }
                    }, this.state.grid)), f.default.createElement(I.default, l({
                        ref: function(t) {
                            e.panels.speedDial = t
                        }
                    }, this.state.grid)), f.default.createElement(M.default, l({}, this.state.grid, {
                        ref: function(t) {
                            e.panels.instantSettings = t
                        }
                    })), f.default.createElement(j.default, {
                        ref: function(t) {
                            e.panels.dialer = t
                        }
                    }), f.default.createElement(L.default, {
                        ref: function(t) {
                            e.panels.qrface = t
                        }
                    }), f.default.createElement(N.default, {
                        ref: function(t) {
                            e.panels.apnselection = t
                        }
                    })), f.default.createElement(q.default, null), f.default.createElement(_.default, null), f.default.createElement(z.default, null), f.default.createElement(g.default, {
                        ref: function(t) {
                            e.panels.softKey = t
                        }
                    }))
                }
            }]), t
        }(m.default);
        p.default.render(f.default.createElement(V, null), document.getElementById("root"))
    },
    24: function(e, t, n) {
        "use strict";

        function i(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function a(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function o(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function r(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var s = Object.assign || function(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = arguments[t];
                    for (var i in n) Object.prototype.hasOwnProperty.call(n, i) && (e[i] = n[i])
                }
                return e
            },
            l = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            u = n(3),
            c = i(u),
            f = n(12),
            d = i(f),
            p = n(131),
            h = i(p),
            m = n(6),
            v = i(m),
            g = n(5),
            y = i(g),
            b = n(114),
            w = i(b);
        n(216);
        var _ = function(e, t, n) {
            return function(i) {
                function u(e) {
                    a(this, u);
                    var t = o(this, (u.__proto__ || Object.getPrototypeOf(u)).call(this, e));
                    return t.state = {
                        popup: null
                    }, t
                }
                return r(u, i), l(u, [{
                    key: "componentDidMount",
                    value: function() {
                        var e = this;
                        this.refs.composed.open = this.refs.composing.open.bind(this.refs.composing), this.refs.composed.close = this.refs.composing.close.bind(this.refs.composing), y.default.register("open", this.refs.composed), y.default.register("close", this.refs.composed), this.refs.composed.isActive = this.refs.composing.isActive.bind(this.refs.composing), this.refs.composing.on("closed", function() {
                            e.refs.composed.emit("closed"), e.emit("closed")
                        }), this.refs.composing.on("opened", function() {
                            e.refs.composed.emit("opened"), e.emit("opened")
                        })
                    }
                }, {
                    key: "open",
                    value: function(e) {
                        this.refs.composing.open(e)
                    }
                }, {
                    key: "focus",
                    value: function() {
                        var e = d.default.findDOMNode(this.refs.composed);
                        e.activeElement ? (e.activeElement.focus(), document.activeElement === document.body && e.focus()) : e.focus()
                    }
                }, {
                    key: "close",
                    value: function(e) {
                        this.refs.composing.close(e)
                    }
                }, {
                    key: "isClosed",
                    value: function() {
                        return "closed" === this.refs.composing.state.transition
                    }
                }, {
                    key: "isTransitioning",
                    value: function() {
                        return this.refs.composing.isTransitioning()
                    }
                }, {
                    key: "getTopMost",
                    value: function() {
                        return this.refs.popup.refs.popup ? this.refs.popup.refs.popup.getTopMost() : this
                    }
                }, {
                    key: "openPopup",
                    value: function(e) {
                        this.refs.popup.setState({
                            popup: e
                        })
                    }
                }, {
                    key: "componentDidUpdate",
                    value: function() {
                        this.refs.popup && this.refs.popup.open()
                    }
                }, {
                    key: "render",
                    value: function() {
                        return c.default.createElement(h.default, {
                            ref: "composing",
                            openAnimation: t,
                            closeAnimation: n
                        }, c.default.createElement(e, s({
                            ref: "composed"
                        }, this.props)), c.default.createElement(w.default, {
                            ref: "popup"
                        }))
                    }
                }]), u
            }(v.default)
        };
        t.default = _
    },
    41: function(e, t, n) {
        "use strict";

        function i(e) {
            if (e && e.__esModule) return e;
            var t = {};
            if (null != e)
                for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
            return t.default = e, t
        }

        function a(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function o(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function r(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function s(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var l = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            u = n(15),
            c = a(u),
            f = n(210),
            d = a(f),
            p = n(5),
            h = a(p),
            m = n(10),
            v = i(m),
            g = ["Communications", "Contact", "KaiOS Plus", "Messages", "Camera", "Snake", "YouTube", "Assistant", "Music", "Gallery", "FM Radio", "Browser", "E-Mail", "Clock", "Video", "Google Search", "Maps", "Twitter", "Calendar", "Settings", "Note", "Calculator", "Recorder", "Unit Converter", "Danger Dash", "Castle Of Magic", "Nitro Street Run 2"],
            y = function(e) {
                function t() {
                    o(this, t);
                    var e = r(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this));
                    return e.init = function() {
                        e.installedApps = new Map, e.apps = [], e.ready = !1, e.browser = null
                    }, e.init(), e
                }
                return s(t, e), l(t, [{
                    key: "getAppIconUrl",
                    value: function(e, t, n) {
                        if (t = t || e.manifest.icons, !t) return null;
                        var i = t[this.getAppIconSize(t, n)];
                        return /^(http|data)/.test(i) || (i = e.origin + i), i
                    }
                }, {
                    key: "getAppIconSize",
                    value: function(e) {
                        var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 56;
                        return Object.keys(e).sort(function(e, n) {
                            return (e - n) * (e >= t ? 1 : -1)
                        })[0]
                    }
                }, {
                    key: "getAppIndex",
                    value: function(e) {
                        return this.apps.findIndex(function(t) {
                            return t.manifestURL === e.manifestURL
                        })
                    }
                }, {
                    key: "getDataMcc",
                    value: function() {
                        var e = "000";
                        return navigator.mozMobileConnections.length > 0 && (e = navigator.mozMobileConnections[0].data.network.mcc), e
                    }
                }, {
                    key: "getSimmcc",
                    value: function() {
                        var e = "";
                        if (0 === SIMSlotManager.getSlots().length) return "";
                        var t = SIMSlotManager.getSlots()[0],
                            n = SIMSlotManager.getSlots()[1];
                        return t && t.simCard && t.simCard.iccInfo ? (dump("[app_store-getSimmcc] SIM 1 is inserted"), e = t.simCard.iccInfo.mcc) : n && n.simCard && n.simCard.iccInfo && (dump("[app_store-getSimmcc] NO SIM 1, select SIM 2"), e = n.simCard.iccInfo.mcc), e
                    }
                }, {
                    key: "getDataMccmnc",
                    value: function() {
                        var e = "00000";
                        return navigator.mozMobileConnections.length > 0 && (e = navigator.mozMobileConnections[0].data.network.mcc + navigator.mozMobileConnections[0].data.network.mnc), e
                    }
                }, {
                    key: "getSimmccmnc",
                    value: function() {
                        var e = "";
                        if (0 === SIMSlotManager.getSlots().length) return "";
                        var t = SIMSlotManager.getSlots()[0],
                            n = SIMSlotManager.getSlots()[1];
                        return t && t.simCard && t.simCard.iccInfo ? e = t.simCard.iccInfo.mcc + t.simCard.iccInfo.mnc : n && n.simCard && n.simCard.iccInfo && (e = n.simCard.iccInfo.mcc + n.simCard.iccInfo.mnc), e
                    }
                }, {
                    key: "updateAppIcon",
                    value: function(e) {
                        var t = this;
                        return new Promise(function(n, i) {
                            var a = t.getAppIndex(e);
                            a < 0 && n();
                            var o = t.apps[a],
                                r = t.getAppIconSize(o.manifest.icons);
                            navigator.mozApps.mgmt.getIcon(e, r).then(function(e) {
                                o.icon_url = URL.createObjectURL(e), n()
                            }).catch(function(e) {
                                i(e)
                            })
                        })
                    }
                }, {
                    key: "checkImage",
                    value: function(e, t, n) {
                        var i = this,
                            a = new Image;
                        a.src = e[t], a.onerror = function() {
                            e[t] = n, i.emit("change")
                        }
                    }
                }, {
                    key: "addApp",
                    value: function(e, t) {
                        var n = e.manifest.icons;
                        n && (e.icon_url = this.getAppIconUrl(e, n), e.icon_url_hq = this.getAppIconUrl(e, n, 112), (/^http/.test(e.icon_url) || /^http/.test(e.icon_url_hq)) && (this.checkImage(e, "icon_url", "./style/images/default_app_56.png"), this.checkImage(e, "icon_url_hq", "./style/images/default_app_112.png"))), null == this.browser && "Browser" === e.manifest.name && (this.browser = e), e.launch = function() {
                            window.performance.mark("appLaunch@" + e.origin), t.launch(e.entry)
                        }, e.shouldHide = function() {
                            return !e.enabled || "dialer" === e.entry || ["system", "input", "theme", "homescreen"].includes(e.manifest.role)
                        }(), e.mozApp = t, e.uid = this.getAppUid(e), this.installedApps.set(this.getAppUid(e), e);
                        var i = e.manifest.name,
                            a = this.appsOrder.indexOf(i);
                        e.order = -1 === a ? 99 : a, e.theme = e.manifest.theme_color || null;
                        var o = void 0;
                        this.apps.some(function(t, n) {
                            var i = t.manifestURL === e.manifestURL;
                            return i && (o = n), i
                        }), void 0 !== o ? this.apps[o] = e : this.apps.push(e)
                    }
                }, {
                    key: "launchBrowser",
                    value: function() {
                        null != this.browser ? this.browser.launch() : this.apps.some(function(e) {
                            var t = "Browser" === e.manifest.name;
                            return t && e.launch(), t
                        })
                    }
                }, {
                    key: "query",
                    value: function(e, t) {
                        return this.ready ? this.apps.find(function(n) {
                            return n.manifestURL === e && (!t || !(!t || n.entry !== t))
                        }, this) : null
                    }
                }, {
                    key: "parse",
                    value: function(e) {
                        var t, n = e.manifest || e.updateManifest,
                            i = n.entry_points;
                        if (i) {
                            var a = "";
                            n = JSON.parse(JSON.stringify(n));
                            for (var o in i) {
                                t = {};
                                var r = i[o];
                                a = new d.default(r).name;
                                for (var s in r) "locale" !== s && "name" !== s && (n[s] = r[s]);
                                for (var l in e) t[l] = e[l];
                                t.manifest = n, t.name = a, t.entry = o, this.addApp(t, e)
                            }
                        } else {
                            t = {};
                            for (var u in e) t[u] = e[u];
                            t.manifest || (t.manifest = t.updateManifest), t.name = new d.default(t.manifest).name, this.addApp(t, e)
                        }
                    }
                }, {
                    key: "getAppUid",
                    value: function(e) {
                        var t = e.manifestURL;
                        return e.entry && (t += "+" + e.entry), t
                    }
                }, {
                    key: "getAppOrder",
                    value: function() {
                        return v.asyncLocalStorage.getItem("app-order")
                    }
                }, {
                    key: "setAppOrder",
                    value: function(e) {
                        this.appsOrder = e, v.asyncLocalStorage.setItem("app-order", JSON.stringify(e)), this.resort()
                    }
                }, {
                    key: "resort",
                    value: function() {
                        var e = this,
                            t = this.apps.length;
                        this.apps.forEach(function(n, i) {
                            var a = n.manifest.name,
                                o = e.appsOrder.indexOf(a);
                            n.order = -1 === o ? t + i : o
                        }), this.emit("change")
                    }
                }, {
                    key: "sort",
                    value: function() {
                        this.apps.sort(function(e, t) {
                            return e.order > t.order
                        }), this.emit("change")
                    }
                }, {
                    key: "start",
                    value: function() {
                        var e = this,
                            t = navigator.mozApps.mgmt;
                        t && (this.getAppOrder().then(function(t) {
                            e.appsOrder = JSON.parse(t) || g, e.updateMgmt()
                        }), window.addEventListener("localized", this), t.addEventListener("update", this), t.addEventListener("install", this), t.addEventListener("uninstall", this), t.addEventListener("enabledstatechange", this))
                    }
                }, {
                    key: "updateMgmt",
                    value: function() {
                        var e = this;
                        navigator.mozApps.mgmt.getAll().onsuccess = function(t) {
                            t.target.result.forEach(function(t) {
                                "pending" !== t.installState && (e.parse(t), t.ondownloadapplied = function(t) {
                                    e.parse(t.application), e.emit("change")
                                })
                            }), e.ready = !0, e.emit("ready")
                        }
                    }
                }, {
                    key: "removeApp",
                    value: function(e) {
                        var t = this,
                            n = this.getAppUid(e);
                        this.apps.some(function(e, i) {
                            return e.uid === n && (t.apps.splice(i, 1), t.emit("change"), !0)
                        })
                    }
                }, {
                    key: "uninstallApp",
                    value: function(e) {
                        h.default.request("showDialog", {
                            type: "confirm",
                            header: v.toL10n("confirmation"),
                            content: v.toL10n("confirm-to-uninstall-app", {
                                appName: e.manifest.name
                            }),
                            translated: !0,
                            onOk: function() {
                                navigator.mozApps.mgmt.uninstall(e)
                            }
                        })
                    }
                }, {
                    key: "_handle_update",
                    value: function(e) {
                        var t = this,
                            n = e.application;
                        this.parse(n), this.updateAppIcon(n).then(function() {
                            t.emit("change")
                        }).catch(function(e) {})
                    }
                }, {
                    key: "_handle_install",
                    value: function(e) {
                        var t = this,
                            n = e.application;
                        return "installed" === n.installState ? (this.parse(n), void this.emit("change")) : (n.ondownloadapplied = function() {
                            t.parse(n), t.emit("change")
                        }, void(n.ondownloaderror = function() {}))
                    }
                }, {
                    key: "_handle_uninstall",
                    value: function(e) {
                        this.removeApp(e.application)
                    }
                }, {
                    key: "_handle_enabledstatechange",
                    value: function() {
                        this.init(), this.start()
                    }
                }, {
                    key: "_handle_localized",
                    value: function() {
                        this.apps.forEach(function(e) {
                            var t = e.entry ? e.manifest.entry_points[e.entry] : e.manifest;
                            e.name = new d.default(t).name
                        }), this.emit("change")
                    }
                }]), t
            }(c.default),
            b = new y;
        b.start(), t.default = b
    },
    42: function(e, t, n) {
        "use strict";

        function i(e) {
            if (e && e.__esModule) return e;
            var t = {};
            if (null != e)
                for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
            return t.default = e, t
        }

        function a(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function o(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function r(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function s(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var l = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            u = n(15),
            c = a(u),
            f = n(109),
            d = a(f),
            p = n(5),
            h = a(p),
            m = n(64),
            v = a(m),
            g = n(41),
            y = a(g),
            b = n(111),
            w = a(b),
            _ = n(10),
            k = i(_),
            S = function(e) {
                function t() {
                    var e, n, i, a;
                    o(this, t);
                    for (var s = arguments.length, l = Array(s), u = 0; u < s; u++) l[u] = arguments[u];
                    return n = i = r(this, (e = t.__proto__ || Object.getPrototypeOf(t)).call.apply(e, [this].concat(l))), i.name = "SpeedDialHelper", a = n, r(i, a)
                }
                return s(t, e), l(t, [{
                    key: "speedDial",
                    value: function(e) {
                        var t = this;
                        dump("launcher,speeddial:" + e), 0 == e && y.default.launchBrowser();
                        var n = e - 1;
                        if (!(n < 0)) return 0 === n ? void this.dialVoicemail() : void navigator.customization.getValue("speed.dial.enable").then(function(i) {
                            dump("speed dial enable result ====== " + i);
                            var a = !0;
                            i || (a = !1), dump("speed dial enable speedDialEnable ====== " + a), a && (0 == n ? navigator.customization.getValue("voice.mail.enable").then(function(n) {
                                n = void 0 === n || n, dump("speed_dial_helper.js voicemail get result = " + n), n ? t.dialOrAssignSpeedDial(e) : dump("speed_dial_helper.js voicemail disabled!!!")
                            }) : t.dialOrAssignSpeedDial(e))
                        })
                    }
                }, {
                    key: "dialOrAssignSpeedDial",
                    value: function(e) {
                        var t = d.default.contacts[e - 1].tel;
                        t ? v.default.dial(t) : this.assignSpeedDial(e)
                    }
                }, {
                    key: "dialVoicemail",
                    value: function() {
                        var e = this;
                        h.default.request("chooseSim", "call").then(function() {
                            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
                                n = d.default["ril.iccInfo.mbdn"],
                                i = (Array.isArray(n) ? n[t] : n) || navigator.mozVoicemail && navigator.mozVoicemail.getNumber(t);
                            i ? v.default.dial(i, !1, t) : e.assignSpeedDial(1)
                        })
                    }
                }, {
                    key: "assignSpeedDial",
                    value: function(e) {
                        e = Number(e), e && (1 === e ? h.default.request("showDialog", {
                            type: "alert",
                            header: "confirmation",
                            content: "assign-voicemail"
                        }) : h.default.request("showDialog", {
                            ok: "assign",
                            type: "confirm",
                            header: k.toL10n("confirmation"),
                            content: k.toL10n("assign-speed-dial", {
                                n: e
                            }),
                            translated: !0,
                            onOk: function() {
                                k.pickContact(function(t) {
                                    var n = t.target.result,
                                        i = n.id;
                                    if (n && i) return n.tel || n.tel[0] || n.tel[0].value ? void d.default.set(e, n.tel[0].value, i) : void window.alert(k.toL10n("alert-for-contacts-without-number"))
                                })
                            }
                        }))
                    }
                }, {
                    key: "removeSpeedDial",
                    value: function(e) {
                        var t = e.number,
                            n = e.name,
                            i = e.cb,
                            a = function() {
                                "function" == typeof i && i()
                            };
                        h.default.request("showDialog", {
                            ok: "remove",
                            type: "confirm",
                            header: k.toL10n("confirmation"),
                            content: k.toL10n("remove-speed-dial", {
                                name: n
                            }),
                            translated: !0,
                            onOk: function() {
                                d.default.remove(t)
                            },
                            onCancel: a,
                            onBack: a
                        })
                    }
                }, {
                    key: "replaceSpeedDial",
                    value: function(e) {
                        var t = e.number,
                            n = e.name,
                            i = e.contactId,
                            a = d.default.contacts[t - 1].tel;
                        k.pickContact(function(e) {
                            var o = e.target.result,
                                r = o.id;
                            if (o && r) {
                                var s = o.tel[0].value,
                                    l = o.name[0] || s;
                                i + "-" + n + "-" + a == r + "-" + l + "-" + s ? d.default.set(t, s, r) : h.default.request("showDialog", {
                                    ok: "replace",
                                    type: "confirm",
                                    header: k.toL10n("confirmation"),
                                    content: k.toL10n("replace-speed-dial", {
                                        name: n,
                                        subName: l
                                    }),
                                    translated: !0,
                                    onOk: function() {
                                        d.default.set(t, s, r)
                                    }
                                })
                            }
                        })
                    }
                }, {
                    key: "register",
                    value: function(e) {
                        e.addEventListener("keydown", this), e.addEventListener("keyup", this)
                    }
                }, {
                    key: "_handle_keyup",
                    value: function(e) {
                        if ("complete" === document.readyState && this.pressingTimer && !h.default.query("LaunchStore.isLaunching")) {
                            var t = w.default.translate(e.key);
                            switch (t) {
                                case "0":
                                case "1":
                                case "2":
                                case "3":
                                case "4":
                                case "5":
                                case "6":
                                case "7":
                                case "8":
                                case "9":
                                case "*":
                                case "#":
                                case "+":
                                    window.clearTimeout(this.pressingTimer), this.pressingTimer = null, h.default.query("App.panelAnimationRunning") || h.default.request("Dialer:show", t)
                            }
                        }
                    }
                }, {
                    key: "_handle_keydown",
                    value: function(e) {
                        var t = this;
                        if ("complete" === document.readyState && !h.default.query("LaunchStore.isLaunching")) {
                            var n = w.default.translate(e.key);
                            switch (this.pressingTimer && (window.clearTimeout(this.pressingTimer), this.pressingTimer = null), n) {
                                case "0":
                                case "1":
                                case "2":
                                case "3":
                                case "4":
                                case "5":
                                case "6":
                                case "7":
                                case "8":
                                case "9":
                                    this.pressingTimer = window.setTimeout(function() {
                                        t.speedDial(parseInt(n, 10)), t.pressingTimer = null
                                    }, 1500);
                                    break;
                                case "*":
                                case "#":
                                case "+":
                                    this.pressingTimer = window.setTimeout(function() {
                                        t.pressingTimer = null
                                    }, 500)
                            }
                        }
                    }
                }]), t
            }(c.default),
            O = new S;
        t.default = O
    },
    43: function(e, t, n) {
        "use strict";

        function i(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function a(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function o(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function r(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var s = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            l = n(15),
            u = i(l),
            c = n(5),
            f = i(c),
            d = n(41),
            p = i(d),
            h = function(e) {
                function t(e) {
                    a(this, t);
                    var n = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                    return n.name = "LaunchStore", n.resetLaunchingMarker = function() {
                        n.isLaunching = !1
                    }, n.ports = {}, window.addEventListener("visibilitychange", n.resetLaunchingMarker), window.addEventListener("blur", n.resetLaunchingMarker), f.default.registerState("isLaunching", n), n
                }
                return r(t, e), s(t, [{
                    key: "launch",
                    value: function(e, t) {
                        e && t && ("iac" === e ? this.launchForIAC(t) : this.launchApp(e, t), this.resetTimer && (clearTimeout(this.resetTimer), this.resetTimer = null), this.resetTimer = setTimeout(this.resetLaunchingMarker, 3e3))
                    }
                }, {
                    key: "launchApp",
                    value: function(e, t) {
                        if (!this.isLaunching) {
                            this.isLaunching = !0;
                            p.default.apps.some(function(n) {
                                var i = n[e] === t;
                                return i && n.launch(), i
                            })
                        }
                    }
                }, {
                    key: "launchForIAC",
                    value: function(e) {
                        var t = this;
                        if (!this.isLaunching) {
                            if (!this.ports[e]) return void(navigator.mozApps.getSelf().onsuccess = function(n) {
                                n.target.result.connect(e).then(function(n) {
                                    t.ports[e] = n, t.launchForIAC(e)
                                }, function(e) {})
                            });
                            this.isLaunching = !0, this.ports[e].forEach(function(e) {
                                e.postMessage({})
                            })
                        }
                    }
                }]), t
            }(u.default);
        t.default = new h
    },
    65: function(e, t, n) {
        "use strict";

        function i(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function a(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function o(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function r(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var s = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            l = n(15),
            u = i(l),
            c = n(103),
            f = i(c),
            d = function(e) {
                function t(e) {
                    a(this, t);
                    var n = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                    return n.name = "SimCardHelper", f.default.observe("outgoingCall", n._handle_outgoingCallChange.bind(n)), f.default.getCardIndexFrom("outgoingCall", function(e) {
                        n.cardIndex = e, n.emit("ready", e)
                    }), n
                }
                return r(t, e), s(t, [{
                    key: "isAlwaysAsk",
                    value: function() {
                        return f.default.ALWAYS_ASK_OPTION_VALUE === this.cardIndex
                    }
                }, {
                    key: "_handle_outgoingCallChange",
                    value: function(e) {
                        this.cardIndex = e, this.emit("change", e)
                    }
                }]), t
            }(u.default);
        t.default = new d
    },
    105: function(e, t, n) {
        "use strict";

        function i(e) {
            if (e && e.__esModule) return e;
            var t = {};
            if (null != e)
                for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
            return t.default = e, t
        }

        function a(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function o(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function r(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function s(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }

        function l(e, t, n) {
            var i = {};
            e: for (var a = 0, o = t.terms.length; a < o; a++)
                for (var r = t.terms[a], s = 0, l = t.fields.length; s < l; s++) {
                    var u = t.fields[s];
                    if (e[u])
                        for (var c = 0, f = e[u].length; c < f; c++) {
                            var d = e[u][c];
                            if ("undefined" != typeof d.value && (d = d.value), i[r] = n(d.trim(), r)) continue e
                        }
                }
            return Object.keys(i).every(function(e) {
                return i[e]
            })
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var u = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            c = n(15),
            f = a(c),
            d = n(10),
            p = i(d),
            h = {
                contains: function(e, t) {
                    return e = e.toLowerCase(), t = t.toLowerCase(), e.contains(t)
                },
                equality: function(e, t) {
                    return e = e.toLowerCase(), t = t.toLowerCase(), e === t
                }
            },
            m = /\s+/,
            v = function(e) {
                function t() {
                    return o(this, t), r(this, (t.__proto__ || Object.getPrototypeOf(t)).apply(this, arguments))
                }
                return s(t, e), u(t, [{
                    key: "start",
                    value: function() {
                        this.contactStore = new Map, this.API = window.mozContacts || navigator.mozContacts, this.API.addEventListener("contactchange", this), this.initObserver()
                    }
                }, {
                    key: "getContactChildren",
                    value: function(e) {
                        return e ? e.querySelectorAll("*[data-contact-number]") : []
                    }
                }, {
                    key: "updateFragment",
                    value: function(e) {
                        "function" == typeof e.hasAttribute && e.hasAttribute("data-contact-number") && this.updateContact(e);
                        for (var t = this.getContactChildren(e), n = 0; n < t.length; n++) this.updateContact(t[n])
                    }
                }, {
                    key: "initObserver",
                    value: function() {
                        var e = this,
                            t = {
                                attributes: !0,
                                characterData: !1,
                                childList: !0,
                                subtree: !0,
                                attributeFilter: ["data-contact-number"]
                            };
                        new MutationObserver(function(n, i) {
                            i.disconnect(), e.updateContacts(n), i.observe(document, t)
                        }).observe(document, t)
                    }
                }, {
                    key: "updateContacts",
                    value: function(e) {
                        for (var t = this, n = void 0, i = new Set, a = 0; a < e.length; a++) {
                            if (n = e[a], "childList" === n.type)
                                for (var o = void 0, r = 0; r < n.addedNodes.length; r++) o = n.addedNodes[r], o.nodeType === Node.ELEMENT_NODE && i.add(o);
                            "attributes" === n.type && i.add(n.target)
                        }
                        i.forEach(function(e) {
                            e.childElementCount ? t.updateFragment(e) : e.dataset.contactNumber && t.updateContact(e)
                        }, this)
                    }
                }, {
                    key: "updateContact",
                    value: function(e) {
                        var t = this,
                            n = e.dataset.contactNumber,
                            i = "name" === e.dataset.contactColumn ? e : e.querySelector("[data-contact-column=name]"),
                            a = "photo" === e.dataset.contactColumn ? e : e.querySelector("[data-contact-column=photo]");
                        this.findByAddress(n, function(o) {
                            var r = p.getContactDetails(n, o, {
                                photoURL: !0
                            });
                            i ? r.name ? i.textContent !== r.name && (t.debug("updating name", r, e), i.textContent = r.name) : i.textContent && (t.debug("cleaning name", r, e), i.textContent = "") : t.debug("no contact name DOM"), a ? (t.debug("updating photo", r, e), a.style.backgroundImage = r.photoURL ? "url(" + r.photoURL + ")" : null) : t.debug("no photo DOM")
                        })
                    }
                }, {
                    key: "_handle_contactchange",
                    value: function() {
                        this.updateFragment(document.body), this.emit("contact-changed")
                    }
                }, {
                    key: "findBy",
                    value: function(e, t) {
                        var n, i, a = [],
                            o = (e.filterValue || "").trim(),
                            r = this;
                        return navigator.mozContacts && o.length ? (n = o.split(m), e.filterValue = 1 === n.length ? n[0] : n.reduce(function(e, t) {
                            return a.push(t.toLowerCase()), t.length > e.length ? t : e
                        }, ""), e.filterValue.length < 3 && (e.filterLimit = 10), a.splice(a.indexOf(e.filterValue.toLowerCase()), 1), a.push.apply(a, n), i = r.API.find(e), i.onsuccess = function() {
                            var e, i = this.result.slice(),
                                o = ["tel", "givenName", "familyName"],
                                r = {
                                    fields: o,
                                    terms: a
                                },
                                s = [];
                            if (n.length > 1)
                                for (; e = i.pop();) l(e, r, h.contains) && s.push(e);
                            else s = i;
                            t(s, {
                                terms: n
                            })
                        }, void(i.onerror = function() {
                            this.onsuccess = null, this.onerror = null, t(null)
                        })) : void setTimeout(function() {
                            t("undefined" == typeof e.filterValue ? null : [], {})
                        })
                    }
                }, {
                    key: "findContactByString",
                    value: function(e, t) {
                        var n = ["tel", "givenName", "familyName"];
                        return this.findBy({
                            filterBy: n,
                            filterOp: "contains",
                            filterValue: e
                        }, t)
                    }
                }, {
                    key: "findExact",
                    value: function(e, t) {
                        return this.findBy({
                            filterBy: ["givenName", "familyName"],
                            filterOp: "contains",
                            filterValue: e
                        }, function(n) {
                            var i = n && n.length ? n[0] : null,
                                a = {
                                    fields: ["name"],
                                    terms: [e]
                                },
                                o = !1;
                            i && (o = l(i, a, h.equality)), t(o ? [i] : [])
                        })
                    }
                }, {
                    key: "findByPhoneNumber",
                    value: function(e, t) {
                        return this.findBy({
                            filterBy: ["tel"],
                            filterOp: "match",
                            filterValue: e.replace(/\s+/g, "")
                        }, function(e) {
                            return e && e.length ? void t(e) : void t([])
                        })
                    }
                }, {
                    key: "findByAddress",
                    value: function(e, t) {
                        this.findByPhoneNumber(e, t)
                    }
                }, {
                    key: "findExactByEmail",
                    value: function(e, t) {
                        return this.findBy({
                            filterBy: ["email"],
                            filterOp: "equals",
                            filterValue: e
                        }, t)
                    }
                }, {
                    key: "findById",
                    value: function(e, t) {
                        return this.findBy({
                            filterBy: ["id"],
                            filterOp: "equals",
                            filterValue: e
                        }, function(e) {
                            var n = [];
                            e && e.length && (n = e[0]), t(n)
                        })
                    }
                }]), t
            }(f.default),
            g = new v;
        g.start(), t.default = g
    },
    108: function(e, t, n) {
        "use strict";

        function i(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function a(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function o(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function r(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var s = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            l = n(15),
            u = i(l),
            c = function(e) {
                function t() {
                    return a(this, t), o(this, (t.__proto__ || Object.getPrototypeOf(t)).apply(this, arguments))
                }
                return r(t, e), s(t, [{
                    key: "start",
                    value: function() {
                        this.emit("user-has-selected")
                    }
                }]), t
            }(u.default),
            f = new c;
        t.default = f
    },
    109: function(e, t, n) {
        "use strict";

        function i(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function a(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function o(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function r(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var s = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            l = n(6),
            u = i(l),
            c = n(105),
            f = i(c),
            d = n(22),
            p = i(d),
            h = function(e) {
                function t() {
                    var e, n, i, r;
                    a(this, t);
                    for (var s = arguments.length, l = Array(s), u = 0; u < s; u++) l[u] = arguments[u];
                    return n = i = o(this, (e = t.__proto__ || Object.getPrototypeOf(t)).call.apply(e, [this].concat(l))), i.SIZE = 9, i.contacts = [], i.voicemailNumber = null, r = n, o(i, r)
                }
                return r(t, e), s(t, [{
                    key: "start",
                    value: function() {
                        this.fetch(), this.getCustomSpeedDials(), p.default.addObserver("ril.iccInfo.mbdn", this), navigator.mozContacts.addEventListener("speeddialchange", this.fetch.bind(this)), navigator.mozContacts.addEventListener("contactchange", this.fetch.bind(this))
                    }
                }, {
                    key: "_observe_ril.iccInfo.mbdn",
                    value: function(e) {
                        this["ril.iccInfo.mbdn"] = e, e = (Array.isArray(e) ? e[0] : e) || navigator.mozVoicemail.getNumber(0), this.voicemailNumber = e, this.contacts[0].tel = e, this.emit("changed")
                    }
                }, {
                    key: "getCustomSpeedDials",
                    value: function() {
                        var e = this;
                        p.default.get("custom.speeddials").then(function(t) {
                            t && (e.customSpeedDials = t, e.fillCustomSpeedDials(t), e.emit("changed"))
                        })
                    }
                }, {
                    key: "fillCustomSpeedDials",
                    value: function() {
                        var e = this;
                        (arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : []).forEach(function(t) {
                            var n = parseInt(t.key, 10);
                            e.contacts[n - 1] = {
                                dial: n,
                                editable: !1,
                                tel: t.tel,
                                name: t.name,
                                id: "customsd"
                            }
                        })
                    }
                }, {
                    key: "fetch",
                    value: function() {
                        var e = this;
                        this.initData(), this.fillCustomSpeedDials(this.customSpeedDials), navigator.mozContacts.getSpeedDials().then(function(t) {
                            e.parse(t)
                        })
                    }
                }, {
                    key: "set",
                    value: function(e, t, n) {
                        navigator.mozContacts.setSpeedDial(e, t, n).onerror = function(e) {}
                    }
                }, {
                    key: "remove",
                    value: function(e) {
                        navigator.mozContacts.removeSpeedDial(e).onerror = function(e) {}
                    }
                }, {
                    key: "initData",
                    value: function() {
                        this.contacts = Array(this.SIZE).fill(null).map(function(e, t) {
                            return {
                                dial: t + 1,
                                editable: !0
                            }
                        }), Object.assign(this.contacts[0], {
                            tel: this.voicemailNumber,
                            editable: !1,
                            voicemail: !0,
                            name: "voicemail",
                            id: "voicemail"
                        })
                    }
                }, {
                    key: "parse",
                    value: function(e) {
                        var t = this,
                            n = e.map(function(e) {
                                return new Promise(function(n) {
                                    f.default.findById(e.contactId, function(i) {
                                        if (!(i instanceof window.mozContact)) return void n();
                                        var a = void 0;
                                        i.photo && i.photo.length && (a = window.URL.createObjectURL(i.photo[0]));
                                        var o = parseInt(e.speedDial, 10);
                                        Object.assign(t.contacts[o - 1], {
                                            id: e.contactId,
                                            name: i.name && i.name[0],
                                            photo: a,
                                            dial: o,
                                            tel: e.tel
                                        }), n()
                                    })
                                })
                            }, this);
                        Promise.all(n).then(function() {
                            t.emit("changed")
                        })
                    }
                }]), t
            }(u.default),
            m = new h;
        m.start(), t.default = m
    },
    110: function(e, t, n) {
        "use strict";

        function i(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function a(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function o(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function r(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var s = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            l = n(15),
            u = i(l),
            c = function(e) {
                function t(e) {
                    a(this, t), dump("cck flashlight constructor");
                    var n = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                    return n.name = "FlashlightHelper", navigator.getFlashlightManager().then(n.init.bind(n)), n
                }
                return r(t, e), s(t, [{
                    key: "init",
                    value: function(e) {
                        dump("cck flashlight init"), e.addEventListener("flashlightchange", this), this.flashlightManager = e, this.emit("ready", e.flashlightEnabled)
                    }
                }, {
                    key: "_handle_flashlightchange",
                    value: function() {
                        dump("cck _handle_flashlightchange flashlightEnabled=" + this.flashlightManager.flashlightEnabled), this.emit("change", this.flashlightManager.flashlightEnabled)
                    }
                }, {
                    key: "toggle",
                    value: function() {
                        dump("cck FlashlightHelper toggle flashlightEnabled=" + this.flashlightManager.flashlightEnabled), this.flashlightManager.flashlightEnabled = !this.flashlightManager.flashlightEnabled
                    }
                }]), t
            }(u.default);
        t.default = new c
    },
    111: function(e, t) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var n = {
            w: "1",
            e: "2",
            r: "3",
            s: "4",
            d: "5",
            f: "6",
            z: "7",
            x: "8",
            c: "9",
            ",": "0",
            o: "+",
            a: "*",
            q: "#"
        };
        t.default = {
            translate: function(e) {
                return n[e] || e
            }
        }
    },
    114: function(e, t, n) {
        "use strict";

        function i(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function a(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function o(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function r(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var s = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            l = n(3),
            u = i(l),
            c = n(12),
            f = i(c),
            d = n(6),
            p = i(d),
            h = function(e) {
                function t(e) {
                    a(this, t);
                    var n = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                    return n.state = {
                        popup: null
                    }, n
                }
                return r(t, e), s(t, [{
                    key: "componentDidMount",
                    value: function() {}
                }, {
                    key: "focus",
                    value: function() {
                        f.default.findDOMNode(this.refs.composed).focus()
                    }
                }, {
                    key: "open",
                    value: function(e) {
                        this.refs.popup && this.refs.popup.open(e)
                    }
                }, {
                    key: "componentDidUpdate",
                    value: function() {
                        var e = this;
                        this.refs.popup && (this.refs.popup.open("bottom-to-up"), this.refs.popup.refs.composed.close = this.close.bind(this), this.refs.popup.refs.composing.on("closing", function() {
                            e.setState({
                                popup: null
                            })
                        }))
                    }
                }, {
                    key: "render",
                    value: function() {
                        var e = this.state.popup ? u.default.cloneElement(this.state.popup, {
                            ref: "popup"
                        }) : null;
                        return u.default.createElement("div", {
                            className: "popup"
                        }, e)
                    }
                }]), t
            }(p.default);
        t.default = h
    },
    129: function(e, t, n) {
        var i;
        "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
            return typeof e
        } : function(e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        };
        ! function(a) {
            i = function(e, t, n) {
                "use strict";

                function i(e, t) {
                    var n = a(t),
                        i = n.measureText(e).width;
                    return r("got text width", i), i
                }

                function a(e) {
                    r("get canvas context", e);
                    var t = s[e];
                    if (t) return t;
                    var n = document.createElement("canvas");
                    n.setAttribute("moz-opaque", "true"), n.setAttribute("width", "1px"), n.setAttribute("height", "1px"), r("created canvas", n);
                    var i = n.getContext("2d", {
                        willReadFrequently: !0
                    });
                    return i.font = e, s[e] = i
                }

                function o(e) {
                    return e.replace(/\s+/g, " ").trim()
                }
                var r = function() {},
                    s = {};
                n.exports = function(e) {
                    r("font fit", e);
                    var t, n, a = e.space - .03 * e.space,
                        s = e.min || 16,
                        l = e.max || 24,
                        u = o(e.text),
                        c = l;
                    do n = e.font.replace(/\d+px/, c + "px"), t = i(u, n); while (t > a && c !== s && c--);
                    return {
                        textWidth: t,
                        fontSize: c,
                        overflowing: t > a
                    }
                }
            }.call(t, n, t, e), !(void 0 !== i && (e.exports = i))
        }(n(236))
    },
    131: function(e, t, n) {
        "use strict";

        function i(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function a(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function o(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function r(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var s = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            l = n(3),
            u = i(l),
            c = n(12),
            f = i(c),
            d = n(6),
            p = i(d),
            h = n(132),
            m = i(h);
        n(217);
        var v = function(e) {
            function t(e) {
                a(this, t);
                var n = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                return n.state = {
                    transition: "closed",
                    animation: "immediate"
                }, n
            }
            return r(t, e), s(t, [{
                key: "isHidden",
                value: function() {
                    return "opened" !== this.state.transition
                }
            }]), s(t, [{
                key: "isActive",
                value: function() {
                    return "opened" === this.state.transition || "opening" === this.state.transition
                }
            }, {
                key: "isTransitioning",
                value: function() {
                    return "opening" === this.state.transition || "closing" === this.state.transition
                }
            }, {
                key: "onAnimationEnd",
                value: function(e) {
                    if (e.target === f.default.findDOMNode(this)) switch (this.state.transition) {
                        case "opening":
                            this.setState({
                                transition: "opened",
                                animation: ""
                            });
                            break;
                        case "closing":
                            this.setState({
                                transition: "closed",
                                animation: ""
                            })
                    }
                }
            }, {
                key: "componentDidMount",
                value: function() {
                    f.default.findDOMNode(this).addEventListener("animationend", this.onAnimationEnd.bind(this), !1)
                }
            }, {
                key: "getActivatedState",
                value: function() {
                    switch (this.state.transition) {
                        case "opening":
                            return "-activating";
                        case "closing":
                            return "-deactivating";
                        case "opened":
                            return "-activated";
                        case "closed":
                            return "-deactivated"
                    }
                }
            }, {
                key: "componentDidUpdate",
                value: function() {
                    this.emit(this.state.transition), this.publish(this.getActivatedState());
                    var e = (u.default.Children.toArray(this.props.children)[0], f.default.findDOMNode(this.refs.shadow).firstChild);
                    if (!e) return void this.debug("no content");
                    if ("opened" === this.state.transition) {
                        if (this.debug("focusing inner content"), this.props.noFocus) return;
                        e.activeElement ? e.activeElement.focus() : e.focus()
                    } else "closing" === this.state.transition && e.blur()
                }
            }, {
                key: "shouldComponentUpdate",
                value: function(e, t) {
                    return t.transition !== this.state.transition || t.animation !== this.state.animation
                }
            }, {
                key: "open",
                value: function(e) {
                    switch (e = e || this.props.openAnimation, this.state.transition) {
                        case "opened":
                            break;
                        case "opening":
                        case "closing":
                        case "closed":
                            "immediate" !== e && e ? this.setState({
                                transition: "opening",
                                animation: e
                            }) : this.setState({
                                transition: "opened",
                                animation: ""
                            })
                    }
                }
            }, {
                key: "focus",
                value: function() {
                    var e = f.default.findDOMNode(this.refs.shadow).firstChild;
                    e && e.focus()
                }
            }, {
                key: "close",
                value: function(e) {
                    switch (e = e || this.props.closeAnimation, this.state.transition) {
                        case "closed":
                            break;
                        case "opening":
                        case "closing":
                        case "opened":
                            "immediate" !== e && e ? this.setState({
                                transition: "closing",
                                animation: e
                            }) : this.setState({
                                transition: "closed",
                                animation: ""
                            })
                    }
                }
            }, {
                key: "render",
                value: function() {
                    return u.default.createElement("div", {
                        tabIndex: "-1",
                        className: "x-window " + this.state.animation,
                        "aria-hidden": "opened" === this.state.transition ? "false" : "true",
                        "data-transition-state": this.state.transition
                    }, u.default.createElement(m.default, {
                        ref: "shadow",
                        transition: this.state.transition,
                        animation: this.state.animation
                    }, this.props.children))
                }
            }]), t
        }(p.default);
        v.defaultProps = {
            openAnimation: "immediate",
            closeAnimation: "immediate",
            noFocus: !1
        }, v.propTypes = {
            openAnimation: u.default.PropTypes.string,
            closeAnimation: u.default.PropTypes.string,
            noFocus: u.default.PropTypes.bool
        }, t.default = v
    },
    132: function(e, t, n) {
        "use strict";

        function i(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function a(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function o(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function r(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var s = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            l = n(3),
            u = i(l),
            c = n(12),
            f = (i(c), function(e) {
                function t() {
                    return a(this, t), o(this, (t.__proto__ || Object.getPrototypeOf(t)).apply(this, arguments))
                }
                return r(t, e), s(t, [{
                    key: "isOpening",
                    value: function(e) {
                        var t = e || this.props;
                        return "opening" === t.transition || "opened" === t.transition && "immediate" === t.animation
                    }
                }, {
                    key: "isClosed",
                    value: function(e) {
                        return "closed" === (e || this.props).transition
                    }
                }, {
                    key: "shouldComponentUpdate",
                    value: function(e, t) {
                        return !!this.isOpening(e)
                    }
                }, {
                    key: "render",
                    value: function() {
                        return u.default.createElement("div", {
                            className: "shadow-window"
                        }, this.props.children)
                    }
                }]), t
            }(u.default.Component));
        f.defaultProps = {
            transition: "",
            animation: ""
        }, f.propTypes = {
            transition: u.default.PropTypes.string.isRequired,
            animation: u.default.PropTypes.string.isRequired
        }, t.default = f
    },
    199: function(e, t, n) {
        "use strict";

        function i(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function a(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function o(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function r(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var s = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            l = n(3),
            u = i(l),
            c = n(6),
            f = i(c),
            d = n(24),
            p = i(d),
            h = n(14),
            m = i(h),
            v = n(63),
            g = i(v),
            y = n(5),
            b = i(y),
            w = n(108),
            _ = i(w);
        n(222);
        var k = ["Contract WAP", "PAYG WAP"],
            S = [{
                l10nId: "apnConfigPostpay",
                value: k[0]
            }, {
                l10nId: "apnConfigPrepay",
                value: k[1]
            }],
            O = function(e) {
                function t(e) {
                    a(this, t);
                    var n = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                    return n.name = "APNSelection", n.FOCUS_SELECTOR = ".list-item", n.state = {
                        whichCard: 0
                    }, n
                }
                return r(t, e), s(t, [{
                    key: "componentDidMount",
                    value: function() {
                        this.navigator = new g.default(this.FOCUS_SELECTOR, this.element), this.updateSoftkeys(), b.default.register("setOperatorId", this)
                    }
                }, {
                    key: "updateSoftkeys",
                    value: function() {
                        m.default.register({
                            center: "select"
                        }, this.element)
                    }
                }, {
                    key: "onKeyDown",
                    value: function(e) {
                        switch (e.key) {
                            case "Enter":
                                var t = document.activeElement.dataset.value;
                                dump("apn_selection value = " + t + ", id = " + this.id);
                                var n = navigator.mozSettings.createLock(),
                                    i = {};
                                t === k[0] ? i["apn_default_show_" + this.id] = "wap.vodafone.co.uk" : t === k[1] ? i["apn_default_show_" + this.id] = "pp.vodafone.co.uk" : i["apn_default_show_" + this.id] = "", n.set(i), this.hide(), 0 == this.id && _.default.start();
                                break;
                            case "Backspace":
                                e.preventDefault(), b.default.request("back")
                        }
                    }
                }, {
                    key: "hide",
                    value: function() {
                        b.default.request("closeSheet", "apnselection"), this._lock && this._lock.unlock && (dump("apn_selection unlock the _lock"), this._lock.unlock(), this._lock = null)
                    }
                }, {
                    key: "setOperatorId",
                    value: function(e) {
                        dump("apn_selection.js setOperatorId id = " + e), this._lock = navigator.requestWakeLock("screen"), this.id = e, this.setState({
                            whichCard: e
                        })
                    }
                }, {
                    key: "render",
                    value: function() {
                        var e = this,
                            t = [];
                        return S.forEach(function(e) {
                            var n = e.value === k[0];
                            t.push(u.default.createElement("li", {
                                tabIndex: "-1",
                                className: "list-item",
                                "data-value": e.value
                            }, u.default.createElement("p", {
                                "data-l10n-id": e.l10nId
                            }), u.default.createElement("div", {
                                className: "apnselection-key"
                            }, u.default.createElement("i", {
                                className: "icon control",
                                "data-icon": n ? "radio-on" : "radio-off",
                                "aria-hidden": "true"
                            }), u.default.createElement("input", {
                                type: "radio",
                                checked: n,
                                className: "hidden"
                            }))))
                        }), u.default.createElement("section", {
                            role: "region",
                            tabIndex: "-1",
                            onKeyDown: function(t) {
                                e.onKeyDown(t)
                            },
                            ref: function(t) {
                                e.element = t
                            }
                        }, u.default.createElement("header", {
                            class: "apnheader"
                        }, u.default.createElement("h1", {
                            "data-l10n-id": 0 == this.state.whichCard ? "apn-sim1" : "apn-sim2"
                        })), u.default.createElement("ul", {
                            id: "apnconfig-list"
                        }, t), u.default.createElement("p", {
                            "data-l10n-id": "apn-defaultconfig-message"
                        }, "Set which type of apn to show"))
                    }
                }]), t
            }(f.default);
        t.default = (0, p.default)(O, "immediate", "immediate")
    },
    200: function(e, t, n) {
        "use strict";

        function i(e) {
            if (e && e.__esModule) return e;
            var t = {};
            if (null != e)
                for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
            return t.default = e, t
        }

        function a(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function o(e) {
            if (Array.isArray(e)) {
                for (var t = 0, n = Array(e.length); t < e.length; t++) n[t] = e[t];
                return n
            }
            return Array.from(e)
        }

        function r(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function s(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function l(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }

        function u(e) {
            return p.default.createElement("div", {
                className: "app-tile",
                style: f({}, e.renderProps.orderStyle)
            }, p.default.createElement("div", {
                tabIndex: "-1",
                role: "menuitem",
                className: "app",
                key: e.uid,
                onClick: e.launch
            }, p.default.createElement("div", {
                className: "app__icon",
                style: e.renderProps.iconStyle
            }, p.default.createElement("div", {
                className: "app__icon--hq",
                style: {
                    backgroundImage: "url('" + e.icon_url_hq + "')"
                }
            })), p.default.createElement("div", {
                className: "app__name"
            }, e.name)))
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var c = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            f = Object.assign || function(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = arguments[t];
                    for (var i in n) Object.prototype.hasOwnProperty.call(n, i) && (e[i] = n[i])
                }
                return e
            },
            d = n(3),
            p = a(d),
            h = n(6),
            m = a(h),
            v = n(24),
            g = a(v),
            y = n(41),
            b = a(y),
            w = n(14),
            _ = a(w),
            k = n(5),
            S = a(k),
            O = n(42),
            E = a(O),
            C = n(10),
            I = i(C),
            P = n(43),
            M = a(P),
            T = n(22),
            j = a(T);
        n(223);
        var A = ["Siberian Strike", "Danger Dash", "Castle Of Magic", "Nitro Street Run 2"],
            L = ["Assistant", "Maps", "Google Search", "YouTube", "Twitter"],
            D = function(e) {
                function t(e) {
                    r(this, t);
                    var n = s(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                    return n.name = "AppList", n.navItemThrottleTime = 60, n.menuOptions = [{
                        id: "move",
                        tags: ["grid", "list"],
                        callback: n.enterReorderMode.bind(n)
                    }, {
                        id: "grid-view",
                        tags: ["list", "single"],
                        callback: n.switchViewMode.bind(n, "grid")
                    }, {
                        id: "list-view",
                        tags: ["grid", "single"],
                        callback: n.switchViewMode.bind(n, "list")
                    }, {
                        id: "uninstall",
                        callback: function() {
                            n.element.focus(), n.focusIfPossible(), b.default.uninstallApp(n.state.apps[n.focusIndex].mozApp)
                        }
                    }], n.initFocus = [0, 0], n.state = {
                        col: n.props.col,
                        apps: [],
                        viewMode: n.props.viewMode,
                        focus: n.initFocus
                    }, n.gridsPerPage = n.props.col * n.props.row, n.onKeyDown = n.onKeyDown.bind(n), n.onFocus = n.onFocus.bind(n), n.currentPage = 0, n.getLauncherApps(), j.default.addObserver("custom.launcher.apps", n), window.addEventListener("visibilitychange", function() {
                        var e = document.activeElement;
                        document.hidden && n.appElements && [].concat(o(n.appElements)).includes(e) && (n.isStickyApp = !0, e && e.classList.add("is-focus-app"))
                    }), I.asyncLocalStorage.getItem("app-view-mode").then(function(e) {
                        e && n.switchViewMode(e)
                    }), n
                }
                return l(t, e), c(t, [{
                    key: "getLauncherApps",
                    value: function() {
                        var e = this;
                        j.default.get("custom.launcher.apps").then(function(t) {
                            e.custom_apps = t, e.updateApps()
                        })
                    }
                }, {
                    key: "_observe_custom.launcher.apps",
                    value: function(e) {
                        this.custom_apps = e, this.updateApps()
                    }
                }, {
                    key: "customAppHandler",
                    value: function(e) {
                        for (var t = [], n = [], i = b.default.getDataMcc(), a = b.default.getSimmcc(), o = 0; o < e.length; o++)
                            if ("TIMGate" != e[o].manifest.name) L.includes(e[o].manifest.name) ? a ? "460" !== a && t.push(e[o]) : "460" !== i && t.push(e[o]) : A.includes(e[o].manifest.name) ? n.push(e[o]) : t.push(e[o]);
                            else {
                                var r = b.default.getDataMccmnc(),
                                    s = b.default.getSimmccmnc();
                                "22201" != r && "22201" != s || t.push(e[o])
                            } if (this.custom_apps)
                            for (var l = this.custom_apps.split(","), u = 0; u < n.length; u++) {
                                var c = A.indexOf(n[u].manifest.name).toString();
                                l.indexOf(c) !== -1 && t.push(n[u])
                            }
                        return t.sort(function(e, t) {
                            return e.order - t.order
                        }), t
                    }
                }, {
                    key: "componentDidMount",
                    value: function() {
                        var e = this;
                        b.default.on("ready", this.updateApps.bind(this)), b.default.on("change", this.updateApps.bind(this)), S.default.register("show", this), S.default.register("hide", this), E.default.register(this.element), this.element.addEventListener("animationstart", function() {
                            e.isAnimationEnd = !1
                        }), this.element.addEventListener("animationend", function() {
                            e.isAnimationEnd = !0
                        })
                    }
                }, {
                    key: "componentDidUpdate",
                    value: function() {
                        this.focusIfPossible(), this.updateSoftKeys(), this.scrollIntoViewIfPossible()
                    }
                }, {
                    key: "scrollIntoViewIfPossible",
                    value: function() {
                        switch (this.state.viewMode) {
                            case "grid":
                                this.goPage(this.getPage(this.state.reorderMode ? this.reorder.focus[0] : this.state.focus[0]));
                                break;
                            case "list":
                                this.scrollIntoViewForListView();
                                break;
                            case "single":
                                document.activeElement.scrollIntoView()
                        }
                    }
                }, {
                    key: "scrollIntoViewForListView",
                    value: function() {
                        if (!this.wrapperBondary) {
                            var e = this.appElements && this.appElements[0].offsetParent;
                            if (!e) return;
                            var t = e.getBoundingClientRect();
                            this.wrapperBondary = {
                                top: t.top,
                                bottom: window.innerHeight - t.bottom
                            }
                        }
                        var n = document.activeElement,
                            i = n.getBoundingClientRect();
                        i.top < this.wrapperBondary.top ? n.scrollIntoView(!0) : i.bottom > window.innerHeight - this.wrapperBondary.bottom && n.scrollIntoView(!1)
                    }
                }, {
                    key: "getPage",
                    value: function(e) {
                        return Math.floor(e / this.props.row)
                    }
                }, {
                    key: "goPage",
                    value: function() {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : this.currentPage;
                        !this.appElements || this.state.pageLength < 2 || this.currentPage !== e && (void 0 === this.pageOffsetY && (this.pageOffsetY = this.appElements[this.gridsPerPage].offsetTop - this.appElements[0].offsetTop), this._container.scrollTop = this.pageOffsetY * e, this.currentPage = e)
                    }
                }, {
                    key: "updateApps",
                    value: function() {
                        var e = this,
                            t = this.element.contains(document.activeElement),
                            n = this.appHandler(b.default.apps);
                        n = this.customAppHandler(n), this.setState(function(t) {
                            return t.apps = n, t.pageLength = Math.ceil(t.apps.length / e.gridsPerPage), t
                        }, function() {
                            t && (e.focus(), e.focusIfPossible())
                        })
                    }
                }, {
                    key: "updateSoftKeys",
                    value: function() {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
                            center: "select",
                            right: "options"
                        };
                        this.state.reorderMode && (e = {
                            center: "set",
                            right: "",
                            left: "cancel"
                        }), _.default.register(e, this.element)
                    }
                }, {
                    key: "onFocus",
                    value: function() {
                        return this.isStickyApp && (this.isStickyApp = !1, document.querySelector(".is-focus-app").classList.remove("is-focus-app")), this.element === document.activeElement ? (this.focusIfPossible(), this.scrollIntoViewIfPossible(), void this.updateSoftKeys()) : (this.element.contains(document.activeElement) || this.element.focus(), void this.updateSoftKeys())
                    }
                }, {
                    key: "focusIfPossible",
                    value: function() {
                        if (this.element.contains(document.activeElement)) {
                            var e = this.getFocusGridElement();
                            return e ? void e.focus() : void this.setState({
                                focus: this.initFocus
                            })
                        }
                    }
                }, {
                    key: "getFocusGridElement",
                    value: function() {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : I.rowColToIndex(this.state.focus, this.state.col),
                            t = this.state.apps.length - 1;
                        return e > t && (e = t, this.state.focus = I.indexToRowCol(e, this.state.col)), this.focusIndex = e, this.appElements || (this.appElements = this.element.getElementsByClassName("app")), this.appElements[e]
                    }
                }, {
                    key: "enterReorderMode",
                    value: function() {
                        this.setState({
                            reorderMode: !0
                        }), this.reorder = {
                            target: this.element.querySelectorAll(".app-tile")[this.focusIndex],
                            focus: this.state.focus,
                            focusIndex: this.focusIndex
                        }
                    }
                }, {
                    key: "exitReorderMode",
                    value: function(e) {
                        var t = this;
                        this.setState(function(n) {
                            if (n.reorderMode = !1, e) n.focus = [].concat(o(t.reorder.focus));
                            else {
                                var i = t.focusIndex;
                                n.apps[i].renderProps.orderStyle.order = t.getOrder(i)
                            }
                            return n
                        }, function() {
                            t.reorder = {}
                        })
                    }
                }, {
                    key: "saveOrderAndExit",
                    value: function() {
                        b.default.setAppOrder([].concat(o(this.state.apps)).sort(function(e, t) {
                            return e.renderProps.orderStyle.order - t.renderProps.orderStyle.order
                        }).map(function(e) {
                            return e.manifest.name
                        })), this.exitReorderMode(!0)
                    }
                }, {
                    key: "handleMoveGrid",
                    value: function(e) {
                        var t = this,
                            n = I.rowColToIndex(e, this.state.col),
                            i = this.focusIndex > n ? -1 : 1;
                        this.reorder.focus = e, this.setState(function(e) {
                            return e.apps[t.focusIndex].renderProps.orderStyle.order = t.getOrder(n, i), e
                        })
                    }
                }, {
                    key: "switchViewMode",
                    value: function() {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "grid",
                            t = "grid" === e ? this.props.col : 1,
                            n = I.rowColToIndex(this.state.focus, this.state.col);
                        this.currentPage = null, this.setState({
                            focus: I.indexToRowCol(n, t),
                            col: t,
                            viewMode: e
                        }), I.asyncLocalStorage.setItem("app-view-mode", e)
                    }
                }, {
                    key: "navItem",
                    value: function(e) {
                        var t = this;
                        this.navItemTimer || (this.navItemTimer = setTimeout(function() {
                            window.clearTimeout(t.navItemTimer), t.navItemTimer = null
                        }, this.navItemThrottleTime), this.state.reorderMode ? this.handleMoveGrid(e) : this.setState({
                            focus: e
                        }))
                    }
                }, {
                    key: "onKeyDown",
                    value: function(e) {
                        var t = this,
                            n = this.state.reorderMode,
                            i = void 0,
                            a = e.key;
                        switch (a) {
                            case "ArrowLeft":
                            case "ArrowRight":
                                if ("grid" !== this.state.viewMode) return;
                            case "ArrowUp":
                            case "ArrowDown":
                                if (!this.isAnimationEnd) return void e.preventDefault();
                                var o = n ? this.reorder.focus || this.state.focus : this.state.focus;
                                i = I.navGrid({
                                    currentRowCol: o,
                                    dir: a,
                                    col: this.state.col,
                                    total: this.state.apps.length
                                }), this.navItem(i);
                                break;
                            case "Call":
                                M.default.launch("manifestURL", "app://communications.gaiamobile.org/manifest.webapp");
                                break;
                            case "SoftRight":
                                if (!n) {
                                    var r = this.state.apps[this.focusIndex].mozApp,
                                        s = this.menuOptions.filter(function(e) {
                                            return "uninstall" === e.id ? r.removable : !!e.tags && e.tags.includes(t.state.viewMode)
                                        });
                                    S.default.request("showOptionMenu", {
                                        options: s
                                    })
                                }
                                break;
                            case "SoftLeft":
                                n && this.exitReorderMode();
                                break;
                            case "Enter":
                                n ? this.saveOrderAndExit() : e.target.click();
                                break;
                            case "EndCall":
                            case "BrowserBack":
                            case "Backspace":
                                n ? this.exitReorderMode() : (this.setState({
                                    focus: this.initFocus
                                }), S.default.request("closeSheet", "appList"))
                        }
                        i && (e.stopPropagation(), e.preventDefault())
                    }
                }, {
                    key: "appHandler",
                    value: function(e) {
                        var t = this;
                        return e.filter(function(e) {
                            return !e.shouldHide
                        }).sort(function(e, t) {
                            return e.order - t.order
                        }).map(function(e, n) {
                            return e.renderProps = {
                                orderStyle: {
                                    order: t.getOrder(n)
                                },
                                iconStyle: {
                                    color: e.theme,
                                    backgroundImage: "url('" + e.icon_url + "')"
                                }
                            }, e
                        })
                    }
                }, {
                    key: "renderPagination",
                    value: function() {
                        var e = void 0,
                            t = this.getPage(this.state.reorderMode ? this.reorder.focus[0] : this.state.focus[0]);
                        if (this.state.pageLength > 1) {
                            var n = Array(this.state.pageLength).fill().map(function(e, n) {
                                var i = n === t ? "page-indicator active" : "page-indicator";
                                return p.default.createElement("div", {
                                    key: "page-indicator--" + n,
                                    className: i
                                })
                            });
                            e = p.default.createElement("div", {
                                className: "pagination"
                            }, n)
                        }
                        return e
                    }
                }, {
                    key: "getOrder",
                    value: function(e) {
                        return 1e3 * (e + 1 + .5 * (arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0))
                    }
                }, {
                    key: "render",
                    value: function() {
                        var e = this,
                            t = "grid" === this.state.viewMode ? this.renderPagination() : null,
                            n = this.state.apps.map(function(e) {
                                return p.default.createElement(u, f({
                                    key: e.manifestURL
                                }, e))
                            }),
                            i = ["appList", this.state.reorderMode ? "is-reordering" : ""].filter(Boolean).join(" ");
                        return p.default.createElement("div", {
                            className: i,
                            "data-view-mode": this.state.viewMode,
                            tabIndex: "-1",
                            onKeyDown: this.onKeyDown,
                            onFocus: this.onFocus,
                            ref: function(t) {
                                e.element = t
                            }
                        }, t, p.default.createElement("h1", {
                            className: "readout-only",
                            id: "all-apps",
                            "data-l10n-id": "all-apps"
                        }), p.default.createElement("div", {
                            className: "appList__container",
                            role: "heading",
                            "aria-labelledby": "all-apps",
                            ref: function(t) {
                                return e._container = t
                            }
                        }, p.default.createElement("div", {
                            className: "app-wall"
                        }, n)))
                    }
                }]), t
            }(m.default);
        D.defaultProps = {
            viewMode: "grid",
            col: 3,
            row: 3
        }, D.propTypes = {
            viewMode: p.default.PropTypes.string,
            col: p.default.PropTypes.number,
            row: p.default.PropTypes.number
        }, t.default = (0, g.default)(D, "immediate", "immediate")
    },
    201: function(e, t, n) {
        "use strict";

        function i(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function a(e) {
            if (Array.isArray(e)) {
                for (var t = 0, n = Array(e.length); t < e.length; t++) n[t] = e[t];
                return n
            }
            return Array.from(e)
        }

        function o(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function r(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function s(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var l = function() {
                function e(e, t) {
                    var n = [],
                        i = !0,
                        a = !1,
                        o = void 0;
                    try {
                        for (var r, s = e[Symbol.iterator](); !(i = (r = s.next()).done) && (n.push(r.value), !t || n.length !== t); i = !0);
                    } catch (l) {
                        a = !0, o = l
                    } finally {
                        try {
                            !i && s.return && s.return()
                        } finally {
                            if (a) throw o
                        }
                    }
                    return n
                }
                return function(t, n) {
                    if (Array.isArray(t)) return t;
                    if (Symbol.iterator in Object(t)) return e(t, n);
                    throw new TypeError("Invalid attempt to destructure non-iterable instance")
                }
            }(),
            u = Object.assign || function(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = arguments[t];
                    for (var i in n) Object.prototype.hasOwnProperty.call(n, i) && (e[i] = n[i])
                }
                return e
            },
            c = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            f = n(3),
            d = i(f),
            p = n(6),
            h = i(p),
            m = n(5),
            v = i(m),
            g = n(22),
            y = i(g);
        n(224), n(225);
        var b = function(e) {
            function t(e) {
                o(this, t);
                var n = r(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                return n.name = "Clock", n.initState = {
                    datetime: "",
                    timeForReadout: "",
                    date: "",
                    h1: "0",
                    h2: "0",
                    m1: "0",
                    m2: "0",
                    ampm: "",
                    visible: "true" === localStorage.getItem("home.clock.visible")
                }, n.state = u({}, n.initState), navigator.mozL10n.ready(function() {
                    null === navigator.mozHour12 && (navigator.mozHour12 = "true" === localStorage.getItem("locale.hour12")), n.refresh()
                }), n.digiKey = 0, n.digiIcons = function() {
                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "bold",
                        t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 10;
                    return n.digiKey += 1, [].concat(a(Array(t))).map(function(t, i) {
                        return d.default.createElement("i", {
                            key: "key_" + i + "_" + e + "_" + n.digiKey,
                            "data-icon": "numeric_" + i + "_" + e,
                            "aria-hidden": "true"
                        })
                    })
                }, n.iconsHtml = {
                    bold: n.digiIcons(),
                    light: n.digiIcons("light")
                }, n
            }
            return s(t, e), c(t, [{
                key: "focus",
                value: function() {}
            }, {
                key: "componentWillMount",
                value: function() {
                    y.default.addObserver("home.clock.visible", this), y.default.addObserver("locale.hour12", this)
                }
            }, {
                key: "componentDidMount",
                value: function() {
                    v.default.register("start", this), v.default.register("stop", this), window.addEventListener("moztimechange", this)
                }
            }, {
                key: "_handle_moztimechange",
                value: function() {
                    this.stop(), this.start()
                }
            }, {
                key: "_handle_timeformatchange",
                value: function() {
                    this.refresh()
                }
            }, {
                key: "_handle_visibilitychange",
                value: function() {
                    "visible" === document.visibilityState ? this.start() : this.stop();
                }
            }, {
                key: "start",
                value: function() {
                    var e = this,
                        t = new Date;
                    this.refresh(), this.timer = setTimeout(function() {
                        e.start()
                    }, 1e3 * (60 - t.getSeconds()))
                }
            }, {
                key: "refresh",
                value: function() {
                    var e = this;
                    this.state.visible && navigator.mozL10n.ready(function() {
                        var t = new Date,
                            n = "%a, %e %b";
                        navigator.language.startsWith("zh") ? n = "%-m月%e日 %a" : navigator.language.startsWith("ko") && (n = "%-m월%e일 %a");
                        var i = navigator.mozHour12 ? "%I" : "%H",
                            a = new navigator.mozL10n.DateTimeFormat,
                            o = a.localeFormat(t, n + " | %p | " + i + " | %M"),
                            r = o.split(" | "),
                            s = l(r, 4),
                            u = s[0],
                            c = s[1],
                            f = s[2],
                            d = s[3];
                        f = ("00" + f).slice(-2).split(""), d = d.split(""), e.setState({
                            datetime: a.localeFormat(t, "%Y-%m-%dT%T"),
                            timeForReadout: a.localeFormat(t, "homescreen %I:%M " + (navigator.mozHour12 ? "%p" : "") + ", %A %B %e"),
                            date: u,
                            h1: f[0],
                            h2: f[1],
                            m1: d[0],
                            m2: d[1],
                            ampm: c
                        })
                    })
                }
            }, {
                key: "_observe_locale.hour12",
                value: function(e) {
                    localStorage.setItem("locale.hour12", e)
                }
            }, {
                key: "_observe_home.clock.visible",
                value: function(e) {
                    localStorage.setItem("home.clock.visible", e), this.setState({
                        visible: e
                    }), this.stop(), e ? (this.start(), window.addEventListener("moztimechange", this), window.addEventListener("timeformatchange", this), window.addEventListener("visibilitychange", this)) : (window.removeEventListener("moztimechange", this), window.removeEventListener("timeformatchange", this), window.removeEventListener("visibilitychange", this))
                }
            }, {
                key: "stop",
                value: function() {
                    clearInterval(this.timer), this.timer = null
                }
            }, {
                key: "render",
                value: function() {
                    return this.state.visible ? d.default.createElement("time", {
                        className: "ClockComponent",
                        dateTime: this.state.datetime,
                        role: "menuitem",
                        "aria-label": this.state.timeForReadout
                    }, d.default.createElement("div", {
                        className: "clock-upper"
                    }, d.default.createElement("bdi", {
                        className: "clockDigi-container"
                    }, d.default.createElement("span", {
                        className: "hour clockDigi-box"
                    }, d.default.createElement("span", {
                        className: "clockDigi clockDigi--time",
                        "data-now": this.state.h1
                    }, this.iconsHtml.bold), d.default.createElement("span", {
                        className: "clockDigi clockDigi--time",
                        "data-now": this.state.h2
                    }, this.iconsHtml.bold)), d.default.createElement("div", {
                        className: "clock-colon"
                    }), d.default.createElement("span", {
                        className: "minute clockDigi-box"
                    }, d.default.createElement("span", {
                        className: "clockDigi clockDigi--time",
                        "data-now": this.state.m1
                    }, this.iconsHtml.bold), d.default.createElement("span", {
                        className: "clockDigi clockDigi--time",
                        "data-now": this.state.m2
                    }, this.iconsHtml.bold))), d.default.createElement("div", {
                        className: "clock-ampm primary",
                        "data-hour-24": !navigator.mozHour12
                    }, this.state.ampm)), d.default.createElement("hr", {
                        className: "clock-divider"
                    }), d.default.createElement("div", {
                        className: "date primary"
                    }, this.state.date)) : null
                }
            }]), t
        }(h.default);
        t.default = b
    },
    203: function(e, t, n) {
        "use strict";

        function i(e) {
            if (e && e.__esModule) return e;
            var t = {};
            if (null != e)
                for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
            return t.default = e, t
        }

        function a(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function o(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function r(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function s(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var l = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            u = n(3),
            c = a(u),
            f = n(5),
            d = a(f),
            p = n(6),
            h = a(p),
            m = n(24),
            v = a(m),
            g = n(204),
            y = a(g),
            b = n(205),
            w = a(b),
            _ = n(10),
            k = i(_),
            S = n(64),
            O = a(S),
            E = n(105),
            C = a(E);
        n(226), n(229);
        var I = function(e) {
            function t(e) {
                o(this, t);
                var n = r(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                return n.name = "Dialer", n.initState = {
                    dialerState: null,
                    matchedContact: null,
                    telNum: "",
                    suggestions: []
                }, n.state = Object.assign({}, n.initState), O.default.on("mmiloading", n.showLoading.bind(n)), O.default.on("mmiloaded", n.showAlert.bind(n)), O.default.on("ussd-received", n.onUssdReceived.bind(n)), n.children = {}, ["onKeyDown", "call", "hide", "updateTelNum", "focusInput"].forEach(function(e) {
                    n[e] = n[e].bind(n)
                }), n.timer = null, d.default.register("show", n), d.default.register("hide", n), d.default.register("resetCallingMarker", n), d.default.registerState("isShown", n), d.default.registerState("isCalling", n), n
            }
            return s(t, e), l(t, [{
                key: "componentDidMount",
                value: function() {
                    var e = this;
                    C.default.on("contact-changed", function() {
                        e.isShown && e.getSuggestions(e.state.telNum)
                    }), this.updateTelTypes()
                }
            }, {
                key: "onUssdReceived",
                value: function(e) {
                    if (O.default.mmiloading && d.default.request("hideDialog"), !e.message) return void d.default.request("showDialog", {
                        type: "alert",
                        header: "Error USSD case!",
                        content: JSON.stringify(e),
                        translated: !0,
                        noClose: !1
                    });
                    var t = navigator.mozMobileConnections[e.serviceId || 0].voice.network,
                        n = t ? t.shortName || t.longName : "";
                    d.default.request("showDialog", {
                        type: "alert",
                        header: n,
                        content: e.message.replace(/\\r\\n|\\r|\\n/g, "\n"),
                        translated: !0,
                        noClose: !1
                    })
                }
            }, {
                key: "show",
                value: function(e) {
                    this.isShown || this.isHidden() && (this.updateTelTypes(), d.default.request("openSheet", "dialer"), this.isShown = !0, this.element.focus(), e && (this.focusInput(), this.children.dialerInput.sendFirstChar(e)))
                }
            }, {
                key: "hide",
                value: function() {
                    this.isHidden() || d.default.request("closeSheet", "dialer"), this.isShown = !1, this.children.dialerInput.element.style.fontSize = "", this.setState(this.initState), this.children.dialerInput.setState({
                        telNum: ""
                    })
                }
            }, {
                key: "updateTelTypes",
                value: function() {
                    var e = this;
                    navigator.mozL10n.ready(function() {
                        e.telTypes = ["personal", "mobile", "home", "work", "fax-home", "fax-office", "fax-other"].reduce(function(e, t) {
                            return e[t] = k.toL10n(t), e
                        }, {})
                    })
                }
            }, {
                key: "isHidden",
                value: function() {
                    for (var e = this.element; e !== document.body && !e.classList.contains("hidden") && "closed" !== e.dataset.transitionState;) e = e.parentElement;
                    return e.classList.contains("hidden") || "closed" === e.dataset.transitionState
                }
            }, {
                key: "updateTelNum",
                value: function(e) {
                    var t = this,
                        n = {
                            telNum: e
                        };
                    e.length < 2 && (n.matchedContact = this.initState.matchedContact, n.suggestions = this.initState.suggestions), this.setState(n, function() {
                        0 === e.length ? t.hide() : O.default.instantDialIfNecessary(e) && (t.children.dialerInput.exitDialer(), O.default.dial(e)), e.length >= 4 && (clearTimeout(t.timer), t.timer = setTimeout(function() {
                            t.getSuggestions(e)
                        }, 500))
                    })
                }
            }, {
                key: "focusInput",
                value: function() {
                    this.stopRenderSteply(), this.children.dialerInput.element.focus()
                }
            }, {
                key: "focusSuggestions",
                value: function() {
                    var e = this;
                    this.state.suggestions.length && (this.children.dialerSuggestions.initFocus(), this.allSuggestions.keyword || setTimeout(function() {
                        e.renderSteply()
                    }, 0))
                }
            }, {
                key: "renderSteply",
                value: function() {
                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 10,
                        t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 50,
                        n = this.state.suggestions.length;
                    if (this.allSuggestions.length <= n) return void this.stopRenderSteply();
                    var i = this.allSuggestions.slice(0, n + e);
                    i.keyword = this.state.telNum, this.setState({
                        suggestions: i
                    }), this.suggestionRenderTimer = setTimeout(this.renderSteply.bind(this), t)
                }
            }, {
                key: "stopRenderSteply",
                value: function() {
                    this.suggestionRenderTimer && (window.clearTimeout(this.suggestionRenderTimer), this.suggestionRenderTimer = null)
                }
            }, {
                key: "call",
                value: function() {
                    var e = this,
                        t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                        n = t.number,
                        i = void 0 === n ? this.state.telNum : n,
                        a = t.isVideo,
                        o = void 0 !== a && a;
                    return this.isCalling ? void d.default.request("showDialog", {
                        ok: "skip",
                        cancel: "cancel",
                        type: "confirm",
                        content: "otherConnectionInUseMessage",
                        onOk: function() {
                            d.default.request("Dialer:resetCallingMarker")
                        }
                    }) : (this.isCalling = !0, this.stopRenderSteply(), void O.default.dial(i, o).then(function() {
                        e.isCalling = !1, d.default.request("Dialer:hide"), d.default.request("hideDialog")
                    }).catch(function() {
                        e.isCalling = !1
                    }))
                }
            }, {
                key: "getSuggestions",
                value: function(e) {
                    O.default.isValid(e) && k.contactNumFilter({
                        telNum: e
                    }).then(this.filterSuggestions.bind(this, e))
                }
            }, {
                key: "filterSuggestions",
                value: function(e, t) {
                    var n = this,
                        i = void 0,
                        a = t.reduce(function(t, a) {
                            return t.concat(a.tel.map(function(t) {
                                var o = {
                                    id: a.id,
                                    name: a.name && a.name[0],
                                    type: n.getL10nFromTelTypes(t.type[0] || "mobile"),
                                    number: t.value
                                };
                                return i || t.value !== e || (i = o), o
                            }))
                        }, []).filter(function(e) {
                            return -1 !== e.number.indexOf(n.state.telNum)
                        });
                    this.allSuggestions = a, this.renderSuggestions(a.slice(0, 5), i, e)
                }
            }, {
                key: "renderSuggestions",
                value: function(e, t, n) {
                    var i = this;
                    n === this.state.telNum && (e.keyword = n, this.setState({
                        matchedContact: t,
                        suggestions: e
                    }, function() {
                        i.children.dialerSuggestions.element.scrollTo(0, 0)
                    }))
                }
            }, {
                key: "onKeyDown",
                value: function(e) {
                    var t = e.key;
                    switch (e.stopPropagation(), t) {
                        case "EndCall":
                            this.hide();
                            break;
                        case "ArrowDown":
                            this.focusSuggestions()
                    }
                }
            }, {
                key: "getMatchedContactInfo",
                value: function() {
                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : this.state.matchedContact,
                        t = void 0;
                    return e && (t = [e.name, e.type].filter(Boolean).join(", ")), t
                }
            }, {
                key: "showAlert",
                value: function(e, t) {
                    this.resetCallingMarker(), d.default.request("Dialer:hide"), (e || t) && d.default.request("showDialog", {
                        type: "alert",
                        header: e,
                        content: k.toL10n(t),
                        translated: !0,
                        noClose: !1
                    })
                }
            }, {
                key: "resetCallingMarker",
                value: function() {
                    this.isCalling = !1
                }
            }, {
                key: "showLoading",
                value: function() {
                    d.default.request("Dialer:hide").then(function() {
                        d.default.request("showDialog", {
                            type: "alert",
                            content: "sending",
                            otherClass: "is-loading",
                            noClose: !1
                        })
                    })
                }
            }, {
                key: "getL10nFromTelTypes",
                value: function(e) {
                    return this.telTypes[e] || e
                }
            }, {
                key: "render",
                value: function() {
                    var e = this;
                    return c.default.createElement("div", {
                        className: "dialerBox",
                        tabIndex: "-1",
                        onKeyDown: this.onKeyDown,
                        ref: function(t) {
                            e.element = t
                        }
                    }, c.default.createElement("div", {
                        className: "dialer-header"
                    }, c.default.createElement("div", {
                        className: "dialer-state text-thi"
                    }, this.state.dialerState), c.default.createElement(y.default, {
                        ref: function(t) {
                            e.children.dialerInput = t
                        },
                        dial: this.call,
                        exitDialer: this.hide,
                        updateTelNum: this.updateTelNum
                    }), c.default.createElement("div", {
                        className: "dialer-info text-thi"
                    }, this.getMatchedContactInfo())), c.default.createElement(w.default, {
                        ref: function(t) {
                            e.children.dialerSuggestions = t
                        },
                        suggestions: this.state.suggestions,
                        exitSuggestions: this.focusInput,
                        dial: this.call
                    }))
                }
            }]), t
        }(h.default);
        t.default = (0, v.default)(I, "immediate", "immediate")
    },
    204: function(e, t, n) {
        "use strict";

        function i(e) {
            if (e && e.__esModule) return e;
            var t = {};
            if (null != e)
                for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
            return t.default = e, t
        }

        function a(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function o(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function r(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function s(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var l = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            u = n(3),
            c = a(u),
            f = n(6),
            d = a(f),
            p = n(5),
            h = a(p),
            m = n(14),
            v = a(m),
            g = n(22),
            y = a(g),
            b = n(129),
            w = a(b),
            _ = n(10),
            k = i(_),
            S = n(111),
            O = a(S),
            E = n(65),
            C = a(E),
            I = n(43),
            P = a(I),
            M = function(e) {
                function t(e) {
                    o(this, t);
                    var n = r(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                    return n.name = "DialerInput", n.SPECIAL_CHARS = ["*", "+", ","], n.telNum = "", n.fontStyles = "", n.isVTSupported = !1, LazyLoader.load(["shared/js/dialer/tone_player.js"], function() {
                        TonePlayer.init("normal"), TonePlayer.gTonesFrequencies = {
                            1: [697, 1209],
                            2: [697, 1336],
                            3: [697, 1477],
                            A: [697, 1633],
                            4: [770, 1209],
                            5: [770, 1336],
                            6: [770, 1477],
                            B: [770, 1633],
                            7: [852, 1209],
                            8: [852, 1336],
                            9: [852, 1477],
                            C: [852, 1633],
                            "*": [941, 1209],
                            0: [941, 1336],
                            "#": [941, 1477],
                            D: [941, 1633],
                            ",": [941, 1209],
                            "+": [941, 1336]
                        }
                    }), n.specialCharsCount = n.SPECIAL_CHARS.length, n.onKeyPress = n.onKeyPress.bind(n), n.onKeyDown = n.onKeyDown.bind(n), n.onKeyUp = n.onKeyUp.bind(n), n.onInput = n.onInput.bind(n), n
                }
                return s(t, e), l(t, [{
                    key: "componentDidMount",
                    value: function() {
                        this.element.setAttribute("x-inputmode", "native"), y.default.addObserver("phone.ring.keypad", this), this.updateSoftKeys(), this.getFontStyles(), this.getVTSupportability()
                    }
                }, {
                    key: "componentDidUpdate",
                    value: function() {
                        this.updateSoftKeys()
                    }
                }, {
                    key: "updateSoftKeys",
                    value: function() {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
                            left: "contacts",
                            center: "call",
                            right: "options"
                        };
                        navigator.mozMobileConnections.length > 1 && !C.default.isAlwaysAsk() && (e.center = {
                            text: "call"
                        }, void 0 !== C.default.cardIndex && (e.center.icon = "sim-" + (C.default.cardIndex + 1))), v.default.register(e, this.element)
                    }
                }, {
                    key: "onInput",
                    value: function() {
                        var e = this.element.value;
                        this.props.updateTelNum(e), this.telNum = e, this.updateFontSize(e), "" === e && this.exitDialer()
                    }
                }, {
                    key: "onKeyPress",
                    value: function(e) {
                        e.preventDefault()
                    }
                }, {
                    key: "onKeyUp",
                    value: function(e) {
                        var t = O.default.translate(e.key);
                        "Backspace" === t && this.clearLongpressDeleteTimer(), "0" === t && (window.clearTimeout(this.longpressSpecialChar), this.longpressSpecialChar = null)
                    }
                }, {
                    key: "onKeyDown",
                    value: function(e) {
                        var t = this,
                            n = O.default.translate(e.key);
                        if (!this.longpressDeleteTimer && (!h.default.query("Dialer.isCalling") || "Call" === n && "Enter" === n)) switch (n) {
                            case "1":
                            case "2":
                            case "3":
                            case "4":
                            case "5":
                            case "6":
                            case "7":
                            case "8":
                            case "9":
                            case "0":
                            case "#":
                            case "+":
                            case "*":
                                if (e.stopPropagation && e.stopPropagation(), "0" === n && void 0 !== e.target && k.isLandscape && (window.clearTimeout(this.longpressSpecialChar), this.longpressSpecialChar = setTimeout(function() {
                                        var e = t.SPECIAL_CHARS[2],
                                            n = t.element.selectionStart,
                                            i = t.element.value;
                                        t.replaceLeftChar(e, n, i), t.playKeyTone(e)
                                    }, 1e3)), !k.isLandscape && "*" === n && -1 !== this.SPECIAL_CHARS.indexOf(this.lastKeyinChar) && this.getNowTime() - this.lastInputTime < 1e3) {
                                    var i = this.element,
                                        a = i.selectionStart,
                                        o = i.value,
                                        r = o[a - 1],
                                        s = this.SPECIAL_CHARS.indexOf(r);
                                    this.element.value = o.slice(0, a - 1) + o.slice(a), this.element.setSelectionRange(a - 1, a - 1), n = this.SPECIAL_CHARS[(s + 1) % this.specialCharsCount]
                                }
                                e.preventDefault && e.preventDefault(), this.insertKeyAtCaret(n), this.playKeyTone(n), this.lastKeyinChar = n, this.lastInputTime = this.getNowTime(), this.onInput();
                                break;
                            case "Backspace":
                                e.stopPropagation(), this.longpressDeleteTimer = setTimeout(this.longpressDelete.bind(this), 1e3);
                                break;
                            case "SoftLeft":
                                e.stopPropagation(), P.default.launch("manifestURL", "app://contact.gaiamobile.org/manifest.webapp");
                                break;
                            case "SoftRight":
                                e.stopPropagation(), this.handleTelNumber();
                                break;
                            case "Enter":
                            case "Call":
                                e.preventDefault(), e.stopPropagation(), this.props.dial({
                                    number: this.telNum
                                });
                                break;
                            case "ArrowDown":
                            case "ArrowUp":
                                e.preventDefault();
                                break;
                            case "ArrowLeft":
                            case "ArrowRight":
                                this.lastKeyinChar = null;
                                break;
                            default:
                                e.stopPropagation(), e.preventDefault()
                        }
                    }
                }, {
                    key: "_observe_phone.ring.keypad",
                    value: function(e) {
                        this._keypadSoundIsEnabled = e
                    }
                }, {
                    key: "insertKeyAtCaret",
                    value: function(e) {
                        var t = this.element.selectionEnd,
                            n = this.element.value;
                        this.element.value = n.substr(0, t) + e + n.substr(t), this.element.selectionEnd = t + 1
                    }
                }, {
                    key: "sendFirstChar",
                    value: function(e) {
                        this.element.value = "", this.onKeyDown({
                            key: e
                        }), this.getFontStyles()
                    }
                }, {
                    key: "getNowTime",
                    value: function() {
                        return +new Date
                    }
                }, {
                    key: "replaceLeftChar",
                    value: function(e, t, n) {
                        var i = t - 1;
                        this.element.value = n.substr(0, i) + e + n.substr(i + e.length), this.element.setSelectionRange(t, t)
                    }
                }, {
                    key: "clearLongpressDeleteTimer",
                    value: function() {
                        window.clearTimeout(this.longpressDeleteTimer), this.longpressDeleteTimer = null
                    }
                }, {
                    key: "longpressDelete",
                    value: function() {
                        this.clearLongpressDeleteTimer(), this.deleteAllText()
                    }
                }, {
                    key: "deleteAllText",
                    value: function() {
                        this.element.value = "", this.onInput()
                    }
                }, {
                    key: "playKeyTone",
                    value: function(e) {
                        this._keypadSoundIsEnabled && TonePlayer.start(TonePlayer.gTonesFrequencies[e], !0)
                    }
                }, {
                    key: "handleTelNumber",
                    value: function() {
                        var e = this,
                            t = [{
                                id: "add-to-existing-contact",
                                callback: function() {
                                    k.sendNumberToContact({
                                        name: "update",
                                        telNum: e.telNum
                                    })
                                }
                            }, {
                                id: "create-new-contact",
                                callback: function() {
                                    k.sendNumberToContact({
                                        name: "new",
                                        telNum: e.telNum
                                    })
                                }
                            }];
                        h.default.request("showOptionMenu", {
                            options: t,
                            onCancel: function() {
                                return e.element.focus()
                            }
                        })
                    }
                }, {
                    key: "getFontStyles",
                    value: function() {
                        var e = this;
                        this.fontStyles = function() {
                            var t = window.getComputedStyle(e.element);
                            return t ? ["font-style", "font-weight", "font-size", "font-family"].map(function(e) {
                                return t[e]
                            }).join(" ") : ""
                        }()
                    }
                }, {
                    key: "updateFontSize",
                    value: function(e) {
                        this.offsetWidth || (this.offsetWidth = this.element.offsetWidth);
                        var t = this.element.style.fontSize,
                            n = (0, w.default)({
                                text: e,
                                font: this.fontStyles,
                                space: this.offsetWidth,
                                min: 22,
                                max: 30
                            }).fontSize + "px";
                        t !== n && (this.element.style.fontSize = n)
                    }
                }, {
                    key: "getVTSupportability",
                    value: function() {
                        var e = this;
                        navigator.hasFeature && navigator.hasFeature("device.capability.vilte").then(function(t) {
                            e.isVTSupported = t
                        })
                    }
                }, {
                    key: "exitDialer",
                    value: function() {
                        this.clearLongpressDeleteTimer(), this.props.exitDialer()
                    }
                }, {
                    key: "render",
                    value: function() {
                        var e = this;
                        return c.default.createElement("input", {
                            tabIndex: "-1",
                            className: "dialer-input",
                            dir: "ltr",
                            onKeyPress: this.onKeyPress,
                            onKeyDown: this.onKeyDown,
                            onKeyUp: this.onKeyUp,
                            onInput: this.onInput,
                            ref: function(t) {
                                e.element = t
                            }
                        })
                    }
                }]), t
            }(d.default);
        M.defaultProps = {
            dial: null,
            exitDialer: null,
            updateTelNum: null
        }, M.propTypes = {
            dial: c.default.PropTypes.func,
            exitDialer: c.default.PropTypes.func,
            updateTelNum: c.default.PropTypes.func
        }, t.default = M
    },
    205: function(e, t, n) {
        "use strict";

        function i(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function a(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function o(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function r(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var s = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            l = n(3),
            u = i(l),
            c = n(6),
            f = i(c),
            d = n(5),
            p = i(d),
            h = n(14),
            m = i(h),
            v = n(63),
            g = i(v),
            y = n(65),
            b = i(y),
            w = function(e) {
                function t(e) {
                    a(this, t);
                    var n = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                    return n.name = "DialerSuggestions", n.onKeyDown = n.onKeyDown.bind(n), n
                }
                return r(t, e), s(t, [{
                    key: "componentDidMount",
                    value: function() {
                        this.suggestionNavigator = new g.default(".dialer-focusable", this.element), this.updateSoftKeys(), this.getVTSupportability()
                    }
                }, {
                    key: "componentDidUpdate",
                    value: function() {
                        this.updateSoftKeys()
                    }
                }, {
                    key: "updateSoftKeys",
                    value: function() {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
                            left: "",
                            center: "call",
                            right: this.isVTSupported ? "options" : ""
                        };
                        navigator.mozMobileConnections.length > 1 && !b.default.isAlwaysAsk() && (e.center = {
                            text: "call",
                            icon: "sim-" + (b.default.cardIndex + 1)
                        }), m.default.register(e, this.element)
                    }
                }, {
                    key: "getVTSupportability",
                    value: function() {
                        var e = this;
                        navigator.hasFeature && navigator.hasFeature("device.capability.vilte").then(function(t) {
                            e.isVTSupported = t
                        })
                    }
                }, {
                    key: "handleOption",
                    value: function() {
                        var e = this;
                        if (this.isVTSupported) {
                            var t = document.activeElement,
                                n = [{
                                    id: "video-call",
                                    callback: function() {
                                        t.focus(), e.props.dial({
                                            number: e.getFocusedSuggestion().number,
                                            isVideo: !0
                                        })
                                    }
                                }];
                            p.default.request("showOptionMenu", {
                                options: n,
                                onCancel: function() {
                                    return e.element.focus()
                                }
                            })
                        }
                    }
                }, {
                    key: "onKeyDown",
                    value: function(e) {
                        if (!p.default.query("Dialer.isCalling")) switch (e.key) {
                            case "SoftRight":
                                e.stopPropagation();
                                break;
                            case "Backspace":
                                e.stopPropagation(), e.preventDefault(), this.props.exitSuggestions();
                                break;
                            case "Enter":
                            case "Call":
                                e.stopPropagation(), this.props.dial({
                                    number: this.getFocusedSuggestion().number
                                })
                        }
                    }
                }, {
                    key: "getFocusedSuggestion",
                    value: function() {
                        var e = this.suggestionNavigator,
                            t = e._candidates.indexOf(e._currentFocus);
                        return this.props.suggestions[t]
                    }
                }, {
                    key: "initFocus",
                    value: function() {
                        var e = this;
                        setTimeout(function() {
                            var t = e.element.querySelector(".dialer-focusable");
                            t.focus(), e.suggestionNavigator.setFocus(t)
                        }, 0)
                    }
                }, {
                    key: "formatMatchedNum",
                    value: function(e) {
                        var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this.props.suggestions.keyword,
                            n = e.indexOf(t);
                        if (-1 !== n) {
                            var i = e.slice(0, n),
                                a = e.slice(n + t.length);
                            return u.default.createElement("span", {
                                dir: "ltr",
                                className: "dialerSuggestion__telNum",
                                dangerouslySetInnerHTML: {
                                    __html: i + "<mark>" + t + "</mark>" + a
                                }
                            })
                        }
                    }
                }, {
                    key: "suggestionsHtml",
                    value: function e() {
                        var t = this;
                        return this.props.suggestions.map(function(e, n) {
                            return u.default.createElement("li", {
                                key: "suggestions-" + n,
                                className: "dialer-focusable",
                                tabIndex: "-1"
                            }, u.default.createElement("div", {
                                className: "dialerSuggestion"
                            }, u.default.createElement("div", {
                                className: "dialerSuggestion__header text-pri"
                            }, e.name), u.default.createElement("div", {
                                className: "dialerSuggestion__detail text-sec"
                            }, e.type, " ", t.formatMatchedNum(e.number))))
                        })
                    }
                }, {
                    key: "render",
                    value: function() {
                        var e = this;
                        return u.default.createElement("ul", {
                            className: "dialerSuggestions",
                            onKeyDown: this.onKeyDown,
                            ref: function(t) {
                                e.element = t
                            }
                        }, this.suggestionsHtml())
                    }
                }]), t
            }(f.default);
        w.defaultProps = {
            dial: null,
            exitSuggestions: null,
            suggestions: null
        }, w.propTypes = {
            dial: u.default.PropTypes.func,
            exitSuggestions: u.default.PropTypes.func,
            suggestions: u.default.PropTypes.arrayOf(u.default.PropTypes.object)
        }, t.default = w
    },
    206: function(e, t, n) {
        "use strict";

        function i(e) {
            if (e && e.__esModule) return e;
            var t = {};
            if (null != e)
                for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
            return t.default = e, t
        }

        function a(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function o(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function r(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function s(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var l = n(30),
            u = a(l),
            c = n(10),
            f = i(c),
            d = function(e) {
                function t(e) {
                    o(this, t);
                    var n = r(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                    return n.name = "GridHelper", f.isLandscape ? n.grid = {
                        row: 2,
                        col: 4
                    } : n.grid = {
                        row: 3,
                        col: 3
                    }, n.emit("change", n.grid), n
                }
                return s(t, e), t
            }(u.default);
        t.default = new d
    },
    207: function(e, t, n) {
        "use strict";

        function i(e) {
            if (e && e.__esModule) return e;
            var t = {};
            if (null != e)
                for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
            return t.default = e, t
        }

        function a(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function o(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function r(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function s(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }

        function l(e) {
            var t = [O, e.isActive ? "is-active" : null, e.isShortcut ? "is-shortcut" : null].filter(Boolean).join(" ");
            return f.default.createElement("div", {
                key: e.name,
                className: "instantSettings__tile"
            }, f.default.createElement("button", {
                className: t,
                "aria-label": e.name,
                "data-icon": e.icon,
                "data-inactived-icon": e.iconInactived
            }, f.default.createElement("div", {
                className: "instantSettings__info"
            }, f.default.createElement("div", {
                className: "instantSettings__title",
                "data-l10n-id": e.title
            }), f.default.createElement("div", {
                className: "instantSettings__subtitle"
            }, S.toL10n(e.subtitle, e.subtitleArgs)))))
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var u = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            c = n(3),
            f = a(c),
            d = n(6),
            p = a(d),
            h = n(24),
            m = a(h),
            v = n(14),
            g = a(v),
            y = n(5),
            b = a(y),
            w = n(208),
            _ = a(w),
            k = n(10),
            S = i(k);
        n(221);
        var O = "instantSettings__icon",
            E = function(e) {
                function t(e) {
                    o(this, t);
                    var n = r(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                    return n.name = "InstantSettings", n.initIndex = 0, n.state = {
                        settings: _.default.settings,
                        focusIndex: n.initIndex
                    }, n.onKeyDown = n.onKeyDown.bind(n), n.onFocus = n.onFocus.bind(n), n.setRef = n.setRef.bind(n), n
                }
                return s(t, e), u(t, [{
                    key: "componentDidMount",
                    value: function() {
                        this.icons = this.element.getElementsByClassName(O), this.updateSettings(), _.default.on("change", this.updateSettings.bind(this))
                    }
                }, {
                    key: "componentDidUpdate",
                    value: function() {
                        this.focusIfPossible(), this.updateSoftKey()
                    }
                }, {
                    key: "updateSoftKey",
                    value: function() {
                        var e = this.state.focusIndex,
                            t = this.state.settings[e];
                        g.default.register({
                            left: "",
                            center: t.isDisabled ? "" : "select",
                            right: ""
                        }, this.element)
                    }
                }, {
                    key: "updateSettings",
                    value: function() {
                        this.setState(function(e) {
                            return e.settings = _.default.settings.filter(function(e) {
                                return !e.removed
                            }), e
                        })
                    }
                }, {
                    key: "onKeyDown",
                    value: function(e) {
                        e.preventDefault(), e.stopPropagation();
                        var t = e.key,
                            n = this.state.focusIndex;
                        if (_.default.volumeManagerTimer) return void _.default.click("volume", t);
                        switch (t) {
                            case "ArrowUp":
                            case "ArrowDown":
                            case "ArrowLeft":
                            case "ArrowRight":
                                this.handleNavGrid(n, t);
                                break;
                            case "Enter":
                                this.onCSK(n);
                                break;
                            case "EndCall":
                            case "Backspace":
                                this.exit()
                        }
                    }
                }, {
                    key: "onFocus",
                    value: function(e) {
                        e.target === this.element && (this.state.focusIndex = this.initIndex, this.focusIfPossible(), _.default.addSimCardObserver(), window.addEventListener("visibilitychange", this))
                    }
                }, {
                    key: "_handle_visibilitychange",
                    value: function() {
                        this.exit()
                    }
                }, {
                    key: "onCSK",
                    value: function() {
                        var e = this.state.settings[this.state.focusIndex];
                        if (!e.isDisabled) {
                            "launch" === _.default.click(e.name) && this.exit()
                        }
                    }
                }, {
                    key: "handleNavGrid",
                    value: function(e, t) {
                        var n = this.state.col,
                            i = S.navGrid({
                                currentRowCol: S.indexToRowCol(e, n),
                                dir: t,
                                col: n,
                                total: this.state.settings.length
                            }),
                            a = S.rowColToIndex(i, n);
                        a !== e && this.setState({
                            focusIndex: a
                        })
                    }
                }, {
                    key: "navGrid",
                    value: function() {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                            t = e.currentIndex,
                            n = void 0 === t ? this.initIndex : t,
                            i = e.dir,
                            a = e.col,
                            o = void 0 === a ? this.props.col : a,
                            r = e.total,
                            s = this.props.col - 1,
                            l = this.props.row - 1,
                            u = [n % o, Math.floor(n / o)],
                            c = u[0],
                            f = u[1],
                            d = S.isRtl(),
                            p = d ? -1 : 1;
                        switch (i) {
                            case "ArrowRight":
                                c = S.clamp(c + p, 0, s);
                                break;
                            case "ArrowLeft":
                                c = S.clamp(c - p, 0, s);
                                break;
                            case "ArrowUp":
                                f = S.clamp(f - 1, 0, l);
                                break;
                            case "ArrowDown":
                                f += 1
                        }
                        var h = f * o + c;
                        return "ArrowDown" === i && h >= r ? -1 : S.clamp(h, 0, r - 1)
                    }
                }, {
                    key: "exit",
                    value: function() {
                        _.default.removeSimCardObserver(), b.default.request("closeSheet", "instantSettings"), window.removeEventListener("visibilitychange", this)
                    }
                }, {
                    key: "isHidden",
                    value: function() {
                        return !this.element.offsetParent
                    }
                }, {
                    key: "focusIfPossible",
                    value: function() {
                        if (this.element && !this.isHidden()) {
                            var e = this.element;
                            this.icons && (e = this.icons[this.state.focusIndex] || this.icons[this.initIndex]), e.focus()
                        }
                    }
                }, {
                    key: "setRef",
                    value: function(e) {
                        this.element = e
                    }
                }, {
                    key: "render",
                    value: function() {
                        return f.default.createElement("section", {
                            className: "instantSettings",
                            tabIndex: "-1",
                            role: "heading",
                            "aria-labelledby": "instantSettings-title",
                            onKeyDown: this.onKeyDown,
                            onFocus: this.onFocus,
                            ref: this.setRef
                        }, f.default.createElement("div", {
                            className: "readout-only",
                            id: "instantSettings-title",
                            "data-l10n-id": "instant-settings"
                        }), f.default.createElement("main", {
                            className: "instantSettings__wall"
                        }, this.state && this.state.settings && this.state.settings.map(function(e) {
                            return l(e)
                        })))
                    }
                }]), t
            }(p.default);
        E.defaultProps = {
            col: 3,
            row: 3
        }, E.propTypes = {
            col: f.default.PropTypes.number,
            row: f.default.PropTypes.number
        }, t.default = (0, m.default)(E, "immediate", "immediate")
    },
    208: function(e, t, n) {
        "use strict";

        function i(e) {
            if (e && e.__esModule) return e;
            var t = {};
            if (null != e)
                for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
            return t.default = e, t
        }

        function a(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function o(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function r(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function s(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var l = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            u = n(15),
            c = a(u),
            f = n(22),
            d = a(f),
            p = n(104),
            h = a(p),
            m = n(110),
            v = a(m),
            g = n(43),
            y = a(g),
            b = n(10),
            w = i(b),
            _ = function(e) {
                function t() {
                    o(this, t);
                    var e = r(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this));
                    return e.name = "InstantSettingsStore", e.oriSettings = [{
                        name: "volume",
                        icon: "sound-max",
                        isShortcut: !0,
                        title: "volume",
                        order: {
                            portrait: 1,
                            landscape: 1
                        },
                        click: function(t) {
                            return e.volumeManagerTimer ? void("ArrowUp" !== t && "ArrowDown" !== t || (e.enterVolumeManagerMode(), navigator.volumeManager["request" + t.slice(5)]())) : (e.enterVolumeManagerMode(), void navigator.volumeManager.requestShow())
                        }
                    }, {
                        name: "brightness",
                        icon: "brightness",
                        isShortcut: !0,
                        title: "brightness",
                        subtitle: "percentage-number",
                        order: {
                            portrait: 2,
                            landscape: 2
                        },
                        cskType: "toggle",
                        click: e.toggleBrightness.bind(e)
                    }, {
                        name: "flashlight",
                        icon: "flashlight-on",
                        iconInactived: "flashlight-off",
                        title: "flashlight",
                        removed: !0,
                        order: {
                            portrait: 4,
                            landscape: 3
                        },
                        cskType: "toggle",
                        click: v.default.toggle.bind(v.default)
                    }, {
                        name: "camera",
                        icon: "camera",
                        isShortcut: !0,
                        title: "camera",
                        order: {
                            portrait: 7,
                            landscape: 6
                        },
                        cskType: "launch",
                        click: function() {
                            y.default.launch("manifestURL", "app://camera.gaiamobile.org/manifest.webapp")
                        }
                    }, {
                        name: "wifi-modem",
                        icon: "wifi-3-32px",
                        iconInactived: "wifi-1-32px",
                        title: "wifi-modem",
                        removed: !0,
                        observerSetting:"tethering.wifi.enabled",
                        order: {
                            portrait: 5,
                            landscape: -1
                        },
                        cskType: "toggle"
                    }, {
                        name: "airplane-mode",
                        icon: "airplane-mode",
                        iconInactived: "airplane-mode-off",
                        title: "airplane-mode",
                        observerSetting: "airplaneMode.enabled",
                        order: {
                            portrait: 8,
                            landscape: 7
                        },
                        cskType: "toggle"
                    }, {
                        name: "wifi",
                        icon: "wifi-32px",
                        iconInactived: "wifi-off-32px",
                        title: "wifi",
                        observerSetting: "wifi.enabled",
                        removed: !0,
                        order: {
                            portrait: 0,
                            landscape: 0
                        },
                        cskType: "toggle"
                    }, {
                        name: "network",
                        icon: "network-activity",
                        iconInactived: "network-activity-off",
                        title: "cellular-data",
                        observerSetting: "ril.data.enabled",
                        order: {
                            portrait: 3,
                            landscape: 4
                        },
                        cskType: "toggle"
                    }, {
                        name: "bluetooth",
                        icon: "bluetooth-32px",
                        iconInactived: "bluetooth-off-32px",
                        title: "bluetooth",
                        removed: !0,
                        observerSetting: "bluetooth.enabled",
                        order: {
                            portrait: 6,
                            landscape: 5
                        },
                        cskType: "toggle"
                    }], e.brightnessMap = {
                        100: .1,
                        10: .4,
                        40: .7,
                        70: 1
                    }, e.orderType = w.isLandscape ? "landscape" : "portrait", e
                }
                return s(t, e), l(t, [{
                    key: "start",
                    value: function() {
                        var e = this,
                            t = this.orderType;
                        this.settings = this.oriSettings.filter(function(e) {
                            return -1 !== e.order[t]
                        }).sort(function(e, n) {
                            return e.order[t] - n.order[t]
                        }), this.settings.forEach(function(t) {
                            t.observerSetting && !t.removed && e.initSettingObserver(t)
                        }), this.initSettingObserverForBrightness(), d.default.addObserver("airplaneMode.status", this), navigator.hasFeature && navigator.hasFeature("device.capability.torch").then(function(t) {
                            if (t) {
                                e.getSetting("flashlight").removed = !1, v.default.on("ready", e.updateFlashlightState.bind(e)), v.default.on("change", e.updateFlashlightState.bind(e))
                            }
                        }), navigator.hasFeature && navigator.hasFeature("device.capability.bt").then(function(t) {
                            if (t) {
                                navigator.mozBluetooth.defaultAdapter || (navigator.mozBluetooth.onattributechanged = function(e) {
                                    e.attrs.includes("defaultAdapter") && (navigator.mozBluetooth.onattributechanged = null)
                                });
                                var n = e.getSetting("bluetooth");
                                n.removed = !1, e.initSettingObserver(n)
                            }
                        }), navigator.hasFeature && navigator.hasFeature("device.capability.wifi").then(function(t) {
                            if (t) {
                                var n = e.getSetting("wifi");
                                n.removed = !1, e.initSettingObserver(n);
                                n=e.getSetting("wifi-modem");
                                n.removed = !1, e.initSettingObserver(n);
                            }
                        }), this.emit("change")
                    }
                }, {
                    key: "initSettingObserver",
                    value: function(e) {
                        var t = this;
                        d.default.addObserver(e.observerSetting, this), this["_observe_" + e.observerSetting] = function(n) {
                            var i = t.getSetting(e.name);
                            i.isActive = n, !0 === n ? i.subtitle = "on" : !1 === n ? i.subtitle = "off" : i.subtitle = n.toString(), t.emit("change")
                        }
                    }
                }, {
                    key: "initSettingObserverForBrightness",
                    value: function() {
                        var e = this;
                        d.default.addObserver("screen.brightness", this), this["_observe_screen.brightness"] = function(t) {
                            e.getSetting("brightness").subtitleArgs = {
                                number: 100 * t
                            }, e.emit("change")
                        }
                    }
                }, {
                    key: "_observe_airplaneMode.status",
                    value: function(e) {
                        var t = "enabling" === e || "disabling" === e;
                        this.getSetting("airplane-mode").isDisabled = t, this.emit("change")
                    }
                }, {
                    key: "updateFlashlightState",
                    value: function() {
                        dump("cck updateFlashlightState entry");
                        var e = this.getSetting("flashlight"),
                            t = v.default.flashlightManager.flashlightEnabled;
                        dump("cck updateFlashlightState _flashlightEnabled=" + t), e.isActive = t, e.subtitle = t ? "on" : "off", this.emit("change")
                    }
                }, {
                    key: "checkSimCardState",
                    value: function() {
                        var e = h.default.noSIMCardOnDevice() || h.default.noSIMCardConnectedToNetwork(),
                            t = -1 !== h.default.getSlots().map(function(e) {
                                return e.conn.voice.state
                            }).indexOf("searching"),
                            n = this.getSetting("network"),
                            i = this.getSetting("airplane-mode");
                        n.isDisabled = i.isActive || e, !t && e && n.isActive && this.toggleSetting(n), this.emit("change")
                    }
                }, {
                    key: "addSimCardObserver",
                    value: function() {
                        var e = this;
                        if (!this.isSimCardObserverAdded) {
                            this.isSimCardObserverAdded = !0, this.checkSimCardState();
                            var t = window.navigator.mozMobileConnections;
                            t && Array.from(t).forEach(function(t) {
                                t.addEventListener("voicechange", e)
                            }, this)
                        }
                    }
                }, {
                    key: "removeSimCardObserver",
                    value: function() {
                        var e = this;
                        this.isSimCardObserverAdded = !1;
                        var t = window.navigator.mozMobileConnections;
                        t && Array.from(t).forEach(function(t) {
                            t.removeEventListener("voicechange", e)
                        }, this)
                    }
                }, {
                    key: "_handle_voicechange",
                    value: function() {
                        this.checkSimCardState()
                    }
                }, {
                    key: "getIndex",
                    value: function(e) {
                        var t = this.settings.findIndex(function(t) {
                            return t.name === e
                        });
                        return t
                    }
                }, {
                    key: "getSetting",
                    value: function(e) {
                        var t = this.settings.find(function(t) {
                            return t.name === e
                        });
                        return t
                    }
                }, {
                    key: "toggleSetting",
                    value: function(e) {
                        var t = this;
                        e.isDisabled = !0, this.emit("change");
                        var n = function() {
                            e.isDisabled = !1, t.emit("change")
                        };
                        d.default.get(e.observerSetting).then(function(t) {
                            var i = {};
                            i[e.observerSetting] = !t, d.default.set(i).then(function() {
                                switch (e.name) {
                                    case "airplane-mode":
                                        break;
                                    case "bluetooth":
                                        w.toggleBletooth(t ? "disable" : "enable").then(function() {
                                            return n()
                                        }, function(e) {
                                            n()
                                        });
                                        break;
                                    default:
                                        n()
                                }
                            })
                        })
                    }
                }, {
                    key: "toggleBrightness",
                    value: function() {
                        var e = this.getSetting("brightness").subtitleArgs.number;
                        d.default.set({
                            "screen.brightness": this.brightnessMap[e] || .1
                        })
                    }
                }, {
                    key: "enterVolumeManagerMode",
                    value: function() {
                        var e = this;
                        this.volumeManagerTimer && this.exitVolumeManagerMode(), this.volumeManagerTimer = setTimeout(function() {
                            e.exitVolumeManagerMode()
                        }, 2e3)
                    }
                }, {
                    key: "exitVolumeManagerMode",
                    value: function() {
                        window.clearTimeout(this.volumeManagerTimer), this.volumeManagerTimer = null
                    }
                }, {
                    key: "click",
                    value: function(e, t) {
                        var n = this.getSetting(e);
                        if (n && !n.isDisabled)
                            if ("toggle" === n.cskType && n.observerSetting) this.toggleSetting(n);
                            else if (n.click) return n.click(t), n.cskType
                    }
                }]), t
            }(c.default),
            k = new _;
        k.start(), t.default = k
    },
    209: function(e, t, n) {
        "use strict";

        function i(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function a(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function o(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function r(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var s = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            l = n(3),
            u = i(l),
            c = n(6),
            f = i(c),
            d = n(14),
            p = i(d),
            h = n(201),
            m = i(h),
            v = n(213),
            g = i(v),
            y = n(5),
            b = i(y),
            w = n(42),
            _ = i(w),
            k = n(110),
            S = i(k),
            O = n(43),
            E = i(O),
            C = n(41),
            I = i(C);
        n(228);
        var P = function(e) {
            function t(e) {
                a(this, t);
                var n = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                return n.name = "MainView", n.longPressDuration = 1500, n.onKeyDown = n.onKeyDown.bind(n), n.onKeyUp = n.onKeyUp.bind(n), window.addEventListener("visibilitychange", function() {
                    document.hidden && (n._longPressActionTriggered = !1)
                }), n
            }
            return r(t, e), s(t, [{
                key: "componentDidMount",
                value: function() {
                    b.default.register("show", this), b.default.register("hide", this), p.default.register({
                        left: "notifications",
                        center: "icon=all-apps",
                        right: "shortcuts"
                    }, this.element), _.default.register(this.element)
                }
            }, {
                key: "onKeyDown",
                value: function(e) {
                    var t = this;
                    if (this.mcc = I.default.getDataMcc(), this.simmcc = I.default.getSimmcc(), "complete" === document.readyState && !b.default.query("LaunchStore.isLaunching")) {
                        var n = e.key;
                        if (!this._longPressTimer) {
                            switch (n) {
                                case "Call":
                                    return void E.default.launch("manifestURL", "app://communications.gaiamobile.org/manifest.webapp");
                                case "SoftLeft":
                                    E.default.launch("iac", "notice");
                                case "Enter":
                                    this.simmcc ? "460" !== this.simmcc && "250" !== this.simmcc || b.default.request("openSheet", "appList") : "460" !== this.mcc && "250" !== this.mcc || b.default.request("openSheet", "appList");
                                    break;
                                case "SoftRight":
                                    return void b.default.request("openSheet", "instantSettings")
                            }
                            this._longPressTimer = setTimeout(function() {
                                switch (t.clearLongPressTimer(), t._longPressActionTriggered = !0, n) {
                                    case "ArrowUp":
                                        S.default.toggle();
                                        break;
                                    case "Enter":
                                        t.simmcc ? "460" !== t.simmcc && "250" !== t.simmcc && I.default.apps.some(function(e) {
                                            "Assistant" === e.manifest.name && e.launch()
                                        }) : "460" !== t.mcc && "250" !== t.mcc && I.default.apps.some(function(e) {
                                            "Assistant" === e.manifest.name && e.launch()
                                        });
                                        break;
                                    default:
                                        t._longPressActionTriggered = !1
                                }
                            }, this.longPressDuration)
                        }
                    }
                }
            }, {
                key: "onKeyUp",
                value: function(e) {
                    var t = e.key;
                    if (this._longPressTimer && !b.default.query("LaunchStore.isLaunching")) {
                        if (this.clearLongPressTimer(), this._longPressActionTriggered) return void(this._longPressActionTriggered = !1);
                        switch (t) {
                            case "Backspace":
                                E.default.isLaunching && (E.default.isLaunching = !1);
                                break;
                            case "Enter":
                                b.default.request("openSheet", "appList")
                        }
                    }
                }
            }, {
                key: "clearLongPressTimer",
                value: function() {
                    this._longPressTimer && (clearTimeout(this._longPressTimer), this._longPressTimer = null)
                }
            }, {
                key: "show",
                value: function() {
                    this.element.classList.remove("hidden"), this.focus()
                }
            }, {
                key: "hide",
                value: function() {
                    this.element.classList.add("hidden")
                }
            }, {
                key: "focus",
                value: function() {
                    this.element.focus()
                }
            }, {
                key: "render",
                value: function() {
                    var e = this;
                    return u.default.createElement("div", {
                        id: "main-view",
                        tabIndex: "-1",
                        onKeyDown: this.onKeyDown,
                        onKeyUp: this.onKeyUp,
                        ref: function(t) {
                            e.element = t
                        }
                    }, u.default.createElement(g.default, null), u.default.createElement(m.default, null))
                }
            }]), t
        }(f.default);
        P.defaultProps = {
            open: null,
            close: null
        }, P.propTypes = {
            open: u.default.PropTypes.func,
            close: u.default.PropTypes.func
        }, t.default = P
    },
    210: function(e, t) {
        "use strict";

        function n(e) {
            var t = this,
                n = t[e],
                o = navigator.mozL10n.language.code || "",
                r = t.default_locale || "";
            return o in navigator.mozL10n.qps && ("name" === e || "description" === e || "short_name" === e) ? n = navigator.mozL10n.qps[navigator.language].translate(n) : t.locales && [o, o.substr(0, o.indexOf("-")), r, r.substr(0, o.indexOf("-"))].some(function(t) {
                return !(!this[t] || !this[t][e]) && (n = this[t][e], !0)
            }, t.locales), "object" !== ("undefined" == typeof n ? "undefined" : a(n)) || n instanceof Array || (n = new i(n)), n
        }

        function i(e) {
            for (var t in e) Object.defineProperty(this, t, {
                get: n.bind(e, t),
                enumerable: !0
            })
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var a = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
            return typeof e
        } : function(e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        };
        Object.defineProperty(i.prototype, "displayName", {
            get: function() {
                return this.short_name || this.name
            }
        }), t.default = i
    },
    211: function(e, t) {
        "use strict";

        function n(e) {
            return "A"
        }

        function i(e) {
            return e >= 0 && e < 32 ? e + 64 : e >= 32 && e < 96 ? e - 32 : e
        }

        function a(e, t) {
            var n = [212222, 222122, 222221, 121223, 121322, 131222, 122213, 122312, 132212, 221213, 221312, 231212, 112232, 122132, 122231, 113222, 123122, 123221, 223211, 221132, 221231, 213212, 223112, 312131, 311222, 321122, 321221, 312212, 322112, 322211, 212123, 212321, 232121, 111323, 131123, 131321, 112313, 132113, 132311, 211313, 231113, 231311, 112133, 112331, 132131, 113123, 113321, 133121, 313121, 211331, 231131, 213113, 213311, 213131, 311123, 311321, 331121, 312113, 312311, 332111, 314111, 221411, 431111, 111224, 111422, 121124, 121421, 141122, 141221, 112214, 112412, 122114, 122411, 142112, 142211, 241211, 221114, 413111, 241112, 134111, 111242, 121142, 121241, 114212, 124112, 124211, 411212, 421112, 421211, 212141, 214121, 412121, 111143, 111341, 131141, 114113, 114311, 411113, 411311, 113141, 114131, 311141, 411131, 211412, 211214, 211232, 23311120],
                a = 106,
                o = [];
            o.add = function(e) {
                var t = n[e];
                0 === this.length ? this.check = e : this.check = this.check + e * this.length, this.push(t || "UNDEFINED:" + e + "->" + t)
            }, o.add(38 + t.charCodeAt(0));
            for (var r = 0; r < e.length; r++) {
                var s = e.charCodeAt(r),
                    l = i(s);
                if (isNaN(l) || l < 0 || l > 106) throw new Error("Unrecognized character (" + s + ") at position " + r + " in code '" + e + "'.");
                o.add(l)
            }
            return o.push(n[o.check % 103], n[a]), o
        }

        function o(e) {
            for (var t = [], n = 0; n < e.length; n += 2) t.push('<div class="bar' + e.charAt(n) + " space" + e.charAt(n + 1) + '"></div>');
            return t.join("")
        }

        function r(e, t) {
            return arguments.length < 2 && (t = n(e)), o(a(e, t).join(""))
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var s = {
            code128: r
        };
        t.default = s
    },
    212: function(e, t, n) {
        "use strict";

        function i(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function a(e) {
            if (Array.isArray(e)) {
                for (var t = 0, n = Array(e.length); t < e.length; t++) n[t] = e[t];
                return n
            }
            return Array.from(e)
        }

        function o(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function r(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function s(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var l = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            u = n(3),
            c = i(u),
            f = n(6),
            d = i(f),
            p = n(24),
            h = i(p),
            m = n(14),
            v = i(m),
            g = n(5),
            y = i(g),
            b = n(211),
            w = i(b);
        n(230);
        var _ = function(e) {
            function t(e) {
                o(this, t);
                var n = r(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                n.name = "QRFace", n.initFocus = [0, 0], n.state = {
                    focus: n.initFocus
                }, y.default.register("show", n), y.default.register("hide", n), y.default.register("isShown", n), n.onFocus = n.onFocus.bind(n), n.onKeyDown = n.onKeyDown.bind(n);
                var i = [].concat(a(navigator.mozMobileConnections)).map(function(e, t) {
                    return e.getDeviceIdentities().then(function(e) {
                        if (e.imei) return e.imei;
                        var n = "Could not retrieve the " + "imei".toUpperCase() + " code for SIM " + t;
                        return Promise.reject(new Error(n))
                    })
                });
                return Promise.all(i).then(function(e) {
                    2 == e.length ? (document.getElementById("qrtitleone").innerHTML = "imei".toUpperCase() + " number", document.getElementById("qrnumberone").innerHTML = e[0], document.getElementById("qrimageone").innerHTML = w.default.code128(e[0], "A"), document.getElementById("qrtitleone").classList.add("titleqrone"), document.getElementById("qrtitletwo").innerHTML = "imei".toUpperCase() + " number", document.getElementById("qrnumbertwo").innerHTML = e[1], document.getElementById("qrimagetwo").innerHTML = w.default.code128(e[1], "A"), document.getElementById("qrtitletwo").classList.add("titleqrtwo")) : (document.getElementById("qrtitleone").classList.add("hidden"), document.getElementById("qrnumberone").classList.add("hidden"), document.getElementById("qrimageone").classList.add("hidden"), document.getElementById("qrtitletwo").innerHTML = "imei".toUpperCase() + " number", document.getElementById("qrnumbertwo").innerHTML = e[0], document.getElementById("qrimagetwo").innerHTML = w.default.code128(e[0], "A"), document.getElementById("qrtitletwo").classList.add("titleqr"))
                }), n
            }
            return s(t, e), l(t, [{
                key: "componentDidMount",
                value: function() {
                    y.default.register("show", this), y.default.register("hide", this), y.default.register("isShown", this), this.updateSoftKeys(), this.onFocus = this.onFocus.bind(this), this.onKeyDown = this.onKeyDown.bind(this)
                }
            }, {
                key: "onFocus",
                value: function() {
                    return this.element === document.activeElement ? void this.updateSoftKeys() : (this.element.focus(), void this.updateSoftKeys())
                }
            }, {
                key: "focusIfPossible",
                value: function() {
                    !this.element.contains(document.activeElement)
                }
            }, {
                key: "updateSoftKeys",
                value: function() {
                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
                        left: "",
                        center: "ok",
                        right: ""
                    };
                    v.default.register(e, this.element)
                }
            }, {
                key: "onKeyDown",
                value: function(e) {
                    switch (e.key) {
                        case "EndCall":
                        case "BrowserBack":
                        case "Backspace":
                        case "Enter":
                            e.stopPropagation(), e.preventDefault(), this.hide();
                            break;
                        case "1":
                        case "2":
                        case "3":
                        case "4":
                        case "5":
                        case "6":
                        case "7":
                        case "8":
                        case "9":
                        case "0":
                        case "*":
                        case "#":
                            e.stopPropagation(), e.preventDefault()
                    }
                }
            }, {
                key: "changeMyState",
                value: function() {
                    this.setState({
                        focus: [0, 2]
                    })
                }
            }, {
                key: "show",
                value: function() {
                    return y.default.query("QRFace.isShown", this) ? void this.focus() : void(this.isHidden() && (y.default.request("openSheet", "qrface"), this.isShown = !0, this.element.focus()))
                }
            }, {
                key: "isHidden",
                value: function() {
                    for (var e = this.element; e !== document.body && !e.classList.contains("hidden") && "closed" !== e.dataset.transitionState;) e = e.parentElement;
                    return e.classList.contains("hidden") || "closed" === e.dataset.transitionState
                }
            }, {
                key: "hide",
                value: function() {
                    y.default.request("closeSheet", "qrface"), this.isShown = !1
                }
            }, {
                key: "focus",
                value: function() {
                    this.element.focus()
                }
            }, {
                key: "render",
                value: function() {
                    var e = this;
                    return c.default.createElement("div", {
                        tabIndex: "-1",
                        className: "qrcode-input",
                        onFocus: this.onFocus,
                        onKeyDown: this.onKeyDown,
                        ref: function(t) {
                            e.element = t
                        }
                    }, c.default.createElement("div", {
                        id: "qrtitleone"
                    }), c.default.createElement("div", {
                        className: "numberqr",
                        id: "qrnumberone"
                    }), c.default.createElement("div", {
                        className: "container"
                    }, c.default.createElement("div", {
                        className: "barcode",
                        id: "qrimageone"
                    })), c.default.createElement("div", {
                        id: "qrtitletwo"
                    }), c.default.createElement("div", {
                        className: "numberqr",
                        id: "qrnumbertwo"
                    }), c.default.createElement("div", {
                        className: "container"
                    }, c.default.createElement("div", {
                        className: "barcode",
                        id: "qrimagetwo"
                    })))
                }
            }]), t
        }(d.default);
        t.default = (0, h.default)(_, "immediate", "immediate")
    },
    213: function(e, t, n) {
        "use strict";

        function i(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function a(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function o(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function r(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var s = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            l = n(3),
            u = i(l),
            c = n(12),
            f = (i(c), n(6)),
            d = i(f),
            p = n(22),
            h = i(p);
        n(231);
        var m = function(e) {
            function t(e) {
                a(this, t);
                var n = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                return n.name = "SimcardInfo", n.DEBUG = !1, n.state = {
                    isAirplaneMode: !1,
                    cardInfos: []
                }, n
            }
            return r(t, e), s(t, [{
                key: "componentDidMount",
                value: function() {
                    this._initCardInfos(), h.default.addObserver("airplaneMode.enabled", this), h.default.addObserver("custom.lockscreen.ui", this)
                }
            }, {
                key: "_observe_airplaneMode.enabled",
                value: function(e) {
                    this.setState({
                        isAirplaneMode: e
                    })
                }
            }, {
                key: "_observe_custom.lockscreen.ui",
                value: function(e) {
                    this._cuzVal = e, dump("launcher simcard_info.js observe setting value = " + e), this._updateCardInfos()
                }
            }, {
                key: "_initCardInfos",
                value: function() {
                    var e = this,
                        t = navigator.mozMobileConnections;
                    if (t) {
                        Array.from(t).forEach(function(t, n) {
                            t.addEventListener("datachange", e), t.addEventListener("voicechange", e), t.addEventListener("signalstrengthchange", e)
                        }), h.default.get("custom.lockscreen.ui").then(function(t) {
                            dump("launcher simcard_info.js get setting value = " + t), e._cuzVal = t, e._updateCardInfos()
                        });
                        var n = this;
                        navigator.customization.getValue("lockscreen.simcard.searching").then(function(e) {
                            dump("launcher simcard_info getcustomvalue = " + e), n._showSearching = e, n._updateCardInfos()
                        })
                    }
                }
            }, {
                key: "_auEmergencyState",
                value: function(e) {
                    var t = navigator.mozMobileConnections[e].iccId,
                        n = navigator.mozIccManager.getIccById(t);
                    if (dump("simcard_info.js icc.cardState: " + (n && n.cardState)), (null == n || "permanentBlocked" === n.cardState || "pinRequired" === n.cardState || "pukRequired" === n.cardState) && navigator.mozMobileConnections[e].voice.network.mcc) {
                        if ("505" == navigator.mozMobileConnections[e].voice.network.mcc) return null == n ? "noSim" : n.cardState
                    }
                    return ""
                }
            }, {
                key: "_customOperatorNameAccordingLanguage",
                value: function(e, t) {
                    var n, i, a;
                    return e && e.voice && e.voice.network && e.voice.connected && (n = e.voice.network.mcc, i = e.voice.network.mnc, "460" != n || "00" != i && "04" != i && "07" != i && "08" != i ? "460" != n || "01" != i && "09" != i ? "460" != n || "03" != i && "11" != i ? "466" == n && "97" == i ? (a = "zh-CN" == t ? "台湾大哥大" : "zh-TW" == t || "zh-HK" == t ? "臺灣大哥大" : e.voice.network.longName, a = this._apt_in_twm(a, t, e.iccId)) : a = "466" == n && "01" == i ? "zh-CN" == t ? "远传电信" : "zh-TW" == t || "zh-HK" == t ? "遠傳電信" : e.voice.network.longName : "466" == n && "92" == i ? "zh-CN" == t ? "中华电信" : "zh-TW" == t || "zh-HK" == t ? "中華電信" : e.voice.network.longName : "466" == n && "89" == i ? "zh-CN" == t ? "台湾之星" : "zh-TW" == t || "zh-HK" == t ? "臺灣之星" : e.voice.network.longName : "466" == n && "05" == i ? "zh-CN" == t ? "亚太电信" : "zh-TW" == t || "zh-HK" == t ? "亞太電信" : e.voice.network.longName : e.voice.network.longName : a = "zh-CN" == t || "zh-TW" == t || "zh-HK" == t ? "中国电信" : e.voice.network.longName : a = "zh-CN" == t || "zh-TW" == t || "zh-HK" == t ? "中国联通" : e.voice.network.longName : a = "zh-CN" == t || "zh-TW" == t || "zh-HK" == t ? "中国移动" : e.voice.network.longName), a
                }
            }, {
                key: "_updateCardInfosImpl",
                value: function(e) {
                    var t = this,
                        n = navigator.mozMobileConnections;
                    if (n) {
                        var i = [];
                        Array.from(n).forEach(function(n, a) {
                            var o = !n.iccId,
                                r = 0,
                                s = !1;
                            o || (r = n.signalStrength ? n.signalStrength.level + 1 || 0 : Math.ceil(n.voice.relSignalStrength / 20) || 0), 0 == r && (s = !0);
                            var l = void 0,
                                u = void 0;
                            dump("launcher simcard_info.js _cuzVal = " + t._cuzVal + ", index = " + a);
                            var c = !(!navigator.mozTelephony || !navigator.mozTelephony.active),
                                f = n.voice && n.voice.connected,
                                d = n.data && n.data.connected,
                                p = t._auEmergencyState(a);
                            1 === t._cuzVal ? o ? null === n.voice.state ? (l = "noSimCard", dump("launcher simcard_info.js noSimCard")) : (l = "eccOnly", dump("launcher simcard_info.js noSimCard but eccOnly")) : null === n.voice.state || s ? (l = "noService", dump("launcher simcard_info.js SIM in but noService")) : f || d || c && navigator.mozTelephony.active.serviceId === a ? n.voice.network && n.voice.network.longName ? u = n.voice.network.longName : (l = "emergencyCallsOnly", dump("launcher simcard_info.js SIM in but emergencyCallsOnly")) : (r = 0, "searching" === n.voice.state ? (l = "searching", dump("launcher simcard_info.js SIM in but searching")) : n.voice.emergencyCallsOnly ? (l = "emergencyCallsOnly", dump("launcher simcard_info.js SIM in emergencyCallsOnly")) : (l = "noService", dump("launcher simcard_info.js SIM in but not searching, noService"))) : 2 === t._cuzVal || p ? o ? (l = "nocard-eccOnly", dump("AU launcher simcard_info.js noSimCard but eccOnly")) : null === n.voice.state || s ? (l = "noService", dump("AU launcher simcard_info.js SIM in but noService")) : f || d || c && navigator.mozTelephony.active.serviceId === a ? n.voice.network && n.voice.network.longName ? u = n.voice.network.longName : (l = "unusablecard-eccOnly", dump("AU launcher simcard_info.js SIM in but emergencyCallsOnly")) : (r = 0, "searching" === n.voice.state ? (l = "unusablecard-eccOnly", dump("AU launcher simcard_info.js SIM in but searching")) : n.voice.emergencyCallsOnly ? (l = "unusablecard-eccOnly", dump("AU launcher simcard_info.js SIM in emergencyCallsOnly")) : (l = "noService", dump("AU launcher simcard_info.js SIM in but not searching, noService"))) : o && "registered" !== n.voice.state ? l = "noSimCard" : "searching" === n.voice.state ? t._showSearching && (l = "searching") : n.voice.emergencyCallsOnly ? l = "noService" : n.voice.connected && n.voice.network ? u = n.voice.network.longName : l = "noService", u = t._customOperatorNameAccordingLanguage(n, e);
                            var h = navigator.mozMobileConnections[a].iccId,
                                m = navigator.mozIccManager.getIccById(h);
                            p || !m || "pinRequired" !== m.cardState && "pukRequired" !== m.cardState || (l = "lockedSim"), dump("launcher simcard_info.js index = " + a + ", signalLevel = " + r + ", carrierName = " + u + ", stateL10nId = " + l), i.push({
                                signalLevel: r,
                                carrierName: u,
                                stateL10nId: l
                            })
                        }), this.setState({
                            cardInfos: i
                        })
                    }
                }
            }, {
                key: "_apt_in_twm",
                value: function(e, t, n) {
                    var i = navigator.mozIccManager.getIccById(n),
                        a = i.iccInfo.mcc,
                        o = i.iccInfo.mnc;
                    return dump("launcher _apt_in_twn simmcc=" + a + ",simmnc=" + o), "466" == a && "05" == o && (e = "zh-CN" == t ? "亚太电信" : "zh-TW" == t || "zh-HK" == t ? "亞太電信" : "APT"), e
                }
            }, {
                key: "_updateCardInfos",
                value: function() {
                    var e = this,
                        t = navigator.mozSettings.createLock().get("language.current");
                    t.onsuccess = function() {
                        var n = t.result["language.current"];
                        e._updateCardInfosImpl(n)
                    }
                }
            }, {
                key: "_getCustomizationVal",
                value: function(e) {
                    dump("launcher simcard_info.js type = " + e + ", _cuzVal = " + this._cuzVal), this._updateCardInfos()
                }
            }, {
                key: "_handle_voicechange",
                value: function(e) {
                    this._getCustomizationVal("voice")
                }
            }, {
                key: "_handle_datachange",
                value: function(e) {
                    this._getCustomizationVal("data")
                }
            }, {
                key: "_handle_signalstrengthchange",
                value: function(e) {
                    this._getCustomizationVal("signal")
                }
            }, {
                key: "render",
                value: function() {
                    var e = SIMSlotManager.isMultiSIM(),
                        t = this.state.cardInfos.map(function(t, n) {
                            var i = e ? u.default.createElement("div", {
                                    className: "icon-wrapper"
                                }, u.default.createElement("div", {
                                    className: "icon inactive",
                                    "data-icon": "sim-" + (n + 1)
                                }, u.default.createElement("div", {
                                    className: "icon",
                                    "data-icon": "signal-0"
                                }))) : u.default.createElement("div", {
                                    className: "icon-wrapper"
                                }, u.default.createElement("div", {
                                    className: "icon inactive",
                                    "data-icon": "no-sim"
                                })),
                                a = u.default.createElement("div", {
                                    className: "icon-wrapper",
                                    "data-is-searching": "searching" === t.stateL10nId
                                }, u.default.createElement("div", {
                                    className: "icon level",
                                    "data-icon": "signal-" + t.signalLevel
                                }), u.default.createElement("div", {
                                    className: "icon bg",
                                    "data-icon": "signal-5"
                                })),
                                o = navigator.mozL10n.get(t.stateL10nId).length > 25,
                                r = SIMSlotManager.isSIMCardAbsent(n) ? "text inactive" : "text";
                            return o ? r += " needscroll" : "", u.default.createElement("div", {
                                className: "info-row",
                                key: n
                            }, ["noSimCard", "lockedSim", "eccOnly", "nocard-eccOnly"].includes(t.stateL10nId) ? i : a, u.default.createElement("div", {
                                className: "carrier-name secondary"
                            }, u.default.createElement("span", {
                                className: r,
                                "data-l10n-id": t.stateL10nId
                            }, t.carrierName)))
                        }),
                        n = u.default.createElement("div", {
                            className: "airplane-mode-info",
                            "data-l10n-id": "airplane-mode"
                        });
                    return u.default.createElement("div", {
                        id: "simcard-info",
                        "data-is-airplane-mode": this.state.isAirplaneMode
                    }, t, n)
                }
            }]), t
        }(d.default);
        t.default = m
    },
    214: function(e, t, n) {
        "use strict";

        function i(e) {
            if (e && e.__esModule) return e;
            var t = {};
            if (null != e)
                for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
            return t.default = e, t
        }

        function a(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function o(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function r(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function s(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }

        function l(e) {
            var t = {
                    "data-dial": e.dial,
                    style: e.photo ? {
                        backgroundImage: "url(" + e.photo + ")"
                    } : null
                },
                n = {
                    "data-l10n-id": "voicemail" === e.attrs["data-id"] ? "voicemail" : null
                };
            return d.default.createElement("div", e.attrs, d.default.createElement("div", {
                className: "photo-box"
            }, d.default.createElement("div", c({
                className: "photo"
            }, t))), d.default.createElement("div", c({
                className: "name"
            }, n), e.title))
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var u = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }
                return function(t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(),
            c = Object.assign || function(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = arguments[t];
                    for (var i in n) Object.prototype.hasOwnProperty.call(n, i) && (e[i] = n[i])
                }
                return e
            },
            f = n(3),
            d = a(f),
            p = n(6),
            h = a(p),
            m = n(24),
            v = a(m),
            g = n(5),
            y = a(g),
            b = n(14),
            w = a(b),
            _ = n(109),
            k = a(_),
            S = n(42),
            O = a(S),
            E = n(65),
            C = a(E),
            I = n(10),
            P = i(I);
        n(232);
        var M = function(e) {
            function t(e) {
                o(this, t);
                var n = r(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                return n.name = "SpeedDial", n.initFocus = [0, 0], n.dialMapping = {}, n.state = {
                    focus: n.initFocus,
                    dials: []
                }, n.gridCount = n.props.col * n.props.row, n.onFocus = n.onFocus.bind(n), n.onKeyDown = n.onKeyDown.bind(n), n
            }
            return s(t, e), u(t, [{
                key: "componentDidMount",
                value: function() {
                    var e = this;
                    y.default.register("show", this), y.default.register("hide", this), this.updateSoftKeys(), k.default.on("changed", function() {
                        var t = P.simpleClone(k.default.contacts);
                        t.length > e.gridCount && (t = t.filter(function(e) {
                            return "voicemail" !== e.id
                        }).slice(0, e.gridCount)), e.contacts = t, e.updateContacts(t)
                    }), this.element.addEventListener("blur", this.blur.bind(this))
                }
            }, {
                key: "updateContacts",
                value: function(e) {
                    var t = this,
                        n = e.map(function(e, n) {
                            var i = e.name || e.tel;
                            return e.title = i, e.attrs = {
                                className: "contact focusable",
                                tabIndex: -1,
                                "data-l10n-id": e.tel ? "speed-dial-not-empty" : "speed-dial-empty",
                                "data-l10n-args": JSON.stringify({
                                    order: e.dial,
                                    name: i
                                }),
                                "data-tel": e.tel || null,
                                "data-id": e.id || null,
                                role: "menuitem",
                                "aria-setsize": k.default.SIZE,
                                "aria-posinset": e.dial,
                                onClick: function() {
                                    return O.default.speedDial(e.dial)
                                }
                            }, e.key = "speed-dial-" + e.dial + "-" + (e.id || ""), "voicemail" === e.id ? (e.attrs.className += " voicemail", e.attrs["data-l10n-id"] = "speed-dial-not-empty") : e.tel || (e.attrs.className += " empty"), t.dialMapping[e.dial] = n, e
                        });
                    this.setState({
                        dials: n
                    }, function() {
                        t.gridElements = t.element.querySelectorAll(".contact"), t.focusIfPossible()
                    })
                }
            }, {
                key: "blur",
                value: function() {
                    this.element.contains(document.activeElement) || this.setState({
                        focus: P.isRtl() ? [0, this.props.col - 1] : this.initFocus
                    })
                }
            }, {
                key: "componentDidUpdate",
                value: function() {
                    this.focusIfPossible(), this.updateSoftKeys()
                }
            }, {
                key: "onKeyDown",
                value: function(e) {
                    var t = e.key,
                        n = document.activeElement,
                        i = this.state.dials[P.rowColToIndex(this.state.focus, this.props.col)],
                        a = !0;
                    switch (t) {
                        case "1":
                        case "2":
                        case "3":
                        case "4":
                        case "5":
                        case "6":
                        case "7":
                        case "8":
                        case "9":
                            var o = this.dialMapping[t];
                            void 0 !== o && this.setState({
                                focus: P.indexToRowCol(o, this.props.col)
                            }, function() {
                                O.default.speedDial(t)
                            });
                            break;
                        case "ArrowLeft":
                        case "ArrowRight":
                        case "ArrowUp":
                        case "ArrowDown":
                            var r = P.navGrid({
                                currentRowCol: this.state.focus,
                                dir: t,
                                col: this.props.col,
                                total: this.gridCount
                            });
                            this.setState({
                                focus: r
                            });
                            break;
                        case "SoftLeft":
                            i.editable && i.tel && this.sendSms(i.tel);
                            break;
                        case "SoftRight":
                            i.editable && i.tel && this.showOptionMenu(i);
                            break;
                        case "Call":
                        case "Enter":
                            n.click();
                            break;
                        case "EndCall":
                        case "Backspace":
                            y.default.request("closeSheet", "speedDial")
                    }
                    a && (e.stopPropagation(), e.preventDefault())
                }
            }, {
                key: "sendSms",
                value: function(e) {
                    new MozActivity({
                        name: "new",
                        data: {
                            type: "websms/sms",
                            number: e
                        }
                    })
                }
            }, {
                key: "showOptionMenu",
                value: function(e) {
                    var t = this,
                        n = e.dial,
                        i = e.title;
                    this.saveFocus(), y.default.request("showOptionMenu", {
                        options: [{
                            id: "option-remove",
                            callback: function() {
                                O.default.removeSpeedDial({
                                    number: n,
                                    name: i,
                                    cb: t.focusLast.bind(t)
                                })
                            }
                        }, {
                            id: "option-replace",
                            callback: function() {
                                O.default.replaceSpeedDial({
                                    number: n,
                                    name: i,
                                    contactId: t._lastFocus.getAttribute("data-id")
                                })
                            }
                        }]
                    })
                }
            }, {
                key: "saveFocus",
                value: function() {
                    this._lastFocus = this._lastFocus || document.activeElement
                }
            }, {
                key: "focusLast",
                value: function() {
                    this._lastFocus && (this._lastFocus.focus(), this._lastFocus = null)
                }
            }, {
                key: "onFocus",
                value: function() {
                    this.focusIfPossible(), this.updateSoftKeys()
                }
            }, {
                key: "updateSoftKeys",
                value: function() {
                    var e = this.state.dials[P.rowColToIndex(this.state.focus, this.props.col)],
                        t = {
                            left: "",
                            center: "select",
                            right: ""
                        };
                    e && e.tel ? (t.center = "call", e.editable && (t.left = "message", t.right = "options"), this.registerCallSoftKeys(t)) : w.default.register(t, this.element)
                }
            }, {
                key: "registerCallSoftKeys",
                value: function(e) {
                    navigator.mozMobileConnections.length > 1 && !C.default.isAlwaysAsk() && (e.center = {
                        text: "call",
                        icon: "sim-" + (C.default.cardIndex + 1)
                    }), w.default.register(e, this.element)
                }
            }, {
                key: "isHidden",
                value: function() {
                    return !this.element.offsetParent
                }
            }, {
                key: "focusIfPossible",
                value: function() {
                    if (!this.isHidden()) {
                        var e = P.rowColToIndex(this.state.focus, this.props.col),
                            t = this.gridElements && this.gridElements[e];
                        return t ? void t.focus() : void this.element.focus()
                    }
                }
            }, {
                key: "render",
                value: function() {
                    var e = this,
                        t = this.state.dials.map(function(e) {
                            return d.default.createElement(l, c({
                                key: e.key
                            }, e))
                        });
                    return d.default.createElement("bdi", {
                        id: "speed-dial",
                        className: "speed-dial",
                        tabIndex: "-1",
                        role: "heading",
                        "aria-labelledby": "speed-dial-title",
                        onFocus: this.onFocus,
                        onKeyDown: this.onKeyDown,
                        ref: function(t) {
                            e.element = t
                        }
                    }, d.default.createElement("h1", {
                        className: "readout-only",
                        id: "speed-dial-title",
                        "data-l10n-id": "speed-dial"
                    }), t)
                }
            }]), t
        }(h.default);
        M.defaultProps = {
            col: 3,
            row: 3
        }, M.propTypes = {
            row: d.default.PropTypes.number,
            col: d.default.PropTypes.number
        }, t.default = (0, v.default)(M, "immediate", "immediate")
    },
    216: 112,
    217: 112,
    221: 112,
    222: 112,
    223: 112,
    224: 112,
    225: 112,
    226: 112,
    228: 112,
    229: 112,
    230: 112,
    231: 112,
    232: 112,
    236: function(e, t) {
        e.exports = function() {
            throw new Error("define cannot be used indirect")
        }
    }
});
//# sourceMappingURL=app.bundle.js.map