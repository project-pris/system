/* global SIMSlotManager, Service, BaseModule, applications */
'use strict';

(function(exports) {
  var SimLockManager = function(core) {
    this.mobileConnections = core.mobileConnections;
    window.addEventListener('radiostatechange', () => {
      var conn = window.navigator.mozMobileConnections[0];
      if (conn.radioState === 'enabled') {
        this.showIfLocked();
      }
    });

  };
  SimLockManager.EVENTS = [
    'simslotready',
    'ftuopen',
    'appopened',
    'simslot-updated',
    'simslot-cardstatechange',
    'simslot-iccinfochange',
    'attentionopening',
    'attentionterminated',
    'simlockskip',
    'simlockback',
    'simlockrequestclose',
    'rocketbar-activating',
    'simdialog-deactivated',
    'simdialog-hide',
    'ftudone',
    'ftuskip'
  ];

  BaseModule.create(SimLockManager, {
    name: 'SimLockManager',
    _duringCall: false,
    _showPrevented: false,
    // Only valid in first check simLock when device reboot or exit APM
    _alreadyShown: false,
    _nextCheckSlotIndex: -1,
    _currentSlotIndex: -1,
    _skipSimDialog: true,
    DEBUG: true,

    _start: function() {
      this.showIfLocked();
    },

    _handle_simslotready: function() {
      this.showIfLocked();
    },

    _sim_lock_system_dialog_loaded: function() {
      this.showIfLocked();
    },

    '_handle_simslot-updated': function(evt) {
      this.showIfLocked(evt.detail.index);
    },

    '_handle_simslot-iccinfochange': function(evt) {
      this.showIfLocked(evt.detail.index);
    },

    '_handle_simslot-cardstatechange': function(evt) {
      this.showIfLocked(evt.detail.index);
    },

    '_handle_ftuopen': function() {
      // XXX, add 3s delay to make focus correct as we got a performance
      // issue here in ftu, show remove later.
      //setTimeout(() => {this.showIfLocked();}, 3000);
      setTimeout(() => {
        this._skipSimDialog = false;
        this.showIfLocked();
      }, 3000);
    },

    _handle_ftuskip: function() {
      this._skipSimDialog = false;
      this.showIfLocked();
    },

    _handle_ftudone: function() {
      this._skipSimDialog = false;
    },

    _handle_simlockback: function(evt) {
      var index = evt.detail._currentSlot.index;
      this.showIfLocked(index - 1);
    },

    _handle_simlockskip: function(evt) {
      if (!evt.detail.bCheckSimStaus) {
        evt.detail.close('skip');
        return;
      } else {
        if (!this.showIfLocked(evt.detail._currentSlot.index)) {
          evt.detail.close('skip');
        }
      }
    },

    _handle_simlockrequestclose: function(evt) {
      var index = evt.detail._currentSlot.index;
      if (index + 1 > this.mobileConnections.length - 1) {
        evt.detail.close();
      } else {
        if (!this.showIfLocked(index + 1)) {
          evt.detail.close();
        }
      }
    },

    _handle_attentionopening: function(evt) {
      if (evt.detail.CLASS_NAME !== 'CallscreenWindow') {
        return;
      }
      this._duringCall = true;
    },

    _handle_attentionterminated: function(evt) {
      if (evt.detail.CLASS_NAME !== 'CallscreenWindow') {
        return;
      }
      this._duringCall = false;
      if (this._showPrevented) {
        this._showPrevented = false;

        // We show the SIM dialog right away otherwise the user won't
        // be able to receive calls.
        this.showIfLocked();
      }
    },

    _handle_appopened: function(evt) {
      // If an app needs 'telephony' or 'sms' permissions (i.e. mobile
      // connection) and the SIM card is locked, the SIM PIN unlock screen
      // should be launched

      var app = evt.detail;
      if (!app || !app.manifest || !app.manifest.permissions) {
        return;
      }

      // Ignore apps that don't require a mobile connection
      if (!('telephony' in app.manifest.permissions ||
            'sms' in app.manifest.permissions)) {
        return;
      }

      // If the Settings app will open, don't prompt for SIM PIN entry
      // although it has 'telephony' permission (Bug 861206)
      var settingsManifestURL =
        'app://settings.gaiamobile.org/manifest.webapp';
      if (app.manifestURL == settingsManifestURL) {
        Service.request('SimDialog:hide');
        return;
      }

      /*var FtuManifestURL =
        'app://ftu.gaiamobile.org/manifest.webapp';
      if (app.manifestURL == FtuManifestURL) {
        Service.request('SimDialog:hide');
        return;
      }*/

      // If SIM is locked, cancel app opening in order to display
      // it after the SIM PIN dialog is shown
      // XXX, always add 1s delay to avoid flash when app opend
      setTimeout(() => {this.showIfLocked(null, true);}, 1000);

      // XXX: We don't block the app from launching if it requires SIM
      // but only put the SIM PIN dialog upon the opening/opened app.
      // Will revisit this in
      // https://bugzilla.mozilla.org/show_bug.cgi?id=SIMPIN-Dialog
    },

    '_handle_rocketbar-activating': function(evt) {
      Service.request('SimDialog:hide');
      this._alreadyShown = false;
    },

    '_handle_simdialog-deactivated': function(evt) {
      this._currentSlotIndex = -1;
      if (this._nextCheckSlotIndex !== -1) {
        window.setTimeout(() => {
          this.showIfLocked(this._nextCheckSlotIndex);
        });
      }
    },

    '_handle_simdialog-hide': function(evt) {
      this._nextCheckSlotIndex = -1;
      this._currentSlotIndex = -1;
    },

    isBothSlotsLocked: function sl_isBothSlotsLocked() {
      if (!SIMSlotManager.isMultiSIM() ||
          SIMSlotManager.hasOnlyOneSIMCardDetected()) {
        return false;
      }

      var simSlots = SIMSlotManager.getSlots();
      var isBothLocked = true;
      for (var i = 0; i < simSlots.length; i++) {
        var currentSlot = simSlots[i];
        var unknownState = currentSlot.isUnknownState();
        var currentLocked = currentSlot.isLocked() || unknownState;
        isBothLocked = isBothLocked && currentLocked;
      }
      return isBothLocked;
    },

    showIfLocked: function sl_showIfLocked(currentSlotIndex, skipSettings) {
      dump("sim_lock_manager, showIflocked, currentslotIndex:"+currentSlotIndex);
      if (this._skipSimDialog) {
        return false;
      }
      if (!SIMSlotManager.ready) {
        this.warn('SIMSlot not ready yet.');
        return false;
      }

      var conn = window.navigator.mozMobileConnections[0];
      if (!conn || (conn.radioState !== 'enabled' &&
        conn.radioState !== 'enabling')) {
        return false;
      }
      // When already in settings, don't show system sim pin dialog.
      var curApp = Service.currentApp ? Service.currentApp.origin : null;
      if (skipSettings && curApp === 'app://settings.gaiamobile.org') {
        Service.request('SimDialog:hide');
        return;
      }

      /*// FTU has its specific SIM PIN UI
      if (Service.query('isFtuRunning')) {
        this.warn('Running full ftu.');
        Service.request('SimDialog:hide');
        return false;
      }*/

      if (this._duringCall) {
        this.warn('During call');
        this._showPrevented = true;
        return false;
      }

      return SIMSlotManager.getSlots().some(function iterator(slot, index) {
        // If currentSlotIndex is present then we just need to handle this slot
        if (currentSlotIndex && index !== currentSlotIndex) {
          return false;
        }

        if (!slot.simCard) {
          this.warn('No SIM card in slot ' + (index + 1));
          return false;
        }
        // Always showing the first slot first.
        if (!this._alreadyShown && this.isBothSlotsLocked() && index > 0) {
          this._nextCheckSlotIndex = index;
          return false;
        }
        if (this._alreadyShown &&
          this._currentSlotIndex !== -1 && index !== this._currentSlotIndex) {
          this._nextCheckSlotIndex = index;
          return false;
        }

        dump("sim_lock_manager, card state:"+slot.getCardState());
        switch (slot.getCardState()) {
          // do nothing in either unknown or null card states
          case null:
          case 'unknown':
            this.warn('unknown SIM card state for slot ' + (index + 1));
            break;
          case 'permanentBlocked':
          case 'pinRequired':
          case 'pukRequired':
          case 'networkLocked':
          case 'networkSubsetLocked':
          case 'network1Locked':
          case 'network2Locked':
          case 'hrpdNetworkLocked':
          case 'corporateLocked':
          case 'serviceProviderLocked':
          case 'simPersonalizationLocked':
          case 'ruimCorporateLocked':
          case 'ruimServiceProviderLocked':

          case 'networkPukRequired':
          case 'networkSubsetPukRequired':
          case 'network1PukRequired':
          case 'network2PukRequired':
          case 'hrpdNetworkPukRequired':
          case 'corporatePukRequired':
          case 'serviceProviderPukRequired':
          case 'simPersonalizationPukRequired':
          case 'ruimCorporatePukRequired':
          case 'ruimServiceProviderPukRequired':
            if (SIMSlotManager.isMultiSIM() && !index &&
              (currentSlotIndex === null || currentSlotIndex === undefined)) {
              this._nextCheckSlotIndex = 1;
            } else if (index) {
              this._nextCheckSlotIndex = -1;
            }
            this._currentSlotIndex = index;
            window.renderSD();
            Service.request('SimDialog:show', slot);
            this._alreadyShown = true;
            return true;
          default:
            this.warn('SIM slot ' + (index + 1) + ' is not locked, skipping');
            return false;
        }
      }, this);
    }
  });
}());

