'use strict';var BATCH_SIZE=15;var PAN_DELTA=6;var GROUPBYDATEANDLOCATION=false;var SORTTYPE=0;var TRANSITION_FRACTION=0.25;var TRANSITION_SPEED=0.75;var thumbnails=$('thumbnails');var fullscreenView;var cropView;var renameDialog;var settingsDialog;var listProgressBar=$('list-progress');var infoDialog;var currentActiveDialog;var isEnable=true;const LAYOUT_MODE={list:'thumbnailListView',select:'thumbnailSelectView',fullscreen:'fullscreenView',edit:'editView',pick:'pickView',crop:'cropView'};var currentView;ScreenLayout.watch('portrait','(orientation: portrait)');var isPortrait=ScreenLayout.getCurrentLayout('portrait');const isPhone=true;window.addEventListener('largetextenabledchanged',()=>{document.body.classList.toggle('large-text',navigator.largeTextEnabled);});var currentFileIndex=0;var editedPhotoIndex;var selectedFileNames=[];var selectedFileNamesToBlobs={};var hasSaved=false;var hasRenamed=false;var renameUpateMediaCallback=null;var editSaveCallback=null;var contentInteractiveTag=false;var lastFocusedThumbnail=null;var isPowerOffShown=false;var firstScanDone=false;var lock;navigator.mozL10n.once(function showBody(){document.body.classList.remove('hidden');window.dispatchEvent(new CustomEvent('moz-chrome-dom-loaded'));if(!isPhone){LazyLoader.load(['shared/js/media/downsample.js','js/frame_scripts.js']);}
init();});function init(){document.body.classList.toggle('large-text',navigator.largeTextEnabled);Gallery.init();window.onresize=resizeHandler;window.performance.mark('navigationInteractive');window.dispatchEvent(new CustomEvent('moz-chrome-interactive'));initDB();if(isEnable&&picking){initHtml();isEnable=false;}
navigator.mozSetMessageHandler('activity',function activityHandler(a){var activityName=a.source.name;switch(activityName){case'browse':if(currentView===LAYOUT_MODE.fullscreen)
setView(LAYOUT_MODE.list);break;case'pick':Pick.initPick();Pick.start(a);break;}});}
function initDB(){photodb=new MediaDB('pictures',metadataParserWrapper,{version:2,autoscan:false,batchHoldTime:2000,batchSize:3});window.performance.mark('navigationLoaded');var loaded=false;function metadataParserWrapper(file,onsuccess,onerror,bigFile){if(loaded){metadataParser(file,onsuccess,onerror,bigFile);return;}
LazyLoader.load(['shared/js/media/downsample.js','js/metadata_scripts.js','shared/js/media/crop_resize_rotate.js'],function(){loaded=true;metadataParser(file,onsuccess,onerror,bigFile);});}
photodb.onupgrading=function(evt){Overlay.show('upgrade');};photodb.onunavailable=function(event){if(!picking){setView(LAYOUT_MODE.list);}else{Pick.restart();}
var why=event.detail;if(why===MediaDB.NOCARD)
Overlay.show('nocard');else if(why===MediaDB.UNMOUNTED)
Overlay.show('pluggedin');};photodb.onenumerable=function(){initThumbnails();window.performance.mark('visuallyLoaded');};photodb.onready=function(){if(Overlay.current==='nocard'||Overlay.current==='pluggedin'||Overlay.current==='upgrade')
Overlay.hide();};photodb.onscanstart=function onscanstart(){if(!lock){lock=navigator.requestWakeLock('screen');}};photodb.onscanend=function onscanend(e){window.performance.mark('fullyLoaded');lock.unlock();lock=null;if(Overlay.current==='scanning'){if(!e.detail.containsData){Overlay.show('emptygallery');}}
else if(!isPhone&&!currentFrame.displayingImage){showFile(0);}
if(!firstScanDone){firstScanDone=true;needNav();window.dispatchEvent(new CustomEvent('moz-app-loaded'));}};photodb.onnomediastorage=function onnomediastorage(e){setView(LAYOUT_MODE.list);Overlay.show('emptygallery');};photodb.oncardremoved=function oncardremoved(){if(picking){Pick.cancel();return;}
setView(LAYOUT_MODE.list);};photodb.oncreated=function(event){forEachFileInfo(event.detail,fileCreated,()=>{if(enumerateDone||hasSaved){if(hasSaved)hasSaved=false;needNav();}});};photodb.ondeleted=function(event){forEachFileInfo(event.detail,fileDeleted,()=>{if(enumerateDone){needNav();}});};function forEachFileInfo(fileinfos,func,alldone){var last=fileinfos.length;for(var i in fileinfos){if(isNaN(i))continue;last--;func(fileinfos[i],0===last?alldone:undefined);}}
window.navigator.mozSettings.addObserver('system.poweroff.enabled',function(evt){isPowerOffShown=evt.settingValue?evt.settingValue:false;});window.addEventListener('visibilitychange',function(e){if(!document.hidden&&!shareProcess&&!isPowerOffShown)
window.focus();});doNotScanInBackgroundHack(photodb);}
function compareFilesByDate(a,b){if(a.date<b.date)
return 1;else if(a.date>b.date)
return-1;return 0;}
function initThumbnails(){if(isInitThumbnail){photodb.scan();return;}
isInitThumbnail=true;thumbnailList=new ThumbnailList(ThumbnailDateGroup,thumbnails);thumbnails.addEventListener('click',thumbnailClickHandler);var batch=[];var batchsize=PAGE_SIZE;var firstBatchDisplayed=false;photodb.enumerate('date',null,'prev',function(fileinfo){if(fileinfo){var metadata=fileinfo.metadata;if(metadata&&metadata.preview&&metadata.preview.filename){metadata.preview.width=Math.floor(metadata.preview.width);metadata.preview.height=Math.floor(metadata.preview.height);}
batch.push(fileinfo);if(batch.length>=batchsize){flush();batchsize*=2;}}
else{done();}});function flush(){batch.forEach(thumb);if(isEnable){setTimeout(function(){initHtml();},150);isEnable=false;}
needNav();batch.length=0;if(!firstBatchDisplayed){firstBatchDisplayed=true;window.dispatchEvent(new CustomEvent('moz-app-visually-complete'));window.dispatchEvent(new CustomEvent('moz-content-interactive'));}}
function thumb(fileinfo){files.push(fileinfo);thumbnailList.addItem(fileinfo);}
function done(){flush();enumerateDone=true;if(files.length===0){SoftkeyManager.softkey&&SoftkeyManager.softkey.hide();Overlay.show('scanning');}else{Overlay.hide();}
photodb.addEventListener('ready',function(){photodb.scan();});if(photodb.state===MediaDB.READY){photodb.scan();}
if(!picking){setView(LAYOUT_MODE.list);}}}
function needNav(){thumbnails.classList.add(NEED_NAV);}
function getInsertPosition(fileInfo,findEleIndex){var fileGroup=thumbnailList.findGroup(fileInfo);if(!fileGroup){console.error('file group does not exist in thumbnail List',fileInfo.name);return-1;}
var index=0;if(findEleIndex)
index=fileGroup.getDisplayPos(fileInfo);else
index=fileGroup.getIndex(fileInfo);if(index<0){console.error(fileInfo.name,' does not exist in thumbnail list');return-1;}
for(var n=0;n<thumbnailList.itemGroups.length;n++){if(thumbnailList.itemGroups[n].groupID===fileGroup.groupID){break;}
index+=thumbnailList.itemGroups[n].getCount();}
return index;}
function fileDeleted(filename,navCallback){var fileIndex=currentFileIndex;for(var n=0;n<files.length;n++){if(files[n].name===filename)
break;}
if(n>=files.length)
return;thumbnailList.removeItem(files[n]);files.splice(n,1);if(navCallback)navCallback();if(n<fileIndex)
fileIndex--;if(fileIndex>=files.length)
fileIndex=files.length-1;if(n<editedPhotoIndex)
editedPhotoIndex--;if(files.length>0&&(currentView===LAYOUT_MODE.fullscreen)){showFile(fileIndex);}else{updateFocusThumbnail(fileIndex);}
window.dispatchEvent(new CustomEvent('filedeleted',{detail:{filename:filename}}));if(files.length===0){if(currentView!==LAYOUT_MODE.pick)
setView(LAYOUT_MODE.list);Overlay.show('emptygallery');}}
function deleteFile(n){if(n<0||n>=files.length)
return;var fileinfo=files[n];photodb.deleteFile(files[n].name);if(fileinfo.metadata.preview&&fileinfo.metadata.preview.filename){var pictures=navigator.getDeviceStorage('pictures');pictures.delete(fileinfo.metadata.preview.filename);}}
function fileCreated(fileinfo,navCallback){photodb.getFileInfo(fileinfo.name,function(fileinfo){var insertPosition;if(Overlay.current==='emptygallery'||Overlay.current==='scanning')
Overlay.hide();files.push(fileinfo);var thumbnailItem=thumbnailList.addItem(fileinfo);if(currentView===LAYOUT_MODE.select){var checkBox=thumbnailItem.imgNode.checkBoxContainerNode;checkBox.classList.remove('hidden');checkBox.reset(!isSelectAll);}
insertPosition=getInsertPosition(fileinfo);if(editSaveCallback)editSaveCallback();if(insertPosition<0)
return;if(editedPhotoIndex>=insertPosition)
editedPhotoIndex++;if(hasSaved){if(currentView===LAYOUT_MODE.fullscreen){setView(LAYOUT_MODE.fullscreen);showFile(currentFileIndex);}}else if(hasRenamed){if(renameUpateMediaCallback)
renameUpateMediaCallback();if(currentView===LAYOUT_MODE.list){var currentItem=thumbnails.querySelector('[data-filename="'+fileinfo.name+'"]');if(currentItem)
NavigationMap.scrollToElement(currentItem.parentNode);}}
if(navCallback)navCallback();});}
function scrollToShowThumbnail(n){if(!files[n])
return;var selector='img[data-filename="'+files[n].name+'"]';var thumbnail=thumbnails.querySelector(selector);if(thumbnail){var screenTop=thumbnails.scrollTop;var screenBottom=screenTop+thumbnails.clientHeight;var thumbnailTop=thumbnail.offsetTop;var thumbnailBottom=thumbnailTop+thumbnail.offsetHeight;var toolbarHeight=40;screenBottom-=toolbarHeight;if(thumbnailTop<screenTop){thumbnails.scrollTop=thumbnailTop;}
else if(thumbnailBottom>screenBottom){thumbnails.scrollTop=thumbnailBottom-thumbnails.clientHeight+toolbarHeight;}}}
function setView(view){if(currentView===view||NavigationMap.inProgress)
return;switch(currentView){case LAYOUT_MODE.select:Array.forEach(thumbnails.querySelectorAll('.thumbnail-list-img'),function(elt){elt.classList.remove('selected');elt.checkBoxContainerNode.classList.add('hidden');});if(!isPhone&&currentFileIndex!==-1)
showFile(currentFileIndex);break;case LAYOUT_MODE.fullscreen:if(!isPhone&&(view===LAYOUT_MODE.list)&&!isPortrait&&currentFileIndex!==-1){resizeFrames();}else{clearFrames();}
break;}
switch(view){case LAYOUT_MODE.list:selectedLock=0;Array.forEach(thumbnails.querySelectorAll('.thumbnail-list-img'),function(elt){elt.lockStatusNode.reset(elt.fileData.metadata.locked);});scrollToShowThumbnail(currentFileIndex);break;case LAYOUT_MODE.fullscreen:resizeFrames();break;case LAYOUT_MODE.select:SoftkeyManager.setSkMenu(LAYOUT_MODE.select);resetSelection();break;case LAYOUT_MODE.edit:break;}
Gallery.lastView=currentView;currentView=view;ViewHelper.resetView();if(!contentInteractiveTag){window.performance.mark('contentInteractive');contentInteractiveTag=true;}
let themColor=document.querySelectorAll('meta[name="theme-color"]')[0];if(themColor){themColor.setAttribute('content',{'editView':'#000000','fullscreenView':'transparent'}[view]||'#48132c');}
document.body.classList.remove(Gallery.lastView);document.body.classList.add(view);}
function thumbnailClickHandler(evt){var target=evt.target;if(!target)
return;target=target.classList.contains('thumbnail')?target.firstElementChild:target;if(!target||!target.classList.contains('thumbnail-list-img')){return;}
if(photodb.state!==MediaDB.READY){return;}
if(picking&&currentView===LAYOUT_MODE.pick&&currentFileIndex>=0){Pick.select(files[currentFileIndex]);cropView.focus();}else if(currentView===LAYOUT_MODE.select){updateSelection(target);}else{if(files[currentFileIndex].metadata.largeSize){var dialogConfig={title:{id:'invalid-image',args:{}},body:{id:'image-too-big',args:{}},accept:{name:'Ok',l10nId:'ok',callback:null}};confirmDialog(dialogConfig);return;}
LazyLoader.load(['shared/js/media/downsample.js','js/frame_scripts.js'],function(){if(isPortrait||isPhone){setView(LAYOUT_MODE.fullscreen);}
showFile(currentFileIndex);fullscreenView.focus();});}}
function updateFocusThumbnail(n){var previousIndex=currentFileIndex;if(n>=0&&n<files.length){currentFileIndex=n;debug('updateFocusThumbnail:currentFileIndex:'+n);}
if(isPhone||currentFileIndex===-1)
return;var newTarget=thumbnailList.findThumbnail(files[currentFileIndex]);if(newTarget)
newTarget.htmlNode.classList.add('focus');if(previousIndex===currentFileIndex)
return;var oldTarget=files[previousIndex]?thumbnailList.findThumbnail(files[previousIndex]):undefined;if(oldTarget)
oldTarget.htmlNode.classList.remove('focus');}
function updateSelection(thumbnail){thumbnail.classList.toggle('selected');var selected=thumbnail.classList.contains('selected');if(selected){thumbnail.checkBoxContainerNode.reset(true);}
else{thumbnail.checkBoxContainerNode.reset(false);}
var selected=thumbnail.classList.contains('selected');var index=currentFileIndex;if(index<0)
return;var filename=files[index].name;if(selected){selectedFileNames.push(filename);updateFocusThumbnail(index);if(files[index].metadata.locked)
selectedLock++;photodb.getFile(filename,function(file){selectedFileNamesToBlobs[filename]=file;});if(!isPhone)
showFile(currentFileIndex);}
else{if(files[index].metadata.locked)
selectedLock--;delete selectedFileNamesToBlobs[filename];var i=selectedFileNames.indexOf(filename);if(i!==-1)
selectedFileNames.splice(i,1);if(currentFileIndex===index&&!isPhone){if(i>0){var lastSelected=selectedFileNames[i-1];var lastSelectedIndex=Gallery.getFileIndexByName(lastSelected);updateFocusThumbnail(lastSelectedIndex);showFile(currentFileIndex);}else{clearFrames();}}}
var numSelected=selectedFileNames.length;navigator.mozL10n.setAttributes($('multiple-select-count'),'tcl-selected-items-count',{n:numSelected});SoftkeyManager.setSkMenu();SoftkeyManager.updateSkText(numSelected===files.length?'tcl-deselect-all':'tcl-select-all');isSelectAll=numSelected===files.length?false:true;Gallery.setSelectSubView(!isSelectAll?'all':(selected?'des':''));}
function launchCameraApp(){var a=new MozActivity({name:'record',data:{type:'photos'}});}
function rename(){var fileinfo=files[currentFileIndex];var filename=fileinfo.name.split('/').pop();var extension='.'+filename.split('.').pop();filename=filename.split(extension).shift();renameDialog.querySelector('input[name="newname"]').value=filename;renameDialog.classList.remove('hidden');SoftkeyManager.showRsk(''!==filename);if(!renameListenerInit){renameListenerInit=true;document.addEventListener('input',(e)=>{if('input_rename'===e.target.id){SoftkeyManager.showRsk(''!==e.target.value);}});}}
function share(blobs,blobName){if(blobs.length===0)
return;var names=[],types=[],fullpaths=[];blobs.forEach(function(blob){var name=blob.name;if(!name&&blobs.length===1)
name=blobName;fullpaths.push(name);name=name.substring(name.lastIndexOf('/')+1);names.push(name);var type=blob.type;if(type)
type=type.substring(0,type.indexOf('/'));types.push(type);});var type;if(types.length===1||types.every(function(t){return t===types[0];}))
type=types[0]+'/*';else
type='application/*';var a=new MozActivity({name:'share',data:{type:type,number:blobs.length,blobs:blobs,filenames:names,filepaths:fullpaths}});shareProcess=true;a.onsuccess=function(){setTimeout(function(){window.focus();shareProcess=false;},500);};a.onerror=function(e){shareProcess=false;if(a.error.name==='NO_PROVIDER'){var msg=navigator.mozL10n.get('share-noprovider');alert(msg);}
else{console.warn('share activity error:',a.error.name);}};}
function resizeHandler(){isPortrait=ScreenLayout.getCurrentLayout('portrait');if(currentView===LAYOUT_MODE.fullscreen||(!isPhone&&!isPortrait&&(currentView===LAYOUT_MODE.list||currentView===LAYOUT_MODE.select))){resizeFrames();setFramesPosition();}}
function doNotScanInBackgroundHack(photodb){const enoughMB=512;var memoryMB=0;window.addEventListener('visibilitychange',backgroundScanKiller);photodb.addEventListener('scanend',function(){window.removeEventListener('visibilitychange',backgroundScanKiller);});function backgroundScanKiller(){if(!document.hidden||memoryMB>=enoughMB){return;}
if(!navigator.getFeature){exit();}
else{navigator.getFeature('hardware.memory').then(function(mem){memoryMB=mem;if(memoryMB<enoughMB){exit();}});}
function exit(){if(document.hidden&&photodb.scanning){console.warn('[Gallery] exiting to avoid background scan.');setTimeout(function(){window.close();},500);}}}}
function initHtml(){enable(fullscreenView=$('fullscreen-view'));enable(infoDialog=$('info-view'));enable(cropView=$('crop-view'));enable($('edit-view'));enable(renameDialog=$('rename-dialog'));enable($('confirm-dialog'));if(!settingsDialog){enable(settingsDialog=$('settings-dialog'));}}
function enable(h){h.innerHTML=h.innerHTML.replace(/(<!--)|(-->)/g,'');}