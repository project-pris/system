/* © 2017 KAI OS TECHNOLOGIES (HONG KONG) LIMITED All rights reserved.
 * This file or any portion thereof may not be reproduced or used in any manner
 * whatsoever without the express written permission of KAI OS TECHNOLOGIES
 * (HONG KONG) LIMITED. KaiOS is the trademark of KAI OS TECHNOLOGIES (HONG KONG)
 * LIMITED or its affiliate company and may be registered in some jurisdictions.
 * All other trademarks are the property of their respective owners.
 */
// ************************************************************************
// * File Name: vibrate.js
// * Description: mmitest -> test item: vibrate test.
// * Note:
// ************************************************************************

/* global DEBUG, dump, TestItem */
'use strict';

function debug(s) {
  if (DEBUG) {
    dump('<mmitest> ------: [vibrate.js] = ' + s + '\n');
  }
}

// ------------------------------------------------------------------------
const TOTAL_DURATION = 1500;

var Vibrate = new TestItem();

Vibrate._vibrateInterval = null;

Vibrate.timeOutCallback = function() {
  this.content.innerHTML = 'Test finished.';
  this.passButton.disabled = '';
  this.failButton.disabled = '';
};

Vibrate.startTest = function() {
  debug('Vibrate.startTest');
  this.content.innerHTML = 'Is vibrator on?';
  this.passButton.disabled = 'disabled';
  this.failButton.disabled = 'disabled';

  if (navigator.vibrate) {
    navigator.vibrate(TOTAL_DURATION);
    this._vibrateInterval = setInterval(() => {
      navigator.vibrate(TOTAL_DURATION);
    }, TOTAL_DURATION);
  } else {
    this.timeOutCallback();
    this.content.innerHTML = 'Device don\'t support vibrate API.'
    debug('Device don\'t support navigator.vibrate API.');
    return;
  }

  window.setTimeout(this.timeOutCallback.bind(this), TOTAL_DURATION);
};

//the following are inherit functions
Vibrate.onInit = function() {
  debug('Vibrate.onInit');
  this.content = document.getElementById('centertext');
  this.startTest();
};

Vibrate.onDeinit = function(){
  if (this._vibrateInterval) {
    window.clearInterval(this._vibrateInterval);
    this._vibrateInterval = null;
    // Stop to vibrate
    navigator.vibrate(0);
  }
};

window.addEventListener('load', Vibrate.init.bind(Vibrate));
window.addEventListener('beforeunload', Vibrate.uninit.bind(Vibrate));
window.addEventListener('keydown', Vibrate.handleKeydown.bind(Vibrate));
