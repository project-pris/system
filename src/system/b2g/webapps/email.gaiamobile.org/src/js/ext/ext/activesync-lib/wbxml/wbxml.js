(function(e, t) {
    if ("object" == typeof exports) {
        module.exports = t(), this.Blob = require("./blob").Blob;
        var n = require("stringencoding");
        this.TextEncoder = n.TextEncoder, this.TextDecoder = n.TextDecoder;
    } else "function" == typeof define && define.amd ? define(t) : e.WBXML = t();
})(this, function() {
    function e(e, t, n) {
        function o() {
            var e = this instanceof o ? this : Object.create(o.prototype), t = Error(), r = 1;
            if (e.stack = t.stack.substring(t.stack.indexOf("\n") + 1), e.message = arguments[0] || t.message, 
            n) {
                r += n.length;
                for (var i = 0; i < n.length; i++) e[n[i]] = arguments[i + 1];
            }
            var s = /@(.+):(.+)/.exec(e.stack);
            return e.fileName = arguments[r] || s && s[1] || "", e.lineNumber = arguments[r + 1] || s && s[2] || 0, 
            e;
        }
        return o.prototype = Object.create((t || Error).prototype), o.prototype.name = e, 
        o.prototype.constructor = o, o;
    }
    function t(e, t) {
        this.strings = [], this.offsets = {};
        for (var n = 0, o = 0; o < e.length; o++) 0 === e[o] && (this.offsets[n] = this.strings.length, 
        this.strings.push(t.decode(e.subarray(n, o))), n = o + 1);
    }
    function n(e) {
        e.__nsnames__ = {}, e.__tagnames__ = {}, e.__attrdata__ = {};
        for (var t in e) {
            var n = e[t];
            if (!t.match(/^__/)) {
                if (n.Tags) {
                    var o, r;
                    for (o in n.Tags) {
                        r = n.Tags[o], e.__nsnames__[r >> 8] = t;
                        break;
                    }
                    for (o in n.Tags) r = n.Tags[o], e.__tagnames__[r] = o;
                }
                if (n.Attrs) for (var i in n.Attrs) {
                    var s = n.Attrs[i];
                    "name" in s || (s.name = i), e.__attrdata__[s.value] = s, n.Attrs[i] = s.value;
                }
            }
        }
    }
    function o(e, t, n) {
        if (this.ownerDocument = e, this.type = t, this._attrs = {}, "string" == typeof n) {
            var o = n.split(":");
            1 === o.length ? this.localTagName = o[0] : (this.namespaceName = o[0], this.localTagName = o[1]);
        } else this.tag = n, Object.defineProperties(this, {
            namespace: {
                get: function() {
                    return this.tag >> 8;
                }
            },
            localTag: {
                get: function() {
                    return 255 & this.tag;
                }
            },
            namespaceName: {
                get: function() {
                    return this.ownerDocument._codepages.__nsnames__[this.namespace];
                }
            },
            localTagName: {
                get: function() {
                    return this.ownerDocument._codepages.__tagnames__[this.tag];
                }
            }
        });
    }
    function r(e) {
        this.ownerDocument = e;
    }
    function i(e, t) {
        this.ownerDocument = e, this.textContent = t;
    }
    function s(e, t, n, o) {
        this.ownerDocument = e, this.subtype = t, this.index = n, this.value = o;
    }
    function a(e) {
        this.ownerDocument = e;
    }
    function c(e, t) {
        this.ownerDocument = e, this.data = t;
    }
    function d(e, t) {
        this._data = e instanceof u ? e.bytes : e, this._codepages = t, this.rewind();
    }
    function u(e, t, n, o, r) {
        this._blobs = "blob" === r ? [] : null, this.dataType = r || "arraybuffer", this._rawbuf = new ArrayBuffer(1024), 
        this._buffer = new Uint8Array(this._rawbuf), this._pos = 0, this._codepage = 0, 
        this._tagStack = [], this._rootTagValue = null;
        var i = e.split(".").map(function(e) {
            return parseInt(e);
        }), s = i[0], a = i[1], c = (s - 1 << 4) + a, d = n;
        if ("string" == typeof n && (d = y[n], void 0 === d)) throw new Error("unknown charset " + n);
        var u = this._encoder = new TextEncoder(n);
        if (this._write(c), this._write(t), this._write(d), o) {
            var l = o.map(function(e) {
                return u.encode(e);
            }), h = l.reduce(function(e, t) {
                return e + t.length + 1;
            }, 0);
            this._write_mb_uint32(h);
            for (var p = 0; p < l.length; p++) {
                var f = l[p];
                this._write_bytes(f), this._write(0);
            }
        } else this._write(0);
    }
    function l() {
        this.listeners = [], this.onerror = function(e) {
            throw e;
        };
    }
    var h = {}, p = {
        SWITCH_PAGE: 0,
        END: 1,
        ENTITY: 2,
        STR_I: 3,
        LITERAL: 4,
        EXT_I_0: 64,
        EXT_I_1: 65,
        EXT_I_2: 66,
        PI: 67,
        LITERAL_C: 68,
        EXT_T_0: 128,
        EXT_T_1: 129,
        EXT_T_2: 130,
        STR_T: 131,
        LITERAL_A: 132,
        EXT_0: 192,
        EXT_1: 193,
        EXT_2: 194,
        OPAQUE: 195,
        LITERAL_AC: 196
    }, f = {
        message: "THIS IS AN INTERNAL CONTROL FLOW HACK THAT YOU SHOULD NOT SEE"
    }, m = e("WBXML.ParseError");
    h.ParseError = m, t.prototype = {
        get: function(e) {
            if (e in this.offsets) return this.strings[this.offsets[e]];
            if (0 > e) throw new m("offset must be >= 0");
            for (var t = 0, n = 0; n < this.strings.length; n++) {
                if (e < t + this.strings[n].length + 1) return this.strings[n].slice(e - t);
                t += this.strings[n].length + 1;
            }
            throw new m("invalid offset");
        }
    }, h.CompileCodepages = n;
    var g = {
        3: "US-ASCII",
        4: "ISO-8859-1",
        5: "ISO-8859-2",
        6: "ISO-8859-3",
        7: "ISO-8859-4",
        8: "ISO-8859-5",
        9: "ISO-8859-6",
        10: "ISO-8859-7",
        11: "ISO-8859-8",
        12: "ISO-8859-9",
        13: "ISO-8859-10",
        106: "UTF-8"
    }, y = {};
    for (var _ in g) {
        var v = g[_];
        y[v] = _;
    }
    return h.Element = o, o.prototype = {
        get tagName() {
            var e = this.namespaceName;
            return e = e ? e + ":" : "", e + this.localTagName;
        },
        getAttributes: function() {
            var e = [];
            for (var t in this._attrs) {
                var n = this._attrs[t], o = t.split(":");
                e.push({
                    name: t,
                    namespace: o[0],
                    localName: o[1],
                    value: this._getAttribute(n)
                });
            }
            return e;
        },
        getAttribute: function(e) {
            return "number" == typeof e ? e = this.ownerDocument._codepages.__attrdata__[e].name : e in this._attrs || null === this.namespace || -1 !== e.indexOf(":") || (e = this.namespaceName + ":" + e), 
            this._getAttribute(this._attrs[e]);
        },
        _getAttribute: function(e) {
            for (var t = "", n = [], o = 0; o < e.length; o++) {
                var r = e[o];
                r instanceof s ? (t && (n.push(t), t = ""), n.push(r)) : t += "number" == typeof r ? this.ownerDocument._codepages.__attrdata__[r].data || "" : r;
            }
            return t && n.push(t), 1 === n.length ? n[0] : n;
        },
        _addAttribute: function(e) {
            if ("string" == typeof e) {
                if (e in this._attrs) throw new m("attribute " + e + " is repeated");
                return this._attrs[e] = [];
            }
            var t = e >> 8, n = 255 & e, o = this.ownerDocument._codepages.__attrdata__[n].name, r = this.ownerDocument._codepages.__nsnames__[t], i = r + ":" + o;
            if (i in this._attrs) throw new m("attribute " + i + " is repeated");
            return this._attrs[i] = [ e ];
        }
    }, h.EndTag = r, r.prototype = {
        get type() {
            return "ETAG";
        }
    }, h.Text = i, i.prototype = {
        get type() {
            return "TEXT";
        }
    }, h.Extension = s, s.prototype = {
        get type() {
            return "EXT";
        }
    }, h.ProcessingInstruction = a, a.prototype = {
        get type() {
            return "PI";
        },
        get target() {
            return "string" == typeof this.targetID ? this.targetID : this.ownerDocument._codepages.__attrdata__[this.targetID].name;
        },
        _setTarget: function(e) {
            return this.targetID = e, this._data = "string" == typeof e ? [] : [ e ];
        },
        _getAttribute: o.prototype._getAttribute,
        get data() {
            return this._getAttribute(this._data);
        }
    }, h.Opaque = c, c.prototype = {
        get type() {
            return "OPAQUE";
        }
    }, h.Reader = d, d.prototype = {
        _get_uint8: function() {
            if (this._index === this._data.length) throw f;
            return this._data[this._index++];
        },
        _get_mb_uint32: function() {
            var e, t = 0;
            do e = this._get_uint8(), t = 128 * t + (127 & e); while (128 & e);
            return t;
        },
        _get_slice: function(e) {
            var t = this._index;
            return this._index += e, this._data.subarray(t, this._index);
        },
        _get_c_string: function() {
            for (var e = this._index; this._get_uint8(); ) ;
            return this._data.subarray(e, this._index - 1);
        },
        rewind: function() {
            this._index = 0;
            var e = this._get_uint8();
            this.version = ((240 & e) + 1).toString() + "." + (15 & e).toString(), this.pid = this._get_mb_uint32(), 
            this.charset = g[this._get_mb_uint32()] || "unknown", this._decoder = new TextDecoder(this.charset);
            var n = this._get_mb_uint32();
            this.strings = new t(this._get_slice(n), this._decoder), this.document = this._getDocument();
        },
        _getDocument: function() {
            var e, t, n = {
                BODY: 0,
                ATTRIBUTES: 1,
                ATTRIBUTE_PI: 2
            }, d = n.BODY, u = 0, l = 0, h = !1, g = [], y = function(o) {
                d === n.BODY ? e ? e.textContent += o : e = new i(this, o) : t.push(o);
            }.bind(this);
            try {
                for (;;) {
                    var _ = this._get_uint8();
                    if (_ === p.SWITCH_PAGE) {
                        if (u = this._get_uint8(), !(u in this._codepages.__nsnames__)) throw new m("unknown codepage " + u);
                    } else if (_ === p.END) if (d === n.BODY && l-- > 0) e && (g.push(e), e = null), 
                    g.push(new r(this)); else {
                        if (d !== n.ATTRIBUTES && d !== n.ATTRIBUTE_PI) throw new m("unexpected END token");
                        d = n.BODY, g.push(e), e = null, t = null;
                    } else if (_ === p.ENTITY) {
                        if (d === n.BODY && 0 === l) throw new m("unexpected ENTITY token");
                        var v = this._get_mb_uint32();
                        y("&#" + v + ";");
                    } else if (_ === p.STR_I) {
                        if (d === n.BODY && 0 === l) throw new m("unexpected STR_I token");
                        y(this._decoder.decode(this._get_c_string()));
                    } else if (_ === p.PI) {
                        if (d !== n.BODY) throw new m("unexpected PI token");
                        d = n.ATTRIBUTE_PI, e && g.push(e), e = new a(this);
                    } else if (_ === p.STR_T) {
                        if (d === n.BODY && 0 === l) throw new m("unexpected STR_T token");
                        var b = this._get_mb_uint32();
                        y(this.strings.get(b));
                    } else if (_ === p.OPAQUE) {
                        if (d !== n.BODY) throw new m("unexpected OPAQUE token");
                        var S = this._get_mb_uint32(), T = this._get_slice(S);
                        e && (g.push(e), e = null), g.push(new c(this, T));
                    } else if ((64 & _ || 128 & _) && 3 > (63 & _)) {
                        var w, I, A = 192 & _, E = 63 & _;
                        A === p.EXT_I_0 ? (w = "string", I = this._decoder.decode(this._get_c_string())) : A === p.EXT_T_0 ? (w = "integer", 
                        I = this._get_mb_uint32()) : (w = "byte", I = null);
                        var C = new s(this, w, E, I);
                        d === n.BODY ? (e && (g.push(e), e = null), g.push(C)) : t.push(C);
                    } else if (d === n.BODY) {
                        if (0 === l) {
                            if (h) throw new m("multiple root nodes found");
                            h = !0;
                        }
                        var M = (u << 8) + (63 & _);
                        if ((63 & _) === p.LITERAL) {
                            var b = this._get_mb_uint32();
                            M = this.strings.get(b);
                        }
                        e && g.push(e), e = new o(this, 64 & _ ? "STAG" : "TAG", M), 64 & _ && l++, 128 & _ ? d = n.ATTRIBUTES : (d = n.BODY, 
                        g.push(e), e = null);
                    } else {
                        var k = (u << 8) + _;
                        if (128 & _) t.push(k); else {
                            if (_ === p.LITERAL) {
                                var b = this._get_mb_uint32();
                                k = this.strings.get(b);
                            }
                            if (d === n.ATTRIBUTE_PI) {
                                if (t) throw new m("unexpected attribute in PI");
                                t = e._setTarget(k);
                            } else t = e._addAttribute(k);
                        }
                    }
                }
            } catch (v) {
                if (v !== f) throw v;
            }
            return g;
        },
        dump: function(e, t) {
            var n = "";
            void 0 === e && (e = 2);
            var o = function(t) {
                return new Array(t * e + 1).join(" ");
            }, r = [];
            t && (n += "Version: " + this.version + "\n", n += "Public ID: " + this.pid + "\n", 
            n += "Charset: " + this.charset + "\n", n += 'String table:\n  "' + this.strings.strings.join('"\n  "') + '"\n\n');
            for (var i = this.document, s = i.length, a = 0; s > a; a++) {
                var c = i[a];
                if ("TAG" === c.type || "STAG" === c.type) {
                    n += o(r.length) + "<" + c.tagName;
                    for (var d = c.getAttributes(), u = 0; u < d.length; u++) {
                        var l = d[u];
                        n += " " + l.name + '="' + l.value + '"';
                    }
                    "STAG" === c.type ? (r.push(c.tagName), n += ">\n") : n += "/>\n";
                } else if ("ETAG" === c.type) {
                    var h = r.pop();
                    n += o(r.length) + "</" + h + ">\n";
                } else if ("TEXT" === c.type) n += o(r.length) + c.textContent + "\n"; else if ("PI" === c.type) n += o(r.length) + "<?" + c.target, 
                c.data && (n += " " + c.data), n += "?>\n"; else {
                    if ("OPAQUE" !== c.type) throw new Error('Unknown node type "' + c.type + '"');
                    n += o(r.length) + "<![CDATA[" + c.data + "]]>\n";
                }
            }
            return n;
        }
    }, h.Writer = u, u.Attribute = function(e, t) {
        if (this.isValue = "number" == typeof e && 128 & e, this.isValue && void 0 !== t) throw new Error("Can't specify a value for attribute value constants");
        this.name = e, this.value = t;
    }, u.StringTableRef = function(e) {
        this.index = e;
    }, u.Entity = function(e) {
        this.code = e;
    }, u.Extension = function(e, t, n) {
        var o = {
            string: {
                value: p.EXT_I_0,
                validator: function(e) {
                    return "string" == typeof e;
                }
            },
            integer: {
                value: p.EXT_T_0,
                validator: function(e) {
                    return "number" == typeof e;
                }
            },
            "byte": {
                value: p.EXT_0,
                validator: function(e) {
                    return null === e || void 0 === e;
                }
            }
        }, r = o[e];
        if (!r) throw new Error("Invalid WBXML Extension type");
        if (!r.validator(n)) throw new Error("Data for WBXML Extension does not match type");
        if (0 !== t && 1 !== t && 2 !== t) throw new Error("Invalid WBXML Extension index");
        this.subtype = r.value, this.index = t, this.data = n;
    }, u.a = function(e, t) {
        return new u.Attribute(e, t);
    }, u.str_t = function(e) {
        return new u.StringTableRef(e);
    }, u.ent = function(e) {
        return new u.Entity(e);
    }, u.ext = function(e, t, n) {
        return new u.Extension(e, t, n);
    }, u.prototype = {
        _write: function(e) {
            if (this._pos === this._buffer.length - 1) {
                this._rawbuf = new ArrayBuffer(2 * this._rawbuf.byteLength);
                for (var t = new Uint8Array(this._rawbuf), n = 0; n < this._buffer.length; n++) t[n] = this._buffer[n];
                this._buffer = t;
            }
            this._buffer[this._pos++] = e;
        },
        _write_mb_uint32: function(e) {
            var t = [];
            for (t.push(e % 128); e >= 128; ) e >>= 7, t.push(128 + e % 128);
            for (var n = t.length - 1; n >= 0; n--) this._write(t[n]);
        },
        _write_bytes: function(e) {
            for (var t = 0; t < e.length; t++) this._write(e[t]);
        },
        _write_str: function(e) {
            this._write_bytes(this._encoder.encode(e));
        },
        _setCodepage: function(e) {
            this._codepage !== e && (this._write(p.SWITCH_PAGE), this._write(e), this._codepage = e);
        },
        _writeTag: function(e, t, n) {
            if (void 0 === e) throw new Error("unknown tag");
            var o = 0;
            if (t && (o += 64), n.length && (o += 128), e instanceof u.StringTableRef ? (this._write(p.LITERAL + o), 
            this._write_mb_uint32(e.index)) : (this._setCodepage(e >> 8), this._write((255 & e) + o), 
            this._rootTagValue || (this._rootTagValue = e)), n.length) {
                for (var r = 0; r < n.length; r++) {
                    var i = n[r];
                    this._writeAttr(i);
                }
                this._write(p.END);
            }
        },
        _writeAttr: function(e) {
            if (!(e instanceof u.Attribute)) throw new Error("Expected an Attribute object");
            if (e.isValue) throw new Error("Can't use attribute value constants here");
            e.name instanceof u.StringTableRef ? (this._write(p.LITERAL), this._write(e.name.index)) : (this._setCodepage(e.name >> 8), 
            this._write(255 & e.name)), this._writeText(e.value, !0);
        },
        _writeText: function(e, t) {
            if (Array.isArray(e)) for (var n = 0; n < e.length; n++) {
                var o = e[n];
                this._writeText(o, t);
            } else if (e instanceof u.StringTableRef) this._write(p.STR_T), this._write_mb_uint32(e.index); else if (e instanceof u.Entity) this._write(p.ENTITY), 
            this._write_mb_uint32(e.code); else if (e instanceof u.Extension) this._write(e.subtype + e.index), 
            e.subtype === p.EXT_I_0 ? (this._write_str(e.data), this._write(0)) : e.subtype === p.EXT_T_0 && this._write_mb_uint32(e.data); else if (e instanceof u.Attribute) {
                if (!e.isValue) throw new Error("Unexpected Attribute object");
                if (!t) throw new Error("Can't use attribute value constants outside of attributes");
                this._setCodepage(e.name >> 8), this._write(255 & e.name);
            } else null !== e && void 0 !== e && (this._write(p.STR_I), this._write_str(e.toString()), 
            this._write(0));
        },
        tag: function(e) {
            var t = arguments.length > 1 ? arguments[arguments.length - 1] : null;
            if (null === t || t instanceof u.Attribute) {
                var n = Array.prototype.slice.call(arguments, 1);
                return this._writeTag(e, !1, n), this;
            }
            var o = Array.prototype.slice.call(arguments, 0, -1);
            return this.stag.apply(this, o).text(t).etag();
        },
        stag: function(e) {
            var t = Array.prototype.slice.call(arguments, 1);
            return this._writeTag(e, !0, t), this._tagStack.push(e), this;
        },
        etag: function(e) {
            if (0 === this._tagStack.length) throw new Error("Spurious etag() call!");
            var t = this._tagStack.pop();
            if (void 0 !== e && e !== t) throw new Error("Closed the wrong tag");
            return this._write(p.END), this;
        },
        text: function(e) {
            return this._writeText(e), this;
        },
        pi: function(e, t) {
            return this._write(p.PI), this._writeAttr(u.a(e, t)), this._write(p.END), this;
        },
        ext: function(e, t, n) {
            return this.text(u.ext(e, t, n));
        },
        opaque: function(e) {
            if (this._write(p.OPAQUE), e instanceof Blob) {
                if (!this._blobs) throw new Error("Writer not opened in blob mode");
                this._write_mb_uint32(e.size), this._blobs.push(this.bytes), this._blobs.push(e), 
                this._rawbuf = new ArrayBuffer(1024), this._buffer = new Uint8Array(this._rawbuf), 
                this._pos = 0;
            } else if ("string" == typeof e) this._write_mb_uint32(e.length), this._write_str(e); else {
                this._write_mb_uint32(e.length);
                for (var t = 0; t < e.length; t++) this._write(e[t]);
            }
            return this;
        },
        get buffer() {
            return this._rawbuf.slice(0, this._pos);
        },
        get bytes() {
            return new Uint8Array(this._rawbuf, 0, this._pos);
        },
        get blob() {
            if (!this._blobs) throw new Error("No blobs!");
            var e = this._blobs;
            this._pos && (e = e.concat([ this.bytes ]));
            var t = new Blob(e);
            return t;
        },
        get rootTag() {
            return this._rootTagValue;
        }
    }, h.EventParser = l, l.prototype = {
        addEventListener: function(e, t) {
            this.listeners.push({
                path: e,
                callback: t
            });
        },
        _pathMatches: function(e, t) {
            return e.length === t.length && e.every(function(e, n) {
                return "*" === t[n] ? !0 : Array.isArray(t[n]) ? -1 !== t[n].indexOf(e) : e === t[n];
            });
        },
        run: function(e) {
            for (var t, n, o = [], r = [], i = 0, s = e.document, a = s.length, c = this.listeners, d = 0; a > d; d++) {
                var u = s[d];
                if ("TAG" === u.type) {
                    for (o.push(u.tag), t = 0; t < c.length; t++) if (n = c[t], this._pathMatches(o, n.path)) {
                        u.children = [];
                        try {
                            n.callback(u);
                        } catch (l) {
                            this.onerror && this.onerror(l);
                        }
                    }
                    o.pop();
                } else if ("STAG" === u.type) for (o.push(u.tag), t = 0; t < c.length; t++) n = c[t], 
                this._pathMatches(o, n.path) && i++; else if ("ETAG" === u.type) {
                    for (t = 0; t < c.length; t++) if (n = c[t], this._pathMatches(o, n.path)) {
                        i--;
                        try {
                            n.callback(r[r.length - 1]);
                        } catch (l) {
                            this.onerror && this.onerror(l);
                        }
                    }
                    o.pop();
                }
                i && ("STAG" === u.type ? (u.type = "TAG", u.children = [], r.length && r[r.length - 1].children.push(u), 
                r.push(u)) : "ETAG" === u.type ? r.pop() : (u.children = [], r[r.length - 1].children.push(u)));
            }
        }
    }, h;
});