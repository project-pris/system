define([ "mimeparser", "mimefuncs", "exports" ], function(e, n, t) {
    function r(n) {
        this._partDef = n;
        var t = this._parser = new e();
        this._totalBytes = 0;
        var r = "", o = "";
        n.params && n.params.charset && (r = '; charset="' + n.params.charset.toLowerCase() + '"'), 
        n.params && n.params.format && (o = '; format="' + n.params.format.toLowerCase() + '"'), 
        t.write("Content-Type: " + n.type.toLowerCase() + "/" + n.subtype.toLowerCase() + r + o + "\r\n"), 
        n.encoding && t.write("Content-Transfer-Encoding: " + n.encoding + "\r\n"), t.write("\r\n"), 
        n.pendingBuffer && this.parse(n.pendingBuffer);
    }
    r.prototype = {
        parse: function(e) {
            this._totalBytes += e.length, this._parser.write(e);
        },
        complete: function() {
            this._parser.end();
            var e = "";
            return this._parser.node.content && (e = n.charset.decode(this._parser.node.content, "utf-8")), 
            {
                bytesFetched: this._totalBytes,
                text: e
            };
        }
    }, t.TextParser = r;
});