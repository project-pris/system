/**
 * Enter basic account info card (name, e-mail address) to try and
 * autoconfigure an account.
 */

define(['require','exports','module','evt','l10n!','cards','form_navigation','./base','template!./setup_account_info.html','./setup_account_error_mixin'],function(require, exports, module) {

var evt = require('evt'),
    mozL10n = require('l10n!'),
    cards = require('cards'),
    FormNavigation = require('form_navigation');

var self;
var dataChanged;

return [
  require('./base')(require('template!./setup_account_info.html')),
  require('./setup_account_error_mixin'),
  {
    createdCallback: function() {
      this.formNavigation = new FormNavigation({
        formElem: this.formNode,
        onLast: this.onNext.bind(this)
      });
    },

    onArgs: function(args) {
      this.args = args;
      self = this;
      dataChanged = false;

      if (args.launchedFromActivity) {
        this.errorRegionNode.classList.remove('collapsed');
        mozL10n.setAttributes(this.errorMessageNode,
            'setup-empty-account-message');
      }
    },

    handleKeyDown: function(e) {
      if (!document.getElementById('Symbols')) {
        switch (e.key) {
          case 'Accept':
          case 'Enter':
            e.preventDefault();
            break;
          case 'Backspace':
            if (document.querySelector('#confirm-dialog-container ' +
                'gaia-confirm')) {
              e.preventDefault();
              cards._endKeyClicked = false;
            } else {
              if (self.args.allowBack) {
                e.preventDefault();
                self.onBack();
              } else {
                self.endkeyHandler(e);
              }
            }
            break;
        }
      }
    },

    endkeyHandler: function(e) {
      var callback = e.detail.callback;
      if (document.getElementById('Symbols')) {
        var evt = new CustomEvent('back-accepted');
        window.dispatchEvent(evt);
      }

      if (dataChanged) {
        var dialogConfig = {
          title: {
            id: 'confirmation-title',
            args: {}
          },
          body: {
            id: 'data-loss-warning-message',
            args: {}
          },
          desc: {
            id: 'back-to-edit-message',
            args: {}
          },
          cancel: {
            l10nId: 'exit',
            priority: 1,
            callback: function() {
              window.close();
            }
          },
          confirm: {
            l10nId: 'return',
            priority: 3,
            callback: function() {
              cards._endKeyClicked = false;
            }
          }
        };

        var dialog = new ConfirmDialogHelper(dialogConfig);
        dialog.show(document.getElementById('confirm-dialog-container'));

        e.preventDefault();
      } else {
        if (callback) {
          callback();
        }
      }
    },

    onCardVisible: function(navDirection) {
      const CARD_NAME = this.localName;
      const QUERY_CHILD = CARD_NAME + ' ' + 'li';
      const CONTROL_ID = CARD_NAME + ' ' + QUERY_CHILD;

      var form = this.formNode;
      var formLi = document.querySelectorAll('.sup-account-form li');
      var i;
      for (i = 0; i < formLi.length; i++) {
        formLi[i].addEventListener('focus', function(e) {
          var inputEl = form.querySelector('.focus input');
          if (inputEl) {
            setTimeout( () => {
              inputEl.focus();
              inputEl.setSelectionRange(inputEl.value.length,
                  inputEl.value.length);
            }, 100);
          }
        });
      }

      // Remove cached softkey panel if it has been restored,
      // so that the card's own softkey panel can be the only
      // one softeky panel instance.
      var cacheSoftkeyNode = document.getElementById('cachedSoftkeyPanel');
      if (cacheSoftkeyNode) {
        document.body.removeChild(cacheSoftkeyNode);
      }

      console.log(this.localName + '.onCardVisible, navDirection=' +
                  navDirection);
      NavigationMap.cardContentHeight = this.scrollBelowNode.clientHeight;
      // forward: new card pushed
      if (navDirection === 'forward') {
        NavigationMap.navSetup(CARD_NAME, QUERY_CHILD);
        NavigationMap.setCurrentControl(CONTROL_ID);
        NavigationMap.setFocus('first');
        this.updateSK();
      }
      // back: hidden card is restored
      else if (navDirection === 'back') {
        NavigationMap.setCurrentControl(CONTROL_ID);
        NavigationMap.setFocus('restore');
        this.updateSK();
      }

      window.addEventListener('keydown', self.handleKeyDown);
      window.addEventListener('email-endkey', self.endkeyHandler);
      self.showpasswordInputNode.addEventListener('change', function() {
        self.passwordNode.type = this.checked ? 'text' : 'password';
        self.updateSK();
      });
    },

    onBack: function(event) {
      evt.emit('setupAccountCanceled', this);
    },

    onNext: function(event) {
      //event.preventDefault(); // Prevent FormNavigation from taking over.

      // remove all focus
      var focused = document.querySelectorAll('.focus');
      for (var i = 0; i < focused.length; i++) {
        focused[i].classList.remove('focus');
      }
      // The progress card is the dude that actually tries to create the
      // account.
      cards.pushCard(
        'setup_progress', 'animate',
        {
          displayName: this.nameNode.value,
          emailAddress: this.emailNode.value,
          password: this.passwordNode.value,
          callingCard: this
        },
        'right');
    },

    updateSK: function() {
      var optManualSetup = {
        name: 'Manual Setup',
        l10nId: 'setup-manual-config2',
        priority: 1,
        method: function() {
          self.onClickManualConfig();
        }
      };
      var optSelect = {
        name: 'Select',
        l10nId: 'select',
        priority: 2
      };
      var optDeSelect = {
        name: 'Deselect',
        l10nId: 'deselect',
        priority: 2
      };
      var optNext = {
        name: 'Next',
        l10nId: 'setup-info-next',
        priority: 3,
        method: function() {
          self.onNext();
        }
      };

      var params = [];
      var focusedItem = this.formNode.querySelector('.focus');
      params.push(optManualSetup);
      if (focusedItem &&
          focusedItem.classList.contains('sup-account-show-password')) {
        if (self.showpasswordInputNode.checked) {
          params.push(optDeSelect);
        } else {
          params.push(optSelect);
        }
      }
      if (this.formNode.checkValidity()) {
        params.push(optNext);
      }
      NavigationMap.setSoftKeyBar(params);
    },

    onFocusChanged: function(queryChild, index) {
      console.log(this.localName + '.onFocusChanged, queryChild=' +
                  queryChild + ", index=" + index);
      this.updateSK();
    },

    onInfoInput: function(event) {
      this.updateSK();
      dataChanged = true;
    },

    onClickManualConfig: function(event) {
      //event.preventDefault(); // Prevent FormNavigation from taking over.
      cards.pushCard(
        'setup_manual_config', 'animate',
        {
          displayName: this.nameNode.value,
          emailAddress: this.emailNode.value
        },
        'right');
    },

    onHidden: function() {
      window.removeEventListener('keydown', self.handleKeyDown);
      window.removeEventListener('email-endkey', self.endkeyHandler);
    },

    die: function() {
      this.formNavigation = null;
      window.removeEventListener('keydown', self.handleKeyDown);
      window.removeEventListener('email-endkey', self.endkeyHandler);
    }
  }
];
});
